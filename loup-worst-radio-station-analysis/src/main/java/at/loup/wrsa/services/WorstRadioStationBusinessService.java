package at.loup.wrsa.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import at.loup.wrsa.data.ShamefulData;
import at.loup.wrsa.data.WorstRadioStationSongInformation;
import at.loup.wrsa.entities.repositories.RawEntityRepository;

@Component
public class WorstRadioStationBusinessService {
	@Autowired
	private ShamefulShittySongHelper sssh;
	@Autowired
	private RawEntityRepository rawEntityRepository;

	private volatile ShamefulData latestShamefuldata;

	public ShamefulData getLatestShamefulData() {
		return latestShamefuldata;
	}

	@Transactional
	public void run() {
		List<WorstRadioStationSongInformation> songInformations = getList();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -1);
		Date now = cal.getTime();

		for (WorstRadioStationSongInformation wrssi : songInformations) {
			if ("Played".equals(wrssi.getStatus())) {
				rawEntityRepository.createFromDTOAndSave(wrssi);
			}
		}

		latestShamefuldata = sssh.createShamefuldata(rawEntityRepository.findAll());
	}

	private List<WorstRadioStationSongInformation> getList() {
		RestTemplate restTemplate = new RestTemplate();
		String s = restTemplate.getForObject("http://oe3meta.orf.at/oe3mdata/WebPlayerFiles/PlayList400.json",
				String.class);

		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();

		Type listType = new TypeToken<ArrayList<WorstRadioStationSongInformation>>() {
		}.getType();
		List<WorstRadioStationSongInformation> result = gson.fromJson(s, listType);

		return result;
	}
}