package at.loup.wrsa.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.loup.commons.services.SpringAwareSafeThreadLooper;

@Component
public class WorstRadioStationPoller extends SpringAwareSafeThreadLooper {
	private static final Logger l = LoggerFactory.getLogger(WorstRadioStationPoller.class);
	private long runNumber = 0;

	@Autowired
	private WorstRadioStationBusinessService worstRadioStationBusinessService;

	public WorstRadioStationPoller() {
		super("WorstRadioStationPoller");
	}

	@Override
	protected void loopMethod() throws Throwable {
		runNumber++;
		if (runNumber != 1) {
			Thread.sleep(60000);
		}
		try {
			worstRadioStationBusinessService.run();
		} catch (Throwable throwable) {
			l.error("WorstRadioStationPoller threw exception.", throwable);
		}
	}
}