package at.loup.wrsa.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WorstRadioStationSongInformation {
	private String Status;
	private String Id;
	private String SongName;
	private String Artist;
	private String Time;
	private Integer Length;
	private String Cover;

	private transient Date timeAsDate;

	public WorstRadioStationSongInformation() {

	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getSongName() {
		return SongName;
	}

	public void setSongName(String songName) {
		SongName = songName;
	}

	public String getArtist() {
		return Artist;
	}

	public void setArtist(String artist) {
		Artist = artist;
	}

	public Date getTimeAsDate() {
		if (timeAsDate == null) {
			// example 2017-01-05T12:25:54+0100
			String usedTimeString = getTime();
			if (usedTimeString == null) {
				return null;
			}
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			try {
				Date date = df.parse(usedTimeString);
				timeAsDate = date;
			} catch (Exception e) {
				throw new RuntimeException("Can't parse date<" + usedTimeString + ">");
			}
		}
		return timeAsDate;
	}

	public String getTime() {
		return Time;
	}

	public void setTime(String time) {
		Time = time;
	}

	public Integer getLength() {
		return Length;
	}

	public void setLength(Integer length) {
		Length = length;
	}

	public String getCover() {
		return Cover;
	}

	public void setCover(String cover) {
		Cover = cover;
	}
}