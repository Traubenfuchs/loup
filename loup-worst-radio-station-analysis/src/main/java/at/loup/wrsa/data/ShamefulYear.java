package at.loup.wrsa.data;

import java.util.HashMap;
import java.util.Map;

public class ShamefulYear {
	private Map<Integer, ShamefulMonth> months;
	private Map<String, ShittySong> songs;

	public Map<Integer, ShamefulMonth> getMonths() {
		if(months == null) {
			months = new HashMap<>();
		}
		return months;
	}

	public void setMonths(Map<Integer, ShamefulMonth> months) {
		this.months = months;
	}

	public Map<String, ShittySong> getSongs() {
		if(songs == null) {
			songs = new HashMap<>();
		}
		return songs;
	}

	public void setSongs(Map<String, ShittySong> songs) {
		this.songs = songs;
	}
}