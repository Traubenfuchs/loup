package at.loup.wrsa.data;

import java.util.HashMap;
import java.util.Map;

public class ShamefulData {
	private Map<Integer, ShamefulYear> years;
	private Map<String, ShittySong> songs;

	public ShamefulData() {

	}

	public Map<Integer, ShamefulYear> getYears() {
		if (years == null) {
			years = new HashMap<>();
		}
		return years;
	}

	public void setYears(Map<Integer, ShamefulYear> years) {
		this.years = years;
	}

	public Map<String, ShittySong> getSongs() {
		if (songs == null) {
			songs = new HashMap<>();
		}
		return songs;
	}

	public void setSongs(Map<String, ShittySong> songs) {
		this.songs = songs;
	}
}