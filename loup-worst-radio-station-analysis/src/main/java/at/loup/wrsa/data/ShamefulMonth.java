package at.loup.wrsa.data;

import java.util.HashMap;
import java.util.Map;

public class ShamefulMonth {
	private Map<Integer, ShamefulDay> days;
	private Map<String, ShittySong> songs;

	public ShamefulMonth() {

	}

	public Map<Integer, ShamefulDay> getDays() {
		if (days == null) {
			days = new HashMap<>();
		}
		return days;
	}

	public void setDays(Map<Integer, ShamefulDay> days) {
		this.days = days;
	}

	public Map<String, ShittySong> getSongs() {
		if (songs == null) {
			songs = new HashMap<>();
		}
		return songs;
	}

	public void setSongs(Map<String, ShittySong> songs) {
		this.songs = songs;
	}
}