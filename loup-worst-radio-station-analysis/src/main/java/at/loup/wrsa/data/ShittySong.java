package at.loup.wrsa.data;

import at.loup.wrsa.entities.RawEntity;

public class ShittySong {
	private Integer playCount;
	private String artistName;
	private String songName;

	public ShittySong() {

	}

	public Integer getPlayCount() {
		return playCount;
	}

	public void setPlayCount(Integer playCount) {
		this.playCount = playCount;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public void incrementPlayCount() {
		setPlayCount(getPlayCount() + 1);
	}

	public static ShittySong createFromEntity(RawEntity rawEntity) {
		ShittySong result = new ShittySong();

		result.setPlayCount(0);
		result.setArtistName(rawEntity.getArtist());
		result.setSongName(rawEntity.getSongName());

		return result;
	}
}