package at.loup.wrsa.entities.repositories.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import at.loup.wrsa.data.WorstRadioStationSongInformation;
import at.loup.wrsa.entities.RawEntity;
import at.loup.wrsa.entities.repositories.RawEntityRepository;

public class RawEntityRepositoryImpl implements RawEntityRepositoryCustom {
	@Autowired
	private RawEntityRepository rawEntityRepository;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public RawEntity createFromDTOAndSave(WorstRadioStationSongInformation wrssi) {
		RawEntity existingRawEntity = rawEntityRepository.findBySongNameAndArtistAndLengthAndTime(
				wrssi.getSongName(),
				wrssi.getArtist(),
				wrssi.getLength(),
				wrssi.getTimeAsDate());

		if (existingRawEntity == null) {
			RawEntity newRawEntity = RawEntity.createFromWRSSI(wrssi);
			newRawEntity = rawEntityRepository.save(newRawEntity);
			return newRawEntity;
		}

		return existingRawEntity;
	}
}