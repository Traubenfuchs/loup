package at.loup.wrsa.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import at.loup.commons.entities.AbstractTimedEntity;
import at.loup.wrsa.data.WorstRadioStationSongInformation;

//@formatter:off
@Entity
@Table(
	name = "raw",
	uniqueConstraints = @UniqueConstraint(columnNames = { "songName", "artist", "length", "time" }),
	indexes = {@Index(name = "sn_a_l_t", columnList = "songName,artist,length,time") })
public class RawEntity extends AbstractTimedEntity<RawEntity> {
	private static final long serialVersionUID = -3055770339395863278L;

	@Column(nullable = false)
	private String senderId;
	@Column(nullable = false)
	private String songName;
	@Column(nullable = false)
	private String artist;
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date time;
	@Column(nullable = false)
	private Integer length;
	@Column(nullable = false)
	private String cover;

	//@formatter:on
	public RawEntity() {

	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public static RawEntity createFromWRSSI(WorstRadioStationSongInformation wrssi) {
		RawEntity result = new RawEntity();

		result.setArtist(wrssi.getArtist());
		result.setCover(wrssi.getCover());
		result.setLength(wrssi.getLength());
		result.setSenderId(wrssi.getId());
		result.setSongName(wrssi.getSongName());
		result.setTime(wrssi.getTimeAsDate());

		return result;
	}
}