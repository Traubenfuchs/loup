package at.loup.wrsa.entities.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.loup.wrsa.entities.RawEntity;
import at.loup.wrsa.entities.repositories.custom.RawEntityRepositoryCustom;

@Repository
public interface RawEntityRepository extends JpaRepository<RawEntity, Long>, RawEntityRepositoryCustom {
	RawEntity findBySongNameAndArtistAndLengthAndTime(String songName, String artist, Integer length, Date time);
}