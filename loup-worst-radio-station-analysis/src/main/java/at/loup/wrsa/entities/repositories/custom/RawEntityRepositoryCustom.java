package at.loup.wrsa.entities.repositories.custom;

import at.loup.wrsa.data.WorstRadioStationSongInformation;
import at.loup.wrsa.entities.RawEntity;

public interface RawEntityRepositoryCustom {
	RawEntity createFromDTOAndSave(WorstRadioStationSongInformation wrssi);
}