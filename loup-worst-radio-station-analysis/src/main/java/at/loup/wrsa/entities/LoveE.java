package at.loup.wrsa.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Table;

import at.loup.commons.entities.AbstractEntity;

@Table(name = "love")
@Entity
public class LoveE extends AbstractEntity<LoveE> {
	/**
	 *
	 */
	private static final long serialVersionUID = 5329732114036444323L;
	public LocalDateTime localDateTime;

	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}
}
