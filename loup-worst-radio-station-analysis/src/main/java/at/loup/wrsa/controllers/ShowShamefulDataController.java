
package at.loup.wrsa.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.loup.wrsa.data.ShamefulData;
import at.loup.wrsa.services.WorstRadioStationBusinessService;

@RestController
public class ShowShamefulDataController {
	@Autowired
	private WorstRadioStationBusinessService worstRadioStationBusinessService;

	@RequestMapping(path="/api/getLatestShamefulData")
	public ShamefulData getLatestShamefulData() {
		ShamefulData result = worstRadioStationBusinessService.getLatestShamefulData();
		return result;
	}
}