package at.loup.bat.data;

import java.util.Calendar;

import at.loup.bat.db.DataLineEntity;

public class DataLineDto {
	private Integer year = 0;
	private Integer month = 0;
	private Integer day = 0;

	private Integer goToBedTimeMinuteOfDay = 0;
	private Integer getUpTimeMinuteOfDay = 0;

	private Integer sleepDuration = 0;
	private Integer deepSleep = 0;
	private Integer remSleep = 0;
	private Integer lightSleep = 0;
	private Integer wakeTime = 0;
	private Integer wakeups = 0;
	private Integer fallAsleepDuration = 0;

	private Double temperatureSleep = 0.0;
	private Integer humiditySleep = 0;

	private Integer workDuration = 0;

	private Integer workoutDuration = 0;

	private Integer sleepRating = 0;
	private Integer dayEnergyRating = 0;

	private Integer stepCount = 0;

	private Double morningWeight = 0.0;
	private Double eveningWeight = 0.0;
	private Integer restingHeartBeat = 0;

	public DataLineDto() {

	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public EDayOfWeek getDayOfWeek() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, getYear());
		calendar.set(Calendar.MONTH, getMonth() - 1);
		calendar.set(Calendar.DAY_OF_MONTH, getDay());

		EDayOfWeek result = EDayOfWeek.getByJavaCalendarEquivalent(calendar);
		return result;
	}

	public Integer getGoToBedTimeMinuteOfDay() {
		return goToBedTimeMinuteOfDay;
	}

	public void setGoToBedTimeMinuteOfDay(Integer goToBedTimeMinuteOfDay) {
		this.goToBedTimeMinuteOfDay = goToBedTimeMinuteOfDay;
	}

	public Integer getGetUpTimeMinuteOfDay() {
		return getUpTimeMinuteOfDay;
	}

	public void setGetUpTimeMinuteOfDay(Integer getUpTimeMinuteOfDay) {
		this.getUpTimeMinuteOfDay = getUpTimeMinuteOfDay;
	}

	public Integer getSleepDuration() {
		return sleepDuration;
	}

	public void setSleepDuration(Integer sleepDuration) {
		this.sleepDuration = sleepDuration;
	}

	public Integer getDeepSleep() {
		return deepSleep;
	}

	public void setDeepSleep(Integer deepSleep) {
		this.deepSleep = deepSleep;
	}

	public Integer getRemSleep() {
		return remSleep;
	}

	public void setRemSleep(Integer remSleep) {
		this.remSleep = remSleep;
	}

	public Integer getLightSleep() {
		return lightSleep;
	}

	public void setLightSleep(Integer lightSleep) {
		this.lightSleep = lightSleep;
	}

	public Integer getWakeTime() {
		return wakeTime;
	}

	public void setWakeTime(Integer wakeTime) {
		this.wakeTime = wakeTime;
	}

	public Integer getWakeups() {
		return wakeups;
	}

	public void setWakeups(Integer wakeups) {
		this.wakeups = wakeups;
	}

	public Double getTemperatureSleep() {
		return temperatureSleep;
	}

	public void setTemperatureSleep(Double temperatureSleep) {
		this.temperatureSleep = temperatureSleep;
	}

	public Integer getHumiditySleep() {
		return humiditySleep;
	}

	public void setHumiditySleep(Integer humiditySleep) {
		this.humiditySleep = humiditySleep;
	}

	public Integer getWorkDuration() {
		return workDuration;
	}

	public void setWorkDuration(Integer workDuration) {
		this.workDuration = workDuration;
	}

	public Integer getWorkoutDuration() {
		return workoutDuration;
	}

	public void setWorkoutDuration(Integer workoutDuration) {
		this.workoutDuration = workoutDuration;
	}

	public Integer getSleepRating() {
		return sleepRating;
	}

	public void setSleepRating(Integer sleepRating) {
		this.sleepRating = sleepRating;
	}

	public Integer getDayEnergyRating() {
		return dayEnergyRating;
	}

	public void setDayEnergyRating(Integer dayEnergyRating) {
		this.dayEnergyRating = dayEnergyRating;
	}

	public Integer getStepCount() {
		return stepCount;
	}

	public void setStepCount(Integer stepCount) {
		this.stepCount = stepCount;
	}

	public Double getMorningWeight() {
		return morningWeight;
	}

	public void setMorningWeight(Double morningWeight) {
		this.morningWeight = morningWeight;
	}

	public Double getEveningWeight() {
		return eveningWeight;
	}

	public void setEveningWeight(Double eveningWeight) {
		this.eveningWeight = eveningWeight;
	}

	public Integer getRestingHeartBeat() {
		return restingHeartBeat;
	}

	public void setRestingHeartBeat(Integer restingHeartBeat) {
		this.restingHeartBeat = restingHeartBeat;
	}

	public Integer getFallAsleepDuration() {
		return fallAsleepDuration;
	}

	public void setFallAsleepDuration(Integer fallAsleepDuration) {
		this.fallAsleepDuration = fallAsleepDuration;
	}

	public DataLineEntity toEntity() {
		DataLineEntity result = new DataLineEntity();

		applyTo(result);

		return result;
	}

	public DataLineEntity applyTo(DataLineEntity result) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, getYear());
		calendar.set(Calendar.MONTH, getMonth() - 1);
		calendar.set(Calendar.DAY_OF_MONTH, getDay());
		result.setDay(calendar.getTime());

		result.setGoToBedTimeMinuteOfDay(getGoToBedTimeMinuteOfDay());
		result.setGetUpTimeMinuteOfDay(getGetUpTimeMinuteOfDay());

		result.setSleepDuration(getSleepDuration());
		result.setDeepSleep(getDeepSleep());
		result.setRemSleep(getRemSleep());
		result.setLightSleep(getLightSleep());
		result.setWakeTime(getWakeTime());
		result.setWakeups(getWakeups());
		result.setFallAsleepDuration(getFallAsleepDuration());

		result.setHumiditySleep(getHumiditySleep());
		result.setTemperatureSleep(getTemperatureSleep());

		result.setWorkDuration(getWorkDuration());

		result.setWorkoutDuration(getWorkoutDuration());

		result.setSleepRating(getSleepRating());
		result.setDayEnergyRating(getDayEnergyRating());

		result.setStepCount(getStepCount());

		result.setEveningWeight(getEveningWeight());
		result.setMorningWeight(getMorningWeight());
		result.setRestingHeartBeat(getRestingHeartBeat());

		return result;
	}

	public static DataLineDto createFromEntity(DataLineEntity dataLineEntity) {
		DataLineDto result = new DataLineDto();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataLineEntity.getDay());
		result.setYear(calendar.get(Calendar.YEAR));
		result.setMonth(calendar.get(Calendar.MONTH) + 1);
		result.setDay(calendar.get(Calendar.DAY_OF_MONTH));

		result.setGoToBedTimeMinuteOfDay(dataLineEntity.getGoToBedTimeMinuteOfDay());
		result.setGetUpTimeMinuteOfDay(dataLineEntity.getGetUpTimeMinuteOfDay());

		result.setSleepDuration(dataLineEntity.getSleepDuration());
		result.setDeepSleep(dataLineEntity.getDeepSleep());
		result.setRemSleep(dataLineEntity.getRemSleep());
		result.setLightSleep(dataLineEntity.getLightSleep());
		result.setWakeTime(dataLineEntity.getWakeTime());
		result.setWakeups(dataLineEntity.getWakeups());
		result.setFallAsleepDuration(dataLineEntity.getFallAsleepDuration());

		result.setHumiditySleep(dataLineEntity.getHumiditySleep());
		result.setTemperatureSleep(dataLineEntity.getTemperatureSleep());

		result.setWorkDuration(dataLineEntity.getWorkDuration());

		result.setWorkoutDuration(dataLineEntity.getWorkoutDuration());

		result.setSleepRating(dataLineEntity.getSleepRating());
		result.setDayEnergyRating(dataLineEntity.getDayEnergyRating());

		result.setStepCount(dataLineEntity.getStepCount());

		result.setEveningWeight(dataLineEntity.getEveningWeight());
		result.setMorningWeight(dataLineEntity.getMorningWeight());
		result.setRestingHeartBeat(dataLineEntity.getRestingHeartBeat());

		return result;
	}
}
