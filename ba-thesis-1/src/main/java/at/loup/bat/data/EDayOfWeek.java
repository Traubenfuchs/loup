package at.loup.bat.data;

import java.util.Calendar;
import java.util.Date;

import at.loup.commons.utilities.ArgumentRuleUtilities;

public enum EDayOfWeek {
	MONDAY(Calendar.MONDAY),
	TUESDAY(Calendar.TUESDAY),
	WEDNESDAY(Calendar.WEDNESDAY),
	THURSDAY(Calendar.THURSDAY),
	FRIDAY(Calendar.FRIDAY),
	SATURDAY(Calendar.SATURDAY),
	SUNDAY(Calendar.SUNDAY);

	private static EDayOfWeek[] javaCalendarEquivalentToEDayOfWeek = new EDayOfWeek[] {
			null,
			SUNDAY,
			MONDAY,
			TUESDAY,
			WEDNESDAY,
			THURSDAY,
			FRIDAY,
			SATURDAY
	};

	private final int javaCalendarEquivalent;

	private EDayOfWeek(int javaCalendarEquivalent) {
		this.javaCalendarEquivalent = javaCalendarEquivalent;
	}

	public int getJavaCalendarEquivalent() {
		return javaCalendarEquivalent;
	}

	public static EDayOfWeek getByJavaCalendarEquivalent(Date date) {
		ArgumentRuleUtilities.notNull("date", date);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		EDayOfWeek result = getByJavaCalendarEquivalent(calendar);

		return result;
	}

	public static EDayOfWeek getByJavaCalendarEquivalent(Calendar calendar) {
		ArgumentRuleUtilities.notNull("calendar", calendar);
		EDayOfWeek result = getByJavaCalendarEquivalent(calendar.get(Calendar.DAY_OF_WEEK));
		return result;
	}

	public static EDayOfWeek getByJavaCalendarEquivalent(int javaCalendarEquivalent) {
		if (javaCalendarEquivalent < 0) {
			throw new IllegalArgumentException("javaCalendarEquivalent must be >= 0 but is <" + javaCalendarEquivalent + ">");
		}
		if (javaCalendarEquivalent > 7) {
			throw new IllegalArgumentException("javaCalendarEquivalent must be <= 7 but is <" + javaCalendarEquivalent + ">");
		}

		EDayOfWeek result = javaCalendarEquivalentToEDayOfWeek[javaCalendarEquivalent];
		return result;
	}
}
