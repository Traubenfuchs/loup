package at.loup.bat.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MathHelper {
	public static double avg(double[] doubles) {
		double sum = 0;
		for (double d : doubles) {
			sum += d;
		}
		return sum / doubles.length;
	}

	public static double avg(int[] integers) {
		long sum = 0;
		for (int i : integers) {
			sum += i;
		}
		return sum / (double) integers.length;
	}

	public static double avg(long[] longs) {
		long sum = 0;
		for (long l : longs) {
			sum += l;
		}
		return sum / (double) longs.length;
	}

	public static double median(int[] integers) {
		if (integers.length == 0) {
			return 0.0;
		}
		if (integers.length % 2 == 0) {
			int centerRight = integers.length / 2;
			return (integers[centerRight] + integers[centerRight - 1]) / (double) integers.length;
		} else {
			return integers[integers.length / 2];
		}
	}

	public static double median(long[] longs) {
		if (longs.length == 0) {
			return 0.0;
		}
		if (longs.length % 2 == 0) {
			int centerRight = longs.length / 2;
			return (longs[centerRight] + longs[centerRight - 1]) / (double) longs.length;
		} else {
			return longs[longs.length / 2];
		}
	}

	public static double median(double[] doubles) {
		if (doubles.length == 0) {
			return 0.0;
		}
		if (doubles.length % 2 == 0) {
			int centerRight = doubles.length / 2;
			return (doubles[centerRight] + doubles[centerRight - 1]) / doubles.length;
		} else {
			return doubles[doubles.length / 2];
		}
	}

	public static class STDVPackage {
		private double average;
		private double variance;
		private double stdv;
		private double min = Double.MAX_VALUE;
		private double max = Double.MIN_VALUE;
		private double median;

		public STDVPackage() {

		}

		public double getAverage() {
			return average;
		}

		public void setAverage(double average) {
			this.average = average;
		}

		public double getVariance() {
			return variance;
		}

		public void setVariance(double variance) {
			this.variance = variance;
		}

		public double getStdv() {
			return stdv;
		}

		public void setStdv(double stdv) {
			this.stdv = stdv;
		}

		public double getMin() {
			return min;
		}

		public void setMin(double min) {
			this.min = min;
		}

		public double getMax() {
			return max;
		}

		public void setMax(double max) {
			this.max = max;
		}

		// public static STDVPackage create(List<Integer> list) {
		// List<Double> doubleList = new ArrayList<>(list.size());
		// for (Integer integer : list) {
		// doubleList.add((double) integer);
		// }
		// STDVPackage result = create(doubleList);
		// return result;
		// }

		public static STDVPackage create(List<? extends Number> listN) {
			List<Double> list = new ArrayList<>();
			for (Number number : listN) {
				if (number == null) {
					continue;
				}
				list.add(number.doubleValue());
			}

			Collections.sort(list);

			int count = list.size();

			STDVPackage result = new STDVPackage();

			double sum = 0;
			for (double d : list) {
				sum += d;
				if (d > result.getMax()) {
					result.setMax(d);
				}
				if (d < result.getMin()) {
					result.setMin(d);
				}
			}
			result.setAverage(sum / count);

			double stdvSum = 0;
			for (double d : list) {
				stdvSum += Math.pow(result.getAverage() - d, 2);
			}
			result.setVariance(stdvSum / count);
			result.setStdv(Math.sqrt(result.getVariance()));

			if (list.size() % 2 == 0) {
				result.median = (list.get(list.size() / 2).doubleValue() +
						list.get(list.size() / 2 + 1).doubleValue())
						/ 2;
			} else {
				result.median = list.get(list.size() / 2).doubleValue();
			}

			return result;
		}

		@Override
		public String toString() {
			StringBuilder result = new StringBuilder();

			result.append("average<").append(average).append("> ");
			result.append("variance<").append(variance).append("> ");
			result.append("stdv<").append(stdv).append("> ");
			result.append("min<").append(min).append("> ");
			result.append("max<").append(max).append("> ");
			result.append("median<").append(median).append(">");

			return result.toString();
		}
	}
}
