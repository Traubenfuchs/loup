package at.loup.bat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.bat.data.DataLineDto;
import at.loup.bat.db.DataLineEntity;
import at.loup.bat.db.repositories.DataLineRepository;
import at.loup.security.aspects.annotations.Secured;
import at.loup.security.entities.UserEntity;
import at.loup.security.entities.repositories.UserEntityRepository;
import at.loup.security.services.UserHolder;

@Secured
@Controller
public class DataPostController {

	@Autowired
	private DataLineRepository dataLineRepository;
	@Autowired
	private UserHolder userHolder;
	@Autowired
	private UserEntityRepository userEntityRepository;

	public DataPostController() {

	}

	@RequestMapping(path = "/api/data", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody DataLineDto updateData(@RequestBody DataLineDto dataLineDto) {
		DataLineEntity dataLineEntity = dataLineDto.toEntity();
		DataLineEntity existingDataLineEntity = dataLineRepository.findByDay(dataLineEntity.getDay());

		if (existingDataLineEntity == null) {
			Long currentUserId = userHolder.getUserDTO().getId();
			UserEntity userEntity = userEntityRepository.findOne(currentUserId);
			dataLineEntity.setUser(userEntity);

			dataLineEntity = dataLineRepository.save(dataLineEntity);

			DataLineDto result = DataLineDto.createFromEntity(dataLineEntity);

			return result;
		} else {
			dataLineDto.applyTo(existingDataLineEntity);
			DataLineDto result = DataLineDto.createFromEntity(existingDataLineEntity);
			return result;
		}
	}
}
