package at.loup.bat.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import at.loup.bat.data.DataLineDto;
import at.loup.bat.data.EDayOfWeek;
import at.loup.bat.db.DataLineEntity;
import at.loup.bat.db.repositories.DataLineRepository;
import at.loup.bat.utilities.MathHelper.STDVPackage;
import at.loup.security.aspects.annotations.Secured;
import at.loup.security.entities.UserEntity;
import at.loup.security.entities.repositories.UserEntityRepository;
import at.loup.security.services.UserHolder;

@Secured()
@RestController
public class DataLoadController {
	private static final Logger logger = LoggerFactory.getLogger(DataLoadController.class);
	@Autowired
	private DataLineRepository dataLineRepository;
	@Autowired
	private UserHolder userHolder;
	@Autowired
	private UserEntityRepository userEntityRepository;

	public DataLoadController() {

	}

	@RequestMapping(path = "/api/data/{year}/{month}/{day}", method = RequestMethod.GET)
	public @ResponseBody DataLineDto loadData(@PathVariable int year, @PathVariable int month, @PathVariable int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, day);

		Date date = calendar.getTime();

		DataLineEntity dataLine = dataLineRepository.findByDay(date);
		if (dataLine == null) {
			dataLine = new DataLineEntity();
			dataLine.setDay(date);
			Long currentUserId = userHolder.getUserDTO().getId();
			UserEntity userEntity = userEntityRepository.findOne(currentUserId);
			dataLine.setUser(userEntity);
		}
		DataLineDto result = DataLineDto.createFromEntity(dataLine);
		return result;
	}

	@RequestMapping(path = "/api/data", method = RequestMethod.GET)
	public @ResponseBody List<DataLineDto> loadData() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());

		List<DataLineDto> result = dataLines.stream().map(DataLineDto::createFromEntity).collect(Collectors.toList());

		return result;
	}

	@RequestMapping(path = "/api/data2", method = RequestMethod.GET)
	public @ResponseBody List<List<Object>> loadData2() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());

		List<List<Object>> result = new ArrayList<>();
		dataLines.forEach(dl -> {
			ArrayList<Object> line = new ArrayList<>();

			line.add(dl.getDay());
			line.add(dl.getSleepDuration());
			line.add(dl.getDeepSleep());
			line.add(dl.getRemSleep());
			line.add(dl.getLightSleep());
			line.add(dl.getWakeTime());
			line.add(dl.getWakeups());

			result.add(line);
		});
		// List<List<String>> result =
		// dataLines.stream().map(DataLineDto::createFromEntity).collect(Collectors.toList());

		return result;
	}

	@Autowired
	ApplicationContext applicationContext;

	@RequestMapping(path = "/api/data/sleep", method = RequestMethod.GET)
	public @ResponseBody List<List<Object>> loadData3() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());

		List<List<Object>> result = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			result.add(new ArrayList<>());
		}

		int varianceDays = 5;

		for (int i = 0; i < dataLines.size(); i++) {
			DataLineEntity dl = dataLines.get(i);

			if (dl.getSleepDuration() == 0) {
				continue;
			}

			result.get(0).add(dl.getDay());
			result.get(1).add(dl.getSleepDuration());
			result.get(2).add(dl.getDeepSleep());
			result.get(3).add(dl.getRemSleep());
			result.get(4).add(dl.getLightSleep());
			result.get(5).add(dl.getWakeTime());
			result.get(6).add(dl.getWakeups());
			result.get(7).add(dl.getFallAsleepDuration());

			int initialweight = 36;
			int varianceReducer = 6;

			double goToBedVariance = 0;
			double getUpVariance = 0;
			int varianceCount = 0;
			int weight = initialweight;

			if (i >= (varianceDays - 1)) {
				double avgGoToBed = 0;
				double avgGetUp = 0;

				for (int i2 = i; i2 > i - varianceDays; i2--) {
					avgGoToBed += weight * dataLines.get(i2).getGoToBedTimeMinuteOfDay();
					avgGetUp += weight * dataLines.get(i2).getGetUpTimeMinuteOfDay();
					varianceCount += weight;
					weight -= varianceReducer;
				}

				weight = initialweight;

				avgGoToBed = avgGoToBed / varianceCount;
				avgGetUp = avgGetUp / varianceCount;

				for (int i2 = i; i2 > i - varianceDays; i2--) {
					for (int i3 = 0; i3 < weight; i3++) {
						getUpVariance += Math.pow(dataLines.get(i2).getGetUpTimeMinuteOfDay() - avgGetUp, 2);
						goToBedVariance += Math.pow(dataLines.get(i2).getGoToBedTimeMinuteOfDay() - avgGoToBed, 2);
					}
					weight -= varianceReducer;
				}
			}
			result.get(8).add(Math.sqrt(goToBedVariance / varianceCount));
			result.get(9).add(Math.sqrt(getUpVariance / varianceCount));
		}

		return result;
	}

	@RequestMapping(path = "/api/data/weight", method = RequestMethod.GET)
	public @ResponseBody List<List<Object>> weight() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());

		List<List<Object>> result = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			result.add(new ArrayList<>());
		}

		dataLines.forEach(dl -> {
			// if(dl.getMorningWeight() == 0 || dl.getEveningWeight() == 0){
			// return;
			// }
			result.get(0).add(dl.getDay());
			result.get(1).add(dl.getMorningWeight());
			result.get(2).add(dl.getEveningWeight());
		});
		return result;
	}

	@RequestMapping(path = "/api/data/environment", method = RequestMethod.GET)
	public @ResponseBody List<List<Object>> environment() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());

		List<List<Object>> result = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			result.add(new ArrayList<>());
		}

		dataLines.forEach(dl -> {
			result.get(0).add(dl.getDay());
			result.get(1).add(dl.getTemperatureSleep());
			result.get(2).add(dl.getHumiditySleep());
		});
		return result;
	}

	@RequestMapping(path = "/api/data/sleepDurations")
	public @ResponseBody List<Integer> sleepDurations() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());
		List<Integer> result = dataLines.stream().map(x -> x.getSleepDuration()).collect(Collectors.toList());
		return result;
	}

	@RequestMapping(path = "/api/data/sleepDurationsFull")
	public @ResponseBody List<Integer> sleepDurationsFull() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());
		List<Integer> result = dataLines.stream()
				.filter(line -> line.getGetUpTimeMinuteOfDay() != 0 && line.getGoToBedTimeMinuteOfDay() != 0)
				.map(x -> (x.getGetUpTimeMinuteOfDay() + 24 * 60) - x.getGoToBedTimeMinuteOfDay())
				.collect(Collectors.toList());
		return result;
	}

	@RequestMapping(path = "/api/data/fallAsleepDurations")
	public @ResponseBody List<Integer> fallAsleepDurations() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());
		List<Integer> result = dataLines.stream().map(x -> x.getFallAsleepDuration()).collect(Collectors.toList());
		return result;
	}

	@RequestMapping(path = "/api/data/wakeTime")
	public @ResponseBody List<Integer> wakeTime() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());
		List<Integer> result = dataLines.stream().map(x -> x.getWakeTime()).collect(Collectors.toList());
		return result;
	}

	@RequestMapping(path = "/api/data/inBetweenWeight")
	public @ResponseBody String inBetween() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());

		List<Double> morningWeightDeviations = new ArrayList<>();
		List<Double> eveningWeightDeviations = new ArrayList<>();

		for (int i = 1; i + 1 < dataLines.size(); i++) {
			DataLineEntity l = dataLines.get(i - 1);
			DataLineEntity m = dataLines.get(i);
			DataLineEntity r = dataLines.get(i + 1);
			morningWeightDeviations
					.add(Math.abs(m.getMorningWeight() - (l.getMorningWeight() + r.getMorningWeight()) / 2));
			eveningWeightDeviations
					.add(Math.abs(m.getEveningWeight() - (l.getEveningWeight() + r.getEveningWeight()) / 2));
		}

		STDVPackage morningPackage = STDVPackage.create(morningWeightDeviations);
		STDVPackage eveningPackage = STDVPackage.create(eveningWeightDeviations);

		return "";
	}

	@RequestMapping(path = "/api/data/weightLossNight")
	public @ResponseBody List<Double> weightLossNight() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());

		dataLines = dataLines.stream().filter(line -> line.getEveningWeight() != 0 && line.getEveningWeight() != 0)
				.collect(Collectors.toList());

		List<Double> values = new ArrayList<>();

		for (int i = 1; i < dataLines.size(); i++) {
			double value = dataLines.get(i - 1).getEveningWeight() - dataLines.get(i).getMorningWeight();
			if (value > 0.4) {
				values.add(value);
			}
		}

		// List<Integer> result = dataLines.stream().map(x ->
		// x.getFallAsleepDuration()).collect(Collectors.toList());
		return values;
	}

	@RequestMapping(path = "/api/data/weightLossNightPerDay")
	public @ResponseBody void weightLossNightPerDay() {
		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());

		Map<EDayOfWeek, List<Double>> x = new HashMap<>();
		x.put(EDayOfWeek.MONDAY, new ArrayList<Double>());
		x.put(EDayOfWeek.TUESDAY, new ArrayList<Double>());
		x.put(EDayOfWeek.WEDNESDAY, new ArrayList<Double>());
		x.put(EDayOfWeek.THURSDAY, new ArrayList<Double>());
		x.put(EDayOfWeek.FRIDAY, new ArrayList<Double>());
		x.put(EDayOfWeek.SATURDAY, new ArrayList<Double>());
		x.put(EDayOfWeek.SUNDAY, new ArrayList<Double>());

		// Map<EDayOfWeek, List<DataLineEntity>> m = dataLines.stream()
		// .filter(line -> line.getEveningWeight() != 0 &&
		// line.getEveningWeight() != 0)
		// .collect(Collectors.groupingBy(DataLineEntity::getDayOfWeek));

		dataLines = dataLines.stream()
				.filter(line -> line.getEveningWeight() != 0 &&
						line.getEveningWeight() != 0)
				.collect(Collectors.toList());

		for (int i = 0; i < dataLines.size(); i++) {
			double weightGain = dataLines.get(i).getEveningWeight() - dataLines.get(i).getMorningWeight();
			x.get(dataLines.get(i).getDayOfWeek()).add(weightGain);
		}

		STDVPackage full = STDVPackage.create(dataLines.stream()
				.map(line -> line.getEveningWeight() - line.getMorningWeight()).collect(Collectors.toList()));

		STDVPackage MONDAY = STDVPackage.create(x.get(EDayOfWeek.MONDAY));
		STDVPackage TUESDAY = STDVPackage.create(x.get(EDayOfWeek.TUESDAY));
		STDVPackage WEDNESDAY = STDVPackage.create(x.get(EDayOfWeek.WEDNESDAY));
		STDVPackage THURSDAY = STDVPackage.create(x.get(EDayOfWeek.THURSDAY));
		STDVPackage FRIDAY = STDVPackage.create(x.get(EDayOfWeek.FRIDAY));
		STDVPackage SATURDAY = STDVPackage.create(x.get(EDayOfWeek.SATURDAY));
		STDVPackage SUNDAY = STDVPackage.create(x.get(EDayOfWeek.SUNDAY));

		"".toCharArray();

	}
}
