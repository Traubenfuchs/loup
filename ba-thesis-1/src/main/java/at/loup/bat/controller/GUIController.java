package at.loup.bat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import at.loup.security.data.LoginResult;
import at.loup.security.data.LoginResult.ELoginResult;
import at.loup.security.data.UserDTO;
import at.loup.security.services.RememberMeComponent;
import at.loup.security.services.UserHolder;

@Controller
public class GUIController {
	@Autowired
	private UserHolder userHolder;
	@Autowired
	private RememberMeComponent rememberMeService;

	public GUIController() {

	}

	@RequestMapping(path = { "/*", "/*/*" }, method = RequestMethod.GET)
	public ModelAndView home() {
		UserDTO userDTO = userHolder.getUserDTO();
		if (userDTO == null) {

			LoginResult rememberMeLoginResult = rememberMeService.tryRememberMeLogin();

			if (rememberMeLoginResult.geteLoginResult() == ELoginResult.loggedInViaRememberMe) {
				ModelAndView result = new ModelAndView("bat1");
				result.addObject("user", rememberMeLoginResult.getUserDTO());
				return result;
			}

			ModelAndView result = new ModelAndView("bat1Login");
			return result;
		}
		ModelAndView result = new ModelAndView("bat1");
		result.addObject("user", userDTO);
		return result;

	}
}
