package at.loup.bat.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import at.loup.commons.utilities.ArgumentRuleUtilities;

public class ARFFFile {
	private final String name;
	private final ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<>();

	private final List<Object[]> values = new ArrayList<>();

	ARFFFile(String name) {
		this.name = name;
	}

	public ARFFFile addInstance(Object... values) {
		ArgumentRuleUtilities.notNull("values", values);
		if (values.length != attributeDefinitions.size()) {
			throw new RuntimeException("" +
					"You gave <" +
					values.length +
					"> values, but this ARFF file demands <" +
					attributeDefinitions.size() +
					"> values per instance.");
		}

		this.values.add(values);

		return this;
	}

	protected void addAttributeDefinition(AttributeDefinition attributeDefinition) {
		attributeDefinitions.add(attributeDefinition);
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder()
				.append("@RELATION ")
				.append(this.name)
				.append('\n')
				.append('\n');

		for (AttributeDefinition attributeDefinition : attributeDefinitions) {

			if (attributeDefinition instanceof NominalAttribute) {
				NominalAttribute na = (NominalAttribute) attributeDefinition;
				result
						.append("@ATTRIBUTE ")
						.append(attributeDefinition.getName())
						.append(' ')

						.append('{')
						.append(StringUtils.join(na.getPossibleValues(), ','))
						.append('}')

						.append('\n');
			} else {
				result
						.append("@ATTRIBUTE ")
						.append(attributeDefinition.getName())
						.append(' ')
						.append(attributeDefinition.getAttributeType().name())
						.append('\n');
			}

		}
		result
				.append('\n')
				.append("@DATA")
				.append('\n');
		;

		for (Object[] instance : values) {
			for (Object value : instance) {
				result
						.append(value)
						.append(',');

			}
			result.deleteCharAt(result.length() - 1);
			result
					.append('\n');
		}

		return result.toString();
	}

	public static ARFFFile create(String name, AttributeDefinition... attributeDefinitions) {
		ARFFFile result = new ARFFFile(name);

		for (AttributeDefinition attributeDefinition : attributeDefinitions) {
			result.addAttributeDefinition(attributeDefinition);
		}

		return result;
	}

	public static class NumericAttribute extends AttributeDefinition {

		public NumericAttribute(String name) {
			super(name);
		}

		@Override
		public EAttributeType getAttributeType() {
			return EAttributeType.NUMERIC;
		}

	}

	public static class NominalAttribute extends AttributeDefinition {

		private final Set<String> possibleValues = new HashSet<>();

		public NominalAttribute(String name, String... possibleValues) {
			super(name);
			this.possibleValues.addAll(Arrays.asList(possibleValues));
		}

		public Set<String> getPossibleValues() {
			return possibleValues;
		}

		@Override
		public EAttributeType getAttributeType() {
			return EAttributeType.NOMINAL;
		}

	}

	public static class StringAttribute extends AttributeDefinition {

		public StringAttribute(String name) {
			super(name);
		}

		@Override
		public EAttributeType getAttributeType() {
			return EAttributeType.STRING;
		}
	}

	protected static abstract class AttributeDefinition {
		private final String name;

		public AttributeDefinition(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public abstract EAttributeType getAttributeType();

	}

	public static enum EAttributeType {
		NUMERIC, STRING, NOMINAL,
	}
}
