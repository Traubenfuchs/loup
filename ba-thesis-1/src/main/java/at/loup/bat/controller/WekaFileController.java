package at.loup.bat.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import at.loup.bat.controller.ARFFFile.NominalAttribute;
import at.loup.bat.controller.ARFFFile.NumericAttribute;
import at.loup.bat.db.DataLineEntity;
import at.loup.bat.db.repositories.DataLineRepository;
import at.loup.security.aspects.annotations.Secured;
import at.loup.security.services.UserHolder;

@Secured()
@RestController
public class WekaFileController {
	@Autowired
	private DataLineRepository dataLineRepository;
	@Autowired
	private UserHolder userHolder;

	public WekaFileController() {

	}

	@RequestMapping(path = "/api/weka/lotsOfData", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String lotsOfData() {
		ARFFFile file = ARFFFile.create("Schlafdauer_plus_Schlafstabilität",
				new NumericAttribute("Schlafdauer"),
				new NumericAttribute("SchlafdauerVoll"),
				new NumericAttribute("AufstehSTDV"),
				new NumericAttribute("ZuBettGehSTDV"),
				new NumericAttribute("Schlafqualität"),

				new NominalAttribute("TagDerWoche", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY",
						"SUNDAY"),
				new NumericAttribute("TiefSchlaf"),
				new NumericAttribute("RemSchlaf"),
				new NumericAttribute("LeichterSchlaf"),
				new NumericAttribute("WachliegeZeit"),
				new NumericAttribute("SchrittZahl"),
				new NumericAttribute("AnzahlAufwachen"),
				new NumericAttribute("FitnessDauer"));

		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());
		dataLines = dataLines.stream().filter(line -> true &&
				line.getEveningWeight() != 0 &&
				line.getEveningWeight() != 0 &&
				line.getSleepDuration() != 0)
				.collect(Collectors.toList());

		int varianceDays = 5;

		for (int i = 0; i < dataLines.size(); i++) {
			DataLineEntity dl = dataLines.get(i);

			if (dl.getSleepDuration() == 0) {
				continue;
			}

			int initialweight = 36;
			int varianceReducer = 6;

			double goToBedVariance = 0;
			double getUpVariance = 0;
			int varianceCount = 0;
			int weight = initialweight;

			if (i >= (varianceDays - 1)) {
				double avgGoToBed = 0;
				double avgGetUp = 0;

				for (int i2 = i; i2 > i - varianceDays; i2--) {
					avgGoToBed += weight * dataLines.get(i2).getGoToBedTimeMinuteOfDay();
					avgGetUp += weight * dataLines.get(i2).getGetUpTimeMinuteOfDay();
					varianceCount += weight;
					weight -= varianceReducer;
				}

				weight = initialweight;

				avgGoToBed = avgGoToBed / varianceCount;
				avgGetUp = avgGetUp / varianceCount;

				for (int i2 = i; i2 > i - varianceDays; i2--) {
					for (int i3 = 0; i3 < weight; i3++) {
						getUpVariance += Math.pow(dataLines.get(i2).getGetUpTimeMinuteOfDay() - avgGetUp, 2);
						goToBedVariance += Math.pow(dataLines.get(i2).getGoToBedTimeMinuteOfDay() - avgGoToBed, 2);
					}
					weight -= varianceReducer;
				}

				double weightedGoToBedSTDV = Math.sqrt(goToBedVariance / varianceCount);
				double weightedGetUpSTDV = Math.sqrt(getUpVariance / varianceCount);
				double sleepRating = dl.getSleepRating();

				file.addInstance(
						dl.getSleepDuration(),
						(24 * 60) - (dl.getGoToBedTimeMinuteOfDay() - dl.getGetUpTimeMinuteOfDay()),
						weightedGetUpSTDV,
						weightedGoToBedSTDV,
						sleepRating,
						dl.getDayOfWeek().name(),
						dl.getDeepSleep(),
						dl.getRemSleep(),
						dl.getLightSleep(),
						dl.getWakeTime(),
						dl.getStepCount(),
						dl.getWakeups(),
						dl.getWorkoutDuration());

			}
		} // ends for

		String result = file.toString();
		return result;
	}

	@RequestMapping(path = "/api/weka/sleepDuration", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String sleepDuration() {
		ARFFFile file = ARFFFile.create("Schlafdauer",
				new NumericAttribute("Schlafdauer"),
				new NumericAttribute("Schlafqualität"));

		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());
		dataLines = dataLines.stream().filter(line -> true &&
				line.getEveningWeight() != 0 &&
				line.getEveningWeight() != 0 &&
				line.getSleepDuration() != 0)
				.collect(Collectors.toList());

		int varianceDays = 5;

		for (int i = 0; i < dataLines.size(); i++) {
			DataLineEntity dl = dataLines.get(i);

			if (dl.getSleepDuration() == 0) {
				continue;
			}
			double sleepRating = dl.getSleepRating();

			file.addInstance(dl.getSleepDuration(),
					sleepRating);

		} // ends for

		String result = file.toString();
		return result;
	}

	@RequestMapping(path = "/api/weka/sleeDurationPlusSleepStability", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String sleeDurationPlusSleepStability() {
		ARFFFile file = ARFFFile.create("Schlafdauer_plus_Schlafstabilität",
				new NumericAttribute("Schlafdauer"),
				new NumericAttribute("AufstehSTDV"),
				new NumericAttribute("ZuBettGehSTDV"),
				new NumericAttribute("Schlafqualität"));

		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());
		dataLines = dataLines.stream().filter(line -> true &&
				line.getEveningWeight() != 0 &&
				line.getEveningWeight() != 0 &&
				line.getSleepDuration() != 0)
				.collect(Collectors.toList());

		int varianceDays = 5;

		for (int i = 0; i < dataLines.size(); i++) {
			DataLineEntity dl = dataLines.get(i);

			if (dl.getSleepDuration() == 0) {
				continue;
			}

			int initialweight = 36;
			int varianceReducer = 6;

			double goToBedVariance = 0;
			double getUpVariance = 0;
			int varianceCount = 0;
			int weight = initialweight;

			if (i >= (varianceDays - 1)) {
				double avgGoToBed = 0;
				double avgGetUp = 0;

				for (int i2 = i; i2 > i - varianceDays; i2--) {
					avgGoToBed += weight * dataLines.get(i2).getGoToBedTimeMinuteOfDay();
					avgGetUp += weight * dataLines.get(i2).getGetUpTimeMinuteOfDay();
					varianceCount += weight;
					weight -= varianceReducer;
				}

				weight = initialweight;

				avgGoToBed = avgGoToBed / varianceCount;
				avgGetUp = avgGetUp / varianceCount;

				for (int i2 = i; i2 > i - varianceDays; i2--) {
					for (int i3 = 0; i3 < weight; i3++) {
						getUpVariance += Math.pow(dataLines.get(i2).getGetUpTimeMinuteOfDay() - avgGetUp, 2);
						goToBedVariance += Math.pow(dataLines.get(i2).getGoToBedTimeMinuteOfDay() - avgGoToBed, 2);
					}
					weight -= varianceReducer;
				}

				double weightedGoToBedSTDV = Math.sqrt(goToBedVariance / varianceCount);
				double weightedGetUpSTDV = Math.sqrt(getUpVariance / varianceCount);
				double sleepRating = dl.getSleepRating();

				file.addInstance(dl.getSleepDuration(),
						weightedGetUpSTDV,
						weightedGoToBedSTDV,
						sleepRating);

			}
		} // ends for

		String result = file.toString();
		return result;
	}

	@RequestMapping(path = "/api/weka/sleepStabilityAndEinschlafzeit", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String sleepStabilityAndEinschlafzeit() {
		ARFFFile file = ARFFFile.create("Schlafdauer_plus_Schlafstabilität",
				new NumericAttribute("AufstehSTDV"),
				new NumericAttribute("ZuBettGehSTDV"),
				new NumericAttribute("Einschlafzeit"));

		List<DataLineEntity> dataLines = dataLineRepository.findAllByUserIdOrderByDayAsc(userHolder.getUserId());
		dataLines = dataLines.stream().filter(line -> true &&
				line.getEveningWeight() != 0 &&
				line.getEveningWeight() != 0 &&
				line.getSleepDuration() != 0)
				.collect(Collectors.toList());

		int varianceDays = 5;

		for (int i = 0; i < dataLines.size(); i++) {
			DataLineEntity dl = dataLines.get(i);

			if (dl.getSleepDuration() == 0) {
				continue;
			}

			int initialweight = 36;
			int varianceReducer = 6;

			double goToBedVariance = 0;
			double getUpVariance = 0;
			int varianceCount = 0;
			int weight = initialweight;

			if (i >= (varianceDays - 1)) {
				double avgGoToBed = 0;
				double avgGetUp = 0;

				for (int i2 = i; i2 > i - varianceDays; i2--) {
					avgGoToBed += weight * dataLines.get(i2).getGoToBedTimeMinuteOfDay();
					avgGetUp += weight * dataLines.get(i2).getGetUpTimeMinuteOfDay();
					varianceCount += weight;
					weight -= varianceReducer;
				}

				weight = initialweight;

				avgGoToBed = avgGoToBed / varianceCount;
				avgGetUp = avgGetUp / varianceCount;

				for (int i2 = i; i2 > i - varianceDays; i2--) {
					for (int i3 = 0; i3 < weight; i3++) {
						getUpVariance += Math.pow(dataLines.get(i2).getGetUpTimeMinuteOfDay() - avgGetUp, 2);
						goToBedVariance += Math.pow(dataLines.get(i2).getGoToBedTimeMinuteOfDay() - avgGoToBed, 2);
					}
					weight -= varianceReducer;
				}

				double weightedGoToBedSTDV = Math.sqrt(goToBedVariance / varianceCount);
				double weightedGetUpSTDV = Math.sqrt(getUpVariance / varianceCount);
				double sleepRating = dl.getFallAsleepDuration();

				file.addInstance(
						weightedGetUpSTDV,
						weightedGoToBedSTDV,
						sleepRating);

			}

		}
		String result = file.toString();
		return result;
	}
}
