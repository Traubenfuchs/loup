package at.loup.bat.db.repositories;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.stereotype.Service;

import at.loup.commons.utilities.ArgumentRuleUtilities;
import weka.core.Instances;

@Service
public class WekaService {
	public WekaService() {

	}

	public void doSomething() throws IOException {
		BufferedReader datafile = readDataFile("weather.txt");

		Instances data = new Instances(datafile);
		data.setClassIndex(data.numAttributes() - 1);

	}

	public static BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			throw new RuntimeException(ex);
		}

		return inputReader;
	}

	public static BufferedReader pathToReader(String filename) {
		ArgumentRuleUtilities.notNull("filename", filename);
		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			throw new RuntimeException("File<" + filename + "> not found!", ex);
		}

		return inputReader;
	}
}
