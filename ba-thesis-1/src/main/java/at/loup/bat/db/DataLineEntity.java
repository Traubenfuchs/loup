package at.loup.bat.db;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import at.loup.bat.data.EDayOfWeek;
import at.loup.commons.entities.AbstractTimedEntity;
import at.loup.security.entities.UserEntity;

@Entity
@Table(name = "dataline", uniqueConstraints = @UniqueConstraint(columnNames = { "userId", "day" }))
public class DataLineEntity extends AbstractTimedEntity<DataLineEntity> {
	private static final long serialVersionUID = 1757352766860555341L;

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date day;
	@Enumerated(EnumType.STRING)
	private EDayOfWeek dayOfWeek = null;
	private Integer goToBedTimeMinuteOfDay = 0;
	private Integer getUpTimeMinuteOfDay = 0;

	private Integer sleepDuration = 0;
	private Integer deepSleep = 0;
	private Integer remSleep = 0;
	private Integer lightSleep = 0;
	private Integer wakeTime = 0;
	private Integer wakeups = 0;
	private Integer fallAsleepDuration = 0;

	private Double temperatureSleep = 0.0;
	private Integer humiditySleep = 0;

	private Integer workDuration = 0;

	private Integer workoutDuration = 0;

	private Integer sleepRating = 0;
	private Integer dayEnergyRating = 0;

	private Integer stepCount = 0;

	private Double morningWeight = 0.0;
	private Double eveningWeight = 0.0;
	private Integer restingHeartBeat = 0;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "userId", nullable = false)
	private UserEntity user;

	@Column(name = "userId", insertable = false, updatable = false)
	private Long userId;

	public DataLineEntity() {
	}

	public Long getUserId() {
		return userId;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(day);
		EDayOfWeek eDayOfWeek = EDayOfWeek.getByJavaCalendarEquivalent(calendar);

		setDayOfWeek(eDayOfWeek);
	}

	public EDayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	protected void setDayOfWeek(EDayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Integer getSleepDuration() {
		return sleepDuration;
	}

	public void setSleepDuration(Integer sleepDuration) {
		this.sleepDuration = sleepDuration;
	}

	public Integer getDeepSleep() {
		return deepSleep;
	}

	public void setDeepSleep(Integer deepSleep) {
		this.deepSleep = deepSleep;
	}

	public Integer getRemSleep() {
		return remSleep;
	}

	public void setRemSleep(Integer remSleep) {
		this.remSleep = remSleep;
	}

	public Integer getGoToBedTimeMinuteOfDay() {
		return goToBedTimeMinuteOfDay;
	}

	public void setGoToBedTimeMinuteOfDay(Integer goToBedTimeMinuteOfDay) {
		this.goToBedTimeMinuteOfDay = goToBedTimeMinuteOfDay;
	}

	public Integer getGetUpTimeMinuteOfDay() {
		return getUpTimeMinuteOfDay;
	}

	public void setGetUpTimeMinuteOfDay(Integer getUpTimeMinuteOfDay) {
		this.getUpTimeMinuteOfDay = getUpTimeMinuteOfDay;
	}

	public Integer getLightSleep() {
		return lightSleep;
	}

	public void setLightSleep(Integer lightSleep) {
		this.lightSleep = lightSleep;
	}

	public Integer getWakeTime() {
		return wakeTime;
	}

	public void setWakeTime(Integer wakeTime) {
		this.wakeTime = wakeTime;
	}

	public Integer getWakeups() {
		return wakeups;
	}

	public void setWakeups(Integer wakeups) {
		this.wakeups = wakeups;
	}

	public Integer getWorkDuration() {
		return workDuration;
	}

	public void setWorkDuration(Integer workDuration) {
		this.workDuration = workDuration;
	}

	public Integer getWorkoutDuration() {
		return workoutDuration;
	}

	public void setWorkoutDuration(Integer workoutDuration) {
		this.workoutDuration = workoutDuration;
	}

	public Integer getSleepRating() {
		return sleepRating;
	}

	public void setSleepRating(Integer sleepRating) {
		this.sleepRating = sleepRating;
	}

	public Integer getStepCount() {
		return stepCount;
	}

	public void setStepCount(Integer stepCount) {
		this.stepCount = stepCount;
	}

	public Double getMorningWeight() {
		return morningWeight;
	}

	public void setMorningWeight(Double morningWeight) {
		this.morningWeight = morningWeight;
	}

	public Double getEveningWeight() {
		return eveningWeight;
	}

	public void setEveningWeight(Double eveningWeight) {
		this.eveningWeight = eveningWeight;
	}

	public Integer getHumiditySleep() {
		return humiditySleep;
	}

	public void setHumiditySleep(Integer humiditySleep) {
		this.humiditySleep = humiditySleep;
	}

	public Integer getDayEnergyRating() {
		return dayEnergyRating;
	}

	public void setDayEnergyRating(Integer dayEnergyRating) {
		this.dayEnergyRating = dayEnergyRating;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
		if (user != null) {
			userId = user.getId();
		}
	}

	public Double getTemperatureSleep() {
		return temperatureSleep;
	}

	public void setTemperatureSleep(Double temperatureSleep) {
		this.temperatureSleep = temperatureSleep;
	}

	public Integer getRestingHeartBeat() {
		return restingHeartBeat;
	}

	public void setRestingHeartBeat(Integer restingHeartBeat) {
		this.restingHeartBeat = restingHeartBeat;
	}

	public Integer getFallAsleepDuration() {
		return fallAsleepDuration;
	}

	public void setFallAsleepDuration(Integer fallAsleepDuration) {
		this.fallAsleepDuration = fallAsleepDuration;
	}
}
