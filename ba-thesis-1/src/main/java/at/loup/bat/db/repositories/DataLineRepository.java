package at.loup.bat.db.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.loup.bat.db.DataLineEntity;

@Repository
public interface DataLineRepository extends JpaRepository<DataLineEntity, Long> {
	public DataLineEntity findByDay(Date date);

	public List<DataLineEntity> findAllByUserIdOrderByDayAsc(Long userId);
}
