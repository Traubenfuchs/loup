package at.loup;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LinearRegression;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

@SpringBootApplication
@ComponentScan(basePackages = "at.loup")
@EnableAutoConfiguration(exclude = { JmxAutoConfiguration.class, WebSocketAutoConfiguration.class,
		JacksonAutoConfiguration.class, GsonAutoConfiguration.class })
@EntityScan(basePackages = { "at.loup.bat.db", "at.loup.commons.entities", "at.loup.security.entities" })
@EnableJpaRepositories(basePackages = { "at.loup.bat.db.repositories", "at.loup.security.entities.repositories" })
public class BaThesis1Application extends SpringBootServletInitializer {

	public static void main(String[] args) throws Exception {

		SpringApplication.run(BaThesis1Application.class, args);
		if (true) {
			return;
		}
		Attribute x = new Attribute("x");
		Attribute y = new Attribute("y");

		ArrayList<Attribute> fvWekaAttributes = new ArrayList<>();
		fvWekaAttributes.add(x);
		fvWekaAttributes.add(y);

		Instances instances = new Instances("Rel", fvWekaAttributes, 10);
		instances.setClassIndex(1);

		adD(instances, 1, 2);
		adD(instances, 2, 4);
		adD(instances, 3, 6);
		adD(instances, 4, 8);
		adD(instances, 5, 10);
		adD(instances, 6, 12);

		Classifier model = new LinearRegression();
		model.buildClassifier(instances);

		Evaluation eTest = new Evaluation(instances);
		eTest.evaluateModel(model, instances);

		String strSummary = eTest.toSummaryString();
		System.out.println(strSummary);

	}

	static void adD(Instances instances, int x, int y) {
		Instance instance = new DenseInstance(2);
		instance.setValue(0, x);
		instance.setValue(1, y);
		instances.add(instance);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BaThesis1Application.class);
	}
}
