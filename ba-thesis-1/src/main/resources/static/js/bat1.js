var Loup = Loup || {};
window.onload = function onload() {
	const uniqueIdentifier = Loup.Random.createNiceString(32);
	l.log('starting up vue application. Vue.js version<' + Vue.version + '>, uniqueIdentifier<' + uniqueIdentifier + '>');
	Loup.Templator.collect('templates');

	const inputComponent = {
		name: 'input',
		template: Loup.Templator.templates.input,
		data: function data() {
			const today = new Date();
			let month = today.getMonth() + 1;
			if (month < 10) {
				month = '0' + month;
			}
			let day = today.getDate();
			if (day < 10) {
				day = '0' + day;
			}

			const result = {
				date: `${today.getFullYear()}-${month}-${day}`,
				dl: {
					sleepRating: 0,
					goToBedTimeMinuteOfDay: 0,
					getUpTimeMinuteOfDay: 0,
					sleepDuration: 0,
					deepSleep: 0,
					remSleep: 0,
					lightSleep: 0,
					wakeTime: 0,
					wakeups: 0,
					temperatureSleep: 0,
					humiditySleep: 0,
					workDuration: 0,
					workoutDuration: 0,
					sleepRating: 0,
					dayEnergyRating: 0,
					stepCount: 0,
					morningWeight: 0,
					eveningWeight: 0,
					restingHeartBeat: 0,
					fallAsleepDuration: 0
				}
			};
			return result;
		},
		methods: {
			save() {
				l.log('saved called');
				this.dl.year = this.Year;
				this.dl.month = this.Month;
				this.dl.day = this.Day;

				Loup.Ajax.post(
					'/api/data',
					this.dl,
					r => l.log('save success'),
					r => l.log('save error')
				);
			},
			load() {
				l.log('load called');
				Loup.Ajax.get(
					`/api/data/${this.Year}/${this.Month}/${this.Day}`,
					r => {
						l.log('load success');
						this.dl = JSON.parse(r.responseText);
						this.dl.date = `${this.dl.year}-${this.dl.month < 10 ? '0' : ''}${this.dl.month}-${this.dl.day < 10 ? '0' : ''}${this.dl.day}`;

						l.log(r.responseText);
					},
					r => l.log('load error')
				);
			}
		},
		computed: {
			Year: {
				get() { return this.date.substring(0, 4); }
			},
			Month: {
				get() { return this.date.substring(5, 7); }
			},
			Day: {
				get() { return this.date.substring(8, 10); }
			},
			Date: {
				get() { return this.date; },
				set(newValue) {
					l.log("Received<" + newValue + ">");

					if (newValue === this.date) {
						return;
					}
					this.date = newValue;
					this.load();
				}
			}
		},
		created() {
			this.load();
		},
		watch: {

		}
	};

	Vue.component(
		'stdv', {
			template: `
				<div class='stdv' style='padding-left:10px'>
					<h1>{{name}}</h1>
					<div class='stdv-barcontainer'>
						<div class='stdv-bar-outer' v-for='percentage in percentages' v-on:mouseenter='mm(percentage)' v-on:mouseleave='disableSelected'>
							<div  class='stdv-bar' v-bind:style="{ height:percentage.percentage + '%' }">
							</div>
						</div>
						<p style='margin-top:auto;transform: rotate(-90deg);width:40px;text-align:right;font-size:0.75em'>Einheitenlose Häufung</p>
					</div>
					<div class='stdv-legend'>
						<p>{{xa0}}</p>
						<p>{{xa25}}</p>
						<p>{{xa50}}</p>
						<p>{{xa75}}</p>
						<p>{{xa100}}</p>
					</div>
					<p v-if='showSelected'>Ausgewählter Wert: {{displayValue + unit}}</p>
					<p>Arithmetisches Mittel: {{avg + unit}}</p>
					<p>Standardabweichung: {{stdv + unit}}</p>
					<p>Varianz: {{variance}}</p>
					<p>Median: {{median + unit}}</p>
				</div>
			`,
			data() {
				return {
					percentages: [],
					selected: -1,
					displayValue: undefined,
					barCount: 750,
					maxSpread: 40,
					modifier: 7,
					minValue: 0,
					maxValue: 0,
					avg: 0,
					stdv: 0,
					variance: 0,
					displayValue: ' ',
					xa0: '',
					xa25: '',
					xa50: '',
					xa75: '',
					xa100: '',
					showSelected: false,
					median: 0,
				};
			},
			props: {
				url: String,
				name: String,
				unit: {
					type: String,
					default: ''
				},
				allowZero: {
					type: Boolean,
					default: false,
				},
			},
			methods: {
				disableSelected() {
					this.showSelected = false;
				},
				mm(percentage) {
					this.select = percentage.position;
					this.displayValue = percentage.value;
					this.showSelected = true;
				},
				calculatePercentages(values) {
					if (!this.allowZero) {
						values = values.filter(v => v !== 0);
					}

					const minValue = Math.min(...values);
					const maxValue = Math.max(...values);

					const newPercentages = [];
					const minna = (maxValue - minValue) / this.barCount;
					for (let i = 0; i < this.barCount; i++) {
						newPercentages.push({
							position: i,
							value: (minna * i + minValue),
							percentage: 0
						});
					}

					const normalizedMaxValue = maxValue - minValue;

					let sumValue = 0;

					values.sort((l, r) => l - r);

					if (values.length % 2 !== 0) {
						this.median = values[Math.floor(values.length / 2)]
					} else {
						this.median = ((values[values.length / 2] + values[1 + values.length / 2]) / 2)
					}

					this.median = this.median.toFixed(2)

					for (const value of values) {
						sumValue += value;

						const normalizedValue = value - minValue;
						const perc1 = normalizedValue / normalizedMaxValue;
						const spot = Math.round(perc1 * (this.barCount - 1));

						let laufer = 7000;
						let modifier = 600;
						let positioner = 1;

						newPercentages[spot].percentage += laufer;

						while (laufer >= 1 && modifier >= 1) {
							const minusSpread = spot - positioner;
							const plusSpread = spot + positioner;
							if (minusSpread >= 0)
								newPercentages[minusSpread].percentage += laufer;
							if (plusSpread < this.barCount)
								newPercentages[plusSpread].percentage += laufer;

							modifier -= 115 / (positioner);

							laufer -= modifier;
							positioner++;
						}
					}

					this.avg = sumValue / values.length;
					let stdvSum = 0;
					for (const value of values) {
						stdvSum += Math.pow(this.avg - value, 2);
					}
					this.variance = stdvSum / values.length;
					this.stdv = Math.sqrt(this.variance);

					this.avg = this.avg.toFixed(2);
					this.stdv = this.stdv.toFixed(2);
					this.variance = this.variance.toFixed(2);

					const minPercentageVal = Math.min(...newPercentages.map(v => v.percentage));
					const maxPercentageVal = Math.max(...newPercentages.map(v => v.percentage));

					for (let i = 0; i < this.barCount; i++) {
						newPercentages[i].percentage = (newPercentages[i].percentage / maxPercentageVal) * 100;
					}
					this.minValue = minValue;
					this.maxValue = maxValue;
					this.percentages = newPercentages;

					this.xa0 = (this.minValue).toFixed(2) + this.unit;
					this.xa25 = (this.minValue + (this.maxValue - this.minValue) / 4).toFixed(2) + this.unit;
					this.xa50 = (this.minValue + (this.maxValue - this.minValue) / 2).toFixed(2) + this.unit;
					this.xa75 = (this.minValue + 3 * ((this.maxValue - this.minValue) / 4)).toFixed(2) + this.unit;
					this.xa100 = (maxValue).toFixed(2) + this.unit;
				}

			},
			mounted() {
				Loup.Ajax.get(
					this.url,
					r => {
						let result = JSON.parse(r.responseText);
						this.calculatePercentages(result);
					}
				);
			}
		});

	const analyticsComponent = {
		name: 'analytics',
		template: `
			<div>
				<stdv url='/api/data/sleepDurations' name='Sleep Duration'></stdv>
			</div>
		`,
		data() {
			return {
				percentages: []
			};
		},
		methods: {
		},
		computed: {

		},
		created() {

		},
		watch: {

		}
	};

	const displayComponent = {
		name: 'displayComponent',
		template: Loup.Templator.templates.display,
		data: function data() {
			const result = {

			};
			return result;
		},
		methods: {

		},
		computed: {

		},
		mounted() {
			Loup.Ajax.get(
				'/api/data/environment',
				r => {
					let loadedData = JSON.parse(r.responseText);
					//l.log(JSON.stringify(loadedData));

					Highcharts.chart(
						't', {
							chart: {
								zoomType: 'xy'
							},
							title: {
								text: 'Environment Data'
							},
							subtitle: {
								text: ''
							},
							xAxis: [{
								categories: loadedData[0], // dates
								crosshair: true
							}],
							yAxis: [
								{ // Primary yAxis celsius
									labels: {
										format: '{value} C°',
										style: {
											color: Highcharts.getOptions().colors[0]
										}
									},
									title: {
										text: 'Temperature',
										style: {
											color: Highcharts.getOptions().colors[0]
										}
									},
									opposite: false
								},
								{ // Primary yAxis hum
									labels: {
										format: '{value} %',
										style: {
											color: Highcharts.getOptions().colors[0]
										}
									},
									title: {
										text: 'rel. hum.',
										style: {
											color: Highcharts.getOptions().colors[0]
										}
									},
									max: 100,
									min: 0,
									opposite: true
								}
							],
							tooltip: {
								shared: true
							},
							legend: {
								layout: 'vertical',
								align: 'left',
								x: 200,
								verticalAlign: 'top',
								y: 55,
								floating: true,
								backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
							},
							series: [
								{
									name: 'Temperature',
									type: 'line',
									dashStyle: 'ShortDot',
									yAxis: 0,
									data: loadedData[1],
									tooltip: {
										valueSuffix: ' C°'
									}

								}, {
									name: 'Humidity',
									type: 'line',
									dashStyle: 'ShortDot',
									yAxis: 1,
									data: loadedData[2],
									tooltip: {
										valueSuffix: ' %'
									}
								}
							] // closes series
						}
					); // closes highcharts
				}
			);

			Loup.Ajax.get(
				'/api/data/weight',
				r => {
					let loadedData = JSON.parse(r.responseText);
					//l.log(JSON.stringify(loadedData));

					Highcharts.chart(
						'z', {
							chart: {
								zoomType: 'xy'
							},
							title: {
								text: 'Gewichtsentwicklung'
							},
							subtitle: {
								text: ''
							},
							xAxis: [{
								categories: loadedData[0], // dates
								crosshair: true
							}],
							yAxis: [
								{ // Primary yAxis minutes
									labels: {
										format: '{value} kg',
										style: {
											color: Highcharts.getOptions().colors[0]
										}
									},
									title: {
										text: 'Weight',
										style: {
											color: Highcharts.getOptions().colors[0]
										}
									},
									opposite: false
								}
							],
							tooltip: {
								shared: true
							},
							legend: {
								layout: 'vertical',
								align: 'left',
								x: 200,
								verticalAlign: 'top',
								y: 55,
								floating: true,
								backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
							},
							series: [
								{
									name: 'Gewicht am Morgen',
									type: 'line',
									dashStyle: 'ShortDot',
									yAxis: 0,
									data: loadedData[1],
									tooltip: {
										valueSuffix: ' kg'
									}

								}, {
									name: 'Gewicht am Abend',
									type: 'line',
									dashStyle: 'ShortDot',
									yAxis: 0,
									data: loadedData[2],
									tooltip: {
										valueSuffix: ' kg'
									}
								}
							] // closes series
						}
					); // closes highcharts
				}
			);

			Loup.Ajax.get(
				'/api/data/sleep',
				r => {
					let loadedData = JSON.parse(r.responseText);
					//l.log(JSON.stringify(loadedData));

					Highcharts.chart(
						'x', {
							chart: {
								zoomType: 'xy'
							},
							title: {
								text: 'Sleep Data'
							},
							subtitle: {
								text: ''
							},
							xAxis: [{
								categories: loadedData[0], // dates
								crosshair: true
							}],
							yAxis: [
								{ // Primary yAxis minutes
									labels: {
										format: '{value} minutes',
										style: {
											color: Highcharts.getOptions().colors[2]
										}
									},
									title: {
										text: 'Duration',
										style: {
											color: Highcharts.getOptions().colors[2]
										}
									},
									opposite: false
								}, { // Secondary yAxis count
									gridLineWidth: 0,
									title: {
										text: 'count',
										style: {
											color: Highcharts.getOptions().colors[0]
										}
									},
									labels: {
										format: '{value} times',
										style: {
											color: Highcharts.getOptions().colors[0]
										}
									},
									min: 0,
									opposite: true
								}
							],
							tooltip: {
								shared: true
							},
							legend: {
								layout: 'vertical',
								align: 'left',
								x: 200,
								verticalAlign: 'top',
								y: 55,
								floating: true,
								backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
							},
							series: [
								{
									name: 'Sleep Duration',
									type: 'line',
									dashStyle: 'ShortDot',
									yAxis: 0,
									data: loadedData[1],
									tooltip: {
										valueSuffix: ' m'
									}

								}
								, {
									name: 'Deep Sleep Duration',
									type: 'line',
									dashStyle: 'ShortDot',
									yAxis: 0,
									data: loadedData[2],
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'Rem Sleep',
									type: 'line',
									dashStyle: 'ShortDot',
									data: loadedData[3],
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'Light Sleep',
									type: 'line',
									dashStyle: 'ShortDot',
									data: loadedData[4],
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'Wake Time',
									type: 'line',
									dashStyle: 'ShortDot',
									data: loadedData[5],
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'Wakeups',
									type: 'line',
									dashStyle: 'ShortDot',
									data: loadedData[6],
									yAxis: 1,
									tooltip: {
										valueSuffix: ''
									}
								}
							] // closes series
						}
					); // closes highcharts

					Highcharts.chart(
						'y', {
							chart: {
								type: 'column'
							},
							title: {
								text: 'Sleep Architecture'
							},
							xAxis: {
								categories: loadedData[0]
							},
							yAxis: [
								{
									min: 0,
									title: {
										text: 'percent'
									}

								}
								, {
									min: 0,
									title: {
										text: 'minutes'
									}

								}
							],
							tooltip: {
								pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
								shared: true
							},
							plotOptions: {
								column: {
									stacking: 'percent'
								}
							},
							series: [
								{
									name: 'Deep Sleep',
									data: loadedData[2],
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'Rem Sleep',
									data: loadedData[3],
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'Light Sleep',
									data: loadedData[4],
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'Fall Asleep Duration',
									data: loadedData[7],
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'go to bed variance',
									type: 'line',
									dashStyle: 'ShortDot',
									data: loadedData[8],
									yAxis: 1,
									tooltip: {
										valueSuffix: ' m'
									}
								}
								, {
									name: 'get up variance',
									type: 'line',
									dashStyle: 'ShortDot',
									data: loadedData[9],
									yAxis: 1,
									tooltip: {
										valueSuffix: ' m'
									}
								}
							]

						}
					);

				}, // closes success r->{}
				r => {

				}
			);
		}, // closes mounted()
		watch: {

		}
	};

	const router = new VueRouter({
		mode: 'history',
		routes: [{
			path: '/input',
			component: inputComponent
		}, {
			path: '/display',
			component: displayComponent
		}, {
			path: '/analytics',
			component: analyticsComponent
		}, {
			path: '/',
			component: {
				template: '<div></div>'
			}
		}
		]
	});

	const vue = new Vue({
		router: router,
		template: Loup.Templator.templates.vueapp,
		data: {
			menuVisible: true,
			menuTop: 0,
			menuRunnerRunning: false
		},
		methods: {
			changeMenu() {
				this.menuVisible = !this.menuVisible;

				if (this.menuRunnerRunning) {
					return;
				}

				let menu = document.getElementById('menu');

				if (this.menuVisible) {
					this.menuTop = menu.offsetHeight;
				} else {
					this.menuTop = 0;
				}


				this.menuRunnerRunning = true;
				let menuTopCalc = this.menuTop;

				let mover = () => {
					let calcSize = menu.offsetHeight / 20;

					if (menuTopCalc > menu.offsetHeight) {
						menuTopCalc = menu.offsetHeight;
					}

					if (this.menuVisible) {
						if (menuTopCalc <= 0) {
							this.menuTop = 0;
							this.menuRunnerRunning = false;
							return;
						}
						menuTopCalc -= calcSize;
					} else {
						if (menuTopCalc >= menu.offsetHeight) {
							this.menuTop = 9999;
							this.menuRunnerRunning = false;
							return;
						}

						menuTopCalc += calcSize;
					}
					this.menuTop = Math.round(menuTopCalc);

					setTimeout(mover, 10);
				}
				mover();
			}
		}
	});
	vue.$mount('#vueapp');
	l.log('vue application startup finished.');
};