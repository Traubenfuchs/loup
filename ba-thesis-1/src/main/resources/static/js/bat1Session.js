function login() {
	const username = document.getElementById('username').value;
	const password = document.getElementById('password').value;
	const rememberMe = document.getElementById('rememberme').checked;
	
	Loup.Security.login(
		username,
		password,
		rememberMe,
		r => {
			document.location.reload(true);
		},
		r => {
			l.log('Login error: ' + r.responseText);
		}
	);
	return false;
}
function logout() {
	Loup.Security.logout(
		r => {
			l.log('logout success');
		},
		r => {
			l.log('logout error');
		},
		r => {
			document.location.reload(true);
		}
	);	
}
function rememberMeLogin() {
	Loup.Security.rememberMe(
		r => {
			l.log('Remember me login success!');
			document.location.reload(true); // true -> don't use cache
		},
		r => {
			l.log('Remember me login failed.');
		}
	);
}