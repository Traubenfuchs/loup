var Loup = Loup || {};

window.onload = function onload() {
	const uniqueIdentifier = Loup.Random.createNiceString(32);
	l.log(`starting up vue application. Vue.js version<${Vue.version}>, uniqueIdentifier<${uniqueIdentifier}>`);

	const aceBeautify = ace.require("ace/ext/beautify");

	// events:
	// editor-count-change: When an editor panel is mounted or destroyed. -> The other panels need to recalculate the ace editors height.
	// loggedIn: When the user explicitly logs in. -> schlurpr-home needs to reload the displayed projects.
	// editor-close-all: Before the schlurp-editor component is destroyed. -> All editor panels should be closed and ace editor cleaned up.
	//
	const eventHub = new Vue();

	let panelIdCounter = 0;

	const schlurpAppHome = {
		name: 'schlurpr-home',
		template: `
			<div>
				<div class='easyButton inline-block' v-on:click='newSchlurp()'>create new Schlurp</div>
				<label for='newSchlurpName'></label>
				<input id='newSchlurpName' name='newSchlurpName' v-model='newSchlurpName'>
				<div v-for='ownedProject in ownedProjects' class='home-projectbox'>
					<div>project id: {{ownedProject.id}}</div>
					<div>project name : {{ownedProject.name}}</div>
					<div class='home-versionline'>
						<div>versions: </div>
						<router-link style='color:black;' v-bind:to="'/w/' + ownedProject.id + '/' + version.version" v-for='version in ownedProject.versions'>{{version.version}}</router-link>
					</div>
				</div>
			</div>
		`,
		data() {
			return {
				newSchlurpName: '',
				ownedProjects: []
			};
		},
		methods: {
			newSchlurp() {
				l.log('newSchlurp() was called.');
				Loup.Ajax.post(
					'/api/project/', {
						name: this.newSchlurpName
					}, r => {
						l.log(r.responseText);
						this.$router.push(`/w/${r.responseObject.id}/0`);
					}
				);
			},
			loadProjects() {
				Loup.Ajax.get(
					'/api/getOwnedProjects', r => {
						this.ownedProjects = r.responseObject;
					}
				);
			},
		},
		mounted() {
			this.loadProjects();
			eventHub.$on('loggedIn', this.loadProjects);
		},
		beforeDestroy() {
			eventHub.$off('loggedIn', this.loadProjects);
		},
	};

	const schlurprEditorComponent = {
		name: 'schlurpr-editor',
		template: `
			<div id='schlurpr-files-panels-seperator'>
				<div id='schlurpr-files'>
					<div id='schlurpr-files-controls'>
						<div id='schlurpr-files-controls-addbutton' v-on:click='newFile()'>add</div>
						<input id='schlurpr-files-controls-newfilename' v-model='newFilename' placeholder='new filename' />
					</div>
					<div id='schlurpr-files-files'>
						<div v-for='file in this.$store.state.sortedFiles' class='schlurpr-files-files-line'>
							<div class='schlurpr-files-files-line-file' v-on:click='open(file.filename)'>{{file.filename}}</div>
							<div class='schlurpr-files-files-line-delete' v-if="file.fileType !== 'html'" v-on:click="deleteFile(file.filename)"> x </div>
						</div>
					</div>
				</div>
				<div id='schlurpr-panel-container'>
					<schlurpr-panel v-bind:filename='openedFile' v-for='openedFile in this.$store.state.openedFiles' v-bind:key='openedFile'/>
				</div>
			</div>
		`,
		data() {
			return {
				newFilename: '',
				store: store,
			};
		},
		methods: {
			newFile() {
				this.baby = !this.baby;
				if (
					!this.newFilename.endsWith('.css') &&
					!this.newFilename.endsWith('.js')) {
					console.log('You can only create files that end with .js or .html');
					return;
				}

				Loup.Ajax.post(
					`/api/project/${this.$router.currentRoute.params.projectId}/projectVersion/${this.$router.currentRoute.params.version}/files/${this.newFilename}/`,
					r => this.loadFiles()
				);
			},
			deleteFile(filename) {
				Loup.Ajax.delete(
					`/api/project/${this.$router.currentRoute.params.projectId}/projectVersion/${this.$router.currentRoute.params.version}/files/${filename}/`,
					r => this.loadFiles()
				);
			},
			loadFiles() {
				this.$store.commit('setPlayLink', `/api/project/${this.$router.currentRoute.params.projectId}/projectVersion/${this.$router.currentRoute.params.version}/file/index.html`);

				Loup.Ajax.get(
					`/api/project/${this.$router.currentRoute.params.projectId}/projectVersion/${this.$router.currentRoute.params.version}/`,
					r => {
						if (r.responseText.length === 0) {
							this.$router.push('/');
							return;
						}
						this.$store.commit('setFiles', r.responseObject.files);
						this.$store.commit('setProjectName', r.responseObject.project.name);
					}
				);
			},
			open(filename) {
				this.$store.commit('openFile', filename);
			}
		},
		created() {
			this.loadFiles();
		},
		watch: {
			"$route": function (to, from) {
				this.$store.commit('setPlayLink', `/api/project/${this.$router.currentRoute.params.projectId}/projectVersion/${this.$router.currentRoute.params.version}/file/index.html`);
				this.loadFiles()
			}
		},
		beforeDestroy() {
			eventHub.$emit('editor-close-all');
			this.$store.commit('setProjectName', undefined);
			this.$store.commit('setPlayLink', undefined);
		},
	};

	Vue.component('schlurpr-panel', {
		template: `
			<div class='schlurpr-panel' v-bind:id='id'>
				<div class='editor-header'>
					<div class='editor-header-button' v-on:click='save()' v-if='modified'>save</div>
					<div class='editor-header-button' v-on:click='reset()' v-if='modified'>reset</div>
					<div class='editor-header-button' v-on:click='close()' >close</div>
					<div class='editor-header-button' v-on:click='beautify()'>beautify</div>
					<p class='editor-header-text'>{{ filename }}</p>
				</div>
				<div v-bind:id="id + '_div'" class='schlurpr-panel-textbox'></div>
			</div>`,
		props: ['filename'],
		data() {
			return {
				id: 'schlurp-panel_' + panelIdCounter++, // there might be multiple panels and each panel needs an ace editor, so they get an id
				store: store,
				session: undefined, // ace editor session
				editor: undefined, // ace editor
				modified: false, // whether the editor content has been modified
			};
		},
		methods: {
			save() {
				Loup.Ajax.put(
					`/api/project/${this.$router.currentRoute.params.projectId}/projectVersion/${this.$router.currentRoute.params.version}/files/${this.filename}/`, {
						filename: this.filename,
						content: this.session.getValue()
					}, r => {
						this.modified = false;
						this.$router.push(`/w/${r.responseObject.projectId}/${r.responseObject.projectVersion}`);
					}
				);
			},
			close() {
				this.$store.commit('closeFile', this.filename);
			},
			beautify() {
				if (this.filename.endsWith('.html')) {
					const newValue = window.html_beautify(
						this.session.getValue(), {
							indent_with_tabs: true,
							wrap_line_length: 0
						});
					this.session.setValue(newValue);

				} else {
					aceBeautify.beautify(this.session);
				}
			},
			reset() {
				Loup.Ajax.get(
					`/api/project/${this.$router.currentRoute.params.projectId}/projectVersion/${this.$router.currentRoute.params.version}/viewFile/${this.filename}/`,
					r => {
						this.session.setValue(r.responseText);
						setTimeout(() => { this.modified = false; }, 50);
					}
				);
			},
			reactToEditorCountChange() {
				this.editor.resize();
			}
		},
		mounted() {
			const editor = ace.edit(this.$el.childNodes[2].id);
			editor.setTheme('ace/theme/tomorrow_night_eighties');
			const session = editor.getSession();

			if (this.filename.endsWith('.js')) {
				session.setMode("ace/mode/javascript");
			} else if (this.filename.endsWith('.css')) {
				session.setMode("ace/mode/css");
			} else if (this.filename.endsWith('.html')) {
				session.setMode("ace/mode/html");
			}

			editor.wrapBehavioursEnabled = true;
			session.setTabSize(4);
			//editor.setShowInvisibles(true);
			session.setUseSoftTabs(false);
			session.setUseWrapMode(true);
			session.setValue(this.$store.state.files[this.filename].content);

			this.session = session;
			this.editor = editor;

			editor.on('input', (a) => {
				let newValue = session.getValue();
				if (newValue !== this.value) {
					this.modified = true;
				}
			});
			eventHub.$on('editor-close-all', this.close);
			eventHub.$on('editor-save-all', this.save);
			eventHub.$on('editor-count-change', this.reactToEditorCountChange);

			eventHub.$emit('editor-count-change');
		},
		beforeDestroy() {
			eventHub.$off('editor-close-all', this.close);
			eventHub.$off('editor-count-change', this.reactToEditorCountChange);
			eventHub.$off('editor-save-all', this.save);
			this.editor.destroy();
		},
		destroyed() {
			eventHub.$emit('editor-count-change');
		},
	});

	const store = new Vuex.Store({
		state: {
			playLink: undefined, // link for the play button in top bar
			files: {}, // files that exist in the current projectVersion
			openedFiles: [], // files as filename that are opened
			sortedFiles: [], // sorted filenames for sidebar display
			projectName: '', // for project name display in top bar
			loggedIn: false, // only used in top bar of component schlurp-application, might be interesting later
			username: '', // only used in top bar of component schlurp-application, might be interesting later
			uniqueIdentifier: uniqueIdentifier
		},
		mutations: {
			setPlayLink(state, playLink) {
				state.playLink = playLink ? playLink : undefined;
			},
			setFiles(state, files) {
				state.files = files;
				state.sortedFiles = [];

				for (const entry in files) {
					state.sortedFiles.push(files[entry]);
				}

				state.sortedFiles.sort((l, r) => {
					if (l.filename < r.filename) {
						return -1;
					} else if (l.filename > r.filename) {
						return 1;
					}
					return 0;
				});
				const newOpenedFiles = [];
				for (const openedFile of state.openedFiles) {
					if (state.files[openedFile]) {
						newOpenedFiles.push(openedFile);
					}
				}
				state.openedFiles = newOpenedFiles;
			},
			openFile(state, filename) {
				l.log(`attempting to open <${filename}>`);
				if (state.files[filename] && state.openedFiles.indexOf(filename) === -1) {
					l.log(`opening <${filename}>`);
					state.openedFiles.push(filename);
				}
			},
			closeFile(state, filename) {
				l.log(`attempting to close<${filename}>`);
				const index = state.openedFiles.indexOf(filename);
				if (index !== -1) {
					l.log(`Closing<${filename}>`);
					state.openedFiles.splice(index, 1);
				}
			},
			setProjectName(state, projectName) {
				state.projectName = projectName;
			},
			setLogin(state, data) {
				state.loggedIn = data.loggedIn;
				state.username = data.username;
			},
		},
	});

	const router = new VueRouter({
		mode: 'history',
		routes: [{
			path: '/w/:projectId/:version',
			component: schlurprEditorComponent
		}, {
			path: '/',
			component: schlurpAppHome,
			exact: true
		}
		]
	});

	new Vue({
		name: 'schlurp-application',
		el: '#vueapp',
		template: `
			<div id='vueapp'>
				<div id='menu'>
					<router-link to='/' exact=''>home</router-link>
					<a id='playLink' v-if='store.state.playLink' v-bind:href='store.state.playLink' v-bind:target='$store.state.uniqueIdentifier'>play!</a>
					<div class='just-white-text' v-if='$store.state.projectName'>Project Name:</div>
					<div class='just-white-text' v-if='$store.state.projectName'>{{$store.state.projectName}}</div>
					<div class='just-white-text' v-if='$store.state.loggedIn'>{{$store.state.username}}</div>
					<a class='easyButton' v-if='!$store.state.loggedIn && !loginOpened' v-on:click='toggleLoginView()'>login</a>
					<a class='easyButton' v-if='!$store.state.loggedIn && !registerOpened' v-on:click='toggleRegisterView()'>register</a>
					<a id='logoutButton' v-on:click='logout()' class='easyButton' v-if='store.state.loggedIn'>logout</a>
				</div>
				<div v-if='loginOpened' class='login-line'>
					<div class='easyButton' v-on:click='toggleLoginView()'>x</div>
					<input id='login_text' name='login_text' placeholder='Username' type='text' v-model='loginUsername'/>
					<input id='login_password' name='login_password' placeholder='Password' type='password' v-model='loginPassword'/>
					<input id='login_rememberme' name='login_rememberme' type='checkbox' v-model='rememberMe' />
					<label for='login_rememberme' style='color:white'>remember me</label>
					<div class='easyButton' v-on:click='login()'>login</div>
				</div>
				<div v-if='registerOpened' class='login-line'>
					<div class='easyButton' v-on:click='toggleRegisterView()'>x</div>
					<input id='register_username' name='register_username' placeholder='Username' type='text' v-model='registerUsername'/>
					<input id='register_password' name='register_password' placeholder='Password' type='password' v-model='registerPassword'/>
					<div class='easyButton' v-on:click='register()'>register</div>
				</div>
				<router-view class='router-view' />
			</div>`,
		router: router,
		store: store,
		data: {
			store: store,
			loginOpened: false, // whether the login line is opened
			registerOpened: false, // whether the registration line is opened
			loginUsername: '', // username for login
			loginPassword: '', // password for login
			rememberMe: true, // remember me for login
			loginRegisterErrorMessage: '', // error message that will be display in the opened registration/login line
			registerUsername: '', // username for registration
			registerPassword: '', // password for registration
		},
		methods: {
			toggleLoginView() {
				// only login or registration view can be opened at once
				this.registerOpened = false;
				this.loginOpened = !this.loginOpened;
				this.loginRegisterErrorMessage = '';
			},
			toggleRegisterView() {
				// only login or registration view can be opened at once
				this.loginOpened = false;
				this.registerOpened = !this.registerOpened;
				this.loginRegisterErrorMessage = '';
			},
			register() {
				this.loginRegisterErrorMessage = '';
				Loup.Ajax.post(
					'/api/register', {
						username: this.registerUsername,
						password: this.registerPassword
					},
					r => {
						l.log('Registration success!');
						this.checkLogin();
					},
					r => {
						l.log('Registration error!');
						this.loginRegisterErrorMessage = "Something went wrong."; //TODO!
					}
				);
			},
			login() {
				loginRegisterErrorMessage = '';
				Loup.Security.login(
					this.loginUsername,
					this.loginPassword,
					this.rememberMe,
					r => {
						l.log('Successfully logged in.');
						this.loginOpened = false;
						this.loginRegisterErrorMessage = '';
						this.$store.commit('setLogin', { loggedIn: true, username: r.responseObject.userDTO.username });
						eventHub.$emit('loggedIn');
					},
					r => {
						l.log('Login error!');
						this.loginRegisterErrorMessage = 'Incorrect username/password, please try again!';
					}
				);
			},
			logout() {
				Loup.Security.logout(
					r => {
						Loup.Security.refreshCSRF(
							r => {
								this.$store.commit('setLogin', { loggedIn: false, username: '' });
								l.log('Logging out. Pushing<' + this.$route.path + '>');
								this.$router.push("/w/7/0");
							});
					});
			},
			checkLogin() {
				l.log('Checking login status.');
				Loup.Security.status(
					r => {
						if (r.responseObject.userDTO) {
							l.log('User is logged in.');
							this.$store.commit('setLogin', { loggedIn: true, username: r.responseObject.userDTO.username });
							this.registerOpened = false;
							this.loginOpened = false;
						} else {
							l.log('User is not logged in.');
							this.$store.commit('setLogin', { loggedIn: false, username: '' });
						}
					},
					r => this.$store.commit('setLogin', { loggedIn: false, username: '' })

				);
			},
		},
		created() {
			this.checkLogin();
		},
	});

	Loup.Vue.sync(store, router);
	l.log('vue application startup finished.');
};