package at.loup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "at.loup")
@EnableAutoConfiguration(exclude = { JmxAutoConfiguration.class, WebSocketAutoConfiguration.class,
		JacksonAutoConfiguration.class, GsonAutoConfiguration.class })
@EntityScan(basePackages = { "at.loup.schlurpr.db", "at.loup.commons.entities", "at.loup.security.entities" })
@EnableJpaRepositories(basePackages = { "at.loup.schlurpr.db.repositories", "at.loup.security.entities.repositories" })
public class SchlurprApplication extends SpringBootServletInitializer {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SchlurprApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SchlurprApplication.class);
	}
}