package at.loup.schlurpr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import at.loup.security.services.RememberMeComponent;
import at.loup.security.services.UserHolder;

@Controller
public class GuiController {
	@Autowired
	private UserHolder userHolder;
	@Autowired
	private RememberMeComponent rememberMeService;

	public GuiController() {

	}

	@RequestMapping(path = { "/*", "/*/*", "/*/*/*" }, method = RequestMethod.GET)
	public ModelAndView app() {
		ModelAndView result = new ModelAndView("schlurprApplication");
		return result;
	}
}