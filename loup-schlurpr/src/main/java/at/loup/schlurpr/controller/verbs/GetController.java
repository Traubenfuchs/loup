package at.loup.schlurpr.controller.verbs;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.schlurpr.data.OwnedProjectResponse;
import at.loup.schlurpr.data.db.ProjectDTO;
import at.loup.schlurpr.data.db.ProjectVersionDTO;
import at.loup.schlurpr.db.FileEntity;
import at.loup.schlurpr.db.ProjectEntity;
import at.loup.schlurpr.db.ProjectVersionEntity;
import at.loup.schlurpr.db.repositories.ProjectEntityRepository;
import at.loup.schlurpr.db.repositories.ProjectVersionEntityRepository;
import at.loup.schlurpr.db.services.AllowedProjectsService;

@Controller
public class GetController {
	@Autowired
	private ProjectEntityRepository projectEntityRepository;
	@Autowired
	private ProjectVersionEntityRepository projectVersionEntityRepository;
	@Autowired
	private AllowedProjectsService allowedProjectsService;

	public GetController() {

	}

	@RequestMapping(path = "/api/project/{projectId}", method = RequestMethod.GET)
	public ProjectDTO projectGET(@PathVariable long projectId) {
		ProjectEntity projectEntity = projectEntityRepository.findOne(projectId);

		ProjectDTO projectDTO = ProjectDTO.createFromEntity(projectEntity);

		return projectDTO;
	}

	@RequestMapping(path = "/api/project/{projectId}/projectVersion/{projectVersion}", method = RequestMethod.GET)
	public @ResponseBody ProjectVersionDTO projectVersionGET(@PathVariable long projectId, @PathVariable int projectVersion) {
		ProjectEntity projectEntity = projectEntityRepository.findOne(projectId);

		if (projectEntity == null) {
			return null;
		}

		List<ProjectVersionEntity> projectVersions = projectEntity.getVersions();
		if (projectVersion >= projectVersions.size()) {
			return null;
		}

		ProjectVersionEntity projectVersionEntity = projectVersions.get(projectVersion);

		ProjectVersionDTO result = ProjectVersionDTO.createFromEntity(projectVersionEntity);

		return result;
	}

	@RequestMapping(path = "/api/project/{projectId}/projectVersion/{projectVersion}/viewFile/{filename:.+}", method = RequestMethod.GET)
	public @ResponseBody String viewFile(@PathVariable long projectId, @PathVariable int projectVersion, @PathVariable String filename) {
		ProjectVersionEntity projectVersionEntity = projectVersionEntityRepository.findByProjectIdAndVersion(projectId, projectVersion);
		FileEntity file = projectVersionEntity.getFiles().get(filename);
		String result = file.getContent();
		return result;
	}

	@RequestMapping(path = "/api/project/{projectId}/projectVersion/{projectVersion}/view", method = RequestMethod.GET)
	public @ResponseBody String view(@PathVariable long projectId, @PathVariable int projectVersion) {
		ProjectVersionEntity projectVersionEntity = projectVersionEntityRepository.findByProjectIdAndVersion(projectId, projectVersion);
		FileEntity file = projectVersionEntity.getFiles().get("index.html");
		String result = file.getContent();
		return result;
	}

	@RequestMapping(path = "/api/project/{projectId}/projectVersion/{projectVersion}/file/{filename:.+}", method = RequestMethod.GET)
	public @ResponseBody String viewV2(@PathVariable long projectId, @PathVariable int projectVersion, @PathVariable String filename) {
		ProjectVersionEntity projectVersionEntity = projectVersionEntityRepository.findByProjectIdAndVersion(projectId, projectVersion);
		FileEntity file = projectVersionEntity.getFiles().get(filename);
		String result = file.getContent();
		return result;
	}

	@RequestMapping(path = "/api/getOwnedProjects", method = RequestMethod.GET)
	public @ResponseBody List<OwnedProjectResponse> getOwnedProjects() {
		List<ProjectEntity> ownedProjectIds = allowedProjectsService.getOwnedProjectEntitiesSorted();

		List<OwnedProjectResponse> result = ownedProjectIds.stream().map(OwnedProjectResponse::createFromEntity).collect(Collectors.toList());

		return result;
	}
}
