package at.loup.schlurpr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.commons.utilities.ArgumentRuleUtilities;
import at.loup.schlurpr.data.SchlurpRegistrationRequest;
import at.loup.security.data.UserDTO;
import at.loup.security.data.reqres.UserRegistrationResult;
import at.loup.security.data.reqres.UserRegistrationResult.EUserRegistrationResult;
import at.loup.security.services.SessionEventComponent;
import at.loup.security.services.coreservice.LoupSecurityCoreService;
import at.loup.security.services.securitymanagers.WebSecurityManager;

@Controller
public class RegisterController {
	@Autowired
	private LoupSecurityCoreService coreService;
	@Autowired
	private WebSecurityManager webSecurityManager;
	@Autowired
	private SessionEventComponent sessionEventService;

	public RegisterController() {

	}

	@RequestMapping(path = "/api/register", method = RequestMethod.POST)
	public @ResponseBody UserRegistrationResult register(@RequestBody SchlurpRegistrationRequest registrationRequest) {
		ArgumentRuleUtilities.notNullEmptyWhitespace("username", registrationRequest.getUsername());
		ArgumentRuleUtilities.notNullEmpty("password", registrationRequest.getPassword());

		UserDTO userDTO = new UserDTO();
		userDTO.setUsername(registrationRequest.getUsername());
		userDTO.setPassword(registrationRequest.getPassword());
		UserRegistrationResult result = coreService.registerUser(userDTO, false);

		if (result.geteUserRegistrationResult() == EUserRegistrationResult.ok) {
			webSecurityManager.login(registrationRequest.getUsername(), registrationRequest.getPassword(), false);
			sessionEventService.invokeLoginEvents();
		}

		return result;
	}
}
