package at.loup.schlurpr.controller.verbs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.schlurpr.data.ChangeResponse;
import at.loup.schlurpr.data.db.FileDTO;
import at.loup.schlurpr.data.db.ProjectDTO;
import at.loup.schlurpr.db.ProjectEntity;
import at.loup.schlurpr.db.repositories.ProjectEntityRepository;
import at.loup.schlurpr.db.repositories.ProjectVersionEntityRepository;
import at.loup.schlurpr.db.services.ChangeService;
import at.loup.schlurpr.db.services.SchlurprSession;
import at.loup.security.services.DBUserHolder;

@Controller
public class PostController {

	@Autowired
	private ProjectEntityRepository projectEntityRepository;

	@Autowired
	private ProjectVersionEntityRepository projectVersionEntityRepository;

	@Autowired
	private ChangeService changeService;

	@Autowired
	private SchlurprSession schlurprSession;

	@Autowired
	private DBUserHolder dbUserHolder;

	@Transactional
	@RequestMapping(path = "/api/project/", method = RequestMethod.POST)
	public @ResponseBody ProjectDTO projectPOST(@RequestBody(required = false) ProjectDTO projectDTO) {
		ProjectEntity newProjectEntity = projectEntityRepository.save(ProjectEntity.create());

		newProjectEntity.setUserEntity(dbUserHolder.getUserEntity());

		if (projectDTO != null) {
			newProjectEntity.setName(projectDTO.getName());
		}

		ProjectDTO result = ProjectDTO.createFromEntity(newProjectEntity);
		schlurprSession.addProject(newProjectEntity.getId());
		return result;
	}

	@Transactional
	@RequestMapping(path = "/api/project/{projectId}/projectVersion/{projectVersion}/files/{filename}", method = RequestMethod.POST)
	public @ResponseBody ChangeResponse createFile(@RequestBody(required = false) FileDTO fileDTO, @PathVariable long projectId, @PathVariable int projectVersion, @PathVariable String filename) {
		ChangeResponse result = changeService.updateOrCreateFile(projectId, projectVersion, filename, fileDTO.getContent());

		// TODO REMOVE
		// ProjectVersionEntity projectVersionEntity =
		// projectVersionEntityRepository.findByProjectIdAndVersion(projectId,
		// projectVersion);
		// if (projectVersionEntity == null) {
		// throw new LoupException(HttpStatus.NOT_FOUND, "Does not exist.");
		// }
		// if (projectVersionEntity.getFiles().containsKey(filename)) {
		// throw new LoupException(HttpStatus.BAD_REQUEST, "Duplicate
		// filename");
		// }
		// FileEntity newFileEntity = FileEntity.create(filename, null,
		// projectVersionEntity, "");
		// if (fileDTO != null) {
		// if (fileDTO.getContent() != null) {
		// newFileEntity.setContent(fileDTO.getContent());
		// }
		//
		// }
		//
		// projectVersionEntity.getFiles().put(filename, newFileEntity);
		return result;
	}
}
