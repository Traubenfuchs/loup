package at.loup.schlurpr.controller.verbs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.schlurpr.data.ChangeResponse;
import at.loup.schlurpr.data.db.FileDTO;
import at.loup.schlurpr.db.repositories.ProjectVersionEntityRepository;
import at.loup.schlurpr.db.services.ChangeService;

@Controller
public class PutController {

	@Autowired
	private ProjectVersionEntityRepository projectVersionEntityRepository;

	@Autowired
	private ChangeService changeService;

	public PutController() {

	}

	@Transactional
	@RequestMapping(path = "/api/project/{projectId}/projectVersion/{projectVersion}/files/{filename}/", method = RequestMethod.PUT)
	public @ResponseBody ChangeResponse updateFile(@RequestBody FileDTO fileDTO, @PathVariable long projectId, @PathVariable int projectVersion, @PathVariable String filename) {
		ChangeResponse result = changeService.updateOrCreateFile(projectId, projectVersion, filename, fileDTO.getContent());
		return result;
		// TODO REMOVE
		// ProjectVersionEntity projectVersionEntity =
		// projectVersionEntityRepository.findByProjectIdAndVersion(projectId,
		// projectVersion);
		// FileEntity fileEntity =
		// projectVersionEntity.getFiles().get(filename); // direct
		// // access
		//
		//
		// fileEntity.setContent(fileDTO.getContent());
		// if (fileDTO.getFilename() != null && fileDTO.getFilename().length()
		// != 0) {
		// fileEntity.setFilename(fileDTO.getFilename());
		// }
		//
		// return;
	}
}
