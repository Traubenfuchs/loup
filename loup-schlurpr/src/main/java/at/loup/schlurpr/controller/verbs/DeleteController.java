package at.loup.schlurpr.controller.verbs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.schlurpr.data.ChangeResponse;
import at.loup.schlurpr.db.services.ChangeService;

@Controller
public class DeleteController {

	@Autowired
	private ChangeService changeService;

	public DeleteController() {

	}

	@Transactional
	@RequestMapping(path = "/api/project/{projectId}/projectVersion/{projectVersion}/files/{filename}", method = RequestMethod.DELETE)
	public @ResponseBody ChangeResponse fileDelete(@PathVariable long projectId, @PathVariable int projectVersion, @PathVariable String filename) {

		ChangeResponse result = changeService.deleteFile(projectId, projectVersion, filename);
		return result;

	}
}
