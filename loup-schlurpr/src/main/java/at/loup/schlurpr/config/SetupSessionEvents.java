package at.loup.schlurpr.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import at.loup.commons.services.TransactionForcer;
import at.loup.schlurpr.db.ProjectEntity;
import at.loup.schlurpr.db.repositories.ProjectEntityRepository;
import at.loup.schlurpr.db.services.SchlurprSession;
import at.loup.security.entities.UserEntity;
import at.loup.security.services.DBUserHolder;
import at.loup.security.services.SessionEventComponent;

@Component
public class SetupSessionEvents {
	@Autowired
	private SessionEventComponent sessionEventService;
	@Autowired
	private ProjectEntityRepository projectEntityRepository;
	@Autowired
	private SchlurprSession schlurprSession;
	@Autowired
	private DBUserHolder dbUserHolder;
	@Autowired
	private TransactionForcer transactionForcer;

	public SetupSessionEvents() {

	}

	@EventListener({ ContextRefreshedEvent.class })
	public void setupSessionEvents() {
		sessionEventService.setLoginEvent("schlurpLoginProjectTakeover", () -> {
			Long[] sessionBoundProjects = schlurprSession.getProjectIds();
			if (sessionBoundProjects == null || sessionBoundProjects.length == 0) {
				return;
			}
			transactionForcer.executeInTransaction(() -> {
				List<ProjectEntity> projectEntities = projectEntityRepository.findByIdIn(new HashSet<>(Arrays.asList(sessionBoundProjects)));
				UserEntity userEntity = dbUserHolder.getUserEntity();
				for (ProjectEntity projectEntity : projectEntities) {
					projectEntity.setUserEntity(userEntity);
				}
			});
		});
	}
}