package at.loup.schlurpr.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

import at.loup.commons.config.LoupCommonsConfig;
import at.loup.commons.js.ClosureSource;
import at.loup.commons.js.FixedClosureSource;

@Configuration
public class SchlurprConfig {
	@Autowired
	private Environment env;

	public SchlurprConfig() {
	}

	@Bean
	@Primary
	public ClosureSource schlurprClosureSource(LoupCommonsConfig loupCommonsConfig) {
		FixedClosureSource result;

		if (env.acceptsProfiles("prod")) {
			result = FixedClosureSource.create(loupCommonsConfig)
				.addSource("schlurpApplication.js",
					"logging.js",
					"ajax.js",
					"loup-security.js",
					"random.js",
					"templator.js",
					"libraries/vue.min.js",
					"libraries/vue-router.min.js",
					"libraries/vuex.min.js",
					"libraries/vuex-router-sync.js",
					"libraries/beautify-html.min.js",
					"schlurprApplication.js");
		} else {
			result = FixedClosureSource.create(loupCommonsConfig)
				.addSource("schlurpApplication.js",
					"logging.js",
					"ajax.js",
					"loup-security.js",
					"random.js",
					"templator.js",
					"libraries/vue.js",
					"libraries/vue-router.js",
					"libraries/vuex.js",
					"libraries/vuex-router-sync.js",
					"libraries/beautify-html.min.js",
					"schlurprApplication.js");
		}

		return result;
	};

}
