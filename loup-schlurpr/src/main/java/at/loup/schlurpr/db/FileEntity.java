package at.loup.schlurpr.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.loup.commons.entities.AbstractTimedEntity;
import at.loup.schlurpr.data.EFileType;

@Entity
@Table(name = "files")
public class FileEntity extends AbstractTimedEntity<ProjectEntity> {
	private static final long serialVersionUID = -1909754342289133318L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "projectVersion", insertable = true, updatable = false, nullable = false)
	private ProjectVersionEntity projectVersion;

	@Column(nullable = false, length = 1024 * 1000)
	@Lob
	private String content;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private EFileType fileType;

	@Column()
	private String filename;

	public FileEntity() {

	}

	public ProjectVersionEntity getProjectVersion() {
		return projectVersion;
	}

	public void setProjectVersion(ProjectVersionEntity projectVersion) {
		this.projectVersion = projectVersion;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		if (content == null) {
			this.content = "";
		}
		this.content = content;
	}

	public EFileType getFileType() {
		return fileType;
	}

	public void setFileType(EFileType fileType) {
		this.fileType = fileType;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public FileEntity clone() {
		FileEntity clonedFile = new FileEntity();
		clonedFile.setContent(getContent());
		clonedFile.setFilename(getFilename());
		clonedFile.setFileType(getFileType());
		clonedFile.setProjectVersion(getProjectVersion());
		return clonedFile;
	}

	public static FileEntity create(String filename, EFileType fileType, ProjectVersionEntity projectVersionEntity, String content) {
		FileEntity result = new FileEntity();

		result.setContent(content);
		result.setFilename(filename);
		if (fileType == null) {
			fileType = EFileType.findByFilename(filename);
		}
		result.setFileType(fileType);
		result.setProjectVersion(projectVersionEntity);

		return result;
	}
}
