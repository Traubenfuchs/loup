package at.loup.schlurpr.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.loup.schlurpr.db.FileEntity;

@Repository
public interface FileEntityRepository extends JpaRepository<FileEntity, Long> {

}
