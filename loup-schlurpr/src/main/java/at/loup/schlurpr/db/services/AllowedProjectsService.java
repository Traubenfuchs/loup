package at.loup.schlurpr.db.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.loup.schlurpr.db.ProjectEntity;
import at.loup.schlurpr.db.repositories.ProjectEntityRepository;
import at.loup.security.services.UserHolder;

@Service
public class AllowedProjectsService {
	@Autowired
	private SchlurprSession schlurprSession;
	@Autowired
	private ProjectEntityRepository projectEntityRepository;
	@Autowired
	private UserHolder userHolder;

	public AllowedProjectsService() {

	}

	// public List<ProjectEntity> getOwnedProjectEntities() {
	// Set<Long> ownedProjects = getOwnedProjects();
	// if (ownedProjects.size() == 0) {
	// return new ArrayList<>();
	// }
	// List<ProjectEntity> result = projectEntityRepository.findByIdIn(ownedProjects);
	//
	// for(ProjectEntity projectEntity : ownedProjects) {
	//
	// }
	//
	// return result;
	// }

	public Set<Long> getOwnedProjectIds() {
		Long[] sessionProjectIds = schlurprSession.getProjectIds();

		Long userId = userHolder.getUserId();
		if (userId == null) {
			HashSet<Long> result = new HashSet<>(Arrays.asList(sessionProjectIds));
			return result;
		}

		List<Long> userProjects = projectEntityRepository.findByUserId(userId);

		Set<Long> result = new HashSet<>();
		for (Long userProject : userProjects) {
			result.add(userProject);
		}

		for (Long sessionProjectId : sessionProjectIds) {
			result.add(sessionProjectId);
		}

		return result;
	}

	public ArrayList<Long> getOwnedProjectIdsSorted() {
		ArrayList<Long> result = new ArrayList<>(getOwnedProjectIds());
		Collections.sort(result);
		return result;
	}

	public List<ProjectEntity> getOwnedProjectEntitiesSorted() {
		Set<Long> ownedProjectIds = getOwnedProjectIds();
		List<ProjectEntity> result = projectEntityRepository.findByIdIn(ownedProjectIds);

		result.sort((l, r) -> l.compareTo(r));

		return result;
	}

	public boolean currentUserOwnsProject(long projectId) {
		boolean result = getOwnedProjectIds().contains(projectId);
		return result;
	}
}
