package at.loup.schlurpr.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.loup.commons.entities.AbstractTimedEntity;
import at.loup.security.entities.UserEntity;

@Entity
@Table(name = "projects")
public class ProjectEntity extends AbstractTimedEntity<ProjectEntity> {
	private static final long serialVersionUID = -7666907776329364957L;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
	private List<ProjectVersionEntity> versions;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private UserEntity userEntity;
	@Column(name = "projectId")
	private Long userId;

	@Column()
	private String name;

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public ProjectEntity() {

	}

	public List<ProjectVersionEntity> getVersions() {
		if (versions == null) {
			versions = new ArrayList<>();
		}
		return versions;
	}

	public void setVersions(List<ProjectVersionEntity> versions) {
		this.versions = versions;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
		if (userEntity != null) {
			this.userId = userEntity.getId();
		}
	}

	public Long getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProjectEntity cloneShallow() {
		ProjectEntity result = new ProjectEntity();

		result.setName(getName());
		result.setUserEntity(userEntity);
		result.setVersions(getVersions());

		return result;
	}

	public static ProjectEntity create() {
		ProjectEntity result = new ProjectEntity();

		ProjectVersionEntity firstProjectVersion = ProjectVersionEntity.createNew(result);
		firstProjectVersion.setVersion(0);
		result.getVersions().add(firstProjectVersion);

		return result;
	}
}
