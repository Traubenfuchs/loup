package at.loup.schlurpr.db.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import at.loup.schlurpr.db.ProjectEntity;

@Repository
public interface ProjectEntityRepository extends JpaRepository<ProjectEntity, Long> {

	@Query("select pe.id from ProjectEntity pe where pe.userId = :userId")
	List<Long> findByUserId(long userId);

	List<ProjectEntity> findByIdIn(Set<Long> ids);

	@Query("select max(pe.version) from ProjectVersionEntity pe where pe.projectId = :projectId")
	int getHighestVersionOfProject(long projectId);
}
