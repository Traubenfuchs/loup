package at.loup.schlurpr.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.loup.schlurpr.db.ProjectVersionEntity;

@Repository
public interface ProjectVersionEntityRepository extends JpaRepository<ProjectVersionEntity, Long> {
	ProjectVersionEntity findByProjectIdAndVersion(long projectId, int version);
}
