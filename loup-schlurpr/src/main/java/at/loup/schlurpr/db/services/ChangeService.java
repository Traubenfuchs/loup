package at.loup.schlurpr.db.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import at.loup.schlurpr.data.ChangeResponse;
import at.loup.schlurpr.data.EFileType;
import at.loup.schlurpr.db.FileEntity;
import at.loup.schlurpr.db.ProjectEntity;
import at.loup.schlurpr.db.ProjectVersionEntity;
import at.loup.schlurpr.db.repositories.ProjectEntityRepository;
import at.loup.schlurpr.db.repositories.ProjectVersionEntityRepository;
import at.loup.security.entities.UserEntity;
import at.loup.security.services.DBUserHolder;

@Service
public class ChangeService {
	@Autowired
	private ProjectEntityRepository projectEntityRepository;
	@Autowired
	private ProjectVersionEntityRepository projectVersionEntityRepository;
	@Autowired
	private AllowedProjectsService allowedProjectsService;
	@Autowired
	private SchlurprSession schlurprSession;
	@Autowired
	private DBUserHolder dbUserHolder;

	public ChangeService() {

	}

	@Transactional
	public ChangeResponse deleteFile(long projectId, int projectVersion, String filename) {
		boolean currentUserOwnsProject = allowedProjectsService.currentUserOwnsProject(projectId);
		UserEntity userEntity = dbUserHolder.getUserEntity();

		ProjectEntity originalProject = projectEntityRepository.findOne(projectId);
		if (originalProject == null) {
			throw new RuntimeException(); // TODO find a better solution
		}

		// 1. clone version
		ProjectVersionEntity originalProjectVersionEntity = projectVersionEntityRepository.findByProjectIdAndVersion(projectId, projectVersion);
		if (originalProjectVersionEntity == null) {
			// project with projectId does not exist
			throw new RuntimeException(); // TODO find a better solution
		}
		if (!originalProjectVersionEntity.getFiles().containsKey(filename)) {
			// file with filename does not exist and can't be deleted
			throw new RuntimeException(); // TODO find a better solution
		}
		ProjectVersionEntity clonedProjectVersionEntity = originalProjectVersionEntity.clone();

		// 2. modify cloned version
		clonedProjectVersionEntity.getFiles().remove(filename);

		if (currentUserOwnsProject) {
			// 3a. set project versiont to the next one
			clonedProjectVersionEntity.setVersion(projectEntityRepository.getHighestVersionOfProject(projectId) + 1);

			// 4a. attach modified cloned version to original project
			originalProject.getVersions().add(clonedProjectVersionEntity);

			ChangeResponse result = ChangeResponse.create(projectId, clonedProjectVersionEntity.getVersion());
			schlurprSession.addProject(result.getProjectId());
			return result;
		} else {
			// 3b. create new project
			ProjectEntity newProject = new ProjectEntity();
			newProject.setName(originalProject.getName());
			newProject.setUserEntity(userEntity);

			// 4b. attach cloned projectVersion to new project and set the
			// projectVersion version to 0
			clonedProjectVersionEntity.setVersion(0);
			newProject.getVersions().add(clonedProjectVersionEntity);

			// 5b. persist new project
			newProject = projectEntityRepository.save(newProject);

			ChangeResponse result = ChangeResponse.create(newProject.getId(), clonedProjectVersionEntity.getVersion());
			schlurprSession.addProject(result.getProjectId());
			return result;
		}
	}

	@Transactional
	public ChangeResponse updateOrCreateFile(long projectId, int projectVersion, String filename, String content) {
		if (content == null) {
			content = "";
		}

		boolean currentUserOwnsProject = allowedProjectsService.currentUserOwnsProject(projectId);
		UserEntity userEntity = dbUserHolder.getUserEntity();

		ProjectEntity originalProject = projectEntityRepository.findOne(projectId);
		if (originalProject == null) {
			// project with projectId does not exist
			throw new RuntimeException(); // TODO find a better solution
		}

		// 1. clone version
		ProjectVersionEntity originalProjectVersionEntity = projectVersionEntityRepository.findByProjectIdAndVersion(projectId, projectVersion);
		if (originalProjectVersionEntity == null) {
			throw new RuntimeException(); // TODO find a better solution
		}
		if (!originalProjectVersionEntity.getFiles().containsKey(filename)) {
			throw new RuntimeException(); // TODO find a better solution
		}
		ProjectVersionEntity clonedProjectVersionEntity = originalProjectVersionEntity.clone();

		// 2. create new file and save it in cloned version
		FileEntity newFile = new FileEntity(); // TODO replace with constructor
												// method
		newFile.setFilename(filename);
		newFile.setFileType(EFileType.findByFilename(filename));
		newFile.setProjectVersion(clonedProjectVersionEntity);
		newFile.setContent(content);
		clonedProjectVersionEntity.getFiles().put(filename, newFile);

		if (currentUserOwnsProject) {
			// 3a. set project versiont to the next one
			clonedProjectVersionEntity.setVersion(projectEntityRepository.getHighestVersionOfProject(projectId) + 1);

			// 4a. attach modified cloned version to original project
			originalProject.getVersions().add(clonedProjectVersionEntity);

			ChangeResponse result = ChangeResponse.create(projectId, clonedProjectVersionEntity.getVersion());
			schlurprSession.addProject(result.getProjectId());
			return result;
		} else {
			// 3b. create new project
			ProjectEntity newProject = new ProjectEntity();
			newProject.setName(originalProject.getName());
			newProject.setUserEntity(userEntity);

			// 4b. attach cloned projectVersion to new project and set the
			// projectVersion version to 0
			clonedProjectVersionEntity.setVersion(0);
			newProject.getVersions().add(clonedProjectVersionEntity);

			// 5b. persist new project
			newProject = projectEntityRepository.save(newProject);

			ChangeResponse result = ChangeResponse.create(newProject.getId(), clonedProjectVersionEntity.getVersion());
			schlurprSession.addProject(result.getProjectId());
			return result;
		}
	}
}
