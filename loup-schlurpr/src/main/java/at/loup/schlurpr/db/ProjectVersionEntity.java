package at.loup.schlurpr.db;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.loup.commons.entities.AbstractTimedEntity;
import at.loup.schlurpr.data.EFileType;

@Entity
@Table(name = "projectVersions")
public class ProjectVersionEntity extends AbstractTimedEntity<ProjectVersionEntity> {
	private static final long serialVersionUID = -8365500627948585820L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "projectId", updatable = false, nullable = false)
	private ProjectEntity project;
	@Column(name = "projectId", insertable = false, updatable = false)
	private Long projectId;

	@OneToMany(mappedBy = "projectVersion", cascade = CascadeType.ALL, orphanRemoval = true)
	@MapKey(name = "filename")
	private Map<String, FileEntity> files;

	@Column(nullable = false)
	private Integer version;

	public ProjectVersionEntity() {

	}

	public ProjectEntity getProject() {

		return project;
	}

	public void setProject(ProjectEntity project) {
		this.project = project;
		this.projectId = project.getId();
	}

	public Map<String, FileEntity> getFiles() {
		if (files == null) {
			files = new HashMap<>();
		}
		return files;
	}

	public void setFiles(Map<String, FileEntity> files) {
		this.files = files;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getProjectId() {
		return projectId;
	}

	@Override
	public ProjectVersionEntity clone() {
		ProjectVersionEntity result = new ProjectVersionEntity();

		result.setVersion(getVersion());
		result.setProject(getProject());

		Map<String, FileEntity> clonedFiles = new HashMap<>();
		for (FileEntity originalFile : getFiles().values()) {
			FileEntity clonedFile = originalFile.clone();
			clonedFile.setProjectVersion(result);
			clonedFiles.put(clonedFile.getFilename(), clonedFile);
		}

		result.setFiles(clonedFiles);

		return result;
	}

	public static ProjectVersionEntity createNew(ProjectEntity projectEntity) {
		ProjectVersionEntity result = new ProjectVersionEntity();

		result.setProject(projectEntity);

		result.getFiles().put("index.html", FileEntity.create("index.html", EFileType.html, result, "" +
				"<!DOCTYPE html>\n" +
				"<html>\n" +
				"	<head>\n" +
				"		<title>Schlurpr Template</title>\n" +
				"		<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no'>\n" +
				"		<script src='./js.js'></script>\n" +
				"		<link rel='stylesheet' href='./css.css' media='screen' type='text/css'>\n" +
				"	</head>\n" +
				"	<body onload='startup()'>\n" +
				"		<p>Hello Html!</p>\n" +
				"	</body>\n" +
				"</html>"));

		result.getFiles().put("css.css", FileEntity.create("css.css", EFileType.css, result, ""));

		result.getFiles().put("js.js", FileEntity.create("js.js", EFileType.js, result, "" +
				"function startup() {\n" +
				"	console.log('Hello JavaScript!');\n" +
				"}"));

		return result;
	}

}
