package at.loup.schlurpr.db.services;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class SchlurprSession {
	private Set<Long> projects = ConcurrentHashMap.newKeySet();

	public SchlurprSession() {

	}

	public void addProject(long id) {
		projects.add(id);
	}

	public Long[] getProjectIds() {
		Long[] result = projects.toArray(new Long[0]);
		return result;
	}
}
