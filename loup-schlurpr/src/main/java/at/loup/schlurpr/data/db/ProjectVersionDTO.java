package at.loup.schlurpr.data.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import at.loup.schlurpr.db.FileEntity;
import at.loup.schlurpr.db.ProjectVersionEntity;

public class ProjectVersionDTO {
	private Long projectId;
	private Integer version;
	private Map<String, FileDTO> files;
	private ProjectDTO project;

	public ProjectVersionDTO() {

	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Map<String, FileDTO> getFiles() {
		if (files == null) {
			files = new HashMap<>();
		}
		return files;
	}

	public void setFiles(Map<String, FileDTO> files) {
		this.files = files;
	}

	public ProjectDTO getProject() {
		return project;
	}

	public void setProject(ProjectDTO project) {
		this.project = project;
	}

	public static ProjectVersionDTO createFromEntity(ProjectVersionEntity projectVersionEntity) {
		ProjectVersionDTO result = new ProjectVersionDTO();

		result.setProjectId(projectVersionEntity.getProject().getId());
		result.setVersion(projectVersionEntity.getVersion());

		Map<String, FileDTO> files = new HashMap<>();
		for (Entry<String, FileEntity> file : projectVersionEntity.getFiles().entrySet()) {
			FileDTO fileDTO = FileDTO.createFromEntity(file.getValue());
			files.put(file.getKey(), fileDTO);
		}

		ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setId(projectVersionEntity.getProject().getId());
		projectDTO.setName(projectVersionEntity.getProject().getName());
		// project does not contain files since that would create a circular DTO
		result.setProject(projectDTO);

		result.setFiles(files);

		return result;
	}
}
