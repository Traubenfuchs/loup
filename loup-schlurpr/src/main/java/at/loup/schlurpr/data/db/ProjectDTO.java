package at.loup.schlurpr.data.db;

import java.util.ArrayList;
import java.util.List;

import at.loup.schlurpr.db.ProjectEntity;
import at.loup.schlurpr.db.ProjectVersionEntity;

public class ProjectDTO {
	private Long id;
	private List<ProjectVersionDTO> versions;
	private String name;

	public ProjectDTO() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ProjectVersionDTO> getVersions() {
		return versions;
	}

	public void setVersions(List<ProjectVersionDTO> versions) {
		this.versions = versions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static ProjectDTO createFromEntity(ProjectEntity projectEntity) {
		ProjectDTO result = new ProjectDTO();

		result.setId(projectEntity.getId());
		result.setName(projectEntity.getName());

		List<ProjectVersionDTO> versions = new ArrayList<>();
		for (ProjectVersionEntity projectVersionEntity : projectEntity.getVersions()) {
			ProjectVersionDTO projectVersionDTO = ProjectVersionDTO.createFromEntity(projectVersionEntity);
			versions.add(projectVersionDTO);
		}
		result.setVersions(versions);

		return result;
	}
}
