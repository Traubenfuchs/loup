package at.loup.schlurpr.data;

import java.util.ArrayList;
import java.util.List;

import at.loup.schlurpr.db.ProjectEntity;
import at.loup.schlurpr.db.ProjectVersionEntity;

public class OwnedProjectResponse {
	private Long id;
	private String name;
	private List<OwnedProjectVersion> versions;

	public OwnedProjectResponse() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<OwnedProjectVersion> getVersions() {
		if (versions == null) {
			versions = new ArrayList<>();
		}
		return versions;
	}

	public void setVersions(List<OwnedProjectVersion> versions) {
		this.versions = versions;
	}

	public static OwnedProjectResponse createFromEntity(ProjectEntity projectEntity) {
		OwnedProjectResponse result = new OwnedProjectResponse();

		result.setId(projectEntity.getId());
		result.setName(projectEntity.getName());

		for (ProjectVersionEntity projectVersionEntity : projectEntity.getVersions()) {
			OwnedProjectVersion ownedProjectVersion = OwnedProjectVersion.createFromEntity(projectVersionEntity);
			result.getVersions().add(ownedProjectVersion);
		}

		return result;
	}

	static class OwnedProjectVersion {
		private Integer version;

		public OwnedProjectVersion() {

		}

		public Integer getVersion() {
			return version;
		}

		public void setVersion(Integer version) {
			this.version = version;
		}

		public static OwnedProjectVersion createFromEntity(ProjectVersionEntity projectVersionEntity) {
			OwnedProjectVersion result = new OwnedProjectVersion();

			result.setVersion(projectVersionEntity.getVersion());

			return result;
		}
	}
}
