package at.loup.schlurpr.data;

public enum EFileType {
	css(".css"),
	js(".js"),
	html(".html"),
	;
	private final String extension;

	private EFileType(String extension) {
		this.extension = extension;
	}

	public String getExtension() {
		return extension;
	}

	public static EFileType findByFilename(String filename) {
		if (filename == null) {
			return null;
		}
		for (EFileType fileType : EFileType.values()) {
			if (filename.endsWith(fileType.getExtension())) {
				return fileType;
			}
		}
		return null;
	}
}
