package at.loup.schlurpr.data;

public class ChangeResponse {
	private Long projectId;
	private Integer projectVersion;

	public ChangeResponse() {

	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectVersion() {
		return projectVersion;
	}

	public void setProjectVersion(Integer projectVersion) {
		this.projectVersion = projectVersion;
	}

	public static ChangeResponse create(long projectId, int projectVersion) {
		ChangeResponse result = new ChangeResponse();

		result.setProjectId(projectId);
		result.setProjectVersion(projectVersion);

		return result;
	}
}
