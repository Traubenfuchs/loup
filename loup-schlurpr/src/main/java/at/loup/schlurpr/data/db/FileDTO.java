package at.loup.schlurpr.data.db;

import at.loup.schlurpr.data.EFileType;
import at.loup.schlurpr.db.FileEntity;

public class FileDTO {
	private String content;
	private String filename;
	private EFileType fileType;

	public FileDTO() {

	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public EFileType getFileType() {
		return fileType;
	}

	public void setFileType(EFileType fileType) {
		this.fileType = fileType;
	}

	public static FileDTO createFromEntity(FileEntity fileEntity) {
		FileDTO result = new FileDTO();

		result.setContent(fileEntity.getContent());
		result.setFilename(fileEntity.getFilename());
		result.setFileType(fileEntity.getFileType());

		return result;
	}
}
