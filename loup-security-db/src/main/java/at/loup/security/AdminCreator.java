package at.loup.security;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.loup.security.data.UserDTO;
import at.loup.security.data.reqres.UserRegistrationResult;
import at.loup.security.entities.UserEntity;
import at.loup.security.entities.repositories.UserEntityRepository;
import at.loup.security.services.coreservice.LoupSecurityCoreService;

@Service
public class AdminCreator {
	@Autowired
	private LoupSecurityCoreService loupSecurityCoreService;
	@Autowired
	private UserEntityRepository userEntityRepository;

	@PostConstruct
	@Transactional
	public void createAdmin() throws SQLException {
		UserEntity userEntity = userEntityRepository.findByEmail("akkira@live.at");

		if (userEntity != null) {
			System.out.println("ADMIN api token<" + userEntity.getApiKey() + ">");
			return;
		}
		UserDTO userDTO = new UserDTO();
		userDTO.setApiKey("abc");
		userDTO.setUsername("admin");
		userDTO.setEmail("akkira@live.at");
		userDTO.setPassword("admin");
		userDTO.getRights().add("USER");
		userDTO.getRights().add("ADMIN");
		userDTO.getRights().add("ACTIVATED");
		UserRegistrationResult cur = loupSecurityCoreService.registerUser(userDTO, false);

		System.out.println("ADMIN api token<" + cur.getUserDTO().getApiKey() + ">");
	}
}