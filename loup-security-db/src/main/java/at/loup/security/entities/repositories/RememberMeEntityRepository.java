package at.loup.security.entities.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.loup.security.entities.RememberMeEntity;

@Repository
public interface RememberMeEntityRepository extends JpaRepository<RememberMeEntity, Long> {
	RememberMeEntity findByToken(String token);

	Long deleteByLastUpdatedAtLessThan(Date date);
}
