package at.loup.security.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import at.loup.commons.entities.AbstractEntity;
import at.loup.commons.random.RandomUtilities;
import at.loup.commons.utilities.ArgumentRuleUtilities;

@Entity
@Table(name = "user_activation")
public class UserActivationEntity extends AbstractEntity<UserActivationEntity> {
	private static final long serialVersionUID = 3771913796903493407L;

	@OneToOne(optional = false)
	private UserEntity user;

	@Column(unique = true, nullable = false, length = 255)
	private String activationString;

	protected UserActivationEntity() {
	}

	public UserEntity getUser() {
		return user;
	}

	protected void setUser(UserEntity user) {
		this.user = user;
	}

	public String getActivationString() {
		return activationString;
	}

	protected void setActivationString(String activationString) {
		this.activationString = activationString;
	}

	public static UserActivationEntity create(UserEntity userEntity) {
		ArgumentRuleUtilities.notNull("userEntity", userEntity);
		ArgumentRuleUtilities.notNull("randomUtilities", userEntity);

		UserActivationEntity result = new UserActivationEntity();
		String activationString = RandomUtilities.createNiceRandomString(255);
		result.setActivationString(activationString);
		result.setUser(userEntity);

		return result;
	}

}