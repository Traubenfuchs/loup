package at.loup.security.entities.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import at.loup.security.entities.UserEntity;

@Repository
public interface UserEntityRepository extends JpaRepository<UserEntity, Long> {
	UserEntity findByEmail(String email);

	UserEntity findByApiKey(String apiKey);

	UserEntity findByUsername(String username);

	@Query(value = "select user from UserEntity user where user.username like %:usernameOrEmail% or user.email like %:usernameOrEmail% order by id desc", countProjection = "user")
	Page<UserEntity> findByUsernameContainsOrEmailContains(String usernameOrEmail, Pageable pageable);
}