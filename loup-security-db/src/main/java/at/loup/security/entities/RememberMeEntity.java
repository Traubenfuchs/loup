package at.loup.security.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.loup.commons.entities.AbstractTimedEntity;
import at.loup.commons.utilities.ArgumentRuleUtilities;

@Entity
@Table(name = "rememberMe")
public class RememberMeEntity extends AbstractTimedEntity<RememberMeEntity> {
	private static final long serialVersionUID = 22202461871688540L;
	@ManyToOne
	private UserEntity userEntity;
	@Column(nullable = false, unique = true, updatable = false, length = 255)
	private String token;

	protected RememberMeEntity() {

	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public static RememberMeEntity create(String token, UserEntity userEntity) {
		ArgumentRuleUtilities.notNullEmpty("token", token);
		ArgumentRuleUtilities.notNull("userEntity", userEntity);

		RememberMeEntity result = new RememberMeEntity();
		result.setToken(token);
		result.setUserEntity(userEntity);
		return result;
	}
}