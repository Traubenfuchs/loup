package at.loup.security.entities.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.loup.security.entities.UserActivationEntity;
import at.loup.security.entities.UserEntity;

@Repository
public interface UserActivationEntityRepository extends JpaRepository<UserActivationEntity, Long> {

	UserActivationEntity findByActivationString(String activationString);

	default UserEntity satisfyActivation(String activationString) {
		UserActivationEntity userActivationEntity = findByActivationString(activationString);
		if (userActivationEntity == null) {
			return null;
		}
		UserEntity result = userActivationEntity.getUser();
		delete(userActivationEntity);
		return result;
	};
}
