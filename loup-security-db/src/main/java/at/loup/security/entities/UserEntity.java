package at.loup.security.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import at.loup.commons.entities.AbstractTimedEntity;
import at.loup.security.data.UserDTO;

@Entity
@Table(name = "users")
public class UserEntity extends AbstractTimedEntity<UserEntity> {
	private static final long serialVersionUID = -6431133386839368847L;

	@Column(nullable = false, unique = true)
	private String username;
	@Column(nullable = true, unique = true)
	private String email;
	@ElementCollection
	@CollectionTable(name = "rights")
	private Set<String> rights;
	@Lob
	@Column(nullable = false)
	private byte[] hashedAndSaltedPassword;
	@Column(nullable = false, unique = true, length = 255)
	private String apiKey;
	private Boolean activated;

	public UserEntity() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<String> getRights() {
		if (rights == null) {
			rights = new HashSet<>();
		}
		return rights;
	}

	public boolean addRight(String right) {
		boolean result = getRights().add(right);
		return result;
	}

	public boolean removeRight(String right) {
		boolean result = getRights().remove(right);
		return result;
	}

	public void setRights(Set<String> rights) {
		if (rights == null) {
			rights = new HashSet<>();
		} else {
			Set<String> usedRights = new HashSet<>();
			for (String right : rights) {
				if (right == null) {
					continue;
				}
				right = right.trim();
				if (right.length() == 0) {
					continue;
				}
				usedRights.add(right);
			}

			this.rights = usedRights;
		}
	}

	public byte[] getHashedAndSaltedPassword() {
		return hashedAndSaltedPassword;
	}

	public void setHashedAndSaltedPassword(byte[] hashedAndSaltedPassword) {
		this.hashedAndSaltedPassword = hashedAndSaltedPassword;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public UserDTO toUserDTO() {
		UserDTO userDTO = new UserDTO();

		userDTO.setId(getId());
		userDTO.setUsername(getUsername());
		userDTO.setEmail(getEmail());
		userDTO.setRights(new HashSet<>(getRights()));
		userDTO.setPassword("[HIDDEN]");
		userDTO.setCreatedAt(getCreatedAt());
		userDTO.setApiKey(getApiKey());
		userDTO.setActivated(getActivated());

		return userDTO;
	}
}