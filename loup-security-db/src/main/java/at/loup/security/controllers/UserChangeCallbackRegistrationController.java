package at.loup.security.controllers;

import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import at.loup.security.aspects.annotations.Secured;
import at.loup.security.data.UserChangeCallbackDeregistrationRequest;
import at.loup.security.data.UserChangeCallbackDeregistrationResponse;
import at.loup.security.data.UserChangeCallbackRegistrationRequest;
import at.loup.security.data.UserChangeCallbackRegistrationResponse;
import at.loup.security.services.EndpointRegistrationService;

/**
 * loup-security using services can register here to receive user update
 * information
 */
@Secured(rights = "ADMIN")
@Profile("loupSecurityServerMode")
@RestController
public class UserChangeCallbackRegistrationController {
	private final EndpointRegistrationService endpointRegistrationService;

	public UserChangeCallbackRegistrationController(EndpointRegistrationService endpointRegistrationService) {
		this.endpointRegistrationService = endpointRegistrationService;
	}

	@PostMapping(path = "/api/loupsecurity/registerCallback")
	public UserChangeCallbackRegistrationResponse register(@RequestBody UserChangeCallbackRegistrationRequest request) {
		endpointRegistrationService.register(request);
		UserChangeCallbackRegistrationResponse result = UserChangeCallbackRegistrationResponse.createSuccess();
		return result;
	}

	@PostMapping(path = "/api/loupsecurity/unregisterCallback")
	public UserChangeCallbackDeregistrationResponse unRegister(
			@RequestBody UserChangeCallbackDeregistrationRequest request) {
		boolean wasUnregistered = endpointRegistrationService.deRegister(request);
		UserChangeCallbackDeregistrationResponse result = UserChangeCallbackDeregistrationResponse
				.createSuccess(wasUnregistered);
		return result;
	}
}