package at.loup.security.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.security.aspects.annotations.Secured;
import at.loup.security.data.UserDTO;
import at.loup.security.data.reqres.UserRegistrationResult;
import at.loup.security.entities.UserEntity;
import at.loup.security.entities.repositories.UserEntityRepository;
import at.loup.security.services.Hasher;
import at.loup.security.services.coreservice.LoupSecurityCoreService;

@Profile("loupSecurityServerMode")
@Secured(rights = "ADMIN")
@Controller
public class ServerApplicationController {
	@Autowired
	private UserEntityRepository userEntityRepository;
	@Autowired
	private Hasher hasher;
	@Autowired
	private LoupSecurityCoreService loupSecurityCoreService;

	public ServerApplicationController() {

	}

	@GetMapping(path = "/api/loaduser")
	public @ResponseBody UserDTO loadUser(@RequestParam long userId) {
		UserEntity userEntity = userEntityRepository.findOne(userId);
		if (userEntity == null) {
			return null;
		}
		UserDTO result = userEntity.toUserDTO();
		return result;
	}

	@GetMapping(path = "/api/findusers", produces = "application/json; charset=UTF-8")
	public @ResponseBody List<UserDTO> findUsers(@RequestParam String usernameOrEmail) {
		Page<UserEntity> userEntities = userEntityRepository.findByUsernameContainsOrEmailContains(usernameOrEmail,
				new PageRequest(0, Integer.MAX_VALUE));
		List<UserDTO> result = new ArrayList<>(userEntities.getNumberOfElements());
		for (UserEntity userEntity : userEntities) {
			UserDTO userDTO = userEntity.toUserDTO();
			userDTO.setPassword(null);
			result.add(userDTO);
		}
		return result;
	}

	@PostMapping(path = "/api/updateuser")
	@Transactional
	public @ResponseBody String updateUser(@RequestBody UserDTO userDTO) {
		UserEntity userEntity = userEntityRepository.findOne(userDTO.getId());
		if (userEntity == null) {
			throw new RuntimeException(); // TODO
		}
		if (userDTO.getApiKey() != null && userDTO.getApiKey().length() != 0) {
			userEntity.setApiKey(userDTO.getApiKey());
		}
		if (userDTO.getEmail() != null) {
			userEntity.setEmail(userDTO.getEmail());
		}
		if (userDTO.getPassword() != null && userDTO.getPassword().length() != 0) {
			byte[] hashedAndSaltedPassword = hasher.hashAndSalt(userDTO.getPassword());
			userEntity.setHashedAndSaltedPassword(hashedAndSaltedPassword);
		}
		if (userDTO.getRights() != null) {
			userEntity.setRights(userDTO.getRights());
		}
		if (userDTO.getUsername() != null && userDTO.getUsername().length() != 0) {
			userEntity.setUsername(userDTO.getUsername());
		}

		return "{\"result\":\"ok\"}";
	}

	@GetMapping(path = "/api/user/{userId}")
	@Transactional
	public @ResponseBody ResponseEntity<UserDTO> findUser(@PathVariable long userId) {
		UserEntity userEntity = userEntityRepository.findOne(userId);

		if (userEntity == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		UserDTO userDTO = userEntity.toUserDTO();
		userDTO.setPassword("");
		return ResponseEntity.ok(userDTO);
	}

	@PostMapping(path = "/api/createUser")
	public @ResponseBody UserRegistrationResult createUser(@RequestBody UserDTO userToBeCreated) {
		UserRegistrationResult result = loupSecurityCoreService.registerUser(userToBeCreated, false);
		return result;
	}
}