package at.loup.security.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import at.loup.commons.json.PageResponse;
import at.loup.security.aspects.annotations.Secured;
import at.loup.security.data.UserDTO;
import at.loup.security.entities.UserEntity;
import at.loup.security.entities.repositories.UserEntityRepository;

@RestController
@RequestMapping(path = "security/search")
@Secured(rights = "ADMIN")
public class SearchUsersController {
	@Autowired
	private UserEntityRepository userEntityRepository;

	@GetMapping
	public PageResponse<UserDTO> searchUsers(
			@RequestParam String searchString,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "100") int size) {
		if (size > 100) {
			size = 100;
		} else if (size < 1) {
			size = 1;
		}
		Page<UserEntity> pageResult;
		if (searchString == null || searchString.length() == 0) {
			pageResult = userEntityRepository.findAll(new PageRequest(page, size, new Sort(Direction.DESC, "id")));
		} else {
			pageResult = userEntityRepository.findByUsernameContainsOrEmailContains(searchString,
					new PageRequest(page, size));
		}

		PageResponse<UserDTO> result = PageResponse.createFromPageWithTransform(pageResult,
				entity -> {
					UserDTO userDTO = entity.toUserDTO();
					userDTO.setPassword("");
					return userDTO;
				});

		return result;
	}

	static class SearchUserResult {
		List<UserDTO> users;
	}
}