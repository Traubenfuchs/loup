package at.loup.security.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import at.loup.security.aspects.annotations.Secured;
import at.loup.security.data.LoginResult;
import at.loup.security.data.UserDTO;
import at.loup.security.data.reqres.DeleteUserResult;
import at.loup.security.data.reqres.UpdateUserResult;
import at.loup.security.data.reqres.UserRegistrationResult;
import at.loup.security.services.coreservice.LoupSecurityCoreService;

@RestController
@Secured(rights = "ADMIN")
@Profile("loupSecurityServerMode")
public class LoupSecurityCoreServiceController extends LoupSecurityCoreService {
	@Autowired
	private LoupSecurityCoreService loupSecurityCoreService;

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/loadViaRememberMeToken")
	public UserDTO loadViaRememberMeToken(@RequestParam String token) {
		UserDTO result = loupSecurityCoreService.loadViaRememberMeToken(token);
		return result;
	}

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/deleteRememberMe")
	public boolean deleteRememberMe(@RequestParam String token) {
		boolean result = loupSecurityCoreService.deleteRememberMe(token);
		return result;
	}

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/loadUserViaCreds")
	public LoginResult loadUserViaCreds(
			@RequestParam String usernameOrEmail,
			@RequestParam String password,
			@RequestParam boolean rememberMe) {
		LoginResult result = loupSecurityCoreService.loadUserViaCreds(usernameOrEmail, password,
				rememberMe);
		return result;
	}

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/loadUserViaApiToken")
	public UserDTO loadUserViaApiToken(@RequestParam String apiKey) {
		UserDTO result = loupSecurityCoreService.loadUserViaApiToken(apiKey);
		return result;
	}

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/updateUser")
	public UpdateUserResult updateUser(@RequestBody UserDTO userDTO) {
		UpdateUserResult result = loupSecurityCoreService.updateUser(userDTO);
		return result;
	}

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/deleteUser")
	public DeleteUserResult deleteUser(@RequestParam long userId) {
		DeleteUserResult result = loupSecurityCoreService.deleteUser(userId);
		return result;
	}

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/registerUser")
	public UserRegistrationResult registerUser(@RequestBody UserDTO userDTO, @RequestParam boolean activationRequired) {
		UserRegistrationResult result = loupSecurityCoreService.registerUser(userDTO, activationRequired);
		return result;
	}

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/activate")
	public UserDTO activate(@RequestParam String token) {
		UserDTO result = loupSecurityCoreService.activate(token);
		return result;
	}

	@Override
	@PostMapping(path = "/api/loupsecurity/coreservice/loadByUserId")
	public UserDTO loadByUserId(@RequestParam long userId) {
		UserDTO result = loupSecurityCoreService.loadByUserId(userId);
		return result;
	}
}