package at.loup.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import at.loup.security.entities.UserEntity;
import at.loup.security.entities.repositories.UserEntityRepository;

/**
 * Provides access to the UserEntity of the currently logged in user.
 */
@Primary
@Component
@Scope(scopeName = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DBUserHolder extends UserHolder {
	@Autowired
	private UserEntityRepository userEntityRepository;

	/**
	 * Returns the entity of the currently logged in user.
	 * 
	 * @return
	 */
	public UserEntity getUserEntity() {
		Long userId = getUserId();
		if (userId == null) {
			return null;
		}
		UserEntity result = userEntityRepository.findOne(userId);
		return result;
	}
}