package at.loup.security.services.coreservice;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import at.loup.commons.exceptions.LoupException;
import at.loup.commons.random.RandomUtilities;
import at.loup.commons.services.MailSenderService;
import at.loup.security.data.LoginResult;
import at.loup.security.data.LoginResult.ELoginResult;
import at.loup.security.data.UserDTO;
import at.loup.security.data.reqres.DeleteUserResult;
import at.loup.security.data.reqres.UpdateUserResult;
import at.loup.security.data.reqres.UserRegistrationResult;
import at.loup.security.entities.RememberMeEntity;
import at.loup.security.entities.UserActivationEntity;
import at.loup.security.entities.UserEntity;
import at.loup.security.entities.repositories.RememberMeEntityRepository;
import at.loup.security.entities.repositories.UserActivationEntityRepository;
import at.loup.security.entities.repositories.UserEntityRepository;
import at.loup.security.services.Hasher;

@Service
@Primary
public class LoupSecurityCoreServiceLocal extends LoupSecurityCoreService {
	@Autowired
	private UserEntityRepository userEntityRepository;
	@Autowired
	private RememberMeEntityRepository rememberMeEntityRepository;
	@Autowired
	private Hasher hasher;
	@Autowired
	private UserActivationEntityRepository userActivationEntityRepository;
	@Autowired
	private MailSenderService mailSenderService;
	private final LoadingCache<Long, UserDTO> userIdToUserDtoCache;
	private final LoadingCache<String, UserDTO> apiKeyToDtoCache;

	public LoupSecurityCoreServiceLocal() {
		userIdToUserDtoCache = CacheBuilder.newBuilder()
				.concurrencyLevel(6)
				.expireAfterAccess(10, TimeUnit.MINUTES)
				.maximumSize(1000)
				.build(new CacheLoader<Long, UserDTO>() {
					@Override
					public UserDTO load(Long key) throws Exception {
						UserEntity userEntity = userEntityRepository.findOne(key);
						UserDTO result = userEntity.toUserDTO();
						return result;
					}
				});

		apiKeyToDtoCache = CacheBuilder.newBuilder()
				.concurrencyLevel(6)
				.expireAfterAccess(10, TimeUnit.MINUTES)
				.maximumSize(1000)
				.build(new CacheLoader<String, UserDTO>() {
					@Override
					public UserDTO load(String key) throws Exception {
						UserEntity userEntity = userEntityRepository.findByApiKey(key);
						UserDTO result = userEntity.toUserDTO();
						return result;
					}
				});
	}

	@Override
	@Transactional
	public UserDTO loadViaRememberMeToken(String token) {
		RememberMeEntity rememberMeEntity = rememberMeEntityRepository.findByToken(token);
		if (rememberMeEntity == null) {
			return null;
		}
		rememberMeEntity.setLastUpdatedAt(null); // forces @PreUpdate to insert
													// a new lastUpdatedAt Date
		UserEntity userEntity = rememberMeEntity.getUserEntity();
		UserDTO result = userEntity.toUserDTO();
		return result;
	}

	@Override
	@Transactional
	public boolean deleteRememberMe(String token) {
		RememberMeEntity rememberMeEntity = rememberMeEntityRepository.findByToken(token);
		if (rememberMeEntity == null) {
			return false;
		}
		rememberMeEntityRepository.delete(rememberMeEntity);
		return true;
	}

	@Override
	@Transactional
	public LoginResult loadUserViaCreds(String usernameOrEmail, String password, boolean rememberMe) {
		if (usernameOrEmail == null || usernameOrEmail.length() == 0 || password == null || password.length() == 0) {
			LoginResult result = ELoginResult.noUserData.createLoginResult();
			return result;
		}

		UserEntity userEntity;
		if (usernameOrEmail.contains("@")) {
			userEntity = userEntityRepository.findByEmail(usernameOrEmail);
		} else {
			userEntity = userEntityRepository.findByUsername(usernameOrEmail);
		}

		if (userEntity == null) {
			LoginResult result = ELoginResult.usernameDoesNotExist.createLoginResult();
			return result;
		}

		boolean correctPassword = hasher.checkHashedAndSaltedPassword(userEntity.getHashedAndSaltedPassword(),
				password);

		if (correctPassword) {
			UserDTO userDTO = userEntity.toUserDTO();
			if (!userDTO.getActivated()) {
				LoginResult result = ELoginResult.notActivated.createLoginResult(userDTO);
				return result;
			}

			LoginResult result = ELoginResult.loggedInViaUsernamePassword.createLoginResult(userDTO);

			if (rememberMe) {
				String rememberMeToken = RandomUtilities.createNiceRandomString(255);
				RememberMeEntity rememberMeEntity = RememberMeEntity.create(rememberMeToken, userEntity);
				rememberMeEntityRepository.save(rememberMeEntity);
				result.setRememberMeToken(rememberMeToken);
			}

			return result;
		} else {
			LoginResult result = ELoginResult.incorrectPassword.createLoginResult();
			return result;
		}
	}

	@Override
	public UserDTO loadUserViaApiToken(String apiKey) {
		UserDTO userDTO;
		try {
			userDTO = apiKeyToDtoCache.get(apiKey);
		} catch (ExecutionException ex) {
			throw new LoupException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Can't resolve user with apiKey <" + apiKey + ">", ex);
		}
		return userDTO;
	}

	@Override
	@Transactional
	public UpdateUserResult updateUser(UserDTO userDTO) {
		UpdateUserResult result;
		UserEntity userEntity = userEntityRepository.findOne(userDTO.getId());

		if (userEntity == null) {
			result = UpdateUserResult.createUserDoesNotExist();
			return result;
		}

		if (userDTO.getApiKey() == null || userDTO.getApiKey().length() == 0) {
			while ((userEntity = userEntityRepository.findByApiKey(userEntity.getApiKey())) != null) {
				String apiKey = RandomUtilities.createNiceRandomString(255);
				userEntity.setApiKey(apiKey);
			}
		}

		userEntity.setEmail(userDTO.getEmail());
		if (userDTO.getPassword() != null && userDTO.getPassword().length() != 0) {
			byte[] newHashedAndSaltedPassword = hasher.hashAndSalt(userDTO.getPassword());
			userEntity.setHashedAndSaltedPassword(newHashedAndSaltedPassword);
		}

		userEntity.setRights(userDTO.getRights());
		userEntity.setUsername(userDTO.getUsername());

		result = UpdateUserResult.createSuccess();

		userIdToUserDtoCache.invalidate(userDTO.getId());
		apiKeyToDtoCache.invalidate(userDTO.getApiKey());

		return result;
	}

	@Override
	public DeleteUserResult deleteUser(long userId) {
		UserEntity userEntity = userEntityRepository.findOne(userId);
		if (userEntity == null) {
			DeleteUserResult result = DeleteUserResult.createUserDoesNotExist();
			return result;
		}

		userEntityRepository.delete(userEntity);
		DeleteUserResult result = DeleteUserResult.createSuccess(userEntity.toUserDTO());

		userIdToUserDtoCache.invalidate(userId);
		apiKeyToDtoCache.invalidate(userEntity.getApiKey());

		return result;
	}

	@Override
	@Transactional
	public UserRegistrationResult registerUser(final UserDTO givenUserDTO, boolean activationRequired) {
		if (givenUserDTO.getUsername().contains("@")) {
			UserRegistrationResult result = UserRegistrationResult
					.createRequirementsError("Username must not contain @");
			return result;
		}

		if (givenUserDTO.getApiKey() == null) {
			String apiKey = RandomUtilities.createNiceRandomString(255);
			givenUserDTO.setApiKey(apiKey);
		}

		UserEntity alreadyExistingUser;
		while ((alreadyExistingUser = userEntityRepository.findByApiKey(givenUserDTO.getApiKey())) != null) {
			String apiKey = RandomUtilities.createNiceRandomString(255);
			givenUserDTO.setApiKey(apiKey);
		}
		alreadyExistingUser = userEntityRepository.findByUsername(givenUserDTO.getUsername());
		if (alreadyExistingUser != null) {
			UserRegistrationResult result = UserRegistrationResult.createUsernameExists();
			return result;
		}
		if (givenUserDTO.getEmail() != null) {
			alreadyExistingUser = userEntityRepository.findByEmail(givenUserDTO.getEmail());
			if (alreadyExistingUser != null) {
				UserRegistrationResult result = UserRegistrationResult.createEmailExists();
				return result;
			}
		}

		UserEntity newUserEntity = new UserEntity();

		newUserEntity.setApiKey(givenUserDTO.getApiKey());
		newUserEntity.setEmail(givenUserDTO.getEmail());
		byte[] newHashedAndSaltedPassword = hasher.hashAndSalt(givenUserDTO.getPassword());
		newUserEntity.setHashedAndSaltedPassword(newHashedAndSaltedPassword);
		newUserEntity.setRights(givenUserDTO.getRights());
		newUserEntity.setUsername(givenUserDTO.getUsername());

		if (activationRequired) {
			newUserEntity.setActivated(false);
			UserActivationEntity userActivationEntity = UserActivationEntity.create(newUserEntity);
			userActivationEntity = userActivationEntityRepository.save(userActivationEntity);

			mailSenderService.send("" +
					"API key<" +
					newUserEntity.getApiKey() +
					">, activate account at <" +
					"TODO!" +
					">",
					"loup activation", newUserEntity.getEmail()); // TODO
		} else {
			newUserEntity.setActivated(true);
			newUserEntity.addRight("ACTIVATED");
		}

		newUserEntity = userEntityRepository.save(newUserEntity);

		UserDTO userDTO = newUserEntity.toUserDTO();

		UserRegistrationResult result = UserRegistrationResult.createSuccess(userDTO);

		return result;
	}

	@Override
	@Transactional
	public UserDTO activate(String token) {
		UserActivationEntity entity = userActivationEntityRepository.findByActivationString(token);
		if (entity == null) {
			return null;
		}

		UserEntity userEntity = entity.getUser();
		if (userEntity == null) {
			return null;
		}
		userEntity.setActivated(true);
		userEntity.addRight("ACTIVATED");

		UserDTO result = userEntity.toUserDTO();

		userIdToUserDtoCache.invalidate(result.getId());
		apiKeyToDtoCache.invalidate(result.getApiKey());

		return result;
	}

	@Override
	public UserDTO loadByUserId(long userId) {
		UserDTO result;
		try {
			result = userIdToUserDtoCache.get(userId);
		} catch (ExecutionException ex) {
			throw new LoupException(HttpStatus.INTERNAL_SERVER_ERROR, "Can't resolve user with id <" + userId + ">",
					ex);
		}
		return result;
	}
}