package at.loup.security.services;

import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import at.loup.commons.utilities.datastructures.MapBuilder;
import at.loup.security.data.UserChange;
import at.loup.security.data.UserChangeCallbackDeregistrationRequest;
import at.loup.security.data.UserChangeCallbackRegistrationRequest;
import at.loup.security.data.UserDTO;

@Profile("loupSecurityServerMode")
@Service
public class EndpointRegistrationService {
	private final ConcurrentHashMap<String, String> endpointToUserChangeKey = new ConcurrentHashMap<>();
	private final RestTemplate restTemplate = new RestTemplate();

	public EndpointRegistrationService() {
	}

	/**
	 * Registers the given url. The endpoint at the given url will receive
	 * updates.
	 *
	 * @param url
	 * @return
	 */
	public boolean register(UserChangeCallbackRegistrationRequest request) {
		String previousValue = endpointToUserChangeKey.put(request.getCallbackUrl(), request.getUserChangeKey());
		return previousValue != null;
	}

	/**
	 * Unregisters the given url.
	 *
	 * @param url
	 * @return true if registration existed and was deleted, false if it didn't
	 *         exist
	 */
	public boolean deRegister(UserChangeCallbackDeregistrationRequest request) {
		String previousValue = endpointToUserChangeKey.remove(request.getCallbackUrl());
		return previousValue != null;
	}

	public void broadcastUpdate(UserDTO userDTO) {
		UserChange userChange = UserChange.createUpdate(userDTO);
		broadcastChange(userChange);
	}

	public void broadcastDelete(UserDTO userDTO) {
		UserChange userChange = UserChange.createDelete(userDTO);
		broadcastChange(userChange);
	}

	protected void broadcastChange(UserChange userChange) {
		for (Entry<String, String> entry : endpointToUserChangeKey.entrySet()) {
			restTemplate.postForObject(entry.getKey(), userChange, null,
					MapBuilder.of("user-change-key", entry.getValue()));
		}
	}
}
