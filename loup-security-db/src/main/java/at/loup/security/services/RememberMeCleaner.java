package at.loup.security.services;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.loup.commons.services.SpringAwareThread;
import at.loup.commons.services.TransactionForcer;
import at.loup.security.entities.repositories.RememberMeEntityRepository;

@Service
class RememberMeCleaner extends SpringAwareThread {
	private final RememberMeEntityRepository rememberMeEntityRepository;
	@Autowired
	private TransactionForcer transactionForcer;

	public RememberMeCleaner(RememberMeEntityRepository rememberMeEntityRepository) {
		super("RememberMeCleaner");
		this.rememberMeEntityRepository = rememberMeEntityRepository;
	}

	@Override
	protected void threadMethod() throws InterruptedException {
		Thread currentThread = Thread.currentThread();
		while (!currentThread.isInterrupted()) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_YEAR, -30);
			Date date = calendar.getTime();
			transactionForcer.executeInTransaction(() -> {
				rememberMeEntityRepository.deleteByLastUpdatedAtLessThan(date);
			});
			Thread.sleep(1000 * 60 * 60 * 24); // once per day should be enough
		}
	}
}
