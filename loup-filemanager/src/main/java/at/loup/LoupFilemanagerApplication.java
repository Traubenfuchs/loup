package at.loup;

import java.time.LocalDateTime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

// TODO allow adding images to a pool + ordering
// TODO allow adding images to a pool from upload + ordering
// TODO gallery shared component
// TODO pool component
// TODO pool edit component
// TODO tag alias / alias chain
// TODO tag description
// TODO tag component
//
//
@SpringBootApplication
@ComponentScan(basePackages = "at.loup", scopedProxy = ScopedProxyMode.TARGET_CLASS)
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableAsync(proxyTargetClass = true)
public class LoupFilemanagerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(LoupFilemanagerApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		SpringApplicationBuilder result = application.sources(LoupFilemanagerApplication.class);
		return result;
	}

	@GetMapping(path = "/")
	public ModelAndView get() {
		ModelAndView result = new ModelAndView("IAmTheViewName");
		result.addObject("currentTime", LocalDateTime.now());
		return result;
	}
}
