package at.loup.filemanager.data;

public class Response {
	private String message;
	private boolean ok;

	private Response() {

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public static Response createOk(String message) {
		Response result = new Response();

		result.setMessage(message);
		result.setOk(true);

		return result;
	}

	public static Response createNotOk(String message) {
		Response result = new Response();

		result.setMessage(message);
		result.setOk(false);

		return result;
	}
}