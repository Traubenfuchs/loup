package at.loup.filemanager.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import at.loup.commons.utilities.ArgumentRuleUtilities;
import at.loup.filemanager.entities.ImageEntityBase;
import at.loup.filemanager.services.ImageCache;

public class ImageSearchResult {
	private int currentPage;
	private int elementsOnCurrentPage;
	private int maxElementsPerPage;
	private long elementsThatExist;
	private int pagesThatExist;
	private List<ImageSearchResultImage> images;

	public ImageSearchResult() {

	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getElementsOnCurrentPage() {
		return elementsOnCurrentPage;
	}

	public void setElementsOnCurrentPage(int elementsOnCurrentPage) {
		this.elementsOnCurrentPage = elementsOnCurrentPage;
	}

	public int getMaxElementsPerPage() {
		return maxElementsPerPage;
	}

	public void setMaxElementsPerPage(int maxElementsPerPage) {
		this.maxElementsPerPage = maxElementsPerPage;
	}

	public long getElementsThatExist() {
		return elementsThatExist;
	}

	public void setElementsThatExist(long elementsThatExist) {
		this.elementsThatExist = elementsThatExist;
	}

	public int getPagesThatExist() {
		return pagesThatExist;
	}

	public void setPagesThatExist(int pagesThatExist) {
		this.pagesThatExist = pagesThatExist;
	}

	public List<ImageSearchResultImage> getImages() {
		if (images == null) {
			images = new ArrayList<>();
		}
		return images;
	}

	public void setImages(List<ImageSearchResultImage> images) {
		this.images = images;
	}

	public static ImageSearchResult createFromEntities(Page<Long> page, ImageCache imageCache) {
		ArgumentRuleUtilities.notNull("page", page);
		ArgumentRuleUtilities.notNull("imageCache", imageCache);

		ImageSearchResult result = new ImageSearchResult();

		result.setCurrentPage(page.getNumber());
		result.setElementsOnCurrentPage(page.getNumberOfElements());
		result.setElementsThatExist(page.getTotalElements());
		result.setMaxElementsPerPage(page.getSize());
		result.setPagesThatExist(page.getTotalPages());

		List<? extends Number> ids = page.getContent();

		for (Number imageId : ids) {
			CachedImage cachedImage = imageCache.loadImage(imageId.longValue(), 0);
			ImageSearchResultImage imageSearchImage = ImageSearchResultImage.createFromCachedImage(cachedImage);
			result.getImages().add(imageSearchImage);
		}

		return result;
	}

	public static class ImageSearchResultImage {
		private String imageUrl;
		private String previewImageUrl;
		private long id;
		private int imageSize;
		private int previewSize;
		private String filename;
		private List<String> tags;
		private long viewCount;

		public ImageSearchResultImage() {

		}

		public long getViewCount() {
			return viewCount;
		}

		public void setViewCount(long viewCount) {
			this.viewCount = viewCount;
		}

		public String getImageUrl() {
			return imageUrl;
		}

		public void setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
		}

		public String getPreviewImageUrl() {
			return previewImageUrl;
		}

		public void setPreviewImageUrl(String previewImageUrl) {
			this.previewImageUrl = previewImageUrl;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public int getImageSize() {
			return imageSize;
		}

		public void setImageSize(int imageSize) {
			this.imageSize = imageSize;
		}

		public int getPreviewSize() {
			return previewSize;
		}

		public void setPreviewSize(int previewSize) {
			this.previewSize = previewSize;
		}

		public String getFilename() {
			return filename;
		}

		public void setFilename(String filename) {
			this.filename = filename;
		}

		public List<String> getTags() {
			if (tags == null) {
				tags = new ArrayList<>();
			}
			return tags;
		}

		public void setTags(List<String> tags) {
			if (tags != null) {
				java.util.Collections.sort(tags);
			}
			this.tags = tags;
		}

		public static ImageSearchResultImage createFromCachedImage(CachedImage cachedImage) {
			ImageSearchResultImage result = new ImageSearchResultImage();

			result.setImageUrl("/file/" + cachedImage.getKey());
			result.setPreviewImageUrl("/filepreview/" + cachedImage.getKey());
			result.setImageSize(cachedImage.getImageBytes().length);
			result.setPreviewSize(cachedImage.getPreviewImageBytes().length);
			result.setId(cachedImage.getKey());
			result.setFilename(cachedImage.getFilename());
			result.setTags(cachedImage.getTags());

			return result;
		}

		public static ImageSearchResultImage createFromEntity(ImageEntityBase imageEntity) {
			ImageSearchResultImage result = new ImageSearchResultImage();

			result.setImageUrl("/file/" + imageEntity.getId());
			result.setPreviewImageUrl("/filepreview/" + imageEntity.getId());
			result.setImageSize(imageEntity.getImageBinary().getSize());
			result.setPreviewSize(imageEntity.getPreviewImageBinary().getSize());
			result.setId(imageEntity.getId());
			result.setFilename(imageEntity.getFileName());
			result.setViewCount(imageEntity.getViewCount());

			List<String> tagsAsString = imageEntity.getTagsAsStrings();
			result.setTags(tagsAsString);

			return result;
		}
	}
}