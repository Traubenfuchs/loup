package at.loup.filemanager.data;

import java.util.ArrayList;
import java.util.List;

import at.loup.filemanager.entities.BinaryEntity;
import at.loup.filemanager.entities.ImageEntityBase;

public class CachedImage {
	private Long key;
	private byte[] imageBytes;
	private byte[] previewImageBytes;
	private String contentType;
	private String contentDisposition;
	private boolean isPublic;
	private long originalUploader;
	private String filename;
	private List<String> tags;
	private long viewCount;

	protected CachedImage() {

	}

	public byte[] getImageBytes() {
		return imageBytes;
	}

	public void setImageBytes(byte[] imageBytes) {
		this.imageBytes = imageBytes;
	}

	public byte[] getPreviewImageBytes() {
		return previewImageBytes;
	}

	public void setPreviewImageBytes(byte[] previewImageBytes) {
		this.previewImageBytes = previewImageBytes;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentDisposition() {
		return contentDisposition;
	}

	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

	public long getOriginalUploader() {
		return originalUploader;
	}

	public void setOriginalUploader(long originalUploader) {
		this.originalUploader = originalUploader;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public long getViewCount() {
		return viewCount;
	}

	public void setViewCount(long viewCount) {
		this.viewCount = viewCount;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<String> getTags() {
		if (tags == null) {
			tags = new ArrayList<>();
		}
		return tags;
	}

	/**
	 * Returns the instances approximate size in bytes.
	 */
	public int getByteSize() {
		return getImageBytes().length + getPreviewImageBytes().length + 1000;
	}

	public static CachedImage create(ImageEntityBase imageEntity) {
		CachedImage result = new CachedImage();

		BinaryEntity binaryEntity = imageEntity.getImageBinary();
		BinaryEntity binaryEntityPreview = imageEntity.getPreviewImageBinary();

		result.setKey(imageEntity.getId());
		result.setImageBytes(binaryEntity.getData());
		result.setPreviewImageBytes(binaryEntityPreview.getData());
		result.setContentType(imageEntity.getMediaType());
		result.setContentDisposition("inline; filename=\"" + imageEntity.getFileName() + "\"");
		result.setOriginalUploader(imageEntity.getOriginalUploader().getId());
		result.setPublic(imageEntity.getPublicFile());
		result.setFilename(imageEntity.getFileName());
		result.setTags(imageEntity.getTagsAsStrings());
		result.setViewCount(imageEntity.getViewCount());
		return result;
	}
}