package at.loup.filemanager.data;

import org.springframework.data.domain.Sort.Direction;

public class ImageSearchRequest {
	private String searchString = "";
	private Integer limit = 50;
	private Integer page = 0;
	private Direction direction = Direction.DESC;

	public ImageSearchRequest() {
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

}