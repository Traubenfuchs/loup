package at.loup.filemanager.data;

import org.springframework.data.domain.Sort.Direction;

import at.loup.commons.utilities.ArgumentRuleUtilities;

public class SafeImageSearchRequest extends ImageSearchRequest {
	public SafeImageSearchRequest() {
	}

	@Override
	public void setSearchString(String searchString) {
		if (searchString == null) {
			searchString = "";
		}
		super.setSearchString(searchString);
	}

	@Override
	public void setLimit(Integer limit) {
		if (limit == null) {
			limit = 50;
		} else if (limit < 0) {
			limit = 1;
		} else if (limit > 100) {
			limit = 100;
		}
		super.setLimit(limit);
	}

	@Override
	public void setDirection(Direction direction) {
		if (direction == null) {
			direction = Direction.DESC;
		}
		super.setDirection(direction);
	}

	@Override
	public void setPage(Integer page) {
		if (page == null) {
			page = 0;
		} else if (page < 0) {
			page = 0;
		}
		super.setPage(page);
	}

	public static SafeImageSearchRequest createFromImageSearchRequest(ImageSearchRequest imageSearchRequest) {
		ArgumentRuleUtilities.notNull("imageSearchRequest", imageSearchRequest);
		SafeImageSearchRequest result = new SafeImageSearchRequest();

		result.setDirection(imageSearchRequest.getDirection());
		result.setLimit(imageSearchRequest.getLimit());
		result.setSearchString(imageSearchRequest.getSearchString());
		result.setPage(imageSearchRequest.getPage());

		return result;
	}
}