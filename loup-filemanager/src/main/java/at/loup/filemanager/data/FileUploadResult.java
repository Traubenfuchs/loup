package at.loup.filemanager.data;

public class FileUploadResult {
	private long id;
	private String message;
	private boolean ok;
	private String usedFilename;

	public FileUploadResult() {

	}

	public String getUsedFilename() {
		return usedFilename;
	}

	public void setUsedFilename(String usedFilename) {
		this.usedFilename = usedFilename;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}
}