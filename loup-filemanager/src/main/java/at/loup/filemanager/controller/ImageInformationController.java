package at.loup.filemanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import at.loup.filemanager.data.ImageSearchResult.ImageSearchResultImage;
import at.loup.filemanager.data.Response;
import at.loup.filemanager.entities.ImageEntityBase;
import at.loup.filemanager.repositories.repositories.ImageEntityCurrentRepository;
import at.loup.filemanager.repositories.repositories.ImageEntityHistoricRepository;
import at.loup.filemanager.services.ImageManager;

@RestController
public class ImageInformationController {
	@Autowired
	private ImageManager imageManager;
	@Autowired
	private ImageEntityCurrentRepository imageEntityCurrentRepository;

	@Autowired
	private ImageEntityHistoricRepository imageEntityHistoricRepository;

	@GetMapping(path = "/file/{imageId}/info")
	public @ResponseBody ImageSearchResultImage imageData(@PathVariable long imageId) {

		// TODO not found
		ImageEntityBase imageEntity = imageEntityCurrentRepository.findOne(imageId);

		ImageSearchResultImage result = ImageSearchResultImage.createFromEntity(imageEntity);

		return result;
	}

	@PostMapping(path = "/file/{imageId}/info")
	public @ResponseBody Response updateImageData(
			@PathVariable long imageId,
			@RequestParam(required = false) MultipartFile file,
			@RequestParam(required = false) String filename,
			@RequestParam(required = false) String tagString) {

		imageManager.updateImage(imageId, file, filename, tagString);

		return Response.createOk("");
	}

}
