package at.loup.filemanager.controller;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import at.loup.filemanager.data.ImageSearchRequest;
import at.loup.filemanager.data.ImageSearchResult;
import at.loup.filemanager.data.SafeImageSearchRequest;
import at.loup.filemanager.services.SearchService;

@RestController
public class ImageSearchController {
	@Autowired
	private SearchService searchService;

	@RequestMapping(path = "/api/searchImage", method = RequestMethod.GET)
	public @ResponseBody Callable<ImageSearchResult> searchImage(
			ImageSearchRequest imageSearchRequest) {
		return () -> {
			searchService.asyncMethod();
			SafeImageSearchRequest safeImageSearchRequest = SafeImageSearchRequest
					.createFromImageSearchRequest(imageSearchRequest);
			ImageSearchResult result = searchService.searchImage(safeImageSearchRequest);

			return result;
		};
	}
}