package at.loup.filemanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import at.loup.security.data.UserDTO;
import at.loup.security.services.UserHolder;

@Controller
public class GUIController {
	@Autowired
	private UserHolder userHolder;

	@GetMapping(path = { "/**" })
	public ModelAndView home() {
		UserDTO userDTO = userHolder.getUserDTO();
		ModelAndView result = new ModelAndView("fileManagerGui2");
		result.addObject("user", userDTO);
		return result;
	}

	@GetMapping(path = { "/x/**" })
	public ModelAndView home2() {
		UserDTO userDTO = userHolder.getUserDTO();
		ModelAndView result = new ModelAndView("fileManagerGui3");
		result.addObject("user", userDTO);
		return result;
	}
}