package at.loup.filemanager.controller;

import java.io.IOException;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import at.loup.filemanager.data.FileUploadResult;
import at.loup.filemanager.repositories.repositories.ImageEntityCurrentRepository;
import at.loup.filemanager.repositories.repositories.ImageEntityHistoricRepository;
import at.loup.filemanager.services.ImageManager;
import at.loup.filemanager.services.PerformanceTestHelper;
import at.loup.security.aspects.annotations.Secured;

@RestController
public class FileManagerController {
	@Autowired
	private ImageEntityCurrentRepository imageEntityCurrentRepository;

	@Autowired
	private ImageEntityHistoricRepository imageEntityHistoricRepository;
	@Autowired
	private ImageManager imageManager;

	@Autowired
	private PerformanceTestHelper performanceTestHelper;

	@Autowired
	private HttpServletResponse httpServletResponse;

	@Secured(checkCSRF = false)
	@GetMapping(path = "/file/{fileId}")
	public ResponseEntity<byte[]> loadFile(@PathVariable long fileId) {
		ResponseEntity<byte[]> result = imageManager.loadFullImageResponseEntity(fileId);
		httpServletResponse.setHeader("Cache-Control", "public, max-age=30672000");
		return result;
	}

	@Secured(checkCSRF = false)
	@GetMapping(path = "/filepreview/{fileId}")
	public ResponseEntity<byte[]> loadPreviewFile(@PathVariable long fileId) {
		ResponseEntity<byte[]> result = imageManager.loadPreviewImageResponseEntity(fileId);
		httpServletResponse.setHeader("Cache-Control", "public, max-age=30672000");
		return result;
	}

	// @RequestMapping(path = "/file/{fileId}", method = RequestMethod.DELETE)
	// @Transactional()
	// public @ResponseBody boolean deleteFile(@PathVariable long fileId) {
	// ImageEntity fileEntity = iamgeEntityRepository.findOne(fileId);
	// if (fileEntity != null) {
	// iamgeEntityRepository.delete(fileId);
	// return true;
	// }
	// return false;
	// }
	@Secured
	@PostMapping(path = "/file", consumes = "application/json")
	public @ResponseBody Callable<FileUploadResult> saveFile(
			@RequestParam(required = true) MultipartFile file,
			@RequestParam(required = false) String filename,
			@RequestParam(required = false) String tagString)
			throws IOException {
		return () -> {
			FileUploadResult result = imageManager.upload(file, filename,
					tagString);

			return result;
		};
	}

}