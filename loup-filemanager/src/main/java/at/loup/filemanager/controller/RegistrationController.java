package at.loup.filemanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.loup.security.data.LoginResult;
import at.loup.security.data.UserDTO;
import at.loup.security.data.reqres.UserRegistrationResult;
import at.loup.security.services.coreservice.LoupSecurityCoreService;
import at.loup.security.services.securitymanagers.WebSecurityManager;

@RestController
public class RegistrationController {
	@Autowired
	private LoupSecurityCoreService lscs;
	@Autowired
	private WebSecurityManager wsm;

	public RegistrationController() {

	}

	@RequestMapping(path = "/api/register")
	public UserRegistrationResult register(@RequestBody RegisterRequest request) {
		UserDTO userDTO = new UserDTO();
		userDTO.setUsername(request.getUsername());
		userDTO.setPassword(request.getPassword());
		userDTO.setEmail(request.getEmail());

		UserRegistrationResult urr = lscs.registerUser(userDTO, false);
		if (urr.getUserDTO() != null) {
			LoginResult loginResult = wsm.login(request.getUsername(), request.getPassword(), request.getRememberMe());
			if (!loginResult.geteLoginResult().isSuccess()) {
				throw new IllegalStateException("User was just created and should be able to login.");
			}
		}
		return urr;
	}

	static class RegisterRequest {
		private String username;
		private String password;
		private String email;
		private Boolean rememberMe;

		public RegisterRequest() {

		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Boolean getRememberMe() {
			if (rememberMe == null) {
				return false;
			}
			return rememberMe;
		}

		public void setRememberMe(Boolean rememberMe) {
			this.rememberMe = rememberMe;
		}
	}
}
