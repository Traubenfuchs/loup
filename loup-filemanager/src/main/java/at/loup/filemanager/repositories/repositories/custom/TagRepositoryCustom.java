package at.loup.filemanager.repositories.repositories.custom;

import java.util.Set;

import at.loup.filemanager.entities.TagEntity;

public interface TagRepositoryCustom {

	/**
	 * Gets or creates the Tagentity for the given tag.<br>
	 * Caches the new tag's id.
	 *
	 * @param tag
	 *            Must not be null, empty or whitespace.
	 * @return
	 */
	TagEntity getOrCreateTagEntity(String tag);

	/**
	 * Loads the tagId from cache.
	 *
	 * @param tag
	 * @return
	 */
	Long getIdForTag(String tag);

	/**
	 * Converts the given tags to their id's. Calls {@link #getOrCreateTagEntity
	 * getOrCreateTagEntity(...)}.
	 *
	 * @param tags
	 * @return
	 */
	Set<Long> tagsToIds(Set<String> tags);

	/**
	 * Converts the given tagString to the tag'd id's. Calls {@link #tagsToIds
	 * tagsToIds(...)}.
	 *
	 * @param tagString
	 * @return
	 */
	Set<TagEntity> tagStringToTagEntities(String tagString);

	Set<TagEntity> getOrCreateTagEntities(String[] tags);
}