package at.loup.filemanager.repositories.repositories.custom;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

import at.loup.commons.utilities.ArgumentRuleUtilities;
import at.loup.filemanager.entities.TagEntity;
import at.loup.filemanager.repositories.repositories.TagRepository;

public class TagRepositoryImpl implements TagRepositoryCustom {
	@Autowired
	private TagRepository tagRepository;

	private final ConcurrentHashMap<String, Long> tagToId = new ConcurrentHashMap<>();

	public TagRepositoryImpl() {

	}

	@Override
	@Transactional
	public Set<TagEntity> getOrCreateTagEntities(String... tags) {
		Set<String> tagsSet = Sets.newHashSet(tags);

		Set<TagEntity> existingTags = tagRepository.findByTagIn(tags);

		if (existingTags.size() == tagsSet.size()) {
			return existingTags;
		}

		Set<TagEntity> result = new HashSet<>();
		result.addAll(existingTags);

		for (String tag : tagsSet) {
			boolean tagFound = false;
			for (TagEntity alreadyExistingTagEntity : existingTags) {
				if (alreadyExistingTagEntity.getTag().equals(tag)) {
					tagFound = true;
					break;
				}
			}
			if (!tagFound) {
				TagEntity entity = getOrCreateTagEntity(tag);
				result.add(entity);
			}
		}

		return result;

	}

	@Override
	@Transactional
	public TagEntity getOrCreateTagEntity(String tag) {
		ArgumentRuleUtilities.notNullEmptyWhitespace(tag, "tag");
		TagEntity tagEntity = tagRepository.findByTag(tag);

		if (tagEntity == null) {
			tagEntity = new TagEntity();
			tagEntity.setTag(tag);
			tagEntity = tagRepository.save(tagEntity);
		}

		tagToId.put(tag, tagEntity.getId());

		return tagEntity;
	}

	@Override
	public Long getIdForTag(String tag) {
		Long result = tagToId.computeIfAbsent(tag, key -> {
			TagEntity tagEntity = tagRepository.findByTag(tag);
			if (tagEntity == null) {
				return null;
			}
			return tagEntity.getId();
		});
		return result;
	}

	/**
	 * Converts tags as String to tagIds.
	 *
	 * @param tags
	 * @return empty Set if no tag is found
	 */
	@Override
	public Set<Long> tagsToIds(Set<String> tags) {
		Set<Long> result = new HashSet<>();

		for (String tag : tags) {
			Long id = getIdForTag(tag);
			if (id != null) {
				result.add(id);
			}
		}

		return result;
	}

	@Override
	public Set<TagEntity> tagStringToTagEntities(String tagString) {
		if (tagString == null || tagString.length() == 0) {
			return new HashSet<>();
		}
		String[] tags = tagString.split(" ");
		Set<TagEntity> tagEntities = new HashSet<>();
		for (String tag : tags) {
			TagEntity tagEntity = getOrCreateTagEntity(tag);
			tagEntities.add(tagEntity);
		}
		return tagEntities;
	}
}