package at.loup.filemanager.repositories.repositories.custom;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ImageEntityCurrentRepositoryCustom {

	Page<Long> doDualTagSearchSmart(Pageable pageable, Set<Long> positiveTags, Set<Long> negativeTags);

}
