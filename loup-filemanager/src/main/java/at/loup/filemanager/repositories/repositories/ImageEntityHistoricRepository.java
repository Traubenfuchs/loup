package at.loup.filemanager.repositories.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.loup.filemanager.entities.ImageEntityHistoric;
import at.loup.filemanager.repositories.repositories.custom.ImageEntityHistoricCustom;

//@formatter:off
@Repository
public interface ImageEntityHistoricRepository
		extends JpaRepository<ImageEntityHistoric, Long>, ImageEntityHistoricCustom {

	ImageEntityHistoric findByLastVersionAndImageVersion(long imageId, int imageVersion);

}
//@formatter:on