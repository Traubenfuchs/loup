package at.loup.filemanager.repositories.repositories;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.loup.filemanager.entities.ImageEntityCurrent;
import at.loup.filemanager.repositories.repositories.custom.ImageEntityCurrentRepositoryCustom;

//@formatter:off
@Repository
public interface ImageEntityCurrentRepository extends JpaRepository<ImageEntityCurrent, Long>, ImageEntityCurrentRepositoryCustom {

	@Query(
		value = "" +
			" select image_current_id " +
			" from images_current2tags " +
			" where tag_id in :positiveTagIds" +
			" group by image_current_id " +
			" having count(*) = :positiveTagCount" +
			" \n#pageable\n",
		countProjection="image_current_id",
		nativeQuery=true)
	Page<Long> doPositiveTagSearch(
		Pageable pageable,
		Set<Long> positiveTagIds,
		long positiveTagCount);


	@Query(
		value = "" +
			" select i.id" +
			" from images_current i " +
			" left join ( " +
			" 	select image_current_id " +
			" 	from images_current2tags " +
			" 	where tag_id in (:negativeTags)" +
			" ) i2tn on i.id = i2tn.image_current_id" +
			" where " +
			"	i2tn.image_current_id is null \n#pageable\n",
		countProjection = "i.id",
		nativeQuery=true)
	Page<Long> doNegativeTagSearch(
		Pageable pageable,
		Set<Long> negativeTags);

	@Deprecated // use ImageEntityCurrentRepositoryCustom.doDualTagSearchSmart
	@Query(
		value = "" +
			" select d.image_current_id" +
			" from" +
			"	images_current2tags d" +
			"	left join images_current2tags u on d.image_current_id = u.image_current_id" +
			"	and u.tag_id in (:negativeTags)" +
			" where" +
			"	d.tag_id in (:positiveTags)" +
			"	and u.image_current_id is null" +
			" group by d.image_current_id" +
			" having count(*) = :positiveTagCount" +
			" \n#pageable\n",
		countProjection="d.image_current_id",
		nativeQuery=true)
	Page<Long> doDualTagSearch(
		Pageable pageable,
		Set<Long> positiveTags,
		int positiveTagCount,
		Set<Long> negativeTags);

	@Query(
		value = "" +
			" select iec.id" +
			" from ImageEntityCurrent iec ",
		countProjection = "iec.id")
	Page<Long> doTagFreeSearch(Pageable pageable);

	@Transactional
	@Modifying
	@Query(
		nativeQuery=true,
		value="" +
			"update images_current " +
			"set view_count = view_count + 1 " +
			"where id = :id")
	public void incrementViewCount(long id);
}
//@formatter:on