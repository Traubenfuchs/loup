package at.loup.filemanager.repositories.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.loup.filemanager.entities.TagEntity;
import at.loup.filemanager.repositories.repositories.custom.TagRepositoryCustom;

@Repository()
public interface TagRepository extends JpaRepository<TagEntity, Long>, TagRepositoryCustom {
	TagEntity findByTag(String tag);

	Set<TagEntity> findByTagIn(String... tags);
}
