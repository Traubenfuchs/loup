package at.loup.filemanager.repositories.repositories.custom;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class ImageEntityCurrentRepositoryImpl implements ImageEntityCurrentRepositoryCustom {
	@Autowired
	private EntityManager em;

	@Override
	public Page<Long> doDualTagSearchSmart(
			Pageable pageable,
			Set<Long> positiveTags,
			Set<Long> negativeTags) {

		List<Long> positiveTagsList = new ArrayList<>(positiveTags);
		List<Long> negativeTagsList = new ArrayList<>(negativeTags);

		Collections.sort(positiveTagsList);
		Collections.sort(negativeTagsList);

		String positiveString = "%," + StringUtils.join(positiveTagsList, ",%,") + ",%";
		String negativeString = "%," + StringUtils.join(negativeTagsList, ",%,") + ",%";

		Query q = em.createNativeQuery("" +
				" select i2t.id" +
				" from images_current i2t " +
				" where " +
				" 	i2t.tag_string like '" + positiveString + "' " +
				" 	and i2t.tag_string not like '" + negativeString + "' " +
				" order by i2t.id desc " +
				" limit " + pageable.getOffset() +
				"," +
				pageable.getPageSize());

		List<Long> l = q.getResultList();

		Query q2 = em.createNativeQuery("" +
				" select count(i2t.id)" +
				" from images_current i2t " +
				" where " +
				" 	i2t.tag_string like '" + positiveString + "' " +
				" 	and i2t.tag_string not like '" + negativeString + "' ");

		Long count = ((BigInteger) q2.getSingleResult()).longValue();

		PageImpl<Long> result = new PageImpl<>(l, pageable, count);

		return result;
	}
}
