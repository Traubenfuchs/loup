package at.loup.filemanager.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "images_current")
public class ImageEntityCurrent extends ImageEntityBase {
	private static final long serialVersionUID = 3926566207755569807L;
	//@formatter:off

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "images_current2tags",
		joinColumns = @JoinColumn(name = "image_current_id"),
		inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private Set<TagEntity> tags;

	@OneToMany(cascade=CascadeType.ALL, mappedBy="lastVersion", fetch=FetchType.LAZY, orphanRemoval=true)
	private List<ImageEntityHistoric> historicImages;

	//@formatter:on
	public ImageEntityCurrent() {

	}

	@Override
	public Set<TagEntity> getTags() {
		if (tags == null) {
			tags = new HashSet<>();
		}
		return tags;
	}

	public void setTags(Set<TagEntity> tags) {
		this.tags = tags;
	}

	public List<ImageEntityHistoric> getHistoricImages() {
		if (historicImages == null) {
			historicImages = new ArrayList<>();
		}
		return historicImages;
	}

	public void setHistoricImages(List<ImageEntityHistoric> historicImages) {
		this.historicImages = historicImages;
	}
}