package at.loup.filemanager.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "images_current2tags", indexes = {
		@Index(name = "it", columnList = "image_current_id,tag_id"),
		@Index(name = "ti", columnList = "tag_id,image_current_id"),
		@Index(name = "t", columnList = "tag_id"),
		@Index(name = "i", columnList = "image_current_id")
})

public class ImagesCurrent2TagsEntity implements Serializable {
	private static final long serialVersionUID = -3647467657232707021L;
	@Id
	@Column(name = "image_current_id")
	private Long imageCurrentId;
	@Id
	@Column(name = "tag_id")
	private Long tagId;

	@ManyToOne
	@JoinColumn(name = "image_current_id")
	private ImageEntityCurrent imageEntityCurrent;

	@ManyToOne
	@JoinColumn(name = "tag_id")
	private TagEntity tagEntity;

	public ImagesCurrent2TagsEntity() {

	}

	public Long getTagId() {
		return tagId;
	}

	public Long getImageCurrentId() {
		return imageCurrentId;
	}

	public ImageEntityCurrent getImageEntityCurrent() {
		return imageEntityCurrent;
	}

	public void setImageEntityCurrent(ImageEntityCurrent imageEntityCurrent) {
		this.imageEntityCurrent = imageEntityCurrent;
	}

	public TagEntity getTagEntity() {
		return tagEntity;
	}

	public void setTagEntity(TagEntity tagEntity) {
		this.tagEntity = tagEntity;
	}
}