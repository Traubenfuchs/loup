package at.loup.filemanager.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "images_historic")
public class ImageEntityHistoric extends ImageEntityBase {
	private static final long serialVersionUID = 6471096819173218283L;
	//@formatter:off

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "images_historic2tags",
		joinColumns = @JoinColumn(name = "image_historic_Id"),
		inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private Set<TagEntity> tags;

	@ManyToOne(optional=false)
	@JoinColumn(name = "last_version")
	private ImageEntityCurrent lastVersion;

	@Column(nullable=false)
	private int imageVersion;

	//@formatter:on
	public ImageEntityHistoric() {

	}

	public int getImageVersion() {
		return imageVersion;
	}

	public void setImageVersion(int imageVersion) {
		this.imageVersion = imageVersion;
	}

	@Override
	public Set<TagEntity> getTags() {
		if (tags == null) {
			tags = new HashSet<>();
		}
		return tags;
	}

	public void setTags(Set<TagEntity> tags) {
		this.tags = tags;
	}

	public ImageEntityCurrent getLastVersion() {
		return lastVersion;
	}

	public void setLastVersion(ImageEntityCurrent lastVersion) {
		this.lastVersion = lastVersion;
	}
}