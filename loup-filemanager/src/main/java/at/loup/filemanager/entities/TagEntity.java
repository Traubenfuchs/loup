package at.loup.filemanager.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import at.loup.commons.entities.AbstractTimedEntity;

@Entity
@Table(name = "tags")
public class TagEntity extends AbstractTimedEntity<TagEntity> {
	//@formatter:off
	private static final long serialVersionUID = -2804501023650516183L;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "images_current2tags",
		joinColumns = @JoinColumn(name = "tag_id"),
		inverseJoinColumns = @JoinColumn(name = "image_current_id")
	)
	private List<ImageEntityCurrent> currentImages;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "images_historic2tags",
		joinColumns = @JoinColumn(name = "tag_id"),
		inverseJoinColumns = @JoinColumn(name = "image_historic_id")
	)
	private List<ImageEntityCurrent> historicImages;

	@Column(nullable = false, unique = true)
	private String tag;

	//@formatter:on

	public TagEntity() {
	}

	public List<ImageEntityCurrent> getCurrentImages() {
		if (currentImages == null) {
			currentImages = new ArrayList<>();
		}
		return currentImages;
	}

	public void setCurrentImages(List<ImageEntityCurrent> currentImages) {
		this.currentImages = currentImages;
	}

	public List<ImageEntityCurrent> getHistoricImages() {
		if (historicImages == null) {
			historicImages = new ArrayList<>();
		}
		return historicImages;
	}

	public void setHistoricImages(List<ImageEntityCurrent> historicImages) {
		this.historicImages = historicImages;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
}