package at.loup.filemanager.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import org.apache.commons.lang3.StringUtils;

import at.loup.commons.entities.AbstractTimedEntity;
import at.loup.security.entities.UserEntity;

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@MappedSuperclass
public abstract class ImageEntityBase extends AbstractTimedEntity<ImageEntityBase> {
	private static final long serialVersionUID = 4218930738106145858L;
	//@formatter:off

	////////////////////
	// image file data

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false,name="image_binary_id")
	private BinaryEntity imageBinary;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "preview_binary_id", nullable = false)
	private BinaryEntity previewImageBinary;

	private String fileName;

	private String mediaType;

	//////////////
	// meta data

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	private UserEntity versionCreator;

	@Column(nullable=false)
	private Boolean publicFile;

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	private UserEntity originalUploader;

	@Column(nullable=false)
	private Long viewCount;

	@Column(length = 12, nullable = false)
	private String uploaderIp;

	@Column(length=600)
	private String tagString;

	//@formatter:on

	public ImageEntityBase() {
	}

	public String getTagString() {
		return tagString;
	}

	protected void setTagString(String tagString) {
		this.tagString = tagString;
	}

	public BinaryEntity getImageBinary() {
		return imageBinary;
	}

	public void setImageBinary(BinaryEntity imageBinary) {
		this.imageBinary = imageBinary;
	}

	public BinaryEntity getPreviewImageBinary() {
		return previewImageBinary;
	}

	public void setPreviewImageBinary(BinaryEntity previewImageBinary) {
		this.previewImageBinary = previewImageBinary;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public UserEntity getVersionCreator() {
		return versionCreator;
	}

	public void setVersionCreator(UserEntity versionCreator) {
		this.versionCreator = versionCreator;
	}

	public Boolean getPublicFile() {
		return publicFile;
	}

	public void setPublicFile(Boolean publicFile) {
		this.publicFile = publicFile;
	}

	public UserEntity getOriginalUploader() {
		return originalUploader;
	}

	public void setOriginalUploader(UserEntity originalUploader) {
		this.originalUploader = originalUploader;
	}

	public Long getViewCount() {
		return viewCount;
	}

	public void setViewCount(Long viewCount) {
		this.viewCount = viewCount;
	}

	public String getUploaderIp() {
		return uploaderIp;
	}

	public void setUploaderIp(String uploaderIp) {
		this.uploaderIp = uploaderIp;
	}

	abstract public Set<TagEntity> getTags();

	public List<String> getTagsAsStrings() {
		Set<TagEntity> tags = getTags();
		List<String> result = new ArrayList<>(tags.size());
		for (TagEntity tagEntity : tags) {
			result.add(tagEntity.getTag());
		}
		return result;
	}

	@PostUpdate
	@PostPersist
	private void updateTagString() {
		Set<TagEntity> tags = getTags();
		String tagString = ","
				+ StringUtils.join(tags.stream().map(TagEntity::getId).sorted().collect(Collectors.toList()), ",")
				+ ",";
		setTagString(tagString);
	}

}