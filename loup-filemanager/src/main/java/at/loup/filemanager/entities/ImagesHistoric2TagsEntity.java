package at.loup.filemanager.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "images_historic2tags", indexes = {
		@Index(name = "it", columnList = "image_historic_id,tag_id"),
		@Index(name = "ti", columnList = "tag_id,image_historic_id"),
		@Index(name = "t", columnList = "tag_id"),
		@Index(name = "i", columnList = "image_historic_id")
})

public class ImagesHistoric2TagsEntity implements Serializable {
	private static final long serialVersionUID = -3647467657232707021L;

	@Id
	@Column(name = "image_historic_id")
	private Long imageCurrentId;

	@Id
	@Column(name = "tag_id")
	private Long tagId;

	@ManyToOne
	@JoinColumn(name = "image_historic_id")
	private ImageEntityHistoric imageEntityHistoric;

	@ManyToOne
	@JoinColumn(name = "tag_id")
	private TagEntity tagEntity;

	public ImagesHistoric2TagsEntity() {

	}

	public Long getTagId() {
		return tagId;
	}

	public Long getImageCurrentId() {
		return imageCurrentId;
	}

	public ImageEntityHistoric getImageEntityHistoric() {
		return imageEntityHistoric;
	}

	public void setImageEntityHistoric(ImageEntityHistoric imageEntityHistoric) {
		this.imageEntityHistoric = imageEntityHistoric;
	}

	public TagEntity getTagEntity() {
		return tagEntity;
	}

	public void setTagEntity(TagEntity tagEntity) {
		this.tagEntity = tagEntity;
	}
}