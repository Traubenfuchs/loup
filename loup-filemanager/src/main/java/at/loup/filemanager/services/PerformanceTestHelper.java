package at.loup.filemanager.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PerformanceTestHelper {
	private final Random random = new SecureRandom();
	private List<String> possibleTags100 = Arrays.asList("heathiest", "nonborrowing", "lumbricoid", "sugarlike",
			"claudius", "crenelating", "gatehouse", "forestage", "lunette", "justiciaries", "tachypnea", "macneice",
			"emotionalist", "steamier", "disendower", "culinarily", "actuating", "enosis", "indulging", "sinuosity",
			"banares", "defeature", "uncarnivorous", "hereditability", "sinfonietta", "imbibe", "initiatorily", "usk",
			"insulting", "undereducated", "statically", "delphyne", "invagination", "intercurl", "preearthly",
			"spermicide", "waller", "dogmatic", "washougal", "superinferred", "preexception", "squirreling", "furlong",
			"concoction", "implicity", "cylices", "sporular", "pregnable", "barrymore", "albite", "pretended", "aesir",
			"fructify", "cryometer", "jesse", "ladybird", "homerically", "ruminated", "portalled", "outpost", "resoil",
			"frolicker", "taxaceous", "thought", "pyorrheic", "dunlop", "dixie", "homologumena", "interfilamentous",
			"outbargain", "southeaster", "churlish", "undevelopmental", "allyl", "microdissection", "grittier",
			"liaotung", "hematal", "tweed", "humphrey", "cabot", "horrors", "unpleaded", "nonchronical", "proleg",
			"affectionate", "balkanising", "desulphurating", "imprudency", "arrayal", "seizable", "nipper", "depth",
			"artificially", "impend", "mudpuppy", "karol", "antitradition", "globularly", "didactically");

	private byte[] singleByte = new byte[1];

	@Autowired
	private ImageManager imageManager;

	private AtomicLong al = new AtomicLong(3893);

	public PerformanceTestHelper() {
		random.nextBytes(singleByte);
	}

	public void x() throws IOException {
		insert1000000ImagesWith100TagsEach();
	}

	public void insert1000000ImagesWith100TagsEach() throws IOException {

		byte[] spxb = Files.readAllBytes(Paths.get("C:\\Users\\akkir\\Desktop\\1px.jpeg"));

		StringBuilder tagBuilder = new StringBuilder();
		for (int i = 0; i < 100; i++) {
			tagBuilder.append(possibleTags100.get(random.nextInt(possibleTags100.size())));
			tagBuilder.append(' ');
		}

		for (int i = 0; i < 100000; i++) {
			try {
				imageManager.upload(spxb, "" + al.getAndIncrement() + ".jpg", tagBuilder.toString());
			} catch (IOException e) {
			}
		}
	}
}
