package at.loup.filemanager.services;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import at.loup.commons.exceptions.LoupException;
import at.loup.filemanager.data.CachedImage;
import at.loup.filemanager.data.FileUploadResult;
import at.loup.filemanager.entities.BinaryEntity;
import at.loup.filemanager.entities.ImageEntityCurrent;
import at.loup.filemanager.repositories.repositories.ImageEntityCurrentRepository;
import at.loup.filemanager.repositories.repositories.ImageEntityHistoricRepository;
import at.loup.filemanager.repositories.repositories.TagRepository;
import at.loup.security.data.UserDTO;
import at.loup.security.entities.UserEntity;
import at.loup.security.services.DBUserHolder;

@Component
public class ImageManager {
	@Autowired
	private ImageCache imageCache;
	@Autowired
	private DBUserHolder userHolder;
	@Autowired
	private ImageEntityCurrentRepository imageEntityCurrentRepository;
	@Autowired
	private ImageEntityHistoricRepository imageEntityHistoricRepository;
	@Autowired
	private TagRepository tagRepository;
	@Autowired
	private HttpServletRequest httpServletRequest;

	protected ResponseEntity<byte[]> loadImageResponseEntity(long fileId, boolean preview) {
		CachedImage cachedImage = imageCache.loadImage(fileId, 0);

		if (!cachedImage.isPublic()) {
			UserDTO userDTO = userHolder.getUserDTO();
			if (userDTO == null) {
				throw new LoupException(HttpStatus.UNAUTHORIZED, "You can not view this image.");
			}
		}

		byte[] resultBytes;
		if (preview) {
			resultBytes = cachedImage.getPreviewImageBytes();
		} else {
			resultBytes = cachedImage.getImageBytes();
		}

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Disposition", cachedImage.getContentDisposition());
		if (preview) {
			// preview images are always jpg
			headers.set("Content-Type", "image/jpeg");
		} else {
			headers.set("Content-Type", cachedImage.getContentType());
		}

		if (!preview) {
			imageEntityCurrentRepository.incrementViewCount(fileId);
		}

		ResponseEntity<byte[]> result = new ResponseEntity<>(resultBytes, headers, HttpStatus.OK);
		return result;
	}

	public ResponseEntity<byte[]> loadFullImageResponseEntity(long fileId) {
		ResponseEntity<byte[]> result = loadImageResponseEntity(fileId, false);
		return result;
	}

	public ResponseEntity<byte[]> loadPreviewImageResponseEntity(long fileId) {
		ResponseEntity<byte[]> result = loadImageResponseEntity(fileId, true);
		return result;
	}

	/**
	 * For testing, allows saving byte[] instead of MultipartFile
	 * 
	 * @param file
	 * @param filename
	 * @param tagString
	 * @return
	 * @throws IOException
	 */
	@Transactional
	public FileUploadResult upload(
			byte[] file,
			String filename,
			String tagString) throws IOException {
		FileUploadResult result = upload(new MultipartFile() {

			@Override
			public void transferTo(File dest) throws IOException, IllegalStateException {

			}

			@Override
			public boolean isEmpty() {
				return false;
			}

			@Override
			public long getSize() {
				return 0;
			}

			@Override
			public String getOriginalFilename() {
				return filename;
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return null;
			}

			@Override
			public String getContentType() {
				return null;
			}

			@Override
			public byte[] getBytes() throws IOException {
				return file;
			}
		}, filename, tagString);
		return result;
	}

	@Transactional
	public FileUploadResult upload(
			MultipartFile file,
			String filename,
			String tagString) throws IOException {
		byte[] bytes = file.getBytes();

		// failes if the upload data is no simple image
		try (InputStream input = new ByteArrayInputStream(bytes)) {
			try {
				BufferedImage bufferedImage = ImageIO.read(input);
				bufferedImage.toString();
			} catch (Exception ex) {
				throw new LoupException(HttpStatus.BAD_REQUEST, "Please only upload images");
			}
		}

		// detects mediaType
		Tika tika = new Tika();
		String mediaType = tika.detect(bytes);

		String usedFilename = filename;
		if (usedFilename == null || usedFilename.length() == 0) {
			usedFilename = file.getOriginalFilename();
		}
		usedFilename = sanitizeImageFileExtension(usedFilename, mediaType);

		String uploaderIp = httpServletRequest.getHeader("X-FORWARDED-FOR");
		if (uploaderIp == null) {
			uploaderIp = httpServletRequest.getRemoteAddr();
		}

		uploaderIp = uploaderIp.replaceAll(".", "");
		uploaderIp = uploaderIp.replaceAll(":", "");

		BinaryEntity binaryEntityImage = BinaryEntity.create(bytes, uploaderIp);
		BinaryEntity binaryEntityPreviewImage;

		// create the thumbnail...
		try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes)) {
			BufferedImage bufferedImage = ImageIO.read(byteArrayInputStream);
			final int originalWidth = bufferedImage.getWidth();
			final int originalHeight = bufferedImage.getHeight();

			if (originalWidth > 300 || originalHeight > 300) {

				final int usedWidth;
				final int usedHeight;

				if (originalWidth > originalHeight) {
					double factor = 300.0 / originalWidth;
					usedWidth = 300;
					usedHeight = (int) Math.round(originalHeight * factor);
				} else {
					double factor = 300.0 / originalHeight;
					usedWidth = (int) Math.round(originalWidth * factor);
					usedHeight = 300;
				}

				Image previewImage = bufferedImage.getScaledInstance(usedWidth, usedHeight, BufferedImage.SCALE_SMOOTH);

				BufferedImage bi = new BufferedImage(
						usedWidth,
						usedHeight,
						BufferedImage.TYPE_INT_RGB);

				Graphics2D g2 = bi.createGraphics();
				g2.drawImage(previewImage, 0, 0, null);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(bi, "jpg", baos);
				byte[] previewBytes = baos.toByteArray();
				binaryEntityPreviewImage = BinaryEntity.create(previewBytes, uploaderIp);
			} else {
				// image is so small we don't need to scale it down
				binaryEntityPreviewImage = binaryEntityImage;
			}
		}

		UserEntity userEntity = userHolder.getUserEntity();
		UserDTO userDTO = userHolder.getUserDTO();
		if (userDTO == null) {
			throw new IllegalStateException();
		}

		ImageEntityCurrent fileEntity = new ImageEntityCurrent();
		fileEntity.setOriginalUploader(userEntity);
		fileEntity.setPublicFile(true);
		fileEntity.setViewCount(0L);
		fileEntity.setVersionCreator(userEntity);
		fileEntity.setFileName(usedFilename);
		fileEntity.setMediaType(mediaType);
		fileEntity.setImageBinary(binaryEntityImage);
		fileEntity.setPreviewImageBinary(binaryEntityPreviewImage);
		fileEntity.setUploaderIp(uploaderIp);

		fileEntity = imageEntityCurrentRepository.save(fileEntity);

		String[] stringTags = tagString.split(" ");
		fileEntity.setTags(tagRepository.getOrCreateTagEntities(stringTags));

		// if (tagString != null) {
		// String[] stringTags = tagString.split(" ");
		// for (String stringTag : stringTags) {
		// TagEntity tagEntity = tagRepository.getOrCreateTagEntity(stringTag);
		// fileEntity.getTags().add(tagEntity);
		// }
		// }

		FileUploadResult result = new FileUploadResult();
		result.setOk(true);
		result.setId(fileEntity.getId());
		result.setUsedFilename(usedFilename);
		return result;
	}

	protected String sanitizeImageFileExtension(final String original, String tikaMediaType) {
		int lastDotIndex = original.lastIndexOf('.');
		String mediaTypeExtension = tikaMediaType.substring(6);
		mediaTypeExtension = mediaTypeExtension.toLowerCase();
		if ("jpeg".equals(mediaTypeExtension)) {
			mediaTypeExtension = "jpg";
		}

		if (lastDotIndex < 0) {
			String result = original + '.' + mediaTypeExtension;
			return result;
		} else {
			String result = original.substring(0, lastDotIndex + 1) + mediaTypeExtension;
			return result;
		}
	}

	@Transactional
	public void updateImage(long imageId, MultipartFile file, String filename, String tagString) {
		ImageEntityCurrent imageEntity = imageEntityCurrentRepository.findOne(imageId);

		imageEntity.setFileName(filename);
		imageEntity.setTags(tagRepository.tagStringToTagEntities(tagString));

		imageCache.evict(imageId);
	}
}