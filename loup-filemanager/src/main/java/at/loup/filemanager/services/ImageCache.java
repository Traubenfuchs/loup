package at.loup.filemanager.services;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import at.loup.commons.exceptions.LoupException;
import at.loup.filemanager.data.CachedImage;
import at.loup.filemanager.data.ImageSearchResult.ImageSearchResultImage;
import at.loup.filemanager.entities.ImageEntityBase;
import at.loup.filemanager.repositories.repositories.ImageEntityCurrentRepository;
import at.loup.filemanager.repositories.repositories.ImageEntityHistoricRepository;

@Component
public class ImageCache {
	@Autowired
	private ImageEntityCurrentRepository imageEntityCurrentRepository;

	@Autowired
	private ImageEntityHistoricRepository imageEntityHistoricRepository;

	private final LoadingCache<ImageCacheKey, CachedImage> loadingCache;

	public ImageCache() {
		this(268435456); // 268435456 bytes = 256 megabytes
	}

	public ImageCache(long cacheSizeLimit) {
		loadingCache = CacheBuilder.newBuilder()
				.concurrencyLevel(6)
				.maximumWeight(cacheSizeLimit)
				.expireAfterAccess(1, TimeUnit.MINUTES)
				.weigher((key, value) -> {
					CachedImage cachedImage = (CachedImage) value;
					return cachedImage.getByteSize();
				})
				.build(new CacheLoader<ImageCacheKey, CachedImage>() {
					@Override
					public CachedImage load(ImageCacheKey key) throws Exception {
						ImageEntityBase imageEntity;

						if (key.getVersion() == 0) {
							imageEntity = imageEntityCurrentRepository.findOne(key.getId());
						} else {
							imageEntity = imageEntityHistoricRepository.findByLastVersionAndImageVersion(key.getId(),
									key.getVersion());
						}

						if (imageEntity == null) {
							return null;
						}

						CachedImage cachedImage = CachedImage.create(imageEntity);
						return cachedImage;
					}
				});

	}

	public ImageSearchResultImage loadImageSearchResult(long imageId, int imageVersion) {
		CachedImage cachedImage = loadImage(imageId, imageVersion);

		ImageSearchResultImage result = ImageSearchResultImage.createFromCachedImage(cachedImage);

		return result;
	}

	/**
	 *
	 * @param imageId
	 * @return true if the image was evicted, false if it didn't exist
	 */
	public void evict(long imageId) {
		loadingCache.invalidate(ImageCacheKey.create(imageId, 0));
	}

	public CachedImage loadImage(long imageId, int imageVersion) {
		CachedImage result;
		try {
			result = loadingCache.get(ImageCacheKey.create(imageId, imageVersion));
			return result;
		} catch (ExecutionException ex) {
			throw new LoupException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Image<" + imageId + "> with version<" + imageVersion + "> could not be loaded.", ex);
		}
	}

	private static class ImageCacheKey {
		private final long id;
		private final int version;

		public ImageCacheKey(long id, int version) {
			this.id = id;
			this.version = version;
		}

		public long getId() {
			return id;
		}

		public int getVersion() {
			return version;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (id ^ (id >>> 32));
			result = prime * result + version;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			ImageCacheKey other = (ImageCacheKey) obj;
			if (id != other.id) {
				return false;
			}
			if (version != other.version) {
				return false;
			}
			return true;
		}

		public static ImageCacheKey create(long id, int version) {
			return new ImageCacheKey(id, version);
		}
	}
}