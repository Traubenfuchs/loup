package at.loup.filemanager.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import at.loup.filemanager.data.ImageSearchRequest;
import at.loup.filemanager.data.ImageSearchResult;
import at.loup.filemanager.repositories.repositories.ImageEntityCurrentRepository;
import at.loup.filemanager.repositories.repositories.ImageEntityHistoricRepository;
import at.loup.filemanager.repositories.repositories.TagRepository;

@Service
public class SearchService {
	@Autowired
	private ImageEntityCurrentRepository imageEntityCurrentRepository;
	@Autowired
	private ImageEntityHistoricRepository imageEntityHistoricRepository;
	@Autowired
	private ImageCache imageCache;
	@Autowired
	private TagRepository tagRepository;

	private final static Page<Long> defaultEmptyPage = new PageImpl<>(new ArrayList<Long>());

	public SearchService() {

	}

	@Async
	public Future<String> asyncMethod() {
		return new AsyncResult<>("hello");

	}

	public ImageSearchResult searchImage(ImageSearchRequest imageSearchRequest) {
		String[] seachStringWords = imageSearchRequest.getSearchString().split(" ");
		Set<String> positiveTags = new HashSet<>();
		Set<String> negativeTags = new HashSet<>();
		for (String tag : seachStringWords) {
			if (tag.length() == 0 || "-".equals(tag)) {
				continue;
			}
			tag = tag.trim();
			if (tag.charAt(0) == '-') {
				negativeTags.add(tag.substring(1));
			} else {
				positiveTags.add(tag);
			}
		}
		Set<Long> positiveTagIds = tagRepository.tagsToIds(positiveTags);
		Set<Long> negativeTagIds = tagRepository.tagsToIds(negativeTags);

		Page<Long> imageIdPage = null;

		if (positiveTags.size() != positiveTagIds.size()) {
			// looked for positive tag does not exist
			imageIdPage = defaultEmptyPage;
		} else if (positiveTagIds.size() == 0 && negativeTagIds.size() == 0) {
			PageRequest pageRequest = new PageRequest(
					imageSearchRequest.getPage() - 1,
					imageSearchRequest.getLimit(),
					imageSearchRequest.getDirection(),
					"id");
			imageIdPage = imageEntityCurrentRepository.doTagFreeSearch(pageRequest);

		} else if (positiveTagIds.size() == 0) {

			PageRequest pageRequest = new PageRequest(
					imageSearchRequest.getPage() - 1,
					imageSearchRequest.getLimit(),
					imageSearchRequest.getDirection(),
					"id");

			imageIdPage = imageEntityCurrentRepository.doNegativeTagSearch(
					pageRequest,
					negativeTagIds);

		} else if (negativeTagIds.size() == 0) {
			PageRequest pageRequest = new PageRequest(
					imageSearchRequest.getPage() - 1,
					imageSearchRequest.getLimit(),
					imageSearchRequest.getDirection(),
					"image_current_id");

			imageIdPage = imageEntityCurrentRepository.doPositiveTagSearch(
					pageRequest,
					positiveTagIds,
					positiveTagIds.size());
		} else {
			PageRequest pageRequest = new PageRequest(
					imageSearchRequest.getPage() - 1,
					imageSearchRequest.getLimit(),
					imageSearchRequest.getDirection(),
					"image_current_id");

			imageIdPage = imageEntityCurrentRepository.doDualTagSearchSmart(
					pageRequest,
					positiveTagIds,
					negativeTagIds);
		}

		ImageSearchResult result = ImageSearchResult.createFromEntities(imageIdPage, imageCache);

		return result;
	}
}
