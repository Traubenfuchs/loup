package at.loup.fun;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import at.loup.security.aspects.annotations.RecaptchaProtected;

@RestController
public class RecaptchaTestController {
	@RecaptchaProtected
	@PostMapping(path = "/api/csrfprotected")
	public String x() {
		return "ok";
	}
}