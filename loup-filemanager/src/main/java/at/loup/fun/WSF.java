package at.loup.fun;

import java.util.concurrent.TimeUnit;

import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class WSF extends TextWebSocketHandler {
	final LoadingCache<String, Integer> lc;

	public WSF() {
		lc = CacheBuilder.newBuilder()
				.concurrencyLevel(6)
				.maximumSize(1000)
				.expireAfterAccess(1, TimeUnit.MINUTES)
				.removalListener(notification -> {

				})
				.build(new CacheLoader<String, Integer>() {

					@Override
					public Integer load(String arg0) throws Exception {
						return 2;
					}
				});

	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		// TODO Auto-generated method stub
		super.handleMessage(session, message);

	}
}
