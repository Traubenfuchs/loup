package at.loup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

import at.loup.commons.config.LoupCommonsConfig;
import at.loup.commons.js.ClosureSource;
import at.loup.commons.js.FixedClosureSource;

@Configuration
public class LoupFilemanagerConfig {
	@Autowired
	private Environment env;

	public LoupFilemanagerConfig() {

	}

	@Bean
	@Primary
	public ClosureSource schlurprClosureSource(LoupCommonsConfig loupCommonsConfig) {
		final FixedClosureSource result = FixedClosureSource.create(loupCommonsConfig);
		if (env.acceptsProfiles("prod")) {
			result.addSource("app2.js",
					"libraries/vue.min.js",
					"libraries/vue-router.min.js",
					"libraries/vuex.min.js",

					"loup.js",
					"libraries/vuex-router-sync.js",
					"loup-security2.js",

					"searchComponent.js",
					"uploadComponent.js",
					"viewComponent.js",
					"galleryComponent.js",
					"loup-pager.js",
					"usermanagementComponent.js",
					"fileManager.js");
		} else {
			result.addSource("app2.js",
					"libraries/vue.js",
					"libraries/vue-router.js",
					"libraries/vuex.js",

					"loup.js",
					"loup-pager.js",
					"libraries/vuex-router-sync.js",
					"loup-security2.js",

					"searchComponent.js",
					"uploadComponent.js",
					"viewComponent.js",
					"galleryComponent.js",
					"usermanagementComponent.js",
					"fileManager.js");
		}

		result.addSource("app3.js",
				"libraries/vue.js",
				"libraries/vue-router.js",
				"libraries/vuex.js",

				"loup.js",
				"libraries/vuex-router-sync.js",
				"loup-security2.js",
				"loup-pager.js",
				"usermanagementComponent.js",
				"vueExample.js");

		return result;
	};
}