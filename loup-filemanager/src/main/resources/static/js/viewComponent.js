Loup.Dependencer.define('view-component', () => {

	return {
		name: 'view-component',
		template: `
		<div id='view'>
			<div id="image-container">
				<div id="image-wrapper"><a v-bind:href="'/file/' + imageData.id"><img id="image" v-bind:src="image"/></a></div>
			</div>
			<div id='imageDetailArea'>
				<div class='infobox'>
					<p class='filename'>filename: {{imageData.filename}}</p>
					<p>filesize: {{Math.round(imageData.imageSize/1024)}} kb</p>
					<p>views: {{Math.round(imageData.viewCount)}}</p>
					<p>tags: </p>
					<div id='tagLinkContainer'>
						<router-link class='tagLink' v-bind:to="'/gallery/50/1/' + tag" v-for='tag in imageData.tags'>{{tag}}</router-link>
					</div>
				</div>
				<div v-on:click='toggleEditImage' v-if='!imageEditorOpen'>edit</div>

				<div id='imageEdit' v-if='imageEditorOpen'>
					<div v-on:click.stop='toggleEditImage'>close</div>
					<label for='filename'>filename:</label>
					<input id='filename' v-model='imageData.filename' placeholder='filename' />
					<div class='tagLine' v-for='tag in imageData.tags'>
						<div>{{tag}}</div>
						<div v-on:click.stop='deleteTag(tag)'>x</div>
					</div>
					<label for='newTag'>new tag:</label>
					<input id='newTag' v-model='newTag'/>
					<div v-on:click.stop='addNewTag(newTag)'>add tag</div>
					<div v-on:click.stop='save'>save</div>
				</div>
			</div>
		</div>
		`,
		data() {
			const result = {
				image: `/file/${this.$router.currentRoute.params.imageId}`,
				imageData: this.$router.currentRoute.params.imageData || {},
				imageEditorOpen: false,
				newTag: ''
			}
			return result
		},
		created() {
			this.loadImageData();
		},
		methods: {
			loadImageData() {
				Loup.Ajax.get(
					`/file/${this.$router.currentRoute.params.imageId}/info`,
					undefined,
					r => {
						this.imageData = JSON.parse(r.responseText)
						this.imageData.tags.sort()
					}
				)
			},
			toggleEditImage() {
				this.imageEditorOpen = !this.imageEditorOpen
			},
			deleteTag(tag) {
				l.log(`Deleting tag ${newTag}`)
				const indexOfTag = this.imageData.tags.indexOf(tag)
				if (indexOfTag < 0) {
					return;
				}
				this.imageData.tags.splice(indexOfTag, 1)
			},
			addNewTag(newTag) {
				l.log(`Adding tag ${newTag}`)
				const indexOfTag = this.imageData.tags.indexOf(newTag)
				if (indexOfTag < 0) {
					this.imageData.tags.push(newTag)
					this.imageData.tags.sort()
				}
				this.newTag = ''
			},
			save() {
				l.log('saving image edit...')

				const formData = new FormData();

				formData.append('filename', this.imageData.filename);
				formData.append('tagString', this.imageData.tags.join(' '));


				this.imageEditorOpen = false
				Loup.Ajax.post(`/file/${this.$router.currentRoute.params.imageId}/info`,
					formData,
					r => {
						l.log('image edit saved successfully!')
					},
					r => {
						l.log('image edit saving error!')
					},
					r => {
						this.loadImageData()
					})
			}
		}
	}
});