window.onload = function () {
	const uniqueIdentifier = 'x'
	const dependencer = Loup.Dependencer.load('loup-dependencer')
	const l = dependencer.load('loup-logger')
	const ajax = dependencer.load('loup-ajax')
	l.log(`starting up vue application. Vue.js version<${Vue.version}>, uniqueIdentifier<${uniqueIdentifier}>`)

	const store = new Vuex.Store({
		state: {
			loggedIn: false,
			user: undefined,
			scrollPosition: undefined
		},
		mutations: {
			setLoggedInUser(state, user) {
				state.loggedIn = user ? true : false
				state.user = user
			},
			setSavedScrollPosition(state, scrollPosition) {
				state.scrollPosition = scrollPosition
			}
		}
	})

	const usermanagementComponent = Loup.Dependencer.load('usermanagement-component')
	const testComponent = Loup.Dependencer.load('test-component')

	const router = new VueRouter({
		base: 'x', // if your browser base address for this application is http://localhost/XXX/, enter XXX as base
		mode: 'history', // hash (with # url) / history (you usually want this) / abstract (server side)
		routes: [{
			path: '/usermanagement',
			component: usermanagementComponent
		}, {
			path: '/x',
			component: testComponent
		}],
		scrollBehavior(to, from, savedPosition) {
			if (savedPosition) {
				store.commit('setSavedScrollPosition', savedPosition)
				return savedPosition
			} else {
				store.commit('setSavedScrollPosition', undefined)
				return false
			}
		}
	})

	const vue = new Vue({
		name: 'fileManager-application',
		el: '#vueapp',
		template: `
		<div id='content-footer-flexer'>
				<div id='menubar'>
					<div id='menubar-buttons'>
						<router-link class='menubar-easybutton' v-if='$store.state.loggedIn' to='/'>HOME</router-link>
						<router-link class='menubar-easybutton' v-if='$store.state.loggedIn' to='/x'>XXXXXXXXXX</router-link>
						<div class='menubar-easybutton' v-if='!$store.state.loggedIn' v-on:click='showLogin'>login</div>
						<div class='menubar-easybutton' v-if='!$store.state.loggedIn' v-on:click='showRegister'>register</div>
						<div class='menubar-easybutton' v-if='$store.state.loggedIn' v-on:click='logout'>logout</div>
					</div>
					<div id='loginRegister'>
						<div id='login' v-if='loginVisible'>
							<input type='text' v-model='loginUsername' placeholder='username'/>
							<input type='password' v-model='loginPassword' placeholder='password'/>
							<label for='loginRememberMe'>Remember Me</label>
							<input id='loginRememberMe' type='checkbox' v-model='loginRememberMe' />
							<button class='menubar-easybutton' v-on:click='login'>login</button>
						</div>
						<div id='register' v-if='registerVisible'>
							<input type='text' v-model='registerUsername' placeholder='username'>
							<input type='password' v-model='registerPassword' placeholder='password'>
							<label for='registerRememberMe'>Remember Me</label>
							<input id='registerRememberMe' type='checkbox' v-model='registerRememberMe' />
							<button class='menubar-easybutton' v-on:click='register'>register</button>
						</div>
					</div>

				</div>
				<div id='content'>
					<router-view class='router-view'/>
				</div>
				<div id='footer'>
					<p>I am the footer</p>
				</div>
			</div>
		`	,
		router: router,
		store: store,
		data: {
			loginUsername: '',
			loginPassword: '',
			loginRememberMe: true,

			registerUsername: '',
			registerPassword: '',
			registerRememberMe: false,

			registerVisible: false,
			loginVisible: false,

			loginRegisterErrorMessage: '',
		},
		methods: {
			showLogin() {
				this.loginVisible = !this.loginVisible
				this.registerVisible = false
			},
			showRegister() {
				this.registerVisible = !this.registerVisible
				this.loginVisible = false
			},
			login() {
				l.log('Attempting login...')
				this.loginRegisterErrorMessage = ''
				Loup.Security.login(
					this.loginUsername,
					this.loginPassword,
					this.loginRememberMe,
					r => {
						l.log('logged in!')
						this.$store.commit('setLoggedInUser', r.responseObject.userDTO)
						this.registerVisible = false
						this.loginVisible = false
					},
					r => {
						l.log('Login error: ' + r.responseText)
						this.$store.commit('setLoggedInUser', undefined)
					}
				)

			},
			logout() {
				l.log('Attempting logout...')
				this.loginRegisterErrorMessage = ''
				store.commit('setLoggedInUser', undefined)
				Loup.Security.logout(
					r => {
						Loup.Security.refreshCSRF(
							r => {

							})
					})
			},
			register() {
				l.log('Attempting registration...')

				this.loginRegisterErrorMessage = ''

				Loup.Ajax.post('/api/register',
					{
						username: this.registerUsername,
						password: this.registerPassword,
						rememberMe: this.registerRememberMe
					},
					r => {

					},
					r => {

					},
					r => {
						this.checkLogin()
					}
				)
			},
			rememberMeLogin() {
				this.loginRegisterErrorMessage = ''
				Loup.Security.rememberMe(
					r => {
						this.registerVisible = false
						this.loginVisible = false
						this.$store.commit('setLoggedInUser', r.responseObject.userDTO)
						l.log('Remember me login success!')
					},
					r => {
						l.log('Remember me login failure!')
					})
			}
		},
		created() {
			this.rememberMeLogin()
		}
	})
	Loup.Vue.sync(store, router)
	l.log('vue application startup finished.')
};