Loup.Dependencer.define('gallery-component', () => {
	const dependencer = Loup.Dependencer.load('loup-dependencer')
	const l = dependencer.load('loup-logger')
	const ajax = dependencer.load('loup-ajax')
	const pager = dependencer.load('loup-pager')

	const component = {
		template: `
		<div>

			<div class='searchComponent-pageSelectorContainer'>
					<router-link exact
						class='searchComponent-pageSelector'

						v-for='link in leftLinks'
						v-bind:to='link.to'
						v-bind:class="{'pager-link-active': link.active}">{{link.displayPage}}
					</router-link>
					<div v-if='centerLinks.length !== 0'>...</div>
					<router-link exact
						class='searchComponent-pageSelector'

						v-for='link in centerLinks'
						v-bind:to='link.to'
						v-bind:class="{'pager-link-active': link.active}">{{link.displayPage}}
					</router-link>
					<div v-if='rightLinks.length !== 0'>...</div>
					<router-link exact
						class='searchComponent-pageSelector'

						v-for='link in rightLinks'
						v-bind:to='link.to'
						v-bind:class="{'pager-link-active': link.active}">{{link.displayPage}}
					</router-link>
				</div>

			<div id='galleryImageContainer'>
				<div v-for='image in images' class='galleryImage' v-bind:key='image.id'>
					<router-link tag='div' v-bind:to="'/view/' + image.id" class='searchComponent-imgLinkBox'>
						<div
							class='searchComponent-img'
							v-bind:style="{backgroundImage : 'url(' + image.previewImageUrl + ')'}"
							v-on:mouseenter="imageMouseEnter(image)"/>
					</router-link>
					<p class='searchComponent-displayedFilename' v-if='showNames' v-bind:title='image.filename'>{{ image.displayedFilename }} </p>
				</div>
			</div>

			<div class='searchComponent-pageSelectorContainer'>
					<router-link exact
						class='searchComponent-pageSelector'

						v-for='link in leftLinks'
						v-bind:to='link.to'
						v-bind:class="{'pager-link-active': link.active}">{{link.displayPage}}
					</router-link>
					<div v-if='centerLinks.length !== 0'>...</div>
					<router-link exact
						class='searchComponent-pageSelector'

						v-for='link in centerLinks'
						v-bind:to='link.to'
						v-bind:class="{'pager-link-active': link.active}">{{link.displayPage}}
					</router-link>
					<div v-if='rightLinks.length !== 0'>...</div>
					<router-link exact
						class='searchComponent-pageSelector'

						v-for='link in rightLinks'
						v-bind:to='link.to'
						v-bind:class="{'pager-link-active': link.active}">{{link.displayPage}}
					</router-link>
				</div>
		</div>
		`,
		data() {
			return {
				transition: '',
				showNames: true,

				leftLinks: [],
				centerLinks: [],
				rightLinks: [],
			}
		},
		props: {
			images: {
				type: Array,
				required: true,
				default: () => []
			},
			currentPage: {
				type: Number,
				required: true,
				default: 0
			},
			pageCount: {
				type: Number,
				required: true,
				default: 0
			},
			pageSize: {
				type: Number,
				required: true,
				default: 0
			},
			generatePagingLink: {
				type: Function,
				required: true,
				default: () => ''
			}
		},
		watch: {
			images(newData) {
				for (let image of this.images) {
					if (image.filename.length > 30) {
						image.displayedFilename = `${image.filename.substring(0, 30)}...`
					} else {
						image.displayedFilename = image.filename
					}
					this.recalculatePager()
				}
			}
		},
		methods: {
			recalculatePager() {
				const ppp = pager.getPages({
					maxElementsPerPage: this.pageSize,
					pagesThatExist: this.pageCount,
					currentPage: this.currentPage
				}, this.generatePagingLink)

				this.leftLinks = ppp.leftLinks
				this.centerLinks = ppp.centerLinks
				this.rightLinks = ppp.rightLinks
			},
			imageMouseEnter(image) {
				if (image.previewImageUrl === image.imageUrl) {
					return
				}
				const img = new Image()
				img.onload = () => {
					image.previewImageUrl = image.imageUrl
				}
				img.src = image.imageUrl
			}
		}
	}
	Vue.component('gallery-component', component)

	return component
});