Loup.Dependencer.define('search-component', () => {
	const dependencer = Loup.Dependencer.load('loup-dependencer')
	const l = dependencer.load('loup-logger')
	const ajax = dependencer.load('loup-ajax')

	return {
		name: 'search-component',
		template: `
			<div id='search'>
				<input id='searchText' type='search' placeholder='space separated search' v-model='TagSearchStringModel'/>
				<gallery-component
					v-bind:images='images'
					v-bind:currentPage='currentPage'
					v-bind:pageCount='pageCount'
					v-bind:pageSize='pageSize'
					v-bind:generatePagingLink='generatePagingLink'
				></gallery-component>
			</div>
		`,
		data() {
			return {
				images: [],
				tagSearchStringModel: this.$router.currentRoute.params.urlSearchString,

				pageSize: parseInt(this.$router.currentRoute.params.pageSize),
				pageNumber: parseInt(this.$router.currentRoute.params.pageNumber),

				opacity: 0,
				transition: '',
				showNames: true,
				currentPage: 0,
				pageCount: 0,

				imageLoadId: 0
			}
		},
		computed: {
			TagSearchStringModel: {
				get() {
					return this.tagSearchStringModel
				},
				set(newValue) {
					if (!newValue) {
						newValue = ''
					}

					if (newValue === this.$router.currentRoute.params.urlSearchString) {
						return
					}

					this.tagSearchStringModel = newValue

					this.$router.push(`/gallery/${this.$router.currentRoute.params.pageSize}/1/${newValue}`)
				}
			}
		},
		mounted() {
			this.searchForAndDisplay(this.$router.currentRoute.params.urlSearchString)
		},
		methods: {
			generatePagingLink(i, currentPage, pageSize, pageCount) {
				let urlSearchString = this.$router.currentRoute.params.urlSearchString
				urlSearchString = !urlSearchString || urlSearchString.length === 0 ? '' : '/' + urlSearchString
				return `/gallery${urlSearchString}/${this.$router.currentRoute.params.pageSize}/${i + 1}`
			},
			searchForAndDisplay(tagString) {
				const usedImageLoadId = ++this.imageLoadId

				if (tagString !== this.$router.currentRoute.params.urlSearchString) {
					l.log('Image Waiter aborted because tag changed.')
					return
				}
				const stopwatch = new Loup.Stopwatch(true)

				ajax.get(
					'/api/searchImage',
					{
						searchString: tagString || '',
						limit: this.$router.currentRoute.params.pageSize,
						page: this.$router.currentRoute.params.pageNumber
					},
					request => {
						if (this.imageLoadId !== usedImageLoadId) {
							l.log('Discarding outdated search result...')
							return
						}
						if (tagString !== this.$router.currentRoute.params.urlSearchString) {
							l.log(`Not displaying returned image search result for<${tagString}> because the result is outdated.`)
							return
						}

						const parsedResult = JSON.parse(request.responseText)

						this.currentPage = parsedResult.currentPage
						this.pageCount = parsedResult.pagesThatExist
						this.pageSize = parsedResult.maxElementsPerPage
						this.images = parsedResult.images
					},
					request => l.error(`Tag search for <${newValue}> failed.`)
					,
					request => l.log(`Loading search results took<${stopwatch.elapsedSeconds}> seconds.`)
				)
			}
		},
		watch: {
			'$route': function (to, from) {
				if (to.fullPath === from.fullPath) {
					l.log(`Current route<${to.fullPath}> loaded again, doing nothing...`)
				} else {
					this.searchForAndDisplay(this.$router.currentRoute.params.urlSearchString)
				}
			}
		}
	}
});