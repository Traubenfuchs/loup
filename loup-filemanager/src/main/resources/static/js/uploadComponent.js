Loup.Dependencer.define('upload-component', () => {
	const blankPixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
	const dependencer = Loup.Dependencer.load('loup-dependencer')
	const l = dependencer.load('loup-logger')
	const ajax = dependencer.load('loup-ajax')

	return {
		name: 'upload-component',
		template: `
			<div id='upload'>

			<div id='uploadComponent-saveButtonContainer'>
					<button id='uploadComponent-saveButton' v-on:click='uploadAll' v-bind:disabled='uploadAllLocked'>upload</button>
			</div>

			<div class='uploadComponent' v-for='fileBox in fileBoxes' v-bind:key='fileBox.fileComponentKey'>
					<div class='uploadComponent-inputFlexerContainer'>
						<div class='uploadComponent-inputFlexer'>
							<div class='uploadComponent-labelInputFlexer'>
								<label v-bind:for="'desiredName_' + fileBox.fileComponentKey">Filename</label>
								<input v-bind:id="'desiredName_' + fileBox.fileComponentKey" type='text' placeholder='desired filename' v-model='fileBox.filename'/>
							</div>
							<div class='uploadComponent-labelInputFlexer'>
								<label v-bind:for="'tagString_' + fileBox.fileComponentKey">Tags</label>
								<input v-bind:id="'tagString_' + fileBox.fileComponentKey" type='text' placeholder='space separated tags' v-model='fileBox.tagString'/>
							</div>
							<input v-bind:id="'file_' + fileBox.fileComponentKey" class='uploadComponent-fileSelector' type='file' v-on:change='fileChanged(fileBox.fileComponentKey)'/>
							<p class='uploadComponent-errorMessage'>{{fileBox.errorMessage}}</p>
						</div>
					</div>
					<div>
						<img class='uploadComponent-imagePreview' v-bind:id="'preview' + fileBox.fileComponentKey" v-bind:src='fileBox.previewSrc'/>
					</div>
				</div>
			</div>
		</div>
		`,
		data: () => {
			return {
				nextFileComponentKey: 2,
				uploadAllLocked: false,
				fileComponentKeysToBeUploaded: [],
				fileBoxes: {
					1: {
						fileComponentKey: 1,
						filename: undefined,
						tagString: '',
						errorMessage: undefined,
						previewSrc: blankPixel
					}
				}
			}
		},
		created() {

		},
		methods: {
			fileChanged(fileComponentKey) {
				const fileInput = document.getElementById(`file_${fileComponentKey}`)

				const fileBox = this.fileBoxes[fileComponentKey]
				fileBox.errorMessage = ''
				const wasEmpty = !fileBox.filename
				const isEmpty = fileInput.files.length === 0

				if (wasEmpty && isEmpty) {
					return
				}

				const nextComponentKey = this.nextFileComponentKey++

				if (wasEmpty) {
					Vue.set(this.fileBoxes, nextComponentKey, {
						fileComponentKey: nextComponentKey,
						filename: undefined,
						tagString: '',
						errorMessage: undefined,
						previewSrc: blankPixel,
					})
				}

				const file = fileInput.files[0]

				reader = new FileReader()
				reader.onload = e => {
					this.previewSrc = e.target.result
					fileBox.previewSrc = e.target.result
				}
				reader.readAsDataURL(file)

				fileBox.filename = file.name
				//fileBox.tagString = document.getElementById(`tagString_${fileComponentKey}`)
				fileBox.errorMessage = ''
			},
			uploadAll() {
				l.log('uploadAll called...')
				if (this.uploadAllLocked) {
					l.log('Attempted to upload all while upload all was in progress!')
					return;
				}
				this.uploadAllLocked = true

				this.fileComponentKeysToBeUploaded = Object.keys(this.fileBoxes)
				this.attemptNextUpload()
			},
			attemptNextUpload() {
				const next = this.fileComponentKeysToBeUploaded.shift()

				if (next) {
					l.log(`Attempting to upload file number <${next}>.`)
					const obj = this.fileBoxes[next]
					const fileDOM = document.getElementById(`file_${obj.fileComponentKey}`)

					if (fileDOM.files.length !== 0) {
						const file = fileDOM.files[0]

						const formData = new FormData()

						formData.append('file', file)
						formData.append('filename', obj.filename)
						formData.append('tagString', obj.tagString)

						l.log('Executing upload...')

						ajax.post(
							'/file',
							formData,
							r => {
								const response = JSON.parse(r.responseText)
								l.log(`Upload for file with fileComponentKey<${this.fileComponentKey}> with filename<${this.desiredName}> ok, image id is <${response.id}>.`)

								Vue.delete(this.fileBoxes, next)
							}, r => {
								this.errorMessage = 'File could not be uploaded...'
								l.log(`Upload for file with fileComponentKey<${this.fileComponentKey}> with filename<${this.desiredName}> failed.`)
								obj.errorMessage = 'ERROR'
							}, r => {
								l.log(`upload response<${r.responseText}>'`)
								this.attemptNextUpload()
							})
					} else {
						this.attemptNextUpload()
					}
				} else {
					this.uploadAllLocked = false
				}

			}
		}
	}
});