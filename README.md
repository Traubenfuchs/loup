# Loup

## loup-commons
Reuseable classes for spring boot applications that I use a lot.
Highlights:
* JPA base entities: Created at, last modified at, etc.
* Controller that offers concatenation and compilation of .js files with Google's closure compiler, creating a somewhat modern .js workflow without npm.
* Controller that offers concatenation and compilation of .css and SASS files via jsass.
* C# inspired interfaces to replace the tragedy that is java.util.function.*: Actions (void methods), Func's (non void methods) and even variants of them that can throw exceptions.
* Spring lifecycle aware thread base classes.

## loup-security
Nearly configuration free poor man's version of spring-security that is the perfect partner for single page applications and (REST) APIs. If you directly use loup-security without loup-security-db you must configure a remote loup-security-db server.
Highlights:
* Offers both server-mode and client-mode: Your user database and your application can run on different servers. 
* AJAX based user login via username/email and password.
* Built in CSRF protection.
* Remember-me cookie functionality.
* Advanced method and class protection via annotations, custom SecurityConditions or simple string based rights.
* apiKey based, stateless alternative to login, perfect for API clients.
* User registration with email activation.

## loup-security-db
Offers direct access to a security DB and optionally provides that acces to remote loup-security applications. Directly accesses a SQL database.