package at.loup.security.data;

import at.loup.commons.utilities.ArgumentRuleUtilities;

public class LoginResult {
	private ELoginResult eLoginResult;
	private UserDTO userDTO;
	private String loginMessage;
	private String rememberMeToken;

	protected LoginResult() {

	}

	public ELoginResult geteLoginResult() {
		return eLoginResult;
	}

	public void seteLoginResult(ELoginResult eLoginResult) {
		this.eLoginResult = eLoginResult;
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public String getErrorMessage() {
		return loginMessage;
	}

	public void setLoginMessage(String loginMessage) {
		this.loginMessage = loginMessage;
	}

	public String getRememberMeToken() {
		return rememberMeToken;
	}

	public void setRememberMeToken(String rememberMeToken) {
		this.rememberMeToken = rememberMeToken;
	}

	public static LoginResult createSuccessViaUsernamePassword(UserDTO userDTO) {
		ArgumentRuleUtilities.notNull("userDTO", userDTO);

		LoginResult result = new LoginResult();
		result.setUserDTO(userDTO);
		result.seteLoginResult(ELoginResult.loggedInViaUsernamePassword);
		return result;
	}

	public static LoginResult createAlreadyLoggedIn() {
		LoginResult result = new LoginResult();
		result.seteLoginResult(ELoginResult.alreadyLoggedIn);
		return result;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public enum ELoginResult {
		loggedInViaUsernamePassword(true, "You are now logged in with your username and password."),
		loggedInViaRememberMe(true, "You are now logged in with your remember me token."),
		// authenticationError(false, "An error occured."),
		alreadyLoggedIn(false, "You are already logged in."),
		incorrectPassword(false, "Username does not exist or password is incorrect!"),
		usernameDoesNotExist(false, "Username does not exist or password is incorrect!"),
		notActivated(false, "Your user must be activated first!"),
		noUserData(false, "Required request data missing."),
		cantRemember(false, "No remember token or invalid remember me token.");

		private final boolean success;
		private final String loginMessage;

		private ELoginResult(boolean success, String loginMessage) {
			this.success = success;
			this.loginMessage = loginMessage;
		}

		public boolean isSuccess() {
			return success;
		}

		public String getLoginMessage() {
			return loginMessage;
		}

		public LoginResult createLoginResult(UserDTO userDTO, String rememberMeToken) {
			LoginResult result = new LoginResult();

			result.seteLoginResult(this);
			result.setLoginMessage(loginMessage);
			result.setRememberMeToken(rememberMeToken);
			result.setUserDTO(userDTO);

			return result;
		}

		public LoginResult createLoginResult(UserDTO userDTO) {
			LoginResult result = new LoginResult();

			result.seteLoginResult(this);
			result.setLoginMessage(loginMessage);
			result.setUserDTO(userDTO);

			return result;
		}

		public LoginResult createLoginResult() {
			LoginResult result = new LoginResult();

			result.seteLoginResult(this);
			result.setLoginMessage(loginMessage);

			return result;
		}
	}
}
