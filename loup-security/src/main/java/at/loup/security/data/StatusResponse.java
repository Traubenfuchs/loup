package at.loup.security.data;

public class StatusResponse {
	private String csrfToken;
	private UserDTO userDTO;

	public StatusResponse(String csrfToken, UserDTO userDTO) {
		this.csrfToken = csrfToken;
		this.userDTO = userDTO;
	}

	public String getCsrfToken() {
		return csrfToken;
	}

	public void setCsrfToken(String csrfToken) {
		this.csrfToken = csrfToken;
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}
}