package at.loup.security.data.reqres;

import at.loup.commons.utilities.ArgumentRuleUtilities;

public class LoadUserViaCredentialsRequest {
	private String usernameOrEmail;
	private String password;
	private Boolean rememberMe;

	protected LoadUserViaCredentialsRequest() {

	}

	public String getUsernameOrEmail() {
		return usernameOrEmail;
	}

	public void setUsernameOrEmail(String usernameOrEmail) {
		this.usernameOrEmail = usernameOrEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getRememberMe() {
		if (rememberMe == null) {
			return false;
		}
		return rememberMe;
	}

	public void setRememberMe(Boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public static LoadUserViaCredentialsRequest create(String usernameOrEmail, String password, boolean rememberMe) {
		ArgumentRuleUtilities.notNull("usernameOrEmail", usernameOrEmail);
		ArgumentRuleUtilities.notNull("password", password);
		LoadUserViaCredentialsRequest result = new LoadUserViaCredentialsRequest();
		result.setUsernameOrEmail(usernameOrEmail);
		result.setPassword(password);
		result.setRememberMe(rememberMe);
		return result;
	}
}
