package at.loup.security.data;

public class UserChangeCallbackRegistrationRequest {
	private String callbackUrl;
	private String userChangeKey;

	public UserChangeCallbackRegistrationRequest() {

	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getUserChangeKey() {
		return userChangeKey;
	}

	public void setUserChangeKey(String userChangeKey) {
		this.userChangeKey = userChangeKey;
	}

	public static UserChangeCallbackRegistrationRequest create(String callbackUrl, String userChangeKey) {
		UserChangeCallbackRegistrationRequest result = new UserChangeCallbackRegistrationRequest();

		result.setCallbackUrl(callbackUrl);
		result.setUserChangeKey(userChangeKey);

		return result;
	}
}