package at.loup.security.data;

public class UserChangeCallbackDeregistrationRequest {
	private String callbackUrl;

	public UserChangeCallbackDeregistrationRequest() {

	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public static UserChangeCallbackDeregistrationRequest create(String callbackUrl) {
		UserChangeCallbackDeregistrationRequest result = new UserChangeCallbackDeregistrationRequest();

		result.setCallbackUrl(callbackUrl);

		return result;
	}
}