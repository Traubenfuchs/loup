package at.loup.security.data.reqres;

import at.loup.commons.utilities.ArgumentRuleUtilities;
import at.loup.security.data.UserDTO;

public class DeleteUserResult {
	private UserDTO userDTO;
	private EDeleteUserResult eDeleteUserResult;

	public DeleteUserResult() {

	}

	public static DeleteUserResult createSuccess(UserDTO userDTO) {
		ArgumentRuleUtilities.notNull("userDTO", userDTO);
		DeleteUserResult result = new DeleteUserResult();
		result.setUserDTO(userDTO);
		result.seteDeleteUserResult(EDeleteUserResult.ok);
		return result;
	}

	public static DeleteUserResult createUserDoesNotExist() {
		DeleteUserResult result = new DeleteUserResult();
		result.seteDeleteUserResult(EDeleteUserResult.userDoesNotExist);
		return result;
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public EDeleteUserResult geteDeleteUserResult() {
		return eDeleteUserResult;
	}

	public void seteDeleteUserResult(EDeleteUserResult eDeleteUserResult) {
		this.eDeleteUserResult = eDeleteUserResult;
	}

	public static enum EDeleteUserResult {
		ok, userDoesNotExist,;
	}
}