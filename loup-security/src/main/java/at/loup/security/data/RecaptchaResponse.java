package at.loup.security.data;

import java.util.List;

public class RecaptchaResponse {
	private boolean success;
	private List<String> errorCodes;

	public boolean getSuccess() {
		return success;
	}

	public List<String> getErrorCodes() {
		return errorCodes;
	}
}
