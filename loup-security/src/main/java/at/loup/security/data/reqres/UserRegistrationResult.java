package at.loup.security.data.reqres;

import at.loup.commons.utilities.ArgumentRuleUtilities;
import at.loup.security.data.UserDTO;

public class UserRegistrationResult {
	private String errorMessage;
	private UserDTO userDTO;
	private EUserRegistrationResult eUserRegistrationResult;

	protected UserRegistrationResult() {

	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public EUserRegistrationResult geteUserRegistrationResult() {
		return eUserRegistrationResult;
	}

	public void seteUserRegistrationResult(EUserRegistrationResult eUserRegistrationResult) {
		this.eUserRegistrationResult = eUserRegistrationResult;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public static UserRegistrationResult createSuccess(UserDTO userDTO) {
		ArgumentRuleUtilities.notNull("userDTO", userDTO);
		UserRegistrationResult result = new UserRegistrationResult();
		result.seteUserRegistrationResult(EUserRegistrationResult.ok);
		result.setUserDTO(userDTO);
		return result;
	}

	public static UserRegistrationResult createUsernameExists() {
		UserRegistrationResult result = new UserRegistrationResult();
		result.seteUserRegistrationResult(EUserRegistrationResult.error_usernameExists);
		return result;
	}

	public static UserRegistrationResult createEmailExists() {
		UserRegistrationResult result = new UserRegistrationResult();
		result.seteUserRegistrationResult(EUserRegistrationResult.error_emailExists);
		return result;
	}

	public static UserRegistrationResult createRequirementsError(String reason) {
		UserRegistrationResult result = new UserRegistrationResult();
		result.seteUserRegistrationResult(EUserRegistrationResult.errorRequirements);
		result.setErrorMessage(reason);
		return result;
	}

	public static enum EUserRegistrationResult {
		ok, error_usernameExists, error_emailExists, errorRequirements;
	}
}