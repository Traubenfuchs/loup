package at.loup.security.data.reqres;

import at.loup.security.data.UserDTO;

public class UpdateUserRequest {

	private UserDTO userDTO;

	protected UpdateUserRequest() {

	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public UpdateUserRequest create(UserDTO userDTO) {
		UpdateUserRequest result = new UpdateUserRequest();
		result.setUserDTO(userDTO);
		return result;
	}
}