package at.loup.security.data;

import at.loup.commons.utilities.ArgumentRuleUtilities;

public class UserChange {
	private UserDTO userDTO;
	private UserChange.EUserUpdateType userUpdateType;

	public UserChange() {

	}

	protected UserChange(UserDTO userDTO, UserChange.EUserUpdateType userUpdateType) {
		this.userDTO = userDTO;
		this.userUpdateType = userUpdateType;
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public UserChange.EUserUpdateType getUserUpdateType() {
		return userUpdateType;
	}

	public void setUserUpdateType(UserChange.EUserUpdateType userUpdateType) {
		this.userUpdateType = userUpdateType;
	}

	public static UserChange createUpdate(UserDTO userDTO) {
		ArgumentRuleUtilities.notNull("userDTO", userDTO);
		UserChange result = new UserChange(userDTO, EUserUpdateType.update);
		return result;
	}

	public static UserChange createDelete(UserDTO userDTO) {
		ArgumentRuleUtilities.notNull("userDTO", userDTO);
		UserChange result = new UserChange(userDTO, EUserUpdateType.delete);
		return result;
	}

	static enum EUserUpdateType {
		update, delete
	}
}