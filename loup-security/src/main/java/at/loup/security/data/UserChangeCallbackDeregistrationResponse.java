package at.loup.security.data;

public class UserChangeCallbackDeregistrationResponse {
	private boolean wasUnregistered;

	public UserChangeCallbackDeregistrationResponse() {

	}

	public boolean isWasUnregistered() {
		return wasUnregistered;
	}

	public void setWasUnregistered(boolean wasUnregistered) {
		this.wasUnregistered = wasUnregistered;
	}

	public static UserChangeCallbackDeregistrationResponse createSuccess(boolean wasUnregistered) {
		UserChangeCallbackDeregistrationResponse result = new UserChangeCallbackDeregistrationResponse();
		result.setWasUnregistered(wasUnregistered);
		return result;
	}
}