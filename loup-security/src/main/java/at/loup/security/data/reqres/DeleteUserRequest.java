package at.loup.security.data.reqres;

import at.loup.commons.utilities.ArgumentRuleUtilities;

public class DeleteUserRequest {
	private Long id;
	private String usernameOrEmail;

	protected DeleteUserRequest() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsernameOrEmail() {
		return usernameOrEmail;
	}

	public void setUsernameOrEmail(String usernameOrEmail) {
		this.usernameOrEmail = usernameOrEmail;
	}

	public static DeleteUserRequest createById(long id) {
		DeleteUserRequest result = new DeleteUserRequest();
		result.setId(id);
		return result;
	}

	public static DeleteUserRequest createByUsernameOrEmail(String usernameOrEmail) {
		ArgumentRuleUtilities.notNull("usernameOrEmail", usernameOrEmail);

		DeleteUserRequest result = new DeleteUserRequest();
		result.setUsernameOrEmail(usernameOrEmail);
		return result;
	}
}