package at.loup.security.data.reqres;

public class UpdateUserResult {

	private String errorMessage;
	private EUpdateUserResult eUpdateUserResult;

	protected UpdateUserResult() {

	}

	public static UpdateUserResult createSuccess() {
		UpdateUserResult result = new UpdateUserResult();
		result.seteUpdateUserResult(EUpdateUserResult.ok);
		return result;
	}

	public static UpdateUserResult createError(String errorMessage) {
		UpdateUserResult result = new UpdateUserResult();
		result.seteUpdateUserResult(EUpdateUserResult.error);
		result.setErrorMessage(errorMessage);
		return result;
	}

	public static UpdateUserResult createUserDoesNotExist() {
		UpdateUserResult result = new UpdateUserResult();
		result.seteUpdateUserResult(EUpdateUserResult.userDoesNotExist);
		return result;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public EUpdateUserResult geteUpdateUserResult() {
		return eUpdateUserResult;
	}

	public void seteUpdateUserResult(EUpdateUserResult eUpdateUserResult) {
		this.eUpdateUserResult = eUpdateUserResult;
	}

	public static enum EUpdateUserResult {
		ok, userDoesNotExist, error,
		;
	}
}