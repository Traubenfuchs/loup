package at.loup.security.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class UserDTO {
	protected Long id;
	protected String username;
	protected String apiKey;
	protected String email;
	protected Set<String> rights;
	protected String password;
	protected Date createdAt;
	private Boolean activated;

	public UserDTO() {
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<String> getRights() {
		if (rights == null) {
			rights = new HashSet<>();
		}
		return rights;
	}

	public boolean addRight(String right) {
		boolean result = getRights().add(right);
		return result;
	}

	public boolean removeRight(String right) {
		boolean result = getRights().remove(right);
		return result;
	}

	public void setRights(Set<String> rights) {
		this.rights = rights;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public boolean hasRights(String... requiredRights) {
		if (requiredRights == null) {
			return true;
		}
		for (String requiredRight : requiredRights) {
			if (!rights.contains(requiredRight)) {
				return false;
			}
		}
		return true;
	}
}