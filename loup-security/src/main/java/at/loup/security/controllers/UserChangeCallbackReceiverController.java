package at.loup.security.controllers;

import org.springframework.web.bind.annotation.RestController;

import at.loup.security.data.UserChange;

@RestController
public class UserChangeCallbackReceiverController {
	public UserChangeCallbackReceiverController() {

	}

	public void receiveUpdate(UserChange userChange) {

	}
}