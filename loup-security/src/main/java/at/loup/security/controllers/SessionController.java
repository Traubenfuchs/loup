package at.loup.security.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import at.loup.security.aspects.annotations.CSRFProtected;
import at.loup.security.data.LoginRequest;
import at.loup.security.data.LoginResult;
import at.loup.security.data.LoginResult.ELoginResult;
import at.loup.security.data.StatusResponse;
import at.loup.security.services.RememberMeComponent;
import at.loup.security.services.SessionEventComponent;
import at.loup.security.services.SessionScopedCSRFComponent;
import at.loup.security.services.UserHolder;
import at.loup.security.services.securitymanagers.WebSecurityManager;

/**
 * This controller offers login, logout, rememberme and csrf update web methods
 */
@RestController
public class SessionController {
	@Autowired
	private WebSecurityManager webSecurityManager;
	@Autowired
	private HttpServletResponse httpServletResponse;
	@Autowired
	private RememberMeComponent rememberMeService;
	@Autowired
	private UserHolder userHolder;
	@Autowired
	private SessionScopedCSRFComponent csrfService;
	@Autowired
	private SessionEventComponent sessionEventService;

	@RequestMapping(path = "/security/logout", method = RequestMethod.POST)
	@CSRFProtected
	public @ResponseBody boolean logout() {
		sessionEventService.invokeLogoutEvents();
		boolean result = webSecurityManager.logout();
		return result;
	}

	@RequestMapping(path = "/security/login", method = RequestMethod.POST)
	@CSRFProtected
	public @ResponseBody LoginResult login(@RequestBody LoginRequest loginRequest) {
		LoginResult loginResult = webSecurityManager.login(loginRequest.getUsernameOrEmail(),
				loginRequest.getPassword(), loginRequest.getRememberMe());

		if (loginResult.geteLoginResult() != ELoginResult.loggedInViaUsernamePassword
				&& loginResult.geteLoginResult() != ELoginResult.loggedInViaRememberMe) {
			httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		sessionEventService.invokeLoginEvents();
		return loginResult;
	}

	@RequestMapping(path = "/security/rememberme", method = RequestMethod.POST)
	@CSRFProtected
	public @ResponseBody LoginResult rememberme() {
		LoginResult result = rememberMeService.tryRememberMeLogin();
		// if (rememberMeUserDTO != null) {
		// LoginResult result =
		// ELoginResult.loggedInViaRememberMe.createLoginResult(rememberMeUserDTO);
		// return result;
		// }
		// httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
		// LoginResult result =
		// ELoginResult.cantRemember.createLoginResult(rememberMeUserDTO);
		return result;
	}

	@RequestMapping(path = "/security/status", method = RequestMethod.GET)
	@CSRFProtected
	public @ResponseBody StatusResponse status() {
		StatusResponse statusResponse = new StatusResponse(csrfService.getToken(), userHolder.getUserDTO());
		return statusResponse;
	}

	@RequestMapping(path = "/security/csrf", method = RequestMethod.GET)
	public @ResponseBody String csrf() {
		String result = csrfService.getToken();
		return result;
	}
}