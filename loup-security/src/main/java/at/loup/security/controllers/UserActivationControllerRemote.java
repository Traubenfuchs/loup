package at.loup.security.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import at.loup.security.data.UserDTO;
import at.loup.security.services.LoupSecuritySessionData;
import at.loup.security.services.coreservice.LoupSecurityCoreService;

@RestController
public class UserActivationControllerRemote {
	@Autowired
	private LoupSecuritySessionData securedUserSession;

	@Autowired
	private LoupSecurityCoreService loupSecurityCoreService;

	@Transactional(propagation = Propagation.REQUIRED)
	public @ResponseBody String activate(HttpServletResponse httpServletResponse, String activationString) {
		UserDTO userDTO = loupSecurityCoreService.activate(activationString);

		if (userDTO == null) {
			httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
			return "{\"message\":\"Given activation Code does not exist\"}";
		}

		securedUserSession.setUserId(userDTO.getId());

		return "{\"message\":\"User activated.\"}";
	}
}