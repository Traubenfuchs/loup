package at.loup.security.exceptions;

import org.springframework.http.HttpStatus;

import at.loup.commons.exceptions.LoupException;

/**
 * This exception should be thrown if an authentication or authorization
 * constraint is violated.
 */
public class LoupSecurityException extends LoupException {
	private static final long serialVersionUID = 7469134085769429998L;

	public LoupSecurityException(HttpStatus httpStatus, String publicMessage) {
		super(httpStatus, publicMessage);
	}

	public LoupSecurityException(HttpStatus httpStatus, String publicMessage, String privateMessage) {
		super(httpStatus, publicMessage, privateMessage);
	}
}