package at.loup.security.interfaces;

/**
 * Classes implementing {@link at.loup.security.interfaces.SecurityCondition}
 * can be used to protect methods or methods of a class with annotaiton @Secured
 */
public interface SecurityCondition {
	/**
	 * Checks if the attempted request is allowed, if not a
	 * {@link at.loup.security.exceptions.LoupSecurityException is thrown}.
	 *
	 * @return
	 */
	boolean authorized();
}