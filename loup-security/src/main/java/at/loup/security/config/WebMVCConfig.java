package at.loup.security.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import at.loup.security.services.SessionScopedCSRFComponent;

@Configuration
public class WebMVCConfig extends WebMvcConfigurerAdapter {
	@Autowired
	private SessionScopedCSRFComponent csrfService;

	@Autowired
	private LoupSecurityConfig loupSecurityConfig;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(csrfTokenAddingInterceptor(csrfService));
	}

	@Bean
	public HandlerInterceptor csrfTokenAddingInterceptor(
			SessionScopedCSRFComponent csrfService) {
		return new HandlerInterceptorAdapter() {
			@Override
			public void postHandle(
					HttpServletRequest request,
					HttpServletResponse response,
					Object handler,
					ModelAndView modelAndView) {
				if (modelAndView == null) {
					return;
				}
				if (modelAndView.getViewName().startsWith("redirect:")) {
					return;
				}

				String csrfToken = csrfService.getToken();

				modelAndView.addObject("csrftoken", csrfToken);
				modelAndView.addObject("csrftokenheader", "csrf-token");

				modelAndView.addObject("recaptchaSitekey", loupSecurityConfig.getRecaptchaSiteKey());
				modelAndView.addObject("recaptchaHeadername", "recaptcha-response");
			}
		};
	}
}