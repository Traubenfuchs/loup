package at.loup.security.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import at.loup.security.services.UserChangeCallbackRegistrationClient;
import at.loup.security.services.coreservice.LoupSecurityCoreServiceClient;
import at.loup.security.services.coreservice.LoupSecurityCoreService;

@Configuration
public class LoupSecurityBeans {
	private static final Logger logger = LoggerFactory.getLogger(LoupSecurityBeans.class);

	private final LoupSecurityConfig loupSecurityConfig;

	public LoupSecurityBeans(LoupSecurityConfig loupSecurityConfig) {
		this.loupSecurityConfig = loupSecurityConfig;
	}

	@Bean
	public LoupSecurityCoreService remoteLoupSecurityCoreService() {
		String remoteUrl = loupSecurityConfig.getRemoteUrl();
		String apiKey = loupSecurityConfig.getApiKey();

		if (remoteUrl == null || remoteUrl.length() == 0 || apiKey == null || apiKey.length() == 0) {
			logger.warn(""
					+ "ApiKey<"
					+ apiKey
					+ "> or remoteUrl<"
					+ remoteUrl
					+ "> invalid. RemoteLoupSecurityCoreService will not be available.");
			return null;
		}
		LoupSecurityCoreService result = new LoupSecurityCoreServiceClient(loupSecurityConfig);
		return result;
	}

	@Bean
	public UserChangeCallbackRegistrationClient userChangeCallbackRegistrationClient() {
		String remoteUrl = loupSecurityConfig.getRemoteUrl();
		String apiKey = loupSecurityConfig.getApiKey();
		String ownUrl = loupSecurityConfig.getOwnUrl();

		if (remoteUrl == null || remoteUrl.length() == 0 || apiKey == null || apiKey.length() == 0 || ownUrl == null
				|| ownUrl.length() == 0) {
			logger.warn(""
					+ "ApiKey<"
					+ apiKey
					+ "> or remoteUrl<"
					+ remoteUrl
					+ "> or ownUrl<"
					+ ownUrl
					+ ">invalid. RemoteLoupSecurityCoreService will not be available.");
			return null;
		}
		UserChangeCallbackRegistrationClient result = new UserChangeCallbackRegistrationClient(loupSecurityConfig);
		return result;
	}
}