package at.loup.security.config;

import org.springframework.beans.factory.annotation.Value;

public class LoupSecurityConfig {
	@Value("${loup.security.remote.ownUrl:}")
	private String ownUrl = null;
	@Value("${loup.security.remote.remoteUrl:}")
	private String remoteUrl = null;
	@Value("${loup.security.remote.apiKey:}")
	private String apiKey;
	@Value("${loup.security.remote.demandActivation:false}")
	private boolean demandActivation; // TODO implement
	@Value("${loup.security.reCaptcha.secretkey:''}")
	private String reCaptchaSecretKey;
	@Value("${loup.security.reCaptcha.sitekey:''}")
	private String recaptchaSiteKey;

	protected LoupSecurityConfig() {

	}

	public String getOwnUrl() {
		return ownUrl;
	}

	public void setOwnUrl(String ownUrl) {
		this.ownUrl = ownUrl;
	}

	public String getRemoteUrl() {
		return remoteUrl;
	}

	public void setRemoteUrl(String remoteUrl) {
		this.remoteUrl = remoteUrl;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public boolean isDemandActivation() {
		return demandActivation;
	}

	public void setDemandActivation(boolean demandActivation) {
		this.demandActivation = demandActivation;
	}

	public String getReCaptchaSecretKey() {
		return reCaptchaSecretKey;
	}

	public void setReCaptchaSecretKey(String reCaptchaSecretKey) {
		this.reCaptchaSecretKey = reCaptchaSecretKey;
	}

	public String getRecaptchaSiteKey() {
		return recaptchaSiteKey;
	}

	public void setRecaptchaSiteKey(String recaptchaSiteKey) {
		this.recaptchaSiteKey = recaptchaSiteKey;
	}
}