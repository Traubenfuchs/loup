package at.loup.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoupSecurityConfigBean {
	@Bean
	public LoupSecurityConfig loupSecurityConfig() {
		return new LoupSecurityConfig();
	}
}