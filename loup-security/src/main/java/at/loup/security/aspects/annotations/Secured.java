package at.loup.security.aspects.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import at.loup.security.interfaces.SecurityCondition;

/**
 * A {@link Secured} method or methods in a {@link Secured} class require an
 * activated {@link at.loup.security.data.UserDTO} user to be logged in or send
 * an apiKey, otherwise the protected method will not be executed. Enforces CSRF
 * protection if no apiKey is used and HTTP method is not GET.<br>
 * Additionally allows you to define required rights or a SPEL expression that
 * must return true.
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Secured {
	/**
	 * The required rights. The user must have all of them. If the array is
	 * empty (default), the user needs to exist.
	 *
	 * @return
	 */
	String[] rights() default {};

	/**
	 * SPEL expression that must return a boolean and must not return null. If
	 * it returns true the protected method can be executed.
	 *
	 * @return true or false, never null
	 */
	String spelBoolean() default "";

	/**
	 * @Secured classes or methods must pass all given SecurityConditions.
	 *          SecurityConditions must be available from the default
	 *          BeanFactory and can have any scope.
	 * @return
	 */
	Class<? extends SecurityCondition>[] securityConditions() default {};

	/**
	 * CSRF protection is turned off if this is set to false.
	 *
	 * @return
	 */
	boolean checkCSRF() default true;
}