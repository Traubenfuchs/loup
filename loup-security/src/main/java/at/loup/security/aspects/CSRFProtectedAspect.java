package at.loup.security.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.loup.security.aspects.annotations.CSRFProtected;
import at.loup.security.data.UserDTO;
import at.loup.security.services.UserHolder;
import at.loup.security.services.securitymanagers.WebSecurityManager;

/**
 * Protects methods or methods of a class from CSRF attacks. If an apiKey is
 * given the UserDTO will be bound to the request.
 */
@Component
@Aspect
public class CSRFProtectedAspect {
	@Autowired
	private WebSecurityManager webSecurityManager;
	@Autowired
	private UserHolder requestScopedUserHolder;

	// method level
	@Around("@annotation(csrfProtected) && !execution(*.new(..))")
	public Object adviseAnnotatedMethods(ProceedingJoinPoint pjp, CSRFProtected csrfProtected)
			throws Throwable {
		Object result = advise(pjp, csrfProtected);
		return result;
	}

	// class level
	@Around("@within(csrfProtected) && !@annotation(at.loup.security.aspects.annotations.CSRFProtected) && !execution(*.new(..))")
	public Object adviseAnnotatedClass(ProceedingJoinPoint pjp, CSRFProtected csrfProtected)
			throws Throwable {
		Object result = advise(pjp, csrfProtected);
		return result;
	}

	private Object advise(ProceedingJoinPoint pjp, CSRFProtected csrfProtected) throws Throwable {
		UserDTO user = webSecurityManager.acquireUserViaApiKeyThrowIfInvalid();
		if (user == null) {
			webSecurityManager.validateCsrfTokenOrThrow();
		} else {
			requestScopedUserHolder.setUserDTO(user);
		}

		Object result = pjp.proceed();
		return result;
	}
}