package at.loup.security.aspects.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * {@link CSRFProtected} methods or methods in a {@link CSRFProtected} class can
 * only be called if the web user supplies the correct csrf-token or uses an
 * apiKey. The user does not need to be logged in.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CSRFProtected {

}