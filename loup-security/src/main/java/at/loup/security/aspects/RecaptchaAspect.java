package at.loup.security.aspects;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import at.loup.security.aspects.annotations.RecaptchaProtected;
import at.loup.security.config.LoupSecurityConfig;
import at.loup.security.data.RecaptchaResponse;
import at.loup.security.exceptions.LoupSecurityException;

@Component
@Aspect
public class RecaptchaAspect {
	@Autowired
	private HttpServletRequest request;
	private final RestTemplate restTemplate = new RestTemplate();
	@Autowired
	private LoupSecurityConfig loupSecurityConfig;

	// method level
	@Around("@annotation(recaptchaProtected) && !execution(*.new(..))")
	public Object adviseAnnotatedMethods(ProceedingJoinPoint pjp, RecaptchaProtected recaptchaProtected)
			throws Throwable {
		Object result = advise(pjp, recaptchaProtected);
		return result;
	}

	// class level
	@Around("@within(recaptchaProtected) && !@annotation(at.loup.security.aspects.annotations.RecaptchaProtected) && !execution(*.new(..))")
	public Object adviseAnnotatedClass(ProceedingJoinPoint pjp, RecaptchaProtected recaptchaProtected)
			throws Throwable {
		Object result = advise(pjp, recaptchaProtected);
		return result;
	}

	private Object advise(ProceedingJoinPoint pjp, RecaptchaProtected recaptchaProtected) throws Throwable {
		String recaptchaResponse = request.getHeader("recaptcha-response");
		String remoteAddr = request.getRemoteAddr();

		MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
		form.add("secret", loupSecurityConfig.getReCaptchaSecretKey());
		form.add("remoteip", remoteAddr);
		form.add("response", recaptchaResponse);

		RecaptchaResponse googleRecaptchaResponse = restTemplate
				.postForEntity("https://www.google.com/recaptcha/api/siteverify", form, RecaptchaResponse.class)
				.getBody();

		if (!googleRecaptchaResponse.getSuccess()) {
			throw new LoupSecurityException(HttpStatus.FORBIDDEN, "ReCaptcha check failed.");
		}

		Object result = pjp.proceed();
		return result;
	}
}