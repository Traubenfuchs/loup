package at.loup.security.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.loup.security.aspects.annotations.Secured;
import at.loup.security.data.UserDTO;
import at.loup.security.exceptions.LoupSecurityException;
import at.loup.security.interfaces.SecurityCondition;
import at.loup.security.services.UserHolder;
import at.loup.security.services.securitymanagers.WebSecurityManager;

/**
 * Protects methods or methods of a class with authentication, authorization and
 * CSRF.
 */
@Component
@Aspect
public class SecuredAspect {
	@Autowired
	private WebSecurityManager webSecurityManager;
	@Autowired
	private UserHolder requestScopedUserHolder;

	// method level
	@Around("@annotation(secured) && !execution(*.new(..))")
	public Object adviseAnnotatedMethods(ProceedingJoinPoint pjp, Secured secured) throws Throwable {
		Object result = advise(pjp, secured);
		return result;
	}

	// class level
	@Around("@within(secured) &&  !@annotation(at.loup.security.aspects.annotations.Secured) && !execution(*.new(..))")
	public Object adviseAnnotatedClass(ProceedingJoinPoint pjp, Secured secured) throws Throwable {
		Object result = advise(pjp, secured);
		return result;
	}

	private Object advise(ProceedingJoinPoint pjp, Secured secured) throws Throwable {
		checkSecurityOrThrow(secured);
		Object result = pjp.proceed();
		return result;
	}

	public void checkSecurityOrThrow(Secured secured) throws LoupSecurityException {
		UserDTO user = webSecurityManager.acquireUserViaApiKeyThrowIfInvalid();
		if (user == null) {
			// no api token -> check CSRF
			if (secured.checkCSRF()) {
				webSecurityManager.validateCsrfTokenOrThrow();
			}
			user = webSecurityManager.getCurrentUserOrThrow();
		} else {
			// Set user for current request only. apiKey -> stateless auth
			requestScopedUserHolder.setUserDTO(user);
		}

		webSecurityManager.checkUserActivatedOrThrow(user);

		String[] requiredRights = secured.rights();
		webSecurityManager.checkRightsOrThrow(user, requiredRights);

		String spelCondition = secured.spelBoolean();
		webSecurityManager.checkSpelConditionOrThrow(spelCondition);

		Class<? extends SecurityCondition>[] securityConditions = secured.securityConditions();
		webSecurityManager.checkSecurityConditionsOrThrow(securityConditions);
	}
}