package at.loup.security.aspects.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Methods annotated with @RecaptchaProtected need to pass Google's Recaptcha.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RecaptchaProtected {

}