package at.loup.security.services;

public interface Hasher {
	byte[] hashAndSalt(String password);

	boolean checkHashedAndSaltedPassword(byte[] hashedAndSaltedCorrectPassword, String challengingPassword);
}