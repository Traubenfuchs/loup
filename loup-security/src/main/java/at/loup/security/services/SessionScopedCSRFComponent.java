package at.loup.security.services;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import at.loup.commons.random.RandomUtilities;

@Component
@SessionScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionScopedCSRFComponent {
	private final String csrfToken;

	@Autowired
	private HttpServletRequest httpServletRequest;

	public SessionScopedCSRFComponent() {
		csrfToken = RandomUtilities.createNiceRandomString(128);
	}

	/**
	 * Checks if the given CSRF token equals the CSRF token of this session.
	 *
	 * @param challengingCSRFToken
	 * @return true if tokens match
	 */
	public boolean checkCSRF(String challengingCSRFToken) {
		boolean result = this.csrfToken.equals(challengingCSRFToken);
		return result;
	}

	/**
	 * Checks if the CSRF token stored in the request header csrf-token matches
	 * the CSRF token of this session.
	 *
	 * @return true if tokens match
	 */
	public boolean checkCSRFViaHeader() {
		String csrfToken = httpServletRequest.getHeader("csrf-token");
		if (csrfToken == null || csrfToken.length() == 0) {
			return false;
		}

		boolean result = this.csrfToken.equals(csrfToken);
		return result;
	}

	/**
	 * Returns the CSRF token of this session
	 *
	 * @return
	 */
	public String getToken() {
		return csrfToken;
	}
}