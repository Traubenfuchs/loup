package at.loup.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.loup.commons.cookies.CookieService;
import at.loup.security.data.LoginResult;
import at.loup.security.data.LoginResult.ELoginResult;
import at.loup.security.data.UserDTO;
import at.loup.security.services.coreservice.LoupSecurityCoreService;

@Component
public class RememberMeComponent {
	public static final String COOKIE_NAME = "loup-remember-me";
	@Autowired
	private LoupSecurityCoreService loupSecurityCoreService;
	@Autowired
	private CookieService cookieService;
	@Autowired
	private LoupSecuritySessionData securitySessionData;

	/**
	 * Creates the rememberMeToken cookie
	 *
	 * @param rememberMeToken
	 */
	public void setRememberMeCookie(String rememberMeToken) {
		cookieService.setCookie(COOKIE_NAME, rememberMeToken, null, "/security", 60 * 60 * 24 * 7);
	}

	/**
	 * Silently deletes the rememberMeToken from the server and deletes the
	 * cookie, if they exist.
	 */
	public void deleteCompleteRememberMe() {
		String rememberMeToken = cookieService.getCookieValue(COOKIE_NAME);
		if (rememberMeToken == null) {
			return;
		}
		loupSecurityCoreService.deleteRememberMe(rememberMeToken);
		cookieService.deleteCookie(COOKIE_NAME);
	}

	/**
	 * Only deletes the rememberMe-cookie
	 */
	public void deleteRememberMeCookie() {
		cookieService.deleteCookie(COOKIE_NAME);
	}

	/**
	 * Logs the user in if a valid rememberMeToken exists. If an invalid
	 * rememberMeToken exists, the cookie is deleted.
	 *
	 * @return The user that has been logged in or null if no token was found or
	 *         the token was invalid.
	 */
	public LoginResult tryRememberMeLogin() {
		String rememberMeToken = cookieService.getCookieValue(COOKIE_NAME);
		if (rememberMeToken == null || rememberMeToken.length() == 0) {
			deleteRememberMeCookie();
			return ELoginResult.cantRemember.createLoginResult();
		}

		UserDTO userDTO = loupSecurityCoreService.loadViaRememberMeToken(rememberMeToken);
		if (userDTO == null) {
			deleteRememberMeCookie();
			return ELoginResult.cantRemember.createLoginResult();
		}
		securitySessionData.setUserId(userDTO.getId());
		return ELoginResult.loggedInViaRememberMe.createLoginResult(userDTO, rememberMeToken);
	}
}
