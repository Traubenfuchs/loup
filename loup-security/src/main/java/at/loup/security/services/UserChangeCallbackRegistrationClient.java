package at.loup.security.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import at.loup.commons.random.RandomUtilities;
import at.loup.security.config.LoupSecurityConfig;
import at.loup.security.data.UserChangeCallbackDeregistrationRequest;
import at.loup.security.data.UserChangeCallbackDeregistrationResponse;
import at.loup.security.data.UserChangeCallbackRegistrationRequest;
import at.loup.security.data.UserChangeCallbackRegistrationResponse;

@Component
public class UserChangeCallbackRegistrationClient {
	private final String userChangeKey;
	private final String ourOwnUrl;
	private final String remoteUrl;
	private final RestTemplate restTemplate = new RestTemplate();

	public UserChangeCallbackRegistrationClient(LoupSecurityConfig loupSecurityConfig) {
		this.remoteUrl = loupSecurityConfig.getRemoteUrl();
		this.ourOwnUrl = loupSecurityConfig.getOwnUrl();
		this.userChangeKey = RandomUtilities.createNiceRandomString(256);

		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add((httpRequest, bytes, execution) -> {
			HttpHeaders headers = httpRequest.getHeaders();

			headers.add("apiKey", loupSecurityConfig.getApiKey());
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			return execution.execute(httpRequest, bytes);
		});
		restTemplate.setInterceptors(interceptors);
	}

	public void register() {
		UserChangeCallbackRegistrationRequest request = UserChangeCallbackRegistrationRequest.create(ourOwnUrl,
				userChangeKey);
		UserChangeCallbackRegistrationResponse result = restTemplate.postForObject(
				remoteUrl + "registerCallback",
				request,
				UserChangeCallbackRegistrationResponse.class);
	}

	public void deregister() {
		UserChangeCallbackDeregistrationRequest request = UserChangeCallbackDeregistrationRequest.create(ourOwnUrl);
		UserChangeCallbackDeregistrationResponse result = restTemplate.postForObject(
				remoteUrl + "unregisterCallback",
				request,
				UserChangeCallbackDeregistrationResponse.class);
	}

	@PostConstruct
	public void postConstruct() {
		register();
	}

	@PreDestroy
	public void preDestroy() {
		deregister();
	}
}
