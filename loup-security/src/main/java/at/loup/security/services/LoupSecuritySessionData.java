package at.loup.security.services;

import java.time.LocalDateTime;

import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LoupSecuritySessionData {
	private final LocalDateTime sessionCreationTimestamp = LocalDateTime.now();
	private volatile Long userId = null;

	public LoupSecuritySessionData() {
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public LocalDateTime getSessionCreationTimestamp() {
		return sessionCreationTimestamp;
	}

	public Long getUserId() {
		return userId;
	}
}