package at.loup.security.services.coreservice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import at.loup.commons.exceptions.LoupException;
import at.loup.commons.utilities.datastructures.MapBuilder;
import at.loup.security.config.LoupSecurityConfig;
import at.loup.security.data.LoginResult;
import at.loup.security.data.UserDTO;
import at.loup.security.data.reqres.DeleteUserResult;
import at.loup.security.data.reqres.UpdateUserResult;
import at.loup.security.data.reqres.UserRegistrationResult;

@Component
public class LoupSecurityCoreServiceClient extends LoupSecurityCoreService {
	private final RestTemplate restTemplate = new RestTemplate();
	private final String loadViaRememberMeTokenUrl;
	private final String deleteRememberMeUrl;
	private final String loadUserViaCredsCredsUrl;
	private final String loadUserViaApiTokenUrl;
	private final String updateUserUrl;
	private final String deleteUserUrl;
	private final String registerUserUrl;
	private final String activateUrl;
	private final String loadByUserIdUrl;

	private final LoadingCache<Long, UserDTO> userIdToUserDtoCache;
	private final LoadingCache<String, UserDTO> apiKeyToDtoCache;

	public LoupSecurityCoreServiceClient(LoupSecurityConfig loupSecurityConfig) {
		String baseUrl = loupSecurityConfig.getRemoteUrl();

		if (!baseUrl.endsWith("/")) {
			baseUrl = baseUrl + '/';
		}

		loadViaRememberMeTokenUrl = baseUrl + "loadViaRememberMeToken" + "?token={token}";
		deleteRememberMeUrl = baseUrl + "deleteRememberMe" + "?token={token}";
		loadUserViaCredsCredsUrl = baseUrl + "loadUserViaCreds"
				+ "?usernameOrEmail={usernameOrEmail}&password={password}&rememberMe={rememberMe}";
		loadUserViaApiTokenUrl = baseUrl + "loadUserViaApiToken" + "?apiKey={apiKey}";
		updateUserUrl = baseUrl + "updateUser";
		deleteUserUrl = baseUrl + "deleteUser?userId={userId}";
		registerUserUrl = baseUrl + "registerUser" + "?activationRequired={activationRequired}";
		activateUrl = baseUrl + "activate" + "?token={token}";
		loadByUserIdUrl = baseUrl + "loadByUserId" + "?userId={userId}";

		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add((httpRequest, bytes, execution) -> {
			HttpHeaders headers = httpRequest.getHeaders();

			headers.add("apiKey", loupSecurityConfig.getApiKey());
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			return execution.execute(httpRequest, bytes);
		});
		restTemplate.setInterceptors(interceptors);

		userIdToUserDtoCache = CacheBuilder.newBuilder()
				.concurrencyLevel(6)
				.expireAfterAccess(10, TimeUnit.MINUTES)
				.maximumSize(1000)
				.build(new CacheLoader<Long, UserDTO>() {
					@Override
					public UserDTO load(Long key) throws Exception {
						UserDTO result = restTemplate.postForObject(
								loadByUserIdUrl,
								null,
								UserDTO.class,
								MapBuilder.createBuilder("userId", key).getMap());
						return result;
					}
				});

		apiKeyToDtoCache = CacheBuilder.newBuilder()
				.concurrencyLevel(6)
				.expireAfterAccess(10, TimeUnit.MINUTES)
				.maximumSize(1000)
				.build(new CacheLoader<String, UserDTO>() {
					@Override
					public UserDTO load(String key) throws Exception {
						UserDTO result = restTemplate.postForObject(
								loadUserViaApiTokenUrl,
								null,
								UserDTO.class,
								MapBuilder.createBuilder("apiKey", key).getMap());
						return result;
					}
				});
	}

	@Override
	public UserDTO loadViaRememberMeToken(String token) {
		UserDTO result = restTemplate.postForObject(
				loadViaRememberMeTokenUrl,
				null,
				UserDTO.class,
				MapBuilder.createBuilder("token", token).getMap());
		return result;
	}

	@Override
	public boolean deleteRememberMe(String token) {
		Boolean result = restTemplate.postForObject(
				deleteRememberMeUrl,
				null,
				Boolean.class,
				MapBuilder.createBuilder("token", token).getMap());
		return result;
	}

	@Override
	public LoginResult loadUserViaCreds(String usernameOrEmail, String password, boolean rememberMe) {
		LoginResult result = restTemplate.postForObject(loadUserViaCredsCredsUrl, null,
				LoginResult.class,
				MapBuilder.<String, Object>createBuilder()
						.put("usernameOrEmail", usernameOrEmail)
						.put("password", password)
						.put("rememberMe", Boolean.toString(rememberMe))
						.getMap());
		return result;
	}

	@Override
	public UserDTO loadUserViaApiToken(String apiKey) {
		UserDTO result;
		try {
			result = apiKeyToDtoCache.get(apiKey);
		} catch (ExecutionException ex) {
			throw new LoupException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Can't resolve user with apiKey <" + apiKey + ">", ex);
		}
		return result;
	}

	@Override
	public UpdateUserResult updateUser(UserDTO userDTO) {
		UpdateUserResult result = restTemplate.postForObject(
				updateUserUrl,
				userDTO,
				UpdateUserResult.class);
		return result;
	}

	@Override
	public DeleteUserResult deleteUser(long userId) {
		DeleteUserResult result = restTemplate.postForObject(
				deleteUserUrl,
				null,
				DeleteUserResult.class,
				MapBuilder
						.createBuilder("userId", Long.toString(userId)).getMap());
		return result;
	}

	@Override
	public UserRegistrationResult registerUser(UserDTO userDTO, boolean activationRequired) {
		UserRegistrationResult result = restTemplate.postForObject(
				registerUserUrl,
				userDTO,
				UserRegistrationResult.class,
				MapBuilder
						.createBuilder("activationRequired", Boolean.toString(activationRequired)).getMap());
		return result;
	}

	@Override
	public UserDTO activate(String token) {
		UserDTO result = restTemplate.postForObject(
				activateUrl,
				null,
				UserDTO.class,
				MapBuilder.createBuilder("token", token).getMap());
		return result;
	}

	@Override
	public UserDTO loadByUserId(long userId) {
		UserDTO result;
		try {
			result = userIdToUserDtoCache.get(userId);
		} catch (ExecutionException ex) {
			throw new LoupException(HttpStatus.INTERNAL_SERVER_ERROR, "Can't resolve user with id <" + userId + ">",
					ex);
		}
		return result;
	}
}