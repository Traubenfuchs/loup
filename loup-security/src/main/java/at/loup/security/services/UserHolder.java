package at.loup.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import at.loup.security.data.UserDTO;
import at.loup.security.services.coreservice.LoupSecurityCoreService;

/**
 * Fixates the current user so user details can not change during one request
 * after the first time the user was accessed.
 */
@Component
@SessionScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserHolder {
	@Autowired
	protected LoupSecuritySessionData loupSecuritySessionData;
	@Autowired
	protected LoupSecurityCoreService loupSecurityCoreService;

	private UserDTO resolvedUser = null;

	/**
	 * Loads the currently logged in user's UserDTO. Fixates UserDTO and userId
	 * at the first call of a request.
	 *
	 * @return The currently logged in user or null
	 */
	public UserDTO getUserDTO() {
		if (resolvedUser != null) {
			return resolvedUser;
		}

		Long currentUserId = loupSecuritySessionData.getUserId();
		if (currentUserId == null) {
			return null;
		}

		resolvedUser = loupSecurityCoreService.loadByUserId(currentUserId);
		if (resolvedUser == null) {
			loupSecuritySessionData.setUserId(null);
		}

		return resolvedUser;
	}

	/**
	 * Loads the currently logged in user's id. Fixates UserDTO and userId at
	 * the first call of a request.
	 *
	 * @return userId of the currently logged in user or null
	 */
	public Long getUserId() {
		UserDTO userDTO = getUserDTO();
		if (userDTO == null) {
			return null;
		}
		Long result = getUserDTO().getId();
		return result;
	}

	/**
	 * Sets the user for the current request.
	 *
	 * @param userDTO
	 */
	public void setUserDTO(UserDTO userDTO) {
		this.resolvedUser = userDTO;
	}
}