package at.loup.security.services.securitymanagers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import at.loup.commons.services.SPELEvaluator;
import at.loup.security.data.LoginResult;
import at.loup.security.data.LoginResult.ELoginResult;
import at.loup.security.data.UserDTO;
import at.loup.security.exceptions.LoupSecurityException;
import at.loup.security.interfaces.SecurityCondition;
import at.loup.security.services.LoupSecuritySessionData;
import at.loup.security.services.RememberMeComponent;
import at.loup.security.services.SessionScopedCSRFComponent;
import at.loup.security.services.UserHolder;
import at.loup.security.services.coreservice.LoupSecurityCoreService;

/**
 * Offers security related methods in a web application context.
 */
@Service
public class WebSecurityManager {
	@Autowired
	private HttpServletRequest httpServletRequest;
	@Autowired
	private LoupSecuritySessionData loupSecuritySessionData;
	@Autowired
	private LoupSecurityCoreService loupSecurityCoreService;
	@Autowired
	private RememberMeComponent rememberMeService;
	@Autowired
	private UserHolder userHolder;
	@Autowired
	private SessionScopedCSRFComponent csrfService;
	@Autowired
	private UserHolder requestScopedUserHolder;
	@Autowired
	private SPELEvaluator spelEvaluator;
	@Autowired
	private BeanFactory beanFactory;

	/**
	 * Gets the apiKey from the request header or request parameter.
	 *
	 * @return apiKey or null
	 */
	public String getApiKey() {
		String apiKey = httpServletRequest.getHeader("apiKey");

		if (apiKey == null) {
			apiKey = httpServletRequest.getParameter("apiKey");
		}

		return apiKey;
	}

	/**
	 * Checks if all given SecurityConditions return true.<br>
	 * Automatically loads SecurityCondition instances via beanFactory.<br>
	 * All classes used as SecurityConditions must be available as Spring bean
	 * from the default beanFactory.
	 *
	 * @param securityConditions
	 * @throws LoupSecurityException
	 */
	public void checkSecurityConditionsOrThrow(Class<? extends SecurityCondition>[] securityConditions)
			throws LoupSecurityException {
		if (securityConditions == null || securityConditions.length == 0) {
			return;
		}
		for (Class<? extends SecurityCondition> securityConditionClass : securityConditions) {
			SecurityCondition securityCondition = beanFactory.getBean(securityConditionClass);
			boolean securityConditionPassed = securityCondition.authorized();
			if (!securityConditionPassed) {
				throw new LoupSecurityException(HttpStatus.FORBIDDEN, "User lacks necessary rights.");
			}
		}
	}

	/**
	 * Checks if the given Spring Expression Language expression returns true.
	 * If not an exception is thrown.
	 *
	 * @param spelCondition
	 * @throws LoupSecurityException
	 */
	public void checkSpelConditionOrThrow(String spelCondition) throws LoupSecurityException {
		if (spelCondition == null || spelCondition.length() == 0) {
			return;
		}
		boolean speConditionEvaluationResult = spelEvaluator.evaluateBoolean(spelCondition);
		if (!speConditionEvaluationResult) {
			throw new LoupSecurityException(HttpStatus.FORBIDDEN, "User lacks necessary rights.");
		}

	}

	/**
	 * Checks if the given user is activated.
	 *
	 * @param user
	 * @throws LoupSecurityException
	 */
	public void checkUserActivatedOrThrow(UserDTO user) throws LoupSecurityException {
		if (!user.getActivated()) {
			throw new LoupSecurityException(HttpStatus.FORBIDDEN, "User not activated!");
		}
	}

	/**
	 * Checks if the given user holds all given rights.
	 *
	 * @param user
	 * @param rights
	 * @throws LoupSecurityException
	 */
	public void checkRightsOrThrow(UserDTO user, String[] rights) throws LoupSecurityException {
		if (!user.hasRights(rights)) {
			throw new LoupSecurityException(HttpStatus.FORBIDDEN, "User lacks necessary rights.");
		}
	}

	/**
	 * Loads the currently authenticated user. Throws an exception if no user is
	 * authenticated.
	 *
	 * @return The currently logged in user.
	 * @throws LoupSecurityException
	 *             If no user is logged in.
	 */
	public UserDTO getCurrentUserOrThrow() throws LoupSecurityException {
		UserDTO user = requestScopedUserHolder.getUserDTO();
		if (user == null) {
			throw new LoupSecurityException(HttpStatus.FORBIDDEN, "Not logged in.");
		}
		return user;
	}

	/**
	 * Checks csrf token via header. If the csrf token is missing or invalid
	 * throws a LoupSecurityException.
	 *
	 * @throws LoupSecurityException
	 */
	public void validateCsrfTokenOrThrow() throws LoupSecurityException {
		if (!csrfService.checkCSRFViaHeader()) {
			throw new LoupSecurityException(HttpStatus.FORBIDDEN, "Incorrect CSRF token.");
		}
	}

	/**
	 * Loads the user by using the apiKey present in the Requests header.
	 *
	 * @return userDTO or null
	 * @throws LoupSecurityException,
	 *             if an apiKey is present but invalid
	 */
	public UserDTO acquireUserViaApiKeyThrowIfInvalid() throws LoupSecurityException {
		String apiKey = getApiKey();
		if (apiKey == null) {
			return null;
		}

		UserDTO result = loupSecurityCoreService.loadUserViaApiToken(apiKey);

		if (result == null) {
			throw new LoupSecurityException(HttpStatus.FORBIDDEN, "Given apiKey lacks rights or does not exist.");
		}

		return result;
	}

	/**
	 * Logs in the user with the given credentials, binds the userId to the
	 * session and binds the UserDTO to the request.
	 *
	 * @param usernameOrEmail
	 * @param password
	 * @param rememberMe
	 * @return
	 */
	public LoginResult login(String usernameOrEmail, String password, boolean rememberMe) {
		UserDTO alreadyLoggedInUser = requestScopedUserHolder.getUserDTO();
		if (alreadyLoggedInUser != null) {
			LoginResult result = ELoginResult.alreadyLoggedIn.createLoginResult(alreadyLoggedInUser);
			return result;
		}

		LoginResult result = loupSecurityCoreService.loadUserViaCreds(usernameOrEmail, password, rememberMe);

		if (result.geteLoginResult().isSuccess()) {
			loupSecuritySessionData.setUserId(result.getUserDTO().getId());
			if (rememberMe) {
				rememberMeService.setRememberMeCookie(result.getRememberMeToken());
			}
		}

		return result;
	}

	/**
	 * Deletes the rememberMe cookie if it exists. Removes the user from the
	 * session and destroys the session.
	 *
	 * @return true if the user was logged in properly, false if he wasn't
	 */
	public boolean logout() {
		rememberMeService.deleteCompleteRememberMe();
		if (userHolder.getUserDTO() == null) {
			return false;
		}
		loupSecuritySessionData.setUserId(null);

		HttpSession httpSession = httpServletRequest.getSession(false);
		if (httpSession != null) {
			httpSession.invalidate();
		}
		return true;
	}
}