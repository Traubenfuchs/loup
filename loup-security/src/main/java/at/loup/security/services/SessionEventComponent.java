package at.loup.security.services;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import at.loup.commons.functional.actions.Action0;
import at.loup.commons.utilities.ArgumentRuleUtilities;

/**
 * Allows setting events for successful logins and logouts.
 */
@Component
public class SessionEventComponent {
	private final ConcurrentHashMap<String, Action0> loginEvents = new ConcurrentHashMap<>();
	private final ConcurrentHashMap<String, Action0> logoutEvents = new ConcurrentHashMap<>();

	public SessionEventComponent() {

	}

	public ESetEventResult setLoginEvent(String key, Action0 eventHandler) {
		ESetEventResult result = putActionInMap(loginEvents, key, eventHandler);
		return result;
	}

	public ESetEventResult setLogoutEvent(String key, Action0 eventHandler) {
		ESetEventResult result = putActionInMap(logoutEvents, key, eventHandler);
		return result;
	}

	protected ESetEventResult putActionInMap(Map<String, Action0> map, String key, Action0 eventHandler) {
		ArgumentRuleUtilities.notNull("map", map);
		ArgumentRuleUtilities.notNull("key", key);

		if (eventHandler == null) {
			Action0 deletedAction = map.remove(key);
			if (deletedAction == null) {
				return ESetEventResult.successNoPreviousEventWithThisKey;
			} else {
				return ESetEventResult.successPreviousEventWithThisKeyReplaced;
			}
		} else {
			Action0 previousAction = map.put(key, eventHandler);
			if (previousAction == null) {
				return ESetEventResult.successNoPreviousEventWithThisKey;
			} else {
				return ESetEventResult.successPreviousEventWithThisKeyReplaced;
			}
		}
	}

	public void invokeLoginEvents() {
		for (Action0 eventHandler : loginEvents.values()) {
			eventHandler.execute();
		}
	}

	public void invokeLogoutEvents() {
		for (Action0 eventHandler : logoutEvents.values()) {
			eventHandler.execute();
		}
	}

	public static enum ESetEventResult {
		successNoPreviousEventWithThisKey, successPreviousEventWithThisKeyReplaced

	}
}
