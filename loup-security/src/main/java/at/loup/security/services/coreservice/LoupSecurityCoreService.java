package at.loup.security.services.coreservice;

import at.loup.security.data.LoginResult;
import at.loup.security.data.UserDTO;
import at.loup.security.data.reqres.DeleteUserResult;
import at.loup.security.data.reqres.UpdateUserResult;
import at.loup.security.data.reqres.UserRegistrationResult;

public abstract class LoupSecurityCoreService {
	public abstract UserDTO loadViaRememberMeToken(String token);

	public abstract boolean deleteRememberMe(String token);

	public abstract LoginResult loadUserViaCreds(String usernameOrEmail, String password, boolean rememberMe);

	public abstract UserDTO loadUserViaApiToken(String apiKey);

	public abstract UpdateUserResult updateUser(UserDTO userDTO);

	public abstract DeleteUserResult deleteUser(long userId);

	public abstract UserRegistrationResult registerUser(UserDTO userDTO, boolean activationRequired);

	public abstract UserDTO activate(String token);

	public abstract UserDTO loadByUserId(long userId);
}