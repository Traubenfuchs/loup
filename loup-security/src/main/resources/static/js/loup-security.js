var Loup = Loup || {}
Loup.Security = {
	Csrf: {
		csrftoken: document.querySelector("meta[name='csrftoken']").getAttribute('content'),
		csrftokenheader: document.querySelector("meta[name='csrftokenheader']").getAttribute('content'),
		csrftokenparameter: document.querySelector("meta[name='csrftokenparameter']").getAttribute('content')
	},
	login(usernameOrEmail, password, rememberMe, success, error, always) {
		Loup.Ajax.post(
			'/security/login', {
				usernameOrEmail: usernameOrEmail,
				password: password,
				rememberMe: (rememberMe === true)
			},
			r => {
				if (success) {
					success(r)
				}
			},
			r => {
				if (error) {
					error(r)
				}
			},
			r => {
				if (always) {
					always(r)
				}
			}
		)
	},
	logout(success, error, always) {
		Loup.Ajax.post(
			'/security/logout',
			r => {
				if (success) {
					success(r)
				}
			},
			r => {
				if (error) {
					error(r)
				}
			},
			r => {
				if (always) {
					always(r)
				}
			}
		)
	},
	// loads information about the current user
	status(success, error, always) {
		Loup.Ajax.get(
			'/security/status',
			r => {
				if (success) {
					success(r)
				}
			},
			r => {
				if (error) {
					error(r)
				}
			},
			r => {
				if (always) {
					always(r)
				}
			}
		)
	},
	// refreshes the CSRF token
	refreshCSRF(success, failure) {
		Loup.Ajax.get(
			'/security/csrf',
			req => {
				Loup.Security.Csrf.csrftoken = req.responseText
				if (success) {
					success(req)
				}
			},
			req => {
				if (failure) {
					failure(req)
				}
			}
		)
	},
	// tries to login via rememberme cookie
	rememberMe(success, error, always) {
		Loup.Ajax.post(
			'/security/rememberme',
			r => {
				if (success && JSON.parse(r.responseText).eLoginResult === 'loggedInViaRememberMe') {
					success(r)
				} else if (error) {
					error(r)
				}
			},
			r => {
				if (error) {
					error(r)
				}
			},
			r => {
				l.log(r.responseText)
				if (always) {
					always(r)
				}
			}
		)
	}
};

(() => {
	if (!Loup.Security.Csrf.csrftoken) {
		l.error('CSRF token meta tag is missing!')
	}
	Loup.Ajax.preparators.push(req => {
		req.setRequestHeader(Loup.Security.Csrf.csrftokenheader, Loup.Security.Csrf.csrftoken)
	})
	Loup.Ajax.errorHandlers.push((req, progressEvent) => {
		if (req.status === 403) {
			if (req.responseText.indexOf('Incorrect CSRF token.') > -1) {
				l.log('Invalid CSRF token, loading fresh CSRF token.')
				Loup.Security.refreshCSRF()
			}
		}
	})
	const autoSessionCSRFRefresher = timeoutDuration => {
		setTimeout(() => {
			l.log('Refreshing session...')

			Loup.Security.refreshCSRF(() => {
				l.log('Session refreshed.')
				setTimeout(autoSessionCSRFRefresher, timeoutDuration || 1000 * 60 * 15)
			},
				r => {
					l.error('Refreshing session failed. Will refresh again in 15 seconds.');
					autoSessionCSRFRefresher(15 * 1000)
				})
		}, 1000 * 60 * 15)
	}
	autoSessionCSRFRefresher()
})();
