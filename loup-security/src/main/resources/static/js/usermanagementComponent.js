Loup.Dependencer.define('test-component', () => {
	const dependencer = Loup.Dependencer.load('loup-dependencer')
	const l = dependencer.load('loup-logger')
	const ajax = dependencer.load('loup-ajax')

	const result = {
		name: 'testComponent',
		template: `
		<div>
			<p>hi</p>
			<div id='reCaptchaContainer'>
			</div>
			<p>{{state}}</p>
			<button v-on:click='create'>yyy</button>
			<button v-on:click='attemptRequest'>go</button>
		</div>
		`,
		data() {
			return {
				state: 'new'
			}
		},
		methods: {
			create() {
				l.log("HU")
				grecaptcha.render('reCaptchaContainer', { 'sitekey': Loup.Security.ReCaptcha.sitekey })
			},
			attemptRequest() {
				const reCaptchaResponse = grecaptcha.getResponse()

				l.log("reca res  " + reCaptchaResponse)

				this.state = 'working'
				ajax.post(
					'/api/csrfprotected',
					{},
					r => {
						this.state = 'success'
					},
					r => {
						this.state = 'error'
					},
					undefined,
					r => {
						r.setRequestHeader(Loup.Security.ReCaptcha.headerName, reCaptchaResponse)
					})
				grecaptcha.reset();
			}
		},
		created() {
			//	grecaptcha.render('reCaptchaContainer', { sitekey: Loup.Security.ReCaptcha.sitekey })


		}
	}


	return result
});

Loup.Dependencer.define('usermanagement-component', () => {
	const loupPager = Loup.Dependencer.load('loup-pager')

	Vue.filter('toDate', function (input) {
		return new Date(input).toString()
	})

	return {
		name: 'usermanagement-component',
		template: `
			<div>
				<input v-model='SearchString' type='search'>
				<button v-on:click='prepareCreatUser' class='pure-button button-success'>create new user</button>

				<table class="pure-table" style='margin-left:auto;margin-right:auto'>
					<thead>
						<th>#</th>
						<th></th>
						<th>username</th>
						<th>email</th>
						<th>created at</th>
						<th>activated</th>
					</thead>
					<tbody>
						<tr v-for='user in users'>
							<td>{{user.id}}</td>
							<td>
								<button class='pure-button button-secondary' v-on:click='loadUser(user.id)'>edit</button>
							</td>
							<td>{{user.username}}</td>
							<td>{{user.email}}</td>
							<td>{{user.createdAt | toDate}}</td>
							<td>{{user.activated}}</td>
						</tr>
					</tbody>
				</table>

					<div class='pager-container'>
						<a
							class='pager-link'
							v-for='link in pages.leftLinks'
							v-bind:class="{'pager-link-active': link.active}"
							v-on:click='link.click'>{{link.displayPage}}
						</a>
						<div v-if='pages.centerLinks.length !== 0'>...</div>
						<a
							class='pager-link'
							v-for='link in pages.centerLinks'
							v-bind:class="{'pager-link-active': link.active}"
							v-on:click='link.click'>{{link.displayPage}}
						</a>
						<div v-if='pages.rightLinks.length !== 0'>...</div>
						<a
							class='pager-link'
							v-for='link in pages.rightLinks'
							v-bind:class="{'pager-link-active': link.active}"
							v-on:click='link.click'>{{link.displayPage}}
						</a>
					</div>

				<div>
					<form class="pure-form pure-form-aligned marginCenter" style='width:500px'>
						<fieldset>
							<div class="pure-control-group">
								<label for="Id">Id</label>
								<input id='Id' placeholder='Server assigned Id' v-model='currentUser.id' readonly>
							</div>
							<div class="pure-control-group">
								<label for='Username'>Username</label>
								<input id='Username' placeholder='Username' v-model='currentUser.username'>
							</div>
							<div class="pure-control-group">
								<label for='apiKey'>apiKey</label>
								<input id='apiKey' placeholder='apiKey' v-model='currentUser.apiKey'>
							</div>
							<div class="pure-control-group">
								<label for='Email'>Email</label>
								<input id='Email' for='Email' placeholder='Email' v-model='currentUser.email'>
							</div>
							<div class="pure-control-group">
								<label for='newPassword'>New Password</label>
								<input id='newPassword' placeholder='New Password' v-model='currentUser.password'>
							</div>
							<div class="pure-controls">
								<label for="Activated" class="pure-checkbox">
									<input id="Activated" v-model='currentUser.activated' type="checkbox">Activated
								</label>
							</div>
							<div class="pure-control-group">
								<label for='rights'>Rights</label>
								<textarea id='rights' class='pure-input-1-2' placeholder='Rights' v-model='CurrentUserRightsString'></textarea>
							</div>

						</fieldset>
						<button type='button' class='pure-button button-success' v-on:click='updateOrSaveUser'>Save</button>
					</form>
				</div>

			</div>
		`,
		data() {
			return {
				searchString: '',
				users: [],
				currentUser: {},
				pages: {
					leftLinks: [],
					centerLinks: [],
					rightLinks: []
				},
				searchId: 0
			}
		},
		computed: {
			SearchString: {
				get() {
					return this.searchString
				},
				set(newValue) {
					if (!newValue) {
						newValue = ''
					}
					this.searchString = newValue

					this.searchUsers(0, 5)

				},
			},
			CurrentUserRightsString: {
				get() {
					return this.currentUser.rights.join(' ')
				},
				set(newValue) {
					if (!newValue) {
						this.currentUser.rights = []
					} else {
						this.currentUser.rights = newValue.split(' ')
					}
				}
			}
		},
		methods: {
			searchUsers(page, pageSize) {
				const usedSearchId = ++this.searchId
				Loup.Ajax.get(
					'/security/search', {
						searchString: this.searchString,
						page: page,
						size: pageSize
					}, r => {
						if (usedSearchId !== this.searchId) {
							l.log(`Search stale result discarded.`)
							return
						}
						this.users = r.responseObject.elements
						this.pages = loupPager.getPages(
							r.responseObject,
							undefined,
							(page, currentPage, pageSize, pageCount) => () => {
								this.searchUsers(page, pageSize)
							})
					}, r => {
					}, r => {
					})
			},
			updateOrSaveUser() {
				if (!this.currentUser.id || this.currentUser.id.toString().length === 0) {
					this.createUser()
				} else {
					this.updateUser()
				}
			},
			loadUser(id) {
				Loup.Ajax.get(
					'/security/coreservice/loadByUserId', {
						userId: id
					},
					r => {
						l.log(JSON.stringify(r.responseObject))
						this.currentUser = r.responseObject
						this.currentUser.password = ''
					})
			},
			updateUser() {
				Loup.Ajax.post(
					'/security/coreservice/updateUser',
					this.currentUser,
					r => {

					},
					r => {

					}
				)
			},
			prepareCreatUser() {
				this.currentUser = {
					id: undefined,
					username: '',
					apiKey: '',
					email: '',
					password: '',
					rights: []
				}
			},
			createUser() {
				Loup.Ajax.post(
					'/security/coreservice/registerUser',
					this.currentUser,
					r => {

					},
					r => {

					}
				)
			}
		},
		created() {
			this.searchUsers(0, 5)
			this.prepareCreatUser()
		}
	}
});