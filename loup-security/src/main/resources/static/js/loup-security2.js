Loup.Security = (() => {
	const loupDependencer = Loup.Dependencer.load('loup-dependencer')
	const ajax = loupDependencer.load('loup-ajax')
	const l = loupDependencer.load('loup-logger')
	const noop = () => { }

	const csrf = {
		csrftoken: document.querySelector("meta[name='csrftoken']").getAttribute('content'),
		csrftokenheader: document.querySelector("meta[name='csrftokenheader']").getAttribute('content')
	}
	const siteKeyMeta = document.querySelector("meta[name='recaptchaSitekey']")
	const reCaptchaHeaderName = document.querySelector("meta[name='recaptchaHeadername']")
	const reCaptcha = {
		sitekey: siteKeyMeta ? siteKeyMeta.getAttribute('content') : '',
		headerName: reCaptchaHeaderName ? reCaptchaHeaderName.getAttribute('content') : '',
		insertRecaptcha() {

		}
	}

	const security = {
		Csrf: csrf,
		ReCaptcha: reCaptcha,
		login(usernameOrEmail, password, rememberMe, success, failure, always) {
			ajax.post(
				'/security/login', {
					usernameOrEmail: usernameOrEmail,
					password: password,
					rememberMe: (rememberMe === true)
				},
				success ? success : noop,
				failure ? failure : noop,
				always ? always : noop
			)
		},
		logout(success, failure, always) {
			ajax.post(
				'/security/logout',
				success ? success : noop,
				failure ? failure : noop,
				always ? always : noop
			)
		},
		// loads information about the current user
		status(success, failure, always) {
			ajax.get(
				'/security/status',
				success ? success : noop,
				failure ? failure : noop,
				always ? always : noop
			)
		},
		// refreshes the CSRF token
		refreshCSRF(success, failure, always) {
			ajax.get(
				'/security/csrf',
				req => {
					Loup.Security.Csrf.csrftoken = req.responseText
					if (success) {
						success(req)
					}
				},
				failure ? failure : noop,
				always ? always : noop
			)
		},
		// tries to login via rememberme cookie
		rememberMe(success, failure, always) {
			ajax.post(
				'/security/rememberme',
				r => {
					if (success && JSON.parse(r.responseText).eLoginResult === 'loggedInViaRememberMe') {
						success(r)
					} else if (failure) {
						failure(r)
					}
				},
				failure ? failure : noop,
				always ? always : noop
			)
		}
	}

	if (!csrf.csrftoken) {
		throw 'CSRF token meta tag is missing!'
	}

	ajax.preparators.push(req => {
		req.setRequestHeader(csrf.csrftokenheader, csrf.csrftoken)
	})

	ajax.errorHandlers.push((req, progressEvent) => {
		if (req.status === 403 && req.responseText.indexOf('Incorrect CSRF token.') > -1) {
			l.log('Invalid CSRF token, loading fresh CSRF token.')
			security.refreshCSRF()
		}
	})

	const autoSessionCSRFRefresher = timeoutDuration => {
		setTimeout(() => {
			l.log('Refreshing session...')

			security.refreshCSRF(() => {
				l.log('Session refreshed.')
				setTimeout(autoSessionCSRFRefresher, timeoutDuration || 1000 * 60 * 15)
			},
				r => {
					l.error('Refreshing session failed. Will refresh again in 15 seconds.')
					autoSessionCSRFRefresher(15 * 1000)
				})
		}, 1000 * 60 * 15)
	}
	autoSessionCSRFRefresher()

	loupDependencer.define('loup-security', security)

	return security
})();