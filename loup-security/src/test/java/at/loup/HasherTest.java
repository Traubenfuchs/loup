// package at.loup;
//
// import org.junit.Assert;
// import org.junit.Test;
//
// import at.loup.security.services.Hasher;
// import at.loup.security.services.HasherImpl;
//
// public class HasherTest extends Assert {
// private static final Hasher hasher = new HasherImpl();
//
// @Test
// public void hasherTest1() {
// assertTrue(checkPasswords("abc", "abc"));
// }
//
// @Test
// public void hasherTest2() {
// assertTrue(checkPasswords("", ""));
// }
//
// @Test
// public void hasherTestFail1() {
// assertFalse(checkPasswords("abc", "a"));
// }
//
// static boolean checkPasswords(String password1, String password2) {
// byte[] hashedAndSaltedPassword = hasher.hashAndSalt(password1);
// boolean result = hasher.checkHashedAndSaltedPassword(hashedAndSaltedPassword,
// password2);
// return result;
// }
// }
