package at.loup.commons.utilities;

import org.junit.Test;

public class ArgumentRuleUtilitiesTests {
	@Test(expected = IllegalArgumentException.class)
	public void notNull1() {
		ArgumentRuleUtilities.notNull("", null);
	}

	@Test
	public void notNull2() {
		ArgumentRuleUtilities.notNull("", "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void notNullEmpty1() {
		ArgumentRuleUtilities.notNullEmpty("", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void notNullEmpty2() {
		ArgumentRuleUtilities.notNullEmpty("", "");
	}

	@Test
	public void notNullEmpty3() {
		ArgumentRuleUtilities.notNullEmpty("", " ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void notNullEmptyWhitespace1() {
		ArgumentRuleUtilities.notNullEmptyWhitespace("", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void notNullEmptyWhitespace2() {
		ArgumentRuleUtilities.notNullEmptyWhitespace("", "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void notNullEmptyWhitespace3() {
		// one space
		ArgumentRuleUtilities.notNullEmptyWhitespace("", " ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void notNullEmptyWhitespace4() {
		// with tab
		ArgumentRuleUtilities.notNullEmptyWhitespace("", " 	");
	}

	@Test
	public void notNullEmptyWhitespace5() {
		ArgumentRuleUtilities.notNullEmptyWhitespace("", "a");
	}

	@Test
	public void max1() {
		ArgumentRuleUtilities.max("", 5, 5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void max2() {
		ArgumentRuleUtilities.max("", 5, 4);
	}

	@Test
	public void min1() {
		ArgumentRuleUtilities.min("", 5, 5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void min2() {
		ArgumentRuleUtilities.min("", 5, 6);
	}
}
