package at.loup.commons.utilities;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class StopWatchTests extends Assert {
	@Test
	@Ignore
	public void test1() {
		double result = StopWatch.measureDurationSeconds(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		});
		assertTrue(result < 0.105 && result > 0.095);

	}

	@Test
	@Ignore
	public void test2() {
		StopWatch sw = new StopWatch();
		sw.start();
		try {
			Thread.sleep(1001);
		} catch (InterruptedException e) {
		}
		sw.stop();
		System.out.println(sw.getElapsedNiceString());
		assertTrue(sw.getElapsedSeconds() < 0.105 && sw.getElapsedSeconds() > 0.095);
		assertTrue(sw.getElapsedMilis() < 105 && sw.getElapsedMilis() > 95);

	}
}
