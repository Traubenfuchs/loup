var Loup = Loup || {};
Loup.Random = {
	niceChars : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz',
	createNiceString(length) {
		let result = '';
		for(let i = 0; i<length; i++) {
			result += Loup.Random.niceChars.charAt(Math.floor(Math.random() * Loup.Random.niceChars.length));
		}
		return result;
	}
};