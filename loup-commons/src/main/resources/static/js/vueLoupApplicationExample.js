// needs:
// script(src='/compressed/js/logging.js')
// script(src='/compressed/js/ajax.js')
// script(src='/compressed/js/random.js')
// script(src='/compressed/js/libraries,vue.js')
// script(src='/compressed/js/libraries,vue-router.js')
// script(src='/compressed/js/libraries,vuex.js')
// script(src='/compressed/js/libraries,vuex-router-sync.js')
// script(src='/compressed/js/blogApplication.js')

window.onload = function onload() {
	const uniqueIdentifier = Loup.Random.createNiceString(32)
	l.log(`starting up vue application. Vue.js version<${Vue.version}>, uniqueIdentifier<${uniqueIdentifier}>`)

	// testEvent: Event to show how global events work.
	const eventHub = new Vue();

	const store = new Vuex.Store({
		state: {
			x: 0
		},
		mutations: {
			exampleMutation(state, newX) {
				state.x = newX;
			}
		}
	})

	const defaultComponent = {
		template: `
			<div>
				<label for='userInput1'>User Input:</label>
				<input id='userInput1' v-model='userInput1' type='text' />
				<p>Value of x in vuex store: {{$store.state.x}}, user input: {{userInput1}}</p>
				<p v-if='userInput1'>User input: {{userInput1}}</p>
				<p v-else>No user input.</p>
				<button v-on:click='clickHandler1'>{{message}}</button>
			</div>
		`,
		data() {
			return {
				userInput1: '',
				message: 'Click me!'
			};
		},
		methods: {
			testEventHandler() {
				this.$store.commit('exampleMutation', this.$store.state.x + 1)
			},
			clickHandler1() {
				eventHub.$emit('testEvent') // triggers the event "testEvent"
			}
		},
		beforeCreate() {
			// Called synchronously after the instance has just been initialized, before data observation and event/watcher setup.
		},
		created() {
			// Called synchronously after the instance is created. At this stage, the instance has finished processing the
			// options which means the following have been set up: data observation, computed properties, methods, watch/event
			// callbacks. However, the mounting phase has not been started, and the $el property will not be available yet.
		},
		beforeMount() {
			// Called right before the mounting begins: the render function is about to be called for the first time.
		},
		mounted() {
			// Called after the instance has just been mounted where el is replaced by the newly created vm.$el.
			// If the root instance is mounted to an in-document element, vm.$el will also be in-document when
			// mounted is called.
			eventHub.$on('testEvent', this.testEventHandler)
		},
		beforeUpdate() {
			// Called when the data changes, before the virtual DOM is re-rendered and patched.
			// You can perform further state changes in this hook and they will not trigger additional re-renders.
		},
		updated() {
			// Called after a data change causes the virtual DOM to be re-rendered and patched.
			// The component’s DOM will be in updated state when this hook is called, so you can
			// perform DOM-dependent operations in this hook. However, in most cases you should
			// avoid changing state in this hook, because it may lead to an infinite update loop.
		},
		activated() {
			// Called when a kept-alive component is activated.
		},
		deactivated() {
			// Called when a kept-alive component is deactivated.
		},
		beforeDestroy() {
			// Called right before a Vue instance is destroyed. At this stage the instance is still fully functional.
			eventHub.$off('testEvent', this.testEventHandler)
		},
		destroyed() {
			// Called after a Vue instance has been destroyed. When this hook is called, all directives of the Vue
			// instance have been unbound, all event listeners have been removed, and all child Vue instances have also been destroyed.
		}
	}

	const extraComponent = {
		template: `
			<div>
				<p>I am the extra component and I also know about the vuex value: {{$store.state.x}}</p>
			</div>
		`
	}

	const router = new VueRouter({
		mode: 'history',
		routes: [{
			path: '/',
			component: defaultComponent
		}, {
			path: '/component2/',
			component: extraComponent,
			exact: true
		}]
	})

	new Vue({
		name: 'schlurp-application',
		el: '#vueapp',
		template: `
			<div id='vueapp'>
				<router-link to='/' exact=''>home</router-link>
				<router-link to='/component2/' exact=''>component2</router-link>
				<router-view class='router-view' />
			</div>
		`,
		router: router,
		store: store,
		data: {
		},
		methods: {

		}
	})


	Loup.Vue.sync(store, router)
	l.log('vue application startup finished.')
}