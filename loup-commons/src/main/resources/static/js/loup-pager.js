Loup.Dependencer.define('loup-pager', {
	getPages(page, toGenerator, clickGenerator) {
		const pageSize = page.maxElementsPerPage
		const pageCount = page.pagesThatExist
		const currentPage = page.currentPage

		const leftLinks = []
		const centerLinks = []
		const rightLinks = []

		const pageSet = new Set()

		if (pageCount >= 1) {
			pageSet.add(0)
			if (pageCount >= 2) {
				pageSet.add(1)
			}
		}

		for (let i = currentPage - 2; i < currentPage + 3; i++) {
			if (i > 0 && i < pageCount) {
				pageSet.add(i)
			}
		}

		if (pageCount > 4) {
			pageSet.add(pageCount - 2)
			pageSet.add(pageCount - 1)
		}

		let state = 0
		let lastValue = -2
		for (let i of pageSet.values()) {
			if (i !== (lastValue + 1)) {
				state++
			}

			lastValue = i

			const generateLinkObj = i => {
				return {
					page: i,
					displayPage: i + 1,
					active: i === currentPage,
					to: toGenerator ? toGenerator(i, currentPage, pageSize, pageCount) : undefined,
					click: clickGenerator ? clickGenerator(i, currentPage, pageSize, pageCount) : () => { }
				}
			}

			if (state === 1) {
				leftLinks.push(generateLinkObj(i))
			} else if (state === 2) {
				centerLinks.push(generateLinkObj(i))
			} else if (state === 3) {
				rightLinks.push(generateLinkObj(i))
			}
		}

		return {
			leftLinks: leftLinks,
			centerLinks: centerLinks,
			rightLinks: rightLinks
		}
	}
});