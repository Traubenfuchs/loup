Loup.Stopwatch = class Stopwatch {
	constructor(start) {
		this.startPoint = start ? new Date().getTime() : 0
		this.elapsedTime = 0
	}
	get elapsedMilis() {
		return this.startPoint ? this.elapsedTime + new Date().getTime() - this.startPoint : this.elapsedTime
	}
	get elapsedSeconds() {
		return this.elapsedMilis / 1000
	}
	pause() {
		this.elapsedTime = new Date().getTime() - startPoint
		this.elapsedTime = 0
	}
	resume() {
		this.startPoint = new Date().getTime()
	}
}