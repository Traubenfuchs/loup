var Loup = Loup || {};
Loup.Templator = {
	templates : {},
	collect : function collect(id, keep) {
		const templateContainer = document.getElementById(id);
		if(!templateContainer) {
			l.log('templator.js: No templates found.');
			return;
		}
		const children = templateContainer.children;
		for (let i = 0; i < children.length; i++) {
			const child = children[i];
			let name = child.getAttribute('componentName');
			if(!name) {
				name = child.id;
			}
			Loup.Templator.templates[name] = child.outerHTML;
		}
		if(!keep) {
			templateContainer.parentNode.removeChild(templateContainer);
		}
	},
};
