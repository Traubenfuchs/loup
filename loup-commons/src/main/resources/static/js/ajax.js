var Loup = Loup || {}

Loup.Ajax = class Ajax {
	constructor() {
		this.preparators = []
		this.errorHandlers = []
	}

	call(method, url, content, successHandler, errorHandler, finalizer, preparator) {
		if (typeof content === 'function') {
			preparator = finalizer
			finalizer = errorHandler
			successHandler = content
			content = undefined
		}

		const request = new XMLHttpRequest()
		request.loupAjax = this

		request.onload = progressEvent => {
			const contentType = request.getResponseHeader('Content-Type')

			try {
				request.responseObject = JSON.parse(request.responseText)
			} catch (e) { }

			if (request.status === 200) {
				if (successHandler) {
					successHandler(request, progressEvent)
				}
			} else {
				if (errorHandler) {
					request.onerror(progressEvent) // non network level error still call onload
				}
				//	l.error(`Server responded with error to AJAX call. Message from server:\n<${request.responseText}>`);
			}
			if (finalizer) finalizer(request, progressEvent)
		}
		request.onerror = progressEvent => {
			if (errorHandler) {
				errorHandler(request, progressEvent)
			}

			request.loupAjax.errorHandlers.forEach(handler => handler(request, progressEvent))

			if (finalizer) {
				finalizer(request, progressEvent)
			}

			l.error(`Server responded with error to AJAX call. Message from server:\n<${request.responseText}>`)
		}

		request.open(method, url, true)
		request.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

		if (!content) {
			content = undefined
		} else if (!(content instanceof FormData) && content instanceof Object) {
			// if content is instanceof FormData, leave it as is
			// if it's an object, stringify it
			content = JSON.stringify(content)
			request.setRequestHeader('Content-Type', 'application/json')
		}

		if (preparator) {
			preparator(request, method, url, content)
		}
		this.preparators.forEach(preparator => preparator(request, method, url, content))

		request.send(content)
		return request
	}

	get(url, queryParams, successHandler, errorHandler, finalizer, preparator) {
		if (typeof queryParams === 'object') {
			let queryString = ''
			for (let key in queryParams) {
				queryString += (queryString.length === 0 ? '?' : '&') + key + '=' + queryParams[key]
			}
			url += queryString
			queryParams = undefined
		}

		return Loup.Ajax.call('GET', url, queryParams, successHandler, errorHandler, finalizer, preparator)
	}

	post(url, content, success, failure, finalizer, preparator) {
		return Loup.Ajax.call('POST', url, content, success, failure, finalizer, preparator)
	}

	delete(url, content, success, failure, finalizer, preparator) {
		return Loup.Ajax.call('DELETE', url, content, success, failure, finalizer, preparator)
	}

	put(url, content, success, failure, finalizer, preparator) {
		return Loup.Ajax.call('PUT', url, content, success, failure, finalizer, preparator)
	}

	patch(url, content, success, failure, finalizer, preparator) {
		return Loup.Ajax.call('PATCH', url, content, success, failure, finalizer, preparator)
	}
}

Loup.Ajax.defaultClient = new Loup.Ajax()
Loup.Ajax.call = Loup.Ajax.defaultClient.call
Loup.Ajax.get = Loup.Ajax.defaultClient.get
Loup.Ajax.post = Loup.Ajax.defaultClient.post
Loup.Ajax.delete = Loup.Ajax.defaultClient.delete
Loup.Ajax.put = Loup.Ajax.defaultClient.put
Loup.Ajax.patch = Loup.Ajax.defaultClient.patch
Loup.Ajax.preparators = Loup.Ajax.defaultClient.preparators
Loup.Ajax.errorHandlers = Loup.Ajax.defaultClient.errorHandlers

Loup.Dependencer.export('loup-ajax', () => Loup.Ajax.defaultClient)

Loup.Ajax.defaultClient.preparators.push((req, method, url, content) => {
	l.log(`Sending request to <${url}>...`)
})