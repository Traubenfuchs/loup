// Loup
//
// Loup.Dependencer
//	define(dependencyName, creatorOrObject)
//		dependencyName: String
//		creatorOrObject: function or anything
//		Stores the given any or a function creating any with dependencyName as key.
//		Non function creatorOrObject are wrapped in an identity function.
//		An exception is thrown if the same dependencyName is used twice
//	load(dependencyName, forceCreation)
//		dependencyName: String
//		forceCreation: boolean
//		Loads a dependency defined by define(...). At the first call executes the creator
//		function to create the dependency and caches it. If forceCreation is true the
//		cache is ignored.
//		You can load Loup.Dependencer by loading 'loup-dependencer'
//	downloadJson(url, callback)
//		url: String
//		callback: function
//		Downloads the JSON at the given url, parses the content, stores the content as
//		parsedResult and calls the callback with (parsedResult, url). The result is cached.
// Loup.Ajax
//	call(method, url, content, successHandler, errorHandler, finalizer, preparator)
//	get(url, queryParams, successHandler, errorHandler, finalizer, preparator)
//	post(url, content, success, failure, finalizer, preparator)
//	delete(url, content, success, failure, finalizer, preparator)
//
//
// Loup.Logger
//
// Loup.Stopwatch
//
// Loup.Random
//
const Loup = (() => {
	const creators = {}
	const dependencies = {}
	const jsonObjects = {}

	const HHmmssms = () => {
		const date = new Date()
		const hour = date.getHours()
		const minute = date.getMinutes()
		const second = date.getSeconds()
		const ms = date.getMilliseconds()

		return (hour < 10 ? '0' : '') + hour +
			(minute < 10 ? ':0' : ':') + minute +
			(second < 10 ? ':0' : ':') + second +
			(ms < 10 ? ':00' : (ms < 100 ? ':0' : ':')) + ms
	}

	const logger = {
		log(message) {
			console.log(`${HHmmssms()} :: ${message}`)
		},
		warn(message) {
			console.warn(`${HHmmssms()} :: ${message}`)
		},
		error(message) {
			console.error(`${HHmmssms()} :: ${message}`)
		}
	}

	const ajax = class Ajax {
		constructor() {
			this.preparators = []
			this.errorHandlers = []
		}
		call(method, url, content, successHandler, errorHandler, finalizer, preparator) {
			if (typeof content === 'function') {
				preparator = finalizer
				finalizer = errorHandler
				successHandler = content
				content = undefined
			}

			const request = new XMLHttpRequest()
			request.loupAjax = this

			request.onload = progressEvent => {
				const contentType = request.getResponseHeader('Content-Type')

				try {
					request.responseObject = JSON.parse(request.responseText)
				} catch (e) { }

				if (request.status === 200) {
					if (successHandler) {
						successHandler(request, progressEvent)
					}
				} else {
					if (errorHandler) {
						request.onerror(progressEvent) // non network level error still call onload
					}
				}
				if (finalizer) finalizer(request, progressEvent)
			}
			request.onerror = progressEvent => {
				if (errorHandler) {
					errorHandler(request, progressEvent)
				}

				request.loupAjax.errorHandlers.forEach(handler => handler(request, progressEvent))

				if (finalizer) finalizer(request, progressEvent)

				logger.error(`Server responded with error to AJAX call. Message from server:\n<${request.responseText}>`)
			}

			request.open(method, url, true)
			request.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

			if (!content) {
				content = undefined
			} else if (!(content instanceof FormData) && content instanceof Object) {
				// if content is instanceof FormData, leave it as is
				// if it's an object, stringify it
				content = JSON.stringify(content)
				request.setRequestHeader('Content-Type', 'application/json')
			}

			if (preparator) {
				preparator(request, method, url, content)
			}
			this.preparators.forEach(preparator => preparator(request, method, url, content))

			request.send(content)
			return request
		}

		get(url, queryParams, successHandler, errorHandler, finalizer, preparator) {
			if (typeof queryParams === 'object') {
				let queryString = ''
				for (let key in queryParams) {
					queryString += (queryString.length === 0 ? '?' : '&') + key + '=' + queryParams[key]
				}
				url += queryString
				queryParams = undefined
			}

			return this.call('GET', url, queryParams, successHandler, errorHandler, finalizer, preparator)
		}

		post(url, content, success, failure, finalizer, preparator) {
			return this.call('POST', url, content, success, failure, finalizer, preparator)
		}

		delete(url, content, success, failure, finalizer, preparator) {
			return this.call('DELETE', url, content, success, failure, finalizer, preparator)
		}

		put(url, content, success, failure, finalizer, preparator) {
			return this.call('PUT', url, content, success, failure, finalizer, preparator)
		}

		patch(url, content, success, failure, finalizer, preparator) {
			return this.call('PATCH', url, content, success, failure, finalizer, preparator)
		}
	}

	const dependencer = {
		define(dependencyName, creatorOrObject) {
			if (creators[dependencyName]) {
				throw `Dependency <${dependencyName}> was already registered!`
			}
			if (typeof creatorOrObject === 'function') {
				creators[dependencyName] = creatorOrObject
			} else {
				creators[dependencyName] = () => creatorOrObject
			}
		},
		load(dependencyName, forceCreation) {
			let result = dependencies[dependencyName]
			if (forceCreation || !result) {
				const creatorFunction = creators[dependencyName]
				if (!creatorFunction) {
					throw `Missing Loup.Dependencer dependency with name ${dependencyName}!`
				}
				result = creatorFunction(dependencyName)
				dependencies[dependencyName] = result
			}
			return result
		},
		downloadJson(url, callback) {
			const jsonObject = jsonObjects[url]

			if (jsonObject) {
				callback(jsonObject)
			} else {
				Loup.Ajax.load('loup-ajax').get(
					url,
					r => {
						jsonObjects[url] = r.responseObject
						callback(r.responseObject, url)
					},
					r => {
						Loup.Logger.error('Error downloading JSON from <${url}>, callback not executed.')
					}
				)
			}
		}
	}

	const stopwatch = class Stopwatch {
		constructor(start) {
			this.startPoint = start ? new Date().getTime() : 0
			this.elapsedTime = 0
		}
		get elapsedMilis() {
			return this.startPoint ? this.elapsedTime + new Date().getTime() - this.startPoint : this.elapsedTime
		}
		get elapsedSeconds() {
			return this.elapsedMilis / 1000
		}
		pause() {
			this.elapsedTime = new Date().getTime() - startPoint
			this.elapsedTime = 0
		}
		resume() {
			this.startPoint = new Date().getTime()
		}
	}

	const random = {
		niceChars: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz_',
		createNiceString(length) {
			let result = '';
			for (let i = 0; i < length; i++) {
				result += Loup.Random.niceChars.charAt(Math.floor(Math.random() * 62))
			}
			return result;
		}
	}

	const defaultAjax = new ajax()
	ajax.defaultAjax = defaultAjax

	ajax.call = defaultAjax.call
	ajax.get = defaultAjax.get
	ajax.post = defaultAjax.post
	ajax.delete = defaultAjax.delete
	ajax.put = defaultAjax.put
	ajax.patch = defaultAjax.patch
	ajax.preparators = defaultAjax.preparators
	ajax.errorHandlers = defaultAjax.errorHandlers

	dependencer.define('loup-ajax', defaultAjax)
	dependencer.define('loup-logger', logger)
	dependencer.define('loup-dependencer', dependencer)

	return {
		Dependencer: dependencer,
		Ajax: ajax,
		Logger: logger,
		Stopwatch: stopwatch,
		Random: random
	}
})();