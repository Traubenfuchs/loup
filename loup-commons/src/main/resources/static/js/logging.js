var Loup = Loup || {}
Loup.Utilities = Loup.Utilities || {}
Loup.Utilities.Logger = {
	HHmmss() {
		const d = new Date(), h = d.getHours(), m = d.getMinutes(), s = d.getSeconds()
		return (h < 10 ? '0' : '') + h + (m < 10 ? ':0' : ':') + m + (s < 10 ? ':0' : ':') + s
	},
	HHmmssms() {
		const d = new Date(), h = d.getHours(), m = d.getMinutes(), s = d.getSeconds(), ms = d.getMilliseconds();
		return (h < 10 ? '0' : '') + h + (m < 10 ? ':0' : ':') + m + (s < 10 ? ':0' : ':') + s + (ms < 10 ? ':00' : (ms < 100 ? ':0' : ':')) + ms
	},
	log(m) {
		console.log(`${Loup.Utilities.Logger.HHmmssms()} :: ${m}`)
	},
	warn(m) {
		console.warn(`${Loup.Utilities.Logger.HHmmssms()} :: ${m}`)
	},
	error(m) {
		console.error(`${Loup.Utilities.Logger.HHmmssms()} :: ${m}`)
	}
}

const l = Loup.Utilities.Logger