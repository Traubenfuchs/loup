var Loup = Loup || {};

// source: http://www.sitepoint.com/how-to-deal-with-cookies-in-javascript/
Loup.Cookies = {
	getCookie(name) {
		const regexp = new RegExp(`(?:^${name}|;\s*${name})=(.*?)(?:;|$)`, 'g');
		const result = regexp.exec(document.cookie);
		return (result === null) ? null : result[1];
	},
	deleteCookie(name, path, domain) {
		// If the cookie exists
		if (Loup.Cookies.getCookie(name)) {
			Loup.Cookies.setCookie(name, '', -1, path, domain);
		}
	},
	setCookie(name, value, expires, path, domain) {
		let cookie = `${name}=${escape(value)};`;

		if (expires) {
			// If it's a date
			if (expires instanceof Date) {
				// If it isn't a valid date
				if (isNaN(expires.getTime())) {
					expires = new Date();
				}
			} else {
				expires = new Date(new Date().getTime() + parseInt(expires) * 1000 * 60 * 60 * 24);
			}

			cookie += `expires=${expires.toGMTString()};`;
		}

		if (path) {
			cookie += `path=${path};`;
		}
		
		if (domain) {
			cookie += `domain=${domain};`;
		}

		document.cookie = cookie;
	}
};