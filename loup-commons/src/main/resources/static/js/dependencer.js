{
	const creators = {}
	const dependencies = {}
	const jsonObjects = {}

	Loup.Dependencer = {
		export(dependencyName, creator) {
			if (creators[dependencyName]) {
				throw `Dependency <${dependencyName}> was already registered!`
			}
			creators[dependencyName] = creator
		},
		load(dependencyName, forceCreation) {
			let result = dependencies[dependencyName]
			if (forceCreation || !result) {
				const creatorFunction = creators[dependencyName]
				if (!creatorFunction) {
					const errorMessage = `Missing Loup.Dependencer dependency with name ${dependencyName}!`
					l.error(errorMessage)
					throw errorMessage
				}
				result = creatorFunction(dependencyName)
				dependencies[dependencyName] = result
			}
			return result
		},
		downloadJson(url, callback) {
			const jsonObject = jsonObjects[url]
			if (jsonObject) {
				callback(jsonObject)
			} else {
				Loup.Dependencer.load('loup-ajax').get(
					url,
					r => {
						jsonObjects[url] = r.responseObject
						callback(r.responseObject, url)
					},
					r => {
						console.error('Error downloading JSON from <${url}>, callback not executed.')
					}
				)
			}
		}
	}
}