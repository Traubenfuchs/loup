package at.loup.commons.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.commons.config.LoupCommonsConfig;
import at.loup.commons.js.ClosureSource;

/**
 * Provides .js files at the url "/compressed/js/...".<br>
 * Is backed by a {@link ClosureSource}.<br>
 * You can create a {@link Primary} annotated implementation of
 * {@link ClosureSource} to override the default {@link ClosureSource}.<br>
 * The default {@link ClosureSource} looks for .js files in
 * "src/main/resources/static/js/...".
 */
@Controller
class ClosureController {
	private final ClosureSource closureSource;
	@Autowired
	private HttpServletResponse httpServletResponse;
	@Autowired
	private LoupCommonsConfig loupCommonsConfig;

	public ClosureController(ClosureSource closureSource) {
		this.closureSource = closureSource;
	}

	@RequestMapping(path = { "/compressed/js/{file:.+}" }, method = { RequestMethod.GET })
	public @ResponseBody String getJS(@PathVariable("file") String file) throws IOException {
		String result = closureSource.getAsString(file);
		if (loupCommonsConfig.getCompileJSCSS()) {
			httpServletResponse.setHeader("Cache-Control", "public, max-age=30672000");
		}
		return result;
	}
}