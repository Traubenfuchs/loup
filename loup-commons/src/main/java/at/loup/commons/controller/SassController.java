package at.loup.commons.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.loup.commons.config.LoupCommonsConfig;
import at.loup.commons.css.SassSource;

/**
 * Provides .css files at the url "/compressed/css/...".<br>
 * Is backed by a {@link SassSource}.<br>
 * You can create a {@link Primary} annotated implementation {@link SassSource}
 * to override the default {@link SassSource}.<br>
 * The default {@link SassSource} looks for .css files in
 * "src/main/resources/static/css/..." and .scss or .sass files in
 * "src/main/resources/static/sass/..."
 */
@Controller
class SassController {
	private final SassSource sassSource;
	@Autowired
	private HttpServletResponse httpServletResponse;
	@Autowired
	private LoupCommonsConfig loupCommonsConfig;

	public SassController(SassSource sassSource) {
		this.sassSource = sassSource;
	}

	@RequestMapping(path = { "/compressed/css/{file:.+}" }, method = { RequestMethod.GET })
	public @ResponseBody String getCSS(@PathVariable("file") String file) throws IOException {
		String result = sassSource.getAsString(file);
		if (loupCommonsConfig.getCompileJSCSS()) {
			httpServletResponse.setHeader("Cache-Control", "public, max-age=30672000");
		}
		return result;
	}
}
