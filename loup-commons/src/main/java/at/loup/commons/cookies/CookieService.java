package at.loup.commons.cookies;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import at.loup.commons.utilities.ArgumentRuleUtilities;

/**
 * Provides easy Cookie manipulation methods.
 */
@Component
public class CookieService {
	protected final HttpServletRequest httpServletRequest;
	protected final HttpServletResponse httpServletResponse;

	public CookieService(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		this.httpServletRequest = httpServletRequest;
		this.httpServletResponse = httpServletResponse;
	}

	/**
	 * Deletes the cookie on the client side.
	 *
	 * @param name
	 * @throws IllegalArgumentException
	 */
	public void deleteCookie(String name) throws IllegalArgumentException {
		ArgumentRuleUtilities.notNullEmpty("name", name);
		Cookie cookie = new Cookie(name, null);
		cookie.setMaxAge(0);
		httpServletResponse.addCookie(cookie);
	}

	/**
	 * Sets the cookie with the given name to value and allows specification of
	 * the maximum age of the cookie in seconds.
	 *
	 * @param name
	 * @param value
	 * @param expiry
	 *            maximum age of the cookie in seconds
	 * @throws IllegalArgumentException
	 */
	public void setCookie(String name, String value, int expiry) throws IllegalArgumentException {
		setCookie(name, value, null, null, expiry);
	}

	/**
	 * Sets the cookie with the given name to value and allows specification of
	 * the maximum age of the cookie in seconds.
	 *
	 * @domain
	 * @param name
	 * @param value
	 * @param expiry
	 *            maximum age of the cookie in seconds
	 * @throws IllegalArgumentException
	 */
	public void setCookie(String name, String value, String domain, String path, int expiry)
			throws IllegalArgumentException {
		ArgumentRuleUtilities.notNullEmpty("name", name);
		DateFormat df = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss 'GMT'", Locale.US);

		StringBuilder cookie = new StringBuilder();

		cookie.append(name).append("=").append(value).append("; ");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, expiry);
		// TODO domain
		cookie.append("Expires=").append(df.format(cal.getTime())).append("; ");
		cookie.append("Max-Age=").append(expiry).append("; ");
		cookie.append("Path=").append(path).append("; ");
		cookie.append("Version=1");

		String setCookieHeaderValue = cookie.toString();
		httpServletResponse.addHeader("Set-Cookie", setCookieHeaderValue);
	}

	/**
	 * Finds a cookie by name from HttpServletRequest
	 *
	 * @param name
	 * @return cookie or null
	 * @throws IllegalArgumentException
	 *             if name is null
	 */
	public Cookie getCookie(String name) throws IllegalArgumentException {
		ArgumentRuleUtilities.notNullEmpty("name", name);
		Cookie[] cookies = httpServletRequest.getCookies();
		if (cookies == null) {
			return null;
		}
		for (Cookie cookie : cookies) {
			if (Objects.equals(cookie.getName(), name)) {
				return cookie;
			}
		}
		return null;
	}

	/**
	 * Returns value of cookie or null if the cookie does not exist
	 *
	 * @param cookieName
	 * @return cookie value or null
	 */
	public String getCookieValue(String cookieName) {
		Cookie cookie = getCookie(cookieName);
		if (cookie == null) {
			return null;
		}
		String result = cookie.getValue();
		return result;
	}
}
