package at.loup.commons.js;

import java.util.HashMap;

import at.loup.commons.config.LoupCommonsConfig;

/**
 * Allows you define what paths are combined and assign a public key to the
 * result
 */
public class FixedClosureSource extends ClosureSource {
	private final HashMap<String, String> keyToCode = new HashMap<>();
	private final HashMap<String, String[]> keyToPaths = new HashMap<>();

	private FixedClosureSource(String basepath, boolean optimize, boolean cache) {
		super(basepath, optimize, cache);
	}

	private FixedClosureSource(LoupCommonsConfig loupCommonsConfig) {
		super(loupCommonsConfig);
	}

	@Override
	public String getAsString(final String key) {
		String result = keyToCode.get(key);
		if (result != null) {
			httpServletResponse.setHeader("Cache-Control", "public, max-age=30672000");
			return result;
		}
		String[] paths = keyToPaths.get(key);
		if (paths == null) {
			// if there are not paths for this key return 404
			httpServletResponse.setStatus(404);
			return "";
		}
		result = compilePaths(paths);
		return result;
	}

	/**
	 * Adds raw code that will be permanently available.
	 *
	 * @param key
	 * @param code
	 * @return
	 */
	public FixedClosureSource addRawCode(String key, String code) {
		keyToCode.put(key, code);
		return this;
	}

	/**
	 * Adds code that will be compiled
	 *
	 * @param key
	 * @param code
	 * @return
	 */
	public FixedClosureSource addCompiledCode(String key, String code) {
		String compiledCode = compile(code);
		keyToCode.put(key, compiledCode);
		return this;
	}

	/**
	 * Concats code from all multipath pieces and compiles it.
	 *
	 * @param key
	 * @param multiPath
	 * @return
	 */
	public FixedClosureSource addCompiledMultipath(String key, String multiPath) {
		String[] paths = splitMultiPath(multiPath);
		addSource(key, paths);

		return this;
	}

	/**
	 * Adds a combination of files that will be concated and compiled if profile
	 * prod is set.
	 *
	 * @param key
	 * @param paths
	 * @return
	 */
	public FixedClosureSource addSource(String key, String... paths) {
		if (!cache) {
			keyToPaths.put(key, paths);
		} else {
			String code = compilePaths(paths);
			keyToCode.put(key, code);
		}
		return this;
	}

	public static FixedClosureSource create(LoupCommonsConfig loupCommonsConfig) {
		FixedClosureSource result = new FixedClosureSource(loupCommonsConfig);
		return result;
	}
}
