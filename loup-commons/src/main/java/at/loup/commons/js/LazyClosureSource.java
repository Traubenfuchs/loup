package at.loup.commons.js;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import at.loup.commons.utilities.ArgumentRuleUtilities;

/**
 * You can create a {@link Primary} annotated implementation of
 * {@link ClosureSource} to override the default {@link ClosureSource}.<br>
 */
@Component
public class LazyClosureSource extends ClosureSource {
	protected final ConcurrentHashMap<String, String> multipathToCode = new ConcurrentHashMap<>();

	@Autowired
	public LazyClosureSource(Environment environment) {
		this("static/js/", environment.acceptsProfiles("prod"), environment.acceptsProfiles("prod"));
	}

	public LazyClosureSource(String basepath, boolean optimize, boolean cache) {
		super(basepath, optimize, cache);
	}

	@Override
	public String getAsString(final String key) {
		ArgumentRuleUtilities.notNull("key", key);
		try {
			String result = multipathToCode.get(key);
			if (result != null) {
				return result;
			}

			String[] paths = splitMultiPath(key);
			result = compilePaths(paths);

			if (cache && result != null && result.length() != 0) {
				multipathToCode.put(key, result);
			}

			return result;
		} catch (Throwable throwable) {
			throw new RuntimeException(throwable);
		}
	}

}
