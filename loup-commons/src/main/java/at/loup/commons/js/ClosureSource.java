package at.loup.commons.js;

import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.javascript.jscomp.CompilationLevel;
import com.google.javascript.jscomp.Compiler;
import com.google.javascript.jscomp.CompilerOptions;
import com.google.javascript.jscomp.CompilerOptions.LanguageMode;
import com.google.javascript.jscomp.SourceFile;

import at.loup.commons.config.LoupCommonsConfig;
import at.loup.commons.services.AbstractResourceSource;

public abstract class ClosureSource extends AbstractResourceSource {
	protected final SourceFile emptySourceFile = SourceFile.fromCode("externs.js", "");
	protected final CompilerOptions options;

	protected final ConcurrentHashMap<String, String> resourcesAsStrings = new ConcurrentHashMap<>();
	protected final boolean optimize;
	protected final boolean cache;
	protected final String basepath;

	@Autowired
	protected HttpServletResponse httpServletResponse;

	public ClosureSource(
			String basepath,
			boolean optimize,
			boolean cache) {

		this.basepath = basepath;
		this.optimize = optimize;
		this.cache = cache;
		options = new CompilerOptions();
		CompilationLevel.SIMPLE_OPTIMIZATIONS.setOptionsForCompilationLevel(options);
		options.setProtectHiddenSideEffects(false);
		options.setLanguageIn(LanguageMode.ECMASCRIPT6);
		options.setLanguageOut(LanguageMode.ECMASCRIPT5);
	}

	public ClosureSource(LoupCommonsConfig loupCommonsConfig) {
		this.basepath = "static/js/";
		this.optimize = loupCommonsConfig.getCompileJSCSS();
		this.cache = loupCommonsConfig.getCacheJSCSS();
		options = new CompilerOptions();
		CompilationLevel.SIMPLE_OPTIMIZATIONS.setOptionsForCompilationLevel(options);
		options.setProtectHiddenSideEffects(false);
		options.setLanguageIn(LanguageMode.ECMASCRIPT6);
		options.setLanguageOut(LanguageMode.ECMASCRIPT5);
	}

	public String compilePaths(String... paths) {
		StringBuilder combinedCode = new StringBuilder();
		for (String path : paths) {
			if (path == null || path.length() == 0) {
				continue;
			}
			combinedCode.append("// ###### ");
			combinedCode.append(path);
			combinedCode.append(" ######");
			combinedCode.append("\n");
			String code = textFromPath(basepath + path);
			combinedCode.append(code);
		}
		String code = combinedCode.toString();
		String result = compile(code);
		return result;
	}

	public String compile(String code) {
		if (!optimize) {
			return code;
		}

		Compiler compiler = new Compiler();
		SourceFile sf = SourceFile.fromCode("input.js", code);
		compiler.compile(emptySourceFile, sf, options);
		String result = compiler.toSource();

		return result;
	}

	/**
	 * If the url "/compressed/js/xxx.js" is requested, argument key will be
	 * "xxx.js".
	 *
	 * @param key
	 * @return JavaScript code as String.
	 */
	public abstract String getAsString(String key);
}
