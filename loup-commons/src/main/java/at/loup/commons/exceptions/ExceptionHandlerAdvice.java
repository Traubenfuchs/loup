package at.loup.commons.exceptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.ViewResolver;

/**
 * Catches Throwables and converts them to somewhat nice error messages. If a
 * {@link RestController} produced this error, a JSON response will be created.
 * Otherwise a HTML response will be sent.<br>
 * Works well in combination with {@link LoupException}.<br>
 * Annotate your api controllers with {@link RestController} or failed api call
 * will receive an HTML response!
 */
@ControllerAdvice
@RestController
public class ExceptionHandlerAdvice {
	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);
	protected final ViewResolver viewResolver;
	protected final HttpServletRequest httpServletRequest;
	protected final HttpServletResponse httpServletResponse;

	public ExceptionHandlerAdvice(ViewResolver viewResolver, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) {
		this.viewResolver = viewResolver;
		this.httpServletRequest = httpServletRequest;
		this.httpServletResponse = httpServletResponse;
	}

	@ExceptionHandler(value = { Throwable.class, NoHandlerFoundException.class })
	public @ResponseBody Object handleException(Throwable throwable, HandlerMethod handlerMethod) {
		logger.error("ExceptionHandlerAdvice catched a throwable.", throwable);

		Class<?> controllerClass = handlerMethod.getBeanType();

		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		String errorMessage = "Something went wrong.";

		if (throwable instanceof LoupException) {
			LoupException loupException = (LoupException) throwable;
			errorMessage = loupException.getPublicMessage();
			status = loupException.getHttpStatus();
		}

		httpServletResponse.setStatus(status.value());

		boolean isCallToApiController = controllerClass.getDeclaredAnnotation(RestController.class) != null;
		if (isCallToApiController) {
			ErrorObject result = ErrorObject.create(status, errorMessage);
			return result;
		} else {
			// TODO where is the nice error page? (;
			return ""
					+ "You created a <"
					+ status.value()
					+ "> error! There will eventually be a nice error page here. Your personal error message: \n"
					+ errorMessage;
		}
	}

	/**
	 * The JSON object that will be returned when the called
	 * {@link Controller}is a {@link RestController}
	 */
	private static class ErrorObject {
		private final String message;
		private final int httpStatus;
		private final String niceHttpStatus;

		private ErrorObject(String message, int httpStatus, String niceHttpStatus) {
			this.message = message;
			this.httpStatus = httpStatus;
			this.niceHttpStatus = niceHttpStatus;
		}

		public String getMessage() {
			return message;
		}

		public int getHttpStatus() {
			return httpStatus;
		}

		public String getNiceHttpStatus() {
			return niceHttpStatus;
		}

		public static ErrorObject create(HttpStatus httpStatus, String message) {
			ErrorObject result = new ErrorObject(message, httpStatus.value(), httpStatus.getReasonPhrase());
			return result;
		}
	}
}