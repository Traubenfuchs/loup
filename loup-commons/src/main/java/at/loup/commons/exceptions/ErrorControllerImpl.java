package at.loup.commons.exceptions;

import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

// @TODO fix this (-;
/**
 * This controller is a mess
 */
@Controller
public class ErrorControllerImpl implements ErrorController {

	@RequestMapping(path = "/error")
	public @ResponseBody String handle(HttpServletResponse response) {
		// TODO Auto-generated method stub
		return "Status Code: " + response.getStatus();
	}

	@Override
	public @ResponseBody String getErrorPath() {
		// TODO Auto-generated method stub
		return "/error";
	}
}
