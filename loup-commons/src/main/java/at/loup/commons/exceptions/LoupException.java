package at.loup.commons.exceptions;

import org.springframework.http.HttpStatus;

/**
 * A developer and web friendly RuntimeException that allows you to set a public
 * message that will be sent to the user and a private message that will be
 * logged. Additionally you can provide the root Throwable and HttpStatus.
 *
 */
public class LoupException extends RuntimeException {
	private static final long serialVersionUID = -1121482838526663857L;
	private final String publicMessage;
	private final String privateMessage;
	private final HttpStatus httpStatus;

	public LoupException(HttpStatus httpStatus, String message) {
		this(httpStatus, message, message, null);
	}

	public LoupException(HttpStatus httpStatus, String message, Throwable throwable) {
		this(httpStatus, message, message, throwable);
	}

	public LoupException(HttpStatus httpStatus, String publicMessage, String privateMessage) {
		this(httpStatus, publicMessage, privateMessage, null);
	}

	public LoupException(HttpStatus httpStatus, String publicMessage, String privateMessage, Throwable cause) {
		super(privateMessage, cause);
		if (publicMessage == null) {
			publicMessage = "Something went wrong.";
		}
		this.httpStatus = httpStatus;
		this.publicMessage = publicMessage;
		this.privateMessage = privateMessage;
	}

	public String getPublicMessage() {
		return publicMessage;
	}

	public String getPrivateMessage() {
		return privateMessage;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public int getHttpStatusCode() {
		return httpStatus.value();
	}
}