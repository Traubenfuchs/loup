package at.loup.commons.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;

/**
 * Executes a thread on ContextRefreshedEvent, interrupts it on
 * ContextClosedEvent or ContextStoppedEvent
 */
public abstract class SpringAwareThread {
	private static final Logger logger = LoggerFactory.getLogger(SpringAwareThread.class);
	private final Object lockObject = new Object();
	private final String threadName;

	private volatile Thread thread = null;

	public SpringAwareThread(String threadName) {
		this.threadName = threadName;
	}

	/**
	 * Stops the thread if one exists.
	 */
	protected void stop() {
		synchronized (lockObject) {
			if (thread == null) {
				logger.debug("\"SpringAwareThread<" + threadName + "> stop has been called, but no thread is running.");
				return;
			}
			thread.interrupt();
			thread = null;
			logger.debug(
					"\"SpringAwareThread<" + threadName + "> stop has been called and the thread was interrupted.");
		}
	}

	/**
	 * Starts the thread if none exists or it is no longer alive.
	 */
	protected void start() {
		synchronized (lockObject) {
			if (thread != null) {
				if (thread.isAlive()) {
					logger.debug("SpringAwareThread<" + threadName
							+ "> start has been called, but a thread that is alive already exists.");
					return;
				} else {
					logger.debug("SpringAwareThread<" + threadName
							+ "> start has been called, a dead thread exists and a new one will be created.");
				}
				return;
			}

			thread = new Thread(() -> {
				try {
					this.threadMethod();
				} catch (InterruptedException e) {
					logger.debug("SpringAwareThread<" + threadName + "> was interrupted and will stop working now.");
					return;
				}
			});
			thread.setName(threadName);
			thread.start();
			logger.debug(
					"SpringAwareThread<" + threadName + "> start has been called and the thread has been started.");
		}
	}

	/**
	 * The method that will be started and stopped by this
	 * SpringAwareThread.<br>
	 * Catches InterruptedException and gracefully shuts down.<br>
	 * Does NOT take care of any other exceptions and does NOT loop:<br>
	 * You need to implement while(!notInterrupted and try/catch yourself!
	 *
	 * @throws InterruptedException
	 */
	protected abstract void threadMethod() throws InterruptedException;

	public String getThreadName() {
		return threadName;
	}

	@EventListener({ ContextStartedEvent.class })
	private void contextStartedEvent() {
		start();
	}

	@EventListener({ ContextRefreshedEvent.class })
	private void contextRefreshedEvent() {
		start();
	}

	@EventListener({ ContextClosedEvent.class })
	private void contextClosedEvent() {
		stop();
	}

	@EventListener({ ContextStoppedEvent.class })
	private void contextStoppedEvent() {
		stop();
	}
}
