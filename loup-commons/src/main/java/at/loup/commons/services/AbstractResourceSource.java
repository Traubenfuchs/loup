package at.loup.commons.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

import at.loup.commons.utilities.ArgumentRuleUtilities;

public class AbstractResourceSource {
	protected final static ClassLoader classLoader = AbstractResourceSource.class.getClassLoader();

	/**
	 * Returns
	 * <b>AbstractResourceSource.class.getClassLoader()getResourceAsStream(path)</b>
	 * as String.<br>
	 * Example: If argument path is "<b>static/css/basic.css</b>" then
	 * "<b>src/main/resources/static/css/basic.css</b>" is returned.
	 *
	 * @param path
	 * @return
	 * @throws IOException
	 */
	protected String textFromPath(String path) {
		ArgumentRuleUtilities.notNull("path", path);
		try {
			try (InputStream inputStream = classLoader.getResourceAsStream(path);) {
				if (inputStream == null) {
					throw new RuntimeException("File<" + path + "> does not exist.");
				}
				String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
				return result;
			}
		} catch (IOException ioex) {
			throw new RuntimeException(ioex);
		}
	}

	/**
	 * Splits given multipath string into one array.<br>
	 * Example: "<b>a,b.txt|a,c.txt|d.txt</b>" is turned into
	 * <b>{"a/b.txt","a/c.txt","d.txt"}</b>
	 *
	 * @param input
	 * @return
	 */
	protected String[] splitMultiPath(String input) {
		ArgumentRuleUtilities.notNull("input", input);
		input = input.replace(',', '/');
		String[] result = input.split("\\|");
		return result;
	}
}