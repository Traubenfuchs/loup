package at.loup.commons.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import at.loup.commons.functional.actions.Action0;

@Service
public class TransactionForcer {
	public TransactionForcer() {

	}

	@Transactional
	public void executeInTransaction(Action0 action) {
		action.execute();
	}
}
