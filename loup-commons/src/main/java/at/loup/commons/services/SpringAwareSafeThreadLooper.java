package at.loup.commons.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SpringAwareSafeThreadLooper extends SpringAwareThread {
	private static final Logger logger = LoggerFactory.getLogger(SpringAwareSafeThreadLooper.class);

	public SpringAwareSafeThreadLooper(String threadName) {
		super(threadName);
	}

	@Override
	protected void threadMethod() throws InterruptedException {
		Thread currentThread = Thread.currentThread();

		Thread.sleep(sleepStart());

		while (!currentThread.isInterrupted()) {
			try {
				loopMethod();
				Thread.sleep(sleepAfterSuccess());
			} catch (InterruptedException interruptedException) {
				throw interruptedException;
			} catch (Throwable throwable) {
				if (!exceptionHandler(throwable)) {
					break;
				}
				Thread.sleep(sleepAfterException());
			}
		}
	}

	/**
	 * Sleep duration before the thread starts.
	 *
	 * @return
	 */
	protected long sleepStart() {
		return 0L;
	}

	/**
	 * Sleep duration after a successful threadMethod() run.
	 *
	 * @return
	 */
	protected long sleepAfterSuccess() {
		return 1000L;
	}

	/**
	 * Sleep duration after an unsuccessful threadMethod() run.
	 *
	 * @return
	 */
	protected long sleepAfterException() {
		return 1000L;
	}

	protected abstract void loopMethod() throws Throwable;

	/**
	 * Overwrite this method for custom exception handling. Allows you to
	 * gracefully end the thread by returning false.<br>
	 * Needs to take care of sleeping, which you should probably put on top.<br>
	 * Can throw whatever exception you want it to.
	 *
	 * @param throwable
	 * @return true to continue the thread, false to end it.
	 */
	protected boolean exceptionHandler(Throwable throwable) {
		logger.error("SpringAwareSafeThreadLooper with name<" + getThreadName() + "> loopMethod threw an exception.",
				throwable);
		return true;
	}
}
