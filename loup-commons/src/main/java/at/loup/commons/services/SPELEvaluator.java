package at.loup.commons.services;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.SpelCompilerMode;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Service;

/**
 * Allows SPEL expression execution with the default BeanFactoryResolver
 */
@Service
public class SPELEvaluator {
	private final ConcurrentHashMap<String, Expression> expressionCache = new ConcurrentHashMap<>();

	SpelExpressionParser parser = new SpelExpressionParser(
			new SpelParserConfiguration(SpelCompilerMode.IMMEDIATE, null));
	StandardEvaluationContext context = new StandardEvaluationContext();

	public SPELEvaluator(BeanFactory beanFactory) {
		BeanFactoryResolver beanFactoryResolver = new BeanFactoryResolver(beanFactory);
		context.setBeanResolver(beanFactoryResolver);
	}

	/**
	 * Evaluates the given expression and ignores the result.
	 *
	 * @param expressionString
	 */
	public void evaluateVoid(String expressionString) {
		Expression expression = expressionCache.computeIfAbsent(expressionString, parser::parseExpression);
		expression.getValue(context);
	}

	/**
	 * Evaluates the given expression and returns it as an instance of the given
	 * class. If the result object can not be casted to the given class an
	 * EvaluationException is thrown.
	 *
	 * @param expressionString
	 * @param clazz
	 * @return
	 * @throws EvaluationException
	 *             If the result object can not be casted to the given class.
	 */
	public <T> T evaluate(String expressionString, Class<T> clazz) {
		Expression expression = expressionCache.computeIfAbsent(expressionString, parser::parseExpression);
		T result = expression.getValue(context, clazz);
		return result;
	}

	/**
	 * Evaluates the given expression. If the result is not a boolean an
	 * exception is thrown.
	 *
	 * @param expressionString
	 * @return
	 * @throws IllegalStateException
	 *             If the result of the expression evaluation is null.
	 * @throws EvaluationException
	 *             If the result of the expression evaluation is anything but a
	 *             boolean or null.
	 */
	public boolean evaluateBoolean(String expressionString) throws IllegalStateException, EvaluationException {
		Expression expression = expressionCache.computeIfAbsent(expressionString, parser::parseExpression);
		Boolean result = expression.getValue(context, Boolean.class);
		if (result == null) {
			throw new IllegalStateException(""
					+ "SPEL-expression must not validate to null, expression was <"
					+ expressionString
					+ ">. Call evaluate(String, Class) to allow null as return value.");
		}
		return result;
	}
}