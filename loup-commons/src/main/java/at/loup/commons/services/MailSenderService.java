package at.loup.commons.services;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import at.loup.commons.exceptions.LoupException;
import at.loup.commons.functional.actions.Action1;
import at.loup.commons.utilities.DumbPool;

@Lazy
@Component
public class MailSenderService {
	private final DumbPool<Session> sessions;

	private final InternetAddress from;

	public MailSenderService(@Value("${Loup.MailSender.email:dummy}") String email,
			@Value("${Loup.MailSender.password:dummy}") String password,
			@Value("${Loup.MailSender.host:dummy}") String host,
			@Value("${Loup.MailSender.port:1234}") int port) throws AddressException {
		from = new InternetAddress(email);

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", Integer.toString(port));

		PasswordAuthentication passwordAuthentication = new PasswordAuthentication(email, password);
		Authenticator authenticator = new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return passwordAuthentication;
			}
		};

		sessions = new DumbPool<>(5, () -> {
			Session session = Session.getInstance(props, authenticator);
			return session;
		});

	}

	public void doWithMailSession(Action1<Session> action) {
		sessions.doWithSilentThrow(action);
	}

	public void send(String body, String subject, String to) {
		sessions.doWithSilentThrow(session -> {
			try {
				InternetAddress[] toIE = InternetAddress.parse(to);

				Message message = new MimeMessage(session);
				message.setFrom(from);
				message.setRecipients(Message.RecipientType.TO, toIE);
				message.setSubject(subject);
				message.setText(body);

				Transport.send(message);
			} catch (MessagingException exception) {
				throw new LoupException(HttpStatus.INTERNAL_SERVER_ERROR, null, "Sending Mail failed", exception);
			}
		});
	}
}
