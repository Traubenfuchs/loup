package at.loup.commons.utilities;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.loup.commons.functional.actions.Action1;
import at.loup.commons.functional.actions.Action3;
import at.loup.commons.functional.actions.EAction1;

/**
 * A multi producer, multi consumer data structure. Not quite as great as the
 * famous Disruptor but much more comfortable. Backed by
 * {@link ArrayBlockingQueue} (maximumValueCapacity<=0) or
 * {@link LinkedBlockingQueue} (maximumValueCapacity>0).<br>
 * <br>
 *
 * @param <T>
 */
public class ParallelConsumer<T> implements AutoCloseable {
	private static final Logger logger = LoggerFactory.getLogger(ParallelConsumer.class);
	private static AtomicLong threadPoolIdCounter = new AtomicLong(0);

	private final long threadPoolId = threadPoolIdCounter.incrementAndGet();
	private final EAction1<Throwable, T> consumer;
	private final Action3<Throwable, T, ParallelConsumer<T>> exceptionHandler;
	private final long timeoutSeconds;
	private final long timeoutDurationMilis;
	private final long timeoutLimit;
	private final int threadCount;

	private final BlockingQueue<T> values;
	private final Thread timeoutThread;
	private final Object[] threadTimeoutUnits;

	private final AtomicLong errorCounter = new AtomicLong(0);
	private final AtomicLong successfulConsumationCounter = new AtomicLong(0);
	private final AtomicLong startedConsumationCounter = new AtomicLong(0);

	private final AtomicLong timeoutCount = new AtomicLong(-1);
	private volatile boolean isRunning = false;

	/**
	 *
	 * @param maximumValueCapacity
	 *            if <1 an unbound {@link LinkedBlockingQueue} is used otherwise
	 *            a {@link ArrayBlockingQueue} is used.
	 * @param givenThreadCount
	 *            How many threads shall run at once. If <1
	 *            Runtime.getRuntime().availableProcessors() is used.
	 * @param timeoutSeconds
	 *            Timeout until a thread that started working on an input will
	 *            be seen as stalled and replaced by a new thread. The old
	 *            thread will be interrupted and renamed to oldName+"_DEAD".
	 * @param maximumAllowedAmountOfTimeouts
	 *            If the maximum amount of timeouts is reached the
	 *            ParallelConsumer will close.
	 * @param consumer
	 *            The consumer of the given items of Type T. Allowed to throw an
	 *            exception.
	 * @param exceptionHandler
	 *            Handles exceptions throw inside of the consumer. Can stop the
	 *            consumer if desired. Takes the original throwable, the item of
	 *            type t that should have been consumed and this instance of the
	 *            ParallelConsumer.
	 */
	public ParallelConsumer(
			int maximumValueCapacity,
			int givenThreadCount,
			long timeoutSeconds,
			long maximumAllowedAmountOfTimeouts,
			EAction1<Throwable, T> consumer,
			Action3<Throwable, T, ParallelConsumer<T>> exceptionHandler) {
		this.consumer = consumer;
		this.exceptionHandler = exceptionHandler;
		this.timeoutSeconds = timeoutSeconds;
		this.timeoutDurationMilis = timeoutSeconds * 1000;
		this.timeoutLimit = maximumAllowedAmountOfTimeouts;

		if (givenThreadCount < 1) {
			givenThreadCount = Runtime.getRuntime().availableProcessors();
		}
		this.threadCount = givenThreadCount < 1 ? Runtime.getRuntime().availableProcessors() : givenThreadCount;

		if (maximumValueCapacity > 0) {
			this.values = new ArrayBlockingQueue<>(maximumValueCapacity, true);
		} else {
			this.values = new LinkedBlockingQueue<>();
		}
		threadTimeoutUnits = new Object[this.threadCount];

		timeoutThread = new Thread(this::timeoutThreadMethod);
		timeoutThread.setName("ParallelConsumer_" + threadPoolId + "_timeoutThread");
	}

	@SuppressWarnings("unchecked")
	protected void timeoutThreadMethod() {
		if (timeoutSeconds < 1) {
			// timeout functionality deactivated
			return;
		}
		Thread currentThread = Thread.currentThread();
		while (!currentThread.isInterrupted()) {
			final long now = System.currentTimeMillis();
			synchronized (this) {
				for (int i = 0; i < threadCount; i++) {
					ConsumerThreadContainer container = (ConsumerThreadContainer) threadTimeoutUnits[i];

					if ((now - container.getLastStartDateMilis()) < timeoutDurationMilis) {
						continue;
					}
					container.setLastStartDateMilis(Long.MAX_VALUE);

					Thread deadThread = container.getThread();
					String oldThreadName = deadThread.getName();
					deadThread.setName(oldThreadName + "_DEAD");
					deadThread.interrupt();

					ConsumerThreadContainer newContainer = new ConsumerThreadContainer(oldThreadName);
					threadTimeoutUnits[i] = newContainer;

					long timeoutNumber = timeoutCount.incrementAndGet();
					if (timeoutLimit < 0) {
						// no timeoutLimit
						logger.warn(""
								+ "PrimitiveThreadPool<"
								+ threadPoolId
								+ "> thread ran in the timout of <"
								+ timeoutSeconds
								+ ">s, no timeout limit is configured. <"
								+ startedConsumationCounter.get()
								+ "> consumations have been started already. <"
								+ successfulConsumationCounter.get()
								+ "> consumations have been completed successfully already. <" +
								+errorCounter.get()
								+ "> consumations threw an error.");
						return;
					}

					if (timeoutNumber >= timeoutLimit) {
						logger.warn(""
								+ "PrimitiveThreadPool<"
								+ threadPoolId
								+ "> thread ran into the timeout of <"
								+ timeoutSeconds
								+ ">s this is the <" + (timeoutNumber + 1)
								+ ">. timeout that happened. The thread pool will abort now because the maximum allowed amount of timeouts was reached. <"
								+ startedConsumationCounter.get()
								+ "> consumations have been started already. <"
								+ successfulConsumationCounter.get()
								+ "> consumations have been completed successfully already. <" +
								+errorCounter.get()
								+ "> consumations threw an error.");
						try {
							close();
						} catch (Exception ignore) {
							// ignore
						}
					} else {
						logger.warn(""
								+ "PrimitiveThreadPool<"
								+ threadPoolId
								+ "> thread ran into the timeout of <"
								+ timeoutSeconds
								+ ">s this is the <"
								+ timeoutNumber
								+ ">. timeout that happened. The thread pool will abort if <"
								+ timeoutLimit
								+ "> timeouts happen! <"
								+ startedConsumationCounter.get()
								+ "> consumations have been started already. <"
								+ successfulConsumationCounter.get()
								+ "> consumations have been completed successfully already. <" +
								+errorCounter.get()
								+ "> consumations threw an error.");
					}
				} // ends for
			} // ends snychronized
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				return;
			}
		} // ends while (!currentThread.isInterrupted()) {
			// die after interruption
	}

	ThreadLocal<String> x = null;

	class ConsumerThreadContainer {
		private final Thread thread;
		private volatile long lastStartDateMilis = Long.MAX_VALUE;

		public ConsumerThreadContainer(String threadName) {
			thread = new Thread(this::threadMethod);
			thread.setName(threadName);
			thread.start();
		}

		public void setLastStartDateMilis(long lastStartDateMilis) {
			this.lastStartDateMilis = lastStartDateMilis;
		}

		public long getLastStartDateMilis() {
			return lastStartDateMilis;
		}

		public Thread getThread() {
			return thread;
		}

		protected void threadMethod() {
			try {
				while (!thread.isInterrupted()) {
					T value = values.take();
					try {
						startedConsumationCounter.incrementAndGet();
						lastStartDateMilis = System.currentTimeMillis();
						consumer.execute(value);
						lastStartDateMilis = Long.MAX_VALUE;
						successfulConsumationCounter.incrementAndGet();
					} catch (Throwable throwable) {
						errorCounter.incrementAndGet();
						if (exceptionHandler != null) {
							exceptionHandler.execute(throwable, value, ParallelConsumer.this);
						}
					}
				}
				// die after interruption
			} catch (InterruptedException ignore) {
				// die on interrupted exception
				return;
			}
		}

		public void stop() {
			thread.interrupt();
		}
	}

	public void enqueue(T value) throws InterruptedException {
		values.put(value);
	}

	public boolean enqueueSilently(T value) {
		try {
			values.put(value);
			return true;
		} catch (InterruptedException e) {
			return false;
		}
	}

	public synchronized void start() {
		if (isRunning) {
			throw new IllegalStateException("This PrimitiveThreadPool is running already. You can't start it again.");
		}
		for (int i = 0; i < threadCount; i++) {
			threadTimeoutUnits[i] = new ConsumerThreadContainer("PrimitiveThreadPool_" + threadPoolId + "_thread_" + i);
		}

		timeoutThread.start();
		isRunning = true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public synchronized void close() throws Exception {
		if (!isRunning) {
			return;
		}
		timeoutThread.interrupt();
		for (Object threadObject : threadTimeoutUnits) {
			ConsumerThreadContainer threadContainer = (ConsumerThreadContainer) threadObject;
			threadContainer.stop();
		}
		isRunning = false;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public boolean isEmpty() {
		return values.isEmpty();
	}

	public PrimitiveThreadPoolEnd<T> waitForEnd() throws InterruptedException {
		Thread currentThread = Thread.currentThread();
		while (!currentThread.isInterrupted()) {
			if (!isRunning) {
				PrimitiveThreadPoolEnd<T> result = new PrimitiveThreadPoolEnd<>(this);
				return result;
			}

			Thread.sleep(100);
		}
		throw new InterruptedException();
	}

	public void waitForEndAsync(Action1<PrimitiveThreadPoolEnd<T>> handler) {
		new Thread(() -> {
			try {
				PrimitiveThreadPoolEnd<T> end = waitForEnd();
				handler.execute(end);
			} catch (InterruptedException e) {
				return;
			}
		}).start();
	}

	public static class PrimitiveThreadPoolEnd<T> {
		private final ParallelConsumer<T> primitiveThreadPool;

		public PrimitiveThreadPoolEnd(ParallelConsumer<T> primitiveThreadPool) {
			this.primitiveThreadPool = primitiveThreadPool;
		}

		public ParallelConsumer<T> getPrimitiveThreadPool() {
			return primitiveThreadPool;
		}
	}

	@Override
	public String toString() {
		return "ParallelConsumer with id<" + threadPoolId + ">";
	}
}
