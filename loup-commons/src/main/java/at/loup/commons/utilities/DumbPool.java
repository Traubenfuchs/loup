package at.loup.commons.utilities;

import java.util.concurrent.ArrayBlockingQueue;

import at.loup.commons.functional.actions.Action1;
import at.loup.commons.functional.actions.EAction1;
import at.loup.commons.functional.functions.Function0;
import at.loup.commons.functional.functions.Function2;

/**
 * An extremely simple pool for poolable objects like sessions/connections.
 *
 * @param <T>
 */
public class DumbPool<T> {
	private final Function0<T> creator;
	private final ArrayBlockingQueue<T> pooledObjects;
	private final Function2<Boolean, T, Throwable> recreationEvaluator;

	/**
	 *
	 * @param size
	 *            Maximum amount of pool class instances.
	 * @param creator
	 *            Method that will be used to create new instances of the pool
	 *            class instances. Can be called a lot of times.
	 */
	public DumbPool(int size, Function0<T> creator) {
		this(size, creator, (oldObject, throwable) -> true);
	}

	/**
	 *
	 * @param size
	 *            Maximum amount of pool class instances.
	 * @param creator
	 *            Method that will be used to create new instances of the pool
	 *            class instances. Can be called a lot of times.
	 * @param recreationEvaluator
	 *            If a doWith method throws an exception, the
	 *            recreationEvaluator returns a boolean indicating whether the
	 *            pooled class instance needs to be recreated by the creator
	 *            method (Because it is broken).
	 */
	public DumbPool(int size, Function0<T> creator, Function2<Boolean, T, Throwable> recreationEvaluator) {
		ArgumentRuleUtilities.notNull("creator", creator);
		ArgumentRuleUtilities.notNull("recreationEvaluator", recreationEvaluator);

		this.creator = creator;
		pooledObjects = new ArrayBlockingQueue<>(size);
		for (int i = 0; i < size; i++) {
			T newPooledObject = creator.execute();
			pooledObjects.add(newPooledObject);
		}
		this.recreationEvaluator = recreationEvaluator;
	}

	public void doWith(Action1<T> action) throws InterruptedException {
		T object = pooledObjects.take();
		try {
			while (true) {
				try {
					action.execute(object);
					break;
				} catch (Throwable throwable) {
					boolean recreateObject = recreationEvaluator.execute(object, throwable);
					if (recreateObject) {
						object = creator.execute();
					} else {
						break;
					}
				}
			}
		} finally {
			pooledObjects.offer(object);
		}
	}

	public void doWithSilentThrow(Action1<T> action) {
		try {
			doWith(action);
		} catch (InterruptedException interruptedException) {
			throw new RuntimeException(interruptedException);
		}
	}

	public <EXCEPTION_TYPE extends Throwable> void doWithThrowing(EAction1<EXCEPTION_TYPE, T> action)
			throws EXCEPTION_TYPE, InterruptedException {
		T object = pooledObjects.take();
		try {
			while (true) {
				try {
					action.execute(object);
					break;
				} catch (Throwable throwable) {
					if (recreationEvaluator.execute(object, throwable)) {
						object = creator.execute();
					} else {
						throw throwable;
					}
				}
			}
		} finally {
			pooledObjects.offer(object);
		}
	}
}
