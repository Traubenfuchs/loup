package at.loup.commons.utilities;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import at.loup.commons.functional.actions.EAction0;

/**
 * StopWatch for duration measurements. Not threadsafe!<br>
 * Provides friendlier output than other Stopwatch classes.
 */
@Component
@RequestScope
public class StopWatch {
	private long lastStartingTime = 0;
	private long elapsedDuration = 0;
	private boolean isRunning = false;

	public StopWatch() {

	}

	public StopWatch(boolean start) {
		if (start) {
			start();
		}
	}

	public void start() {
		if (isRunning) {
			return;
		}
		lastStartingTime = System.nanoTime();
		isRunning = true;
	}

	public void stop() {
		if (!isRunning) {
			return;
		}
		elapsedDuration += (System.nanoTime() - lastStartingTime);
		isRunning = false;
	}

	public void reset() {
		lastStartingTime = 0;
		elapsedDuration = 0;
	}

	public void restart() {
		lastStartingTime = 0;
		elapsedDuration = 0;
		isRunning = false;
		start();
	}

	public long getElapsedNanos() {
		if (isRunning) {
			return elapsedDuration + (System.nanoTime() - lastStartingTime);
		} else {
			return elapsedDuration;
		}
	}

	public double getElapsedMilis() {
		return getElapsedNanos() / 1000000.0;
	}

	public double getElapsedSeconds() {
		return getElapsedNanos() / 1000000000.0;
	}

	public double getElapsedMinutes() {
		return getElapsedNanos() / 60000000000.0;
	}

	public String getElapsedNiceString() {
		return nsDurationToNiceString(getElapsedNanos());
	}

	public static String nsDurationToNiceString(long elapsedNanos) {
		if (elapsedNanos > 3600000000000L) { // 3600000000000ms = 1h
			// HH:MM:SS

			long elapsedHours = (elapsedNanos / 3600000000000L);
			elapsedNanos -= (elapsedHours * 3600000000000L);

			long elapsedMinutes = (elapsedNanos / 60000000000L);
			elapsedNanos -= (elapsedMinutes * 60000000000L);

			long elapsedSeconds = (elapsedNanos / 60000000000L);

			return elapsedHours + ":" + elapsedMinutes + ":" + elapsedSeconds + " hh:mm:ss elapsed";
		}

		if (elapsedNanos >= 60000000000L) { // 60000000000ms = 1 min
			// MM:SS
			long elapsedMinutes = (elapsedNanos / 60000000000L);
			elapsedNanos -= (elapsedMinutes * 60000000000L);

			long elapsedSeconds = (elapsedNanos / 60000000000L);

			return elapsedMinutes + ":" + elapsedSeconds + " mm:ss elapsed";
		}

		if (elapsedNanos >= 1000000000) {
			double elapsedSeconds = elapsedNanos / 1000000000.0;
			return elapsedSeconds + " <- second elapsed";
		}

		return (elapsedNanos / 1000000.0) + "ms elapsed";
	}

	/**
	 * Measures how long execution of the given lamda takes.
	 *
	 * @param action
	 * @return duration in seconds
	 */
	public static double measureDurationSeconds(EAction0<RuntimeException> action) {
		ArgumentRuleUtilities.notNull("action", action);
		long startTime = System.nanoTime();

		action.execute();

		long stopTime = System.nanoTime();
		return ((stopTime - startTime) / 1000000000.0);
	}
}
