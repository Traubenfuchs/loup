package at.loup.commons.utilities;

/**
 * Allows single line checking and exception throwing of illegal arguments. <br>
 * There are endless classes like this.
 */
public class ArgumentRuleUtilities {
	private ArgumentRuleUtilities() {

	}

	public static void notNull(String argumentName, Object object) throws IllegalArgumentException {
		if (object == null) {
			throw new IllegalArgumentException("Argument <" + argumentName + "> must not be null!");
		}
	}

	public static void notNullEmpty(String argumentName, String input) throws IllegalArgumentException {
		if (input == null) {
			throw new IllegalArgumentException("Argument <" + argumentName + "> must not be null!");
		}
		if (input.length() == 0) {
			throw new IllegalArgumentException("String argument <" + argumentName + "> must not be empty!");
		}
	}

	public static void notNullEmptyWhitespace(String argumentName, String input) throws IllegalArgumentException {
		notNullEmpty(argumentName, input);
		if (input.trim().length() == 0) {
			throw new IllegalArgumentException("String argument <" + argumentName + "> must not be only whitespace!");
		}
	}

	public static void min(String argumentName, int value, int minValue) {
		if (value < minValue) {
			throw new IllegalArgumentException(
					"int argument <" + argumentName + "> must not be smaller than <" + minValue + ">");
		}
	}

	public static void max(String argumentName, int value, int maxValue) {
		if (value > maxValue) {
			throw new IllegalArgumentException(
					"int argument <" + argumentName + "> must not be bigger than <" + maxValue + ">");
		}
	}

	public static void minMax(String argumentName, int value, int minValue, int maxValue) {
		min(argumentName, value, minValue);
		max(argumentName, value, maxValue);
	}

	public static void min(String argumentName, Integer value, int minValue) {
		notNull(argumentName, value);
		if (value < minValue) {
			throw new IllegalArgumentException(
					"Integer argument <" + argumentName + "> must not be smaller than <" + minValue + ">");
		}
	}

	public static void max(String argumentName, Integer value, int maxValue) {
		notNull(argumentName, value);
		if (value > maxValue) {
			throw new IllegalArgumentException(
					"Integer argument <" + argumentName + "> must not be bigger than <" + maxValue + ">");
		}
	}

	public static void minMax(String argumentName, Integer value, int minValue, int maxValue) {
		notNull(argumentName, value);
		min(argumentName, value, minValue);
		max(argumentName, value, maxValue);
	}

	public static void min(String argumentName, long value, long minValue) {
		if (value < minValue) {
			throw new IllegalArgumentException(
					"long argument <" + argumentName + "> must not be smaller than <" + minValue + ">");
		}
	}

	public static void max(String argumentName, long value, long maxValue) {
		if (value > maxValue) {
			throw new IllegalArgumentException(
					"long argument <" + argumentName + "> must not be bigger than <" + maxValue + ">");
		}
	}

	public static void minMax(String argumentName, long value, long minValue, long maxValue) {
		min(argumentName, value, minValue);
		max(argumentName, value, maxValue);
	}

	public static void min(String argumentName, Long value, long minValue) {
		notNull(argumentName, value);
		if (value < minValue) {
			throw new IllegalArgumentException(
					"long argument <" + argumentName + "> must not be smaller than <" + minValue + ">");
		}
	}

	public static void max(String argumentName, Long value, long maxValue) {
		notNull(argumentName, value);
		if (value > maxValue) {
			throw new IllegalArgumentException(
					"long argument <" + argumentName + "> must not be bigger than <" + maxValue + ">");
		}
	}

	public static void minMax(String argumentName, Long value, long minValue, long maxValue) {
		notNull(argumentName, value);
		min(argumentName, value, minValue);
		max(argumentName, value, maxValue);
	}
}
