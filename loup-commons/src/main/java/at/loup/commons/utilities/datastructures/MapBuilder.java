package at.loup.commons.utilities.datastructures;

import java.util.HashMap;
import java.util.Map;

//TODO this class is a mess.
/**
 * Provides methods to easily create a map.
 * 
 * @author akkir
 *
 * @param <KEY>
 * @param <VALUE>
 */
public class MapBuilder<KEY, VALUE> {
	private final HashMap<KEY, VALUE> map = new HashMap<>();

	public MapBuilder<KEY, VALUE> put(KEY key, VALUE value) {
		map.put(key, value);
		return this;
	}

	public Map<KEY, VALUE> getMap() {
		return map;
	}

	public static <KEY, VALUE> MapBuilder<KEY, VALUE> createBuilder() {
		MapBuilder<KEY, VALUE> result = new MapBuilder<>();
		return result;
	}

	public static <KEY, VALUE> MapBuilder<KEY, VALUE> createBuilder(KEY key, VALUE value) {
		MapBuilder<KEY, VALUE> result = new MapBuilder<>();
		result.getMap().put(key, value);
		return result;
	}

	public static <KEY, VALUE> Map<KEY, VALUE> of(KEY key, VALUE value) {
		HashMap<KEY, VALUE> result = new HashMap<>();

		result.put(key, value);

		return result;
	}

	public static <KEY, VALUE> Map<KEY, VALUE> of(KEY key0, VALUE value0, KEY key1, VALUE value1) {
		HashMap<KEY, VALUE> result = new HashMap<>();

		result.put(key0, value0);
		result.put(key1, value1);

		return result;
	}

	public static <KEY, VALUE> Map<KEY, VALUE> of(KEY key0, VALUE value0, KEY key1, VALUE value1, KEY key2,
			VALUE value2) {
		HashMap<KEY, VALUE> result = new HashMap<>();

		result.put(key0, value0);
		result.put(key1, value1);
		result.put(key2, value2);

		return result;
	}

	public static <KEY, VALUE> Map<KEY, VALUE> of(KEY key0, VALUE value0, KEY key1, VALUE value1, KEY key2,
			VALUE value2, KEY key3, VALUE value3) {
		HashMap<KEY, VALUE> result = new HashMap<>();

		result.put(key0, value0);
		result.put(key1, value1);
		result.put(key2, value2);
		result.put(key3, value3);

		return result;
	}
}
