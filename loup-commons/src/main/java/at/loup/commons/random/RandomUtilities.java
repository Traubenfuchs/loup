package at.loup.commons.random;

import java.security.SecureRandom;

import at.loup.commons.utilities.ArgumentRuleUtilities;

/**
 * Random methods I keep using again and again.
 */
public class RandomUtilities {
	public static final char[] NICE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
			.toCharArray();
	private final static SecureRandom secureRandom = new SecureRandom();

	private RandomUtilities() {

	}

	public static String createNiceRandomString(int length) {
		ArgumentRuleUtilities.min("length", length, 0);

		char[] resultChars = new char[length];
		for (int i = 0; i < length; i++) {
			resultChars[i] = NICE_CHARS[secureRandom.nextInt(NICE_CHARS.length)];
		}
		return new String(resultChars);
	}

	public static byte[] createRandomByteArray(int size) {
		ArgumentRuleUtilities.min("size", size, 0);
		byte[] result = new byte[size];
		secureRandom.nextBytes(result);
		return result;
	}
}
