package at.loup.commons.entities;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * A simple base entity with an automatically generated Long key and
 * automatically maintained createdAt and lastUpdatedAt dates. Implements proper
 * equals, hashCode and compareTo methods. If possible you should only rely on
 * those methods after this entity got an Id.<br>
 * Call setLastUpdatedAt(null) in a transaction to update the timestamp.
 *
 * @param <T>
 *            The implementing class.
 */
@MappedSuperclass
public class AbstractTimedEntity<T extends AbstractTimedEntity<T>> extends AbstractEntity<T> {
	private static final long serialVersionUID = 1930715335104511376L;
	@Temporal(TemporalType.TIMESTAMP)
	@Basic(optional = false)
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Basic(optional = false)
	private Date lastUpdatedAt;

	public Date getCreatedAt() {
		return createdAt;
	}

	protected void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	@PrePersist
	private void prePersistTimes() {
		Date now = new Date();
		setLastUpdatedAt(now);
		setCreatedAt(now);
	}

	@PreUpdate
	private void preUpdateCreationTime() {
		setLastUpdatedAt(new Date());
	}
}