package at.loup.commons.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import at.loup.commons.utilities.ArgumentRuleUtilities;

/**
 * A simple base entity with an automatically generated Long key. Implements
 * proper equals, hashCode and compareTo methods. If possible you should only
 * rely on those methods after this entity got an Id.
 *
 * @param <T>
 *            The implementing class.
 */
@MappedSuperclass
public abstract class AbstractEntity<T extends AbstractEntity<T>> implements Serializable, Comparable<T> {
	private static final long serialVersionUID = 2650178834734453074L;

	@Id
	@GeneratedValue
	private Long id;

	protected AbstractEntity() {
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return 0;
		}
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (getId() == null) {
			boolean result = this == obj;
			return result;
		}

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}
		AbstractEntity<?> other = (AbstractEntity<?>) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(T other) {
		ArgumentRuleUtilities.notNull("o", other);

		Long otherLong = other.getId();

		if (otherLong == null) {
			throw new IllegalArgumentException("Other object's id must not be null");
		}

		int result = id.compareTo(otherLong);
		return result;
	}

	@Override
	public String toString() {
		return "Entity of class <" + this.getClass().getName() + "> with id <" + getId() + ">";
	}
}