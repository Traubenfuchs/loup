package at.loup.commons.entities;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * A simple base entity with an automatically generated Long key and
 * automatically maintained createdAt and lastUpdatedAt dates and a Long
 * {@link Version}. Implements proper equals, hashCode and compareTo methods. If
 * possible you should only rely on those methods after this entity got an
 * Id.<br>
 * Call setLastUpdatedAt(null) in a transaction to update the timestamp.
 *
 * @param <T>
 *            The implementing class.
 */
@MappedSuperclass
public class AbstractTimedVersionedEntity<T extends AbstractTimedVersionedEntity<T>> extends AbstractTimedEntity<T> {
	private static final long serialVersionUID = -4988481664854833215L;
	@Version
	private Long version;

	public Long getVersion() {
		return version;
	}
}
