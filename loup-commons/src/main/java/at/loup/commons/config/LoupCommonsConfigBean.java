package at.loup.commons.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Instantiates the {@link LoupCommonsConfig} object.
 */
@Configuration
class LoupCommonsConfigBean {
	@Bean
	public LoupCommonsConfig loupCommonsConfig(Environment environment) {
		return new LoupCommonsConfig(environment);
	}
}
