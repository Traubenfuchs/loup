package at.loup.commons.config;

import org.springframework.context.annotation.Configuration;

/**
 * Implement this interface and make the implementation a {@link Configuration}
 * class to configure loup-commons beans.
 */
public interface LoupCommonsConfigurer {
	void configure(LoupCommonsConfig config);
}
