package at.loup.commons.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * This class can be used to configure loup-commons beans. Do not inject it,
 * implement {@link LoupCommonsConfigurer} in a {@link Configuration} class
 * instead.
 */
public class LoupCommonsConfig {
	private boolean instantiateMailSenderService;
	private boolean cacheJSCSS;
	private boolean compileJSCSS;

	protected LoupCommonsConfig(Environment environment) {
		if (environment.acceptsProfiles("prod")) {
			cacheJSCSS = true;
			compileJSCSS = true;
		} else {
			cacheJSCSS = false;
			compileJSCSS = false;
		}
	}

	public boolean getInstantiateMailSenderService() {
		return instantiateMailSenderService;
	}

	public void setInstantiateMailSenderService(boolean instantiateMailSenderService) {
		this.instantiateMailSenderService = instantiateMailSenderService;
	}

	public boolean getCacheJSCSS() {
		return cacheJSCSS;
	}

	public void setCacheJSCSS(boolean cacheJSCSS) {
		this.cacheJSCSS = cacheJSCSS;
	}

	public boolean getCompileJSCSS() {
		return compileJSCSS;
	}

	public void setCompileJSCSS(boolean compileJSCSS) {
		this.compileJSCSS = compileJSCSS;
	}
}
