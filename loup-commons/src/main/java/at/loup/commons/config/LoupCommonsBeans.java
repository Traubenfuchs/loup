package at.loup.commons.config;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.ViewResolver;

import de.neuland.jade4j.JadeConfiguration;
import de.neuland.jade4j.spring.template.SpringTemplateLoader;
import de.neuland.jade4j.spring.view.JadeViewResolver;

/**
 * Instantiates loup-commons beans that require special, environment and setting
 * dependant configuration.
 */
@Configuration
public class LoupCommonsBeans {
	@Autowired
	protected LoupCommonsConfigBean loupCommonsConfigBean;
	@Autowired
	protected Environment environment;
	@Autowired
	protected LoupCommonsConfig loupCommonsConfig;

	public LoupCommonsBeans() {

	}

	@Bean
	public JadeConfiguration jadeConfiguration() throws IOException {
		JadeConfiguration configuration = new JadeConfiguration();

		if (environment.acceptsProfiles("prod")) {
			configuration.setCaching(true);
			configuration.setPrettyPrint(false);
		} else {
			configuration.setCaching(false);
			configuration.setPrettyPrint(true);
		}

		configuration.setTemplateLoader(templateLoader());
		return configuration;
	}

	@Bean
	public SpringTemplateLoader templateLoader() throws IOException {
		SpringTemplateLoader springTemplateLoader = new SpringTemplateLoader();
		springTemplateLoader.setBasePath("classpath:templates/");
		springTemplateLoader.setEncoding("UTF-8");
		springTemplateLoader.setSuffix(".jade");

		return springTemplateLoader;
	}

	@Bean
	public ViewResolver viewResolver() throws IOException {
		JadeViewResolver viewResolver = new JadeViewResolver();
		viewResolver.setConfiguration(jadeConfiguration());
		viewResolver.setExposeRequestAttributes(true);
		viewResolver.setExposeSessionAttributes(true);
		return viewResolver;
	}

	@Bean
	public ServletContextInitializer servletContextInitializer(
			@Value("${loup.session-cookie-name:session-cookie}") String sessionCookie) {
		return new ServletContextInitializer() {
			@Override
			public void onStartup(ServletContext servletContext) throws ServletException {
				servletContext.getSessionCookieConfig().setName(sessionCookie);
			}
		};
	}
}