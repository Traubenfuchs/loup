package at.loup.commons.json;

import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

/**
 * Allows transforming spring-data Page objects into self explanatory objects
 * that can be used as JSON response. Allows adding a transformation method that
 * converts the content of a page.
 *
 * @param <T>
 */
public class PageResponse<T> {
	private int currentPage;
	private int elementsOnCurrentPage;
	private int maxElementsPerPage;
	private long elementsThatExist;
	private int pagesThatExist;

	private Iterable<T> elements;

	protected PageResponse() {

	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getElementsOnCurrentPage() {
		return elementsOnCurrentPage;
	}

	public void setElementsOnCurrentPage(int elementsOnCurrentPage) {
		this.elementsOnCurrentPage = elementsOnCurrentPage;
	}

	public int getMaxElementsPerPage() {
		return maxElementsPerPage;
	}

	public void setMaxElementsPerPage(int maxElementsPerPage) {
		this.maxElementsPerPage = maxElementsPerPage;
	}

	public long getElementsThatExist() {
		return elementsThatExist;
	}

	public void setElementsThatExist(long elementsThatExist) {
		this.elementsThatExist = elementsThatExist;
	}

	public int getPagesThatExist() {
		return pagesThatExist;
	}

	public void setPagesThatExist(int pagesThatExist) {
		this.pagesThatExist = pagesThatExist;
	}

	public Iterable<T> getElements() {
		return elements;
	}

	public void setElements(Iterable<T> elements) {
		this.elements = elements;
	}

	/**
	 * Converts the given Page into a PageResponse.
	 *
	 * @param page
	 * @return
	 */
	public static <OUTPUT> PageResponse<OUTPUT> createFromPage(Page<OUTPUT> page) {
		PageResponse<OUTPUT> result = new PageResponse<>();

		result.setCurrentPage(page.getNumber());
		result.setElementsOnCurrentPage(page.getNumberOfElements());
		result.setElementsThatExist(page.getTotalElements());
		result.setMaxElementsPerPage(page.getSize());
		result.setPagesThatExist(page.getTotalPages());

		result.setElements(page.getContent());

		return result;
	}

	/**
	 * Converts the given page into a PageResponse and transforms the content of
	 * the Page. For example, you can convert an entity to one of its DTO
	 * partner.
	 *
	 * @param page
	 * @param valueTransformer
	 * @return
	 */
	public static <INPUT, OUTPUT> PageResponse<OUTPUT> createFromPageWithTransform(
			Page<INPUT> page,
			Function<INPUT, OUTPUT> valueTransformer) {
		PageResponse<OUTPUT> result = new PageResponse<>();

		result.setCurrentPage(page.getNumber());
		result.setElementsOnCurrentPage(page.getNumberOfElements());
		result.setElementsThatExist(page.getTotalElements());
		result.setMaxElementsPerPage(page.getSize());
		result.setPagesThatExist(page.getTotalPages());

		result.setElements(page.getContent().stream().map(valueTransformer).collect(Collectors.toList()));

		return result;
	}
}
