package at.loup.commons.functional.functions;

@FunctionalInterface
public interface EFunction3<EXCEPTION_TYPE extends Throwable, RETURN_TYPE, VALUE_1, VALUE_2, VALUE_3> {
	public RETURN_TYPE execute(VALUE_1 value1, VALUE_2 value2, VALUE_3 value3) throws EXCEPTION_TYPE;
}