package at.loup.commons.functional.actions;

@FunctionalInterface
public interface EAction0<EXCEPTION_TYPE extends Throwable> {
	void execute() throws EXCEPTION_TYPE;
}
