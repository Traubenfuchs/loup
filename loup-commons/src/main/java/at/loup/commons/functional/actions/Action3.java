package at.loup.commons.functional.actions;

@FunctionalInterface
public interface Action3<VALUE_1, VALUE_2, VALUE_3> {
	void execute(VALUE_1 value1, VALUE_2 value2, VALUE_3 value3);
}
