package at.loup.commons.functional.functions;

@FunctionalInterface
public interface EFunction0<EXCEPTION_TYPE extends Throwable, RETURN_TYPE> {
	public RETURN_TYPE execute() throws EXCEPTION_TYPE;
}
