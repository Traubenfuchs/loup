package at.loup.commons.functional.functions;

@FunctionalInterface
public interface Function1<RETURN_TYPE, VALUE_1> {
	public RETURN_TYPE execute(VALUE_1 value1);
}
