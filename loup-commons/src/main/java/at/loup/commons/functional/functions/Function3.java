package at.loup.commons.functional.functions;

@FunctionalInterface
public interface Function3<RETURN_TYPE, VALUE_1, VALUE_2, VALUE_3> {
	public RETURN_TYPE execute(VALUE_1 value1, VALUE_2 value2, VALUE_3 value3);
}
