package at.loup.commons.functional.functions;

@FunctionalInterface
public interface Function0<RETURN_TYPE> {
	public RETURN_TYPE execute();
}
