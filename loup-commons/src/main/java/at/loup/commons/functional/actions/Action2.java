package at.loup.commons.functional.actions;

@FunctionalInterface
public interface Action2<VALUE_1, VALUE_2> {
	void execute(VALUE_1 value1, VALUE_2 value2);
}
