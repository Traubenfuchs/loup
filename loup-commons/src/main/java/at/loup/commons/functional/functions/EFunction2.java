package at.loup.commons.functional.functions;

@FunctionalInterface
public interface EFunction2<EXCEPTION_TYPE extends Throwable, RETURN_TYPE, VALUE_1, VALUE_2> {
	public RETURN_TYPE execute(VALUE_1 value1, VALUE_2 value2) throws EXCEPTION_TYPE;
}