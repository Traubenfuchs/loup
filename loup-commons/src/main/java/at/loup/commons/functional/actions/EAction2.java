package at.loup.commons.functional.actions;

@FunctionalInterface
public interface EAction2<EXCEPTION_TYPE extends Throwable, VALUE_1, VALUE_2> {
	void execute(VALUE_1 value1, VALUE_2 value2) throws EXCEPTION_TYPE;
}
