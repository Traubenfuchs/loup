package at.loup.commons.functional.actions;

@FunctionalInterface
public interface Action1<VALUE_1> {
	void execute(VALUE_1 value1);
}
