package at.loup.commons.functional.actions;

@FunctionalInterface
public interface EAction1<EXCEPTION_TYPE extends Throwable, VALUE_1> {
	void execute(VALUE_1 value1) throws EXCEPTION_TYPE;
}
