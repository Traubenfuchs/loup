package at.loup.commons.functional.actions;

@FunctionalInterface
public interface EAction3<EXCEPTION_TYPE extends Throwable, VALUE_1, VALUE_2, VALUE_3> {
	void execute(VALUE_1 value1, VALUE_2 value2, VALUE_3 value3) throws EXCEPTION_TYPE;
}
