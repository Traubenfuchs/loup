package at.loup.commons.functional.functions;

@FunctionalInterface
public interface EFunction1<EXCEPTION_TYPE extends Throwable, RETURN_TYPE, VALUE_1> {
	public RETURN_TYPE execute(VALUE_1 value1) throws EXCEPTION_TYPE;
}