package at.loup.commons.css;

/**
 * Offers CSS/SASS/SCSS files as CSS Strings.
 */
public interface SassSource {
	/**
	 * * If the url "/compressed/css/xxx.css" is requested, argument key will be
	 * "xxx.css".
	 *
	 * @param key
	 * @return CSS as String.
	 */
	String getAsString(String key);
}
