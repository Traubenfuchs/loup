package at.loup.commons.css;

import java.io.File;
import java.net.URI;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import at.loup.commons.config.LoupCommonsConfig;
import at.loup.commons.services.AbstractResourceSource;
import io.bit3.jsass.CompilationException;
import io.bit3.jsass.Compiler;
import io.bit3.jsass.Options;
import io.bit3.jsass.Output;
import io.bit3.jsass.OutputStyle;

/**
 * Default implementation of {@link SassSource}.<br>
 * Looks for .css files in "src/main/resources/static/css/..." and .scss or
 * .sass files in "src/main/resources/static/sass/...".<br>
 * You can create a {@link Primary} annotated implementation {@link SassSource}
 * to override the default {@link SassSource}.
 */
@Component
public class SassSourceImpl extends AbstractResourceSource implements SassSource {
	protected final ConcurrentHashMap<String, String> resourcesAsStrings = new ConcurrentHashMap<>();
	protected final boolean cache;
	protected final String cssBasepath;
	protected final String sassBasepath;
	@Autowired
	protected HttpServletResponse httpServletResponse;
	@Autowired
	protected LoupCommonsConfig loupCommonsConfig;

	@Autowired
	public SassSourceImpl(Environment environment) {
		this("static/css/", "static/sass/", environment.acceptsProfiles("prod"));
	}

	public SassSourceImpl(String cssBasepath, String sassBasepath, boolean cache) {
		this.cssBasepath = cssBasepath;
		this.sassBasepath = sassBasepath;
		this.cache = cache;
	}

	public SassSourceImpl(LoupCommonsConfig loupCommonsConfig) {
		this.cssBasepath = "static/css/";
		this.sassBasepath = "static/sass/";
		this.cache = loupCommonsConfig.getCacheJSCSS();
	}

	@Override
	public String getAsString(final String multiPath) {
		String result = resourcesAsStrings.get(multiPath);
		if (result != null) {
			httpServletResponse.setHeader("Cache-Control", "public, max-age=30672000");
			return result;
		}

		String[] paths = splitMultiPath(multiPath);

		result = compilePaths(paths);

		if (cache && result != null && result.length() != 0) {
			resourcesAsStrings.put(multiPath, result);
		}
		return result;
	}

	public String compile(String input) {
		URI inputFile = new File("stylesheet.scss").toURI();
		URI outputFile = new File("stylesheet.css").toURI();
		Compiler compiler = new Compiler();
		Options options = new Options();
		if (cache) {
			options.setOutputStyle(OutputStyle.COMPRESSED);
		} else {
			options.setOutputStyle(OutputStyle.EXPANDED);
		}
		Output output;
		try {
			output = compiler.compileString(input, inputFile, outputFile, options);
		} catch (CompilationException ex) {
			throw new RuntimeException(ex);
		}
		String css = output.getCss();
		return css;
	}

	public String compilePaths(String... paths) {
		StringBuilder combinedCssSass = new StringBuilder();

		for (String path : paths) {
			if (path == null || path.length() == 0) {
				continue;
			}
			combinedCssSass.append("// ###### ");
			combinedCssSass.append(path);
			combinedCssSass.append(" ######");
			combinedCssSass.append("\n");

			String code;
			if (path.endsWith(".css")) {
				code = textFromPath(cssBasepath + path);
			} else if (path.endsWith(".sass") || path.endsWith(".scss")) {
				code = textFromPath(sassBasepath + path);
			} else {
				throw new IllegalArgumentException("Only the extensions .css/.scss/.sass are allowed");
			}
			combinedCssSass.append(code);
		}

		String raw = combinedCssSass.toString();
		String result = compile(raw);
		return result;
	}
}