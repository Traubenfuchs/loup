package at.loup.commons.css;

import java.util.HashMap;

import at.loup.commons.config.LoupCommonsConfig;

public class FixedSassSource extends SassSourceImpl {

	private final HashMap<String, String> keyToCode = new HashMap<>();
	private final HashMap<String, String[]> keyToPaths = new HashMap<>();

	private FixedSassSource(String cssBasepath, String sassBasepath, boolean cache) {
		super(cssBasepath, sassBasepath, cache);
		// TODO Auto-generated constructor stub
	}

	public FixedSassSource(LoupCommonsConfig loupCommonsConfig) {
		super(loupCommonsConfig);
	}

	@Override
	public String getAsString(String key) {
		String result = keyToCode.get(key);
		if (result != null) {
			httpServletResponse.setHeader("Cache-Control", "public, max-age=30672000");
			return result;
		}
		String[] paths = keyToPaths.get(key);
		if (paths == null) {
			// if there are not paths for this key return 404
			httpServletResponse.setStatus(404);
			return "";
		}
		result = compilePaths(paths);
		return result;
	}

	// TODO remove duplicate code from parent
	public FixedSassSource addSource(String key, String... paths) {
		if (!cache) {
			keyToPaths.put(key, paths);
		} else {

			String compiledCss = compilePaths(paths);

			keyToCode.put(key, compiledCss);
		}
		return this;
	}

	public static FixedSassSource create(LoupCommonsConfig loupCommonsConfig) {
		FixedSassSource result = new FixedSassSource(loupCommonsConfig);
		return result;
	}
}