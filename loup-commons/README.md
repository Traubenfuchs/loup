# loup-commons
Reuseable classes for spring boot applications.  
Some highlights:

## JPA

### AbstractEntity
@MappedSuperclass with a @GeneratedValue Long id.  
Equals() and hashCode() are based on the JPA assigned id. If no id has been assigned by the JPA provider, calling those methods will fail.  
I know that equals based on the id in an abstract parent class carries its dangers, but if you use your entities in a sane way and don't take them out of transactions it is the best way to do it. If you don't let your entities leave transactions and turn them into DTOs if you need to take the information outside it's much easier to reason about what is going on. 

### AbstractTimedEntity
@MappedSuperclass extending AbstractEntity, adding a creation timestamp and lastUpdatedAt timestamp (via @PreUpdate).

### AbstractTimedVersionedEntity
@MappedSuperclass extending AbstractTimedEntity, adding a @Version Long.

## Functional (at.loup.commons.functional.*)
Java has a few basic @FunctionalInterfaces like Consumer<T>,  BiConsumer<T,U>, Function<T,R> and BiFunction<T,U,R>.  
Those are somewhat limited:  
C# did it how it was meant to be: There are actions (Action) (return void) and functions (Func) (return something) and for each there are different versions that take up to 16 different arguments! Besides that you don't need to care about exceptions because C# doesn't have checked exceptions.  
My @FunctionInterfaces help me achieve the same thing.  
You can define methods as objects that throw exceptions and take up to 3 arguments.

## .js optimization and combination 
All .js files that can be found in any projects __resources/static/js/**__ folder can be accessed from a browser with the url __/compressed/js/filename.js__. 

If you started the spring boot application with the profile "__prod__" the .js files will be optimized by [Googles closure compiler](https://developers.google.com/closure/compiler/) and cached permanently. If you use the profile "__dev__" or don't use the profile "__prod__", files will not be optimized and not cached.  

Additionally, you can compile multiple .js files to one so only one needs to be loaded on your websites. An example url: __/compressed/js/logging.js|libraries,prefixfree.js__. This would load the file __/static/js/logging.js__ and __/static/js/libraries/prefixfree.js__ and combine them to one file.  

__LazyClosureSource.java__ is responsible for compiling and storing .js files, while __ClosureController.java__ offers them to the browser.

## CSS & SASS Compiler
If you want to access minified .css use the url __/compressed/css/filename.css__.  
You can access .css files from __resources/static/css/**__ and .sass or .scss files from __resources/static/sass/**__.  
Just like for .js files there is a __SassSource.java__ and __SassController.java__.  
Thanks, [jsass](https://github.com/bit3/jsass)!

## Jade
By default the [templating engine __Pug__](https://github.com/pugjs/pug) is used, which was [previously known as __Jade__](https://github.com/pugjs/pug/issues/2184). You should give it a try, compared to thymeleaf it is more comfortable to use.
